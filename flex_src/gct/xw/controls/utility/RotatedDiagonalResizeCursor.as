package gct.xw.controls.utility
{

    public class RotatedDiagonalResizeCursor extends HorizontalResizeCursor
    {
	public function RotatedDiagonalResizeCursor()
	{
	    super();
            cursor.rotation = -45;
	}
    }
    
}
