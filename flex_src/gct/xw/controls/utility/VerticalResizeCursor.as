package gct.xw.controls.utility
{
    /**                                                                                                                                                                 
     *  @private                                                                                                                                                        
     */
    public class VerticalResizeCursor extends HorizontalResizeCursor
    {
	public function VerticalResizeCursor()
	{
	    super();
	    cursor.rotation=90;
	}
    }

}
