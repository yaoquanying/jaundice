package gct.com.wyh
{
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.controls.CheckBox;
	import mx.controls.DataGrid;
	import mx.events.DataGridEvent;
	
	public class CheckBoxHeaderRenderer extends CheckBox
	{
		private var sele:String;
		public function CheckBoxHeaderRenderer()
		{
			super();
			this.addEventListener(Event.CHANGE, clickHandlers);
		}
		
		override public function set data(value:Object):void
		{
			DataGrid(listData.owner).addEventListener
				(DataGridEvent.HEADER_RELEASE,sortEventHandler);
			this.selected=this.sele=="true"?true:false;
		}
		
		private function sortEventHandler(event:DataGridEvent):void
		{
			if (event.itemRenderer == this)
				event.preventDefault();
		}
		
		protected function clickHandlers(event:Event):void
		{
			sele=(event.currentTarget.selected).toString();
			var a:ArrayCollection=DataGrid(listData.owner).dataProvider as ArrayCollection;
			for(var i:int=0;i<a.length;i++){
				
				(DataGrid(listData.owner).dataProvider as ArrayCollection)
				.getItemAt(i).selected=(event.currentTarget.selected).toString();
			}
			(DataGrid(listData.owner).dataProvider as ArrayCollection).refresh();
		}
		
	}
}  