package gct.com.wyh
{
	import mx.collections.ArrayList;
	import mx.containers.Canvas;
	import mx.core.UIComponent;
	import mx.messaging.management.Attribute;
	public class componenttttt extends Canvas
	{
		public var infostr:String=new String();
		public var zuobiao:Array=new Array();//记录在工程图上的坐标
		public var Gpd:Boolean=new Boolean();//G门的判断，true可以连黑线，false不可以
		public var layer:int=0;//层数，合成时布局用
		public var col:int=0;//遍历标记
		public var termpd:Boolean=false;
		
		private var chuyin:Array=new Array();//是否是初因事件,false为非初因，true为初因
		private var cy:Boolean=true;//最终显示初因或非出因
		private var attri:Number=new Number();//类别，1为B，2为X，3为G，4为D
		private var comname:String=new String();//名称
		private var info:String=new String();//文字说明
		private var relattion:Array=new Array();//其所有父变量
		private var num:Number=new Number();//编号
		private var state:Number=new Number();//所处状态
		private var lineout:Array=new Array();//指出去的线
		private var linein:Array=new Array();//指进来的线
		private var grade:Array=new Array();//状态名称
		private var gradeIds:Array=new Array();
		private var colour:Array=new Array();//状态颜色
		private var lineinnum:Number=0;//指入线个数
		private var lineoutnum:Number=0;//指出线个数
		private var Disconnected:Boolean=false;//D变量是否已经相连
		private var gradenumber:Number=0;//状态总数
		private var p:Array=new Array();//状态概率，用于B，G，D
		private var issetted:Boolean=false;//是否已经赋值
		private var isadded:Boolean=false;//是否添加
		private var numwrite:Number=new Number();//写txt的编号
		public function addlineinat(lt:line):void
		{
			var num:int=lt.getnumasin();
			var p:int;
			for(p=lineinnum-1;p>=num;p--)
			{
				linein[p+1]=linein[p];
				line(linein[p+1]).setnumasin(p+1);
			}
			linein[num]=lt;
			lineinnum++;
		}
		public function addlineoutat(lt:line):void
		{
			var num:int=lt.getnumasout();
			var p:int;
			for(p=lineoutnum-1;p>=num;p--)
			{
				lineout[p+1]=lineout[p];
				line(lineout[p+1]).setnumasout(p+1);
			}
			lineout[num]=lt;
			lineoutnum++;
		}
		
		public function getcopy():componenttttt
		{
			var t:componenttttt=new componenttttt();
			var pp:int;
			t.setatt(attri);
			t.setnum(num);
			t.infostr=infostr;
			t.setinfo(info);
			t.setname(comname);
			t.setDcon(Disconnected);
			return t;
		}
		/** 
		 * 设置文字说明
		 */ 
		public function setinfo(str:String):void
		{
			info=str;
		}
		/** 
		 * 读取文字说明
		 */ 
		public function getinfo():String
		{
			return info;
		}
		/** 
		 * 设置是否已经添加到画板上
		 */ 
		public function setadded(chu:Boolean):void
		{
			this.isadded=chu;
		}
		/** 
		 * 读取是否已添加到画板上
		 */ 
		public function getadded():Boolean
		{
			return this.isadded;
		}
		/** 
		 * 读取是否是初因状态
		 */ 
		public function getchuyin(num:Number):Boolean
		{
			return this.chuyin[num];
		}
		/** 
		 * 读取是否是初因变量
		 */ 
		public function getcy():Boolean
		{
			var p:Number=new Number();
			for(p=0;p<gradenumber;p++)
			{
				cy=cy&&chuyin[p];
			}
			return cy;
		}
		/** 
		 * 设定是否赋过值
		 */  
		public function setsetted(setted:Boolean):void
		{
			this.issetted=setted;
		}
		/** 
		 * 读取是否赋过值
		 */  
		public function getsetted():Boolean
		{
			return this.issetted;
		}
		/** 
		 * 添加状态及概率
		 */  
		public function addgrade(gra:String,pp:Number,cc:Boolean,co:String):void
		{
			this.grade[this.gradenumber]=gra;
			this.colour[this.gradenumber]=co;
			if(attri==1)
			{
				this.p[this.gradenumber]=pp;
				this.chuyin[this.gradenumber]=cc;
				this.gradenumber++;
			}
			else if(attri==4)
			{
				this.p[0]=pp;
				this.gradenumber=1;
			}
			else if(attri==2||attri==3)
			{
				this.gradenumber++;
			}
		}
		/**
		 *  删除所有状态
		 */
		public function removeallgrade():void
		{
			var t:Number=new Number();
			for(t=0;t<this.gradenumber;t++)
			{
				this.grade[t]=null;
				this.p[t]=null;
			}
			this.gradenumber=0;
		}
		/** 
		 * 获取状态名称
		 */ 
		public function getcolour(pp:Number):String
		{
			return colour[pp];
		}
		/** 
		 * 获取状态名称
		 */  
		public function getgrade(pp:Number):String
		{
			return this.grade[pp];
		}
		/** 
		 * 获取状态的个数
		 */  
		public function getgradenum():Number
		{
			return this.gradenumber;
		}
		/** 
		 * 获取某个位置的概率
		 */  
		public function getgradep(pp:Number):Number
		{
			return this.p[pp];
		}
		/** 
		 * 设定D变量是否已经与某个X变量相连
		 */  
		public function setDcon(con:Boolean):void
		{
			this.Disconnected=con;
		}
		/** 
		 * 获取D变量是否已经与X变量相连
		 */  
		public function getDcon():Boolean
		{
			return this.Disconnected;
		}
		/** 
		 * 设定名称
		 */  
		public function setname(name:String):void
		{
			this.comname=name;
		}
		/** 
		 * 读取名称
		 */  
		public function readname():String
		{
			return this.comname;
		}
		/** 
		 * 设定编号
		 */  
		public function setnum(num:Number):void
		{
			this.num=num;
		}
		/** 
		 * 读取编号
		 */  
		public function readnum():Number
		{
			return this.num;
		}
		/** 
		 * 设定写入编号
		 */  
		public function setwritenum(num:Number):void
		{
			this.numwrite=num;
		}
		/** 
		 * 读取写入编号
		 */  
		public function getwritenum():Number
		{
			return this.numwrite;
		}
		/** 
		 * 设定其所在状态
		 */  
		public function setstate(num:Number):void
		{
			this.state=num;
		}
		/** 
		 * 读取其所在状态
		 */  
		public function readstate():Number
		{
			return this.state;
		}
		/** 
		 * 添加指出的线
		 */  
		public function addlineout(li:line):void
		{
			li.setnumasout(lineoutnum);
			lineout[lineoutnum]=li;
			lineoutnum++;
		}
		/** 
		 * 删除指出的线
		 */  
		public function dellineout(num:Number):void
		{
			var p:Number=new Number();
			var linetmp:line=new line();
			for(p=num+1;p<lineoutnum;p++)
			{
				linetmp=lineout[p];
				linetmp.setnumasout(p-1);
				lineout[p-1]=linetmp;
			}
			lineout[lineoutnum-1]=null;
			lineoutnum--;
		}
		/** 
		 * 读取指出的线
		 */  
		public function getlineout(num:Number):line
		{
			return lineout[num];
		}
		/** 
		 * 添加指入的线
		 */  
		public function addlinein(li:line):void
		{
			li.setnumasin(lineinnum);
			linein[lineinnum]=li;
			lineinnum++;
		}
		/** 
		 * 删除指入的线
		 */  
		public function dellinein(num:Number):void
		{
			var p:Number=new Number();
			var linetmp:line=new line();
			for(p=num+1;p<lineinnum;p++)
			{
				linetmp=linein[p];
				linetmp.setnumasin(p-1);
				linein[p-1]=linetmp;
			}
			linein[lineinnum-1]=null;
			lineinnum--;
		}
		/** 
		 * 读取指入的线
		 */  
		public function getlinein(num:Number):line
		{
			return linein[num];
		}
		/** 
		 * 读取指入的线的个数
		 */  
		public function getlineinnum():Number
		{
			return lineinnum;
		}
		/** 
		 * 读取指出的线的个数
		 */  
		public function getlineoutnum():Number
		{
			return lineoutnum;
		}
		/** 
		 * 设定属性
		 */  
		public function setatt(att:Number):void
		{
			this.attri=att;
			if(att==4)gradenumber=1;
			if(att!=1)layer=1;
		}
		/** 
		 * 读取属性
		 */  
		public function getatt():Number
		{
			return this.attri;
		}
		
		/**
		 * read state ids
		 */
		public function getStateIds():Array{
			return this.gradeIds;
		}
		
		/**
		 * read state id at specified index
		 */
		public function getStateId(index:Number):Number{
			return this.gradeIds[index];
		}
		
		
		/**
		 * set state id at current position
		 */
		public function setStateId(id:Number):void{
			this.gradeIds[this.gradenumber-1] = id;
		}
	}
}