package gct.com.wyh
{
	import mx.collections.ArrayCollection;
	
	import spark.components.Group;
	import spark.components.NavigatorContent;
	import spark.components.Scroller;
	import spark.components.VGroup;

	public class DUCGmodule extends NavigatorContent
	{
		public var scrller:Scroller = new Scroller();
		public var user:String=new String();
		public var modulename:String=new String();	//名称
		public var draw:Group=new Group();			//画板
		public var number:Number=new Number();		//编号
		public var comnum:Array=new Array();		//所有变量
		public var comn:Number=1;					//变量数量
		public var arr:Array=new Array();			//所有线条
		public var arrnum:Number=0;					//线条数量
		public var component:Array=new Array();
		
		public var mundoarr:Array=new Array();		//撤销堆栈
		public var mundonum:Number=0;				//栈顶
		public var mredoarr:Array=new Array();		//重做堆栈
		public var mredonum:Number=0;				//栈顶
		
		public var firstcan:ArrayCollection=new ArrayCollection();
		public var firstli:ArrayCollection=new ArrayCollection();
		public var secondcan:ArrayCollection=new ArrayCollection();
		public var secondli:ArrayCollection=new ArrayCollection();
		public var firstshowpd:Boolean=false;
		public var secondshowpd:Boolean=false;
		public var readed:Boolean=false;
		public var readed2:Boolean=false;
	}
}