package gct.com.wyh
{
	public class Matrix
	{
		public var mData:Array;
		public var linesCount:int;
		public var columsCount:int;
		
		public function Matrix(lines:int, colums:int)
		{
			mData = new Array(lines);
			for(var i:int = 0 ; i < lines; i++){
				mData[i] = new Array(colums);
			}
			this.linesCount = lines;
			this.columsCount = colums;
		}
		
		public function setCellAt(line_index:int, column_index:int, data:Number):void{
			mData[line_index][column_index] = data;
		}
		
		public function getCellAt(line_index:int, column_index:int):Number{
			return mData[line_index][column_index];
		}
		
		public function addNewLine(line_index : int):void{
			for(var i:int = mData.length; i > line_index ; i--){
				mData[i] = mData[i-1];
			}
			
			mData[line_index] = new Array(columsCount);
			for(var i:int = 0 ; i < columsCount; i++)
				mData[line_index][i] = NaN;
			
			linesCount ++;
		}
		
		public function removeLineAt(line_index : int):void{
			for(var i:int = line_index; i < linesCount-1; i++)
				mData[i] = mData[i+1];
			mData.pop();
			
			linesCount --;
		}
		
		public function addNewColumn(column_index:int):void{
			for(var i:int = 0 ; i < linesCount; i++){
				for(var j:int = columsCount; j > column_index; j--)
					mData[i][j] = mData[i][j-1];
				mData[i][column_index] = NaN;
			}
			
			columsCount ++;
		}
		
		public function removeColumn(column_index:int):void{
			for(var i:int = 0 ; i < linesCount; i++){
				for(var j:int = column_index; j < columsCount -1; j++)
					mData[i][j] = mData[i][j+1];
				mData[i].pop();
			}
			
			columsCount --;
		}
		
		public function toString():String{
			var buf:String = "";
			for(var i:int = 0 ; i < linesCount ; i++)
				for(var j:int = 0 ; j < columsCount ; j++)
					buf += String(mData[i][j])+",";
			return buf;
		}
	}
}