package gct.com.wyh
//bxs 自动消失的提示窗口 2016.6
{
	import flash.display.DisplayObject;
	import flash.display.Stage;
	import flash.events.MouseEvent;
	import flash.system.Capabilities;
	
	import gct.com.wyh.*;
	
	import mx.containers.Panel;
	import mx.controls.Alert;
	import mx.controls.Image;
	import mx.core.FlexGlobals;
	import mx.events.FlexMouseEvent;
	import mx.managers.PopUpManager;
	
	import spark.components.Label;

	public class MsgBox extends Panel
	{
		private var _label:Label;
		private var _context:String;
		private var _image:Image;
		private var _imageType:int;
		private var _x:int;
		private var _y:int;
		
		private var _fadeEffect:FadeEffect;
		
		public static var imageType_warning:int = 1;
		public static var imageType_error:int = 2;
		public static var imageType_right:int = 3;
		public static var _width:int=0;
		public static var _height:int=0;
		[Bindable]
		[Embed(source="../../../../pic/icoRight.jpg")]
		public var icoRight:Class
		
		[Bindable]
		[Embed(source="../../../../pic/icoError.jpg")]
		public var icoError:Class
		
		[Bindable]
		[Embed(source="../../../../pic/icoWarning.jpg")]
		public var icoWarning:Class
		private var time:int=0;
		public function MsgBox(context:String, imageType:int, x:int, y:int,time:int)
		{
			super();
			this.setStyle("headerHeight",0);
			this._context = context;
			this._imageType = imageType;
			
			this._fadeEffect = new FadeEffect(this);
			
			this.x = x;
			
			this.y = y;
		    this.time=time;
		}
		
		override protected function createChildren() : void {
			super.createChildren();
			addChild(createLabel());
			addChild(createImage());
		}
				
		public static function show(context:String, imageType:int = 1, x:int = 1000, y:int = 0, time:int = 3500):void {
			var msgBox:MsgBox = new MsgBox(context, imageType, x, y, time);
			PopUpManager.addPopUp(msgBox,(DisplayObject)(FlexGlobals.topLevelApplication));
			//PopUpManager.centerPopUp(msgBox);
			//PopUpManager.
			msgBox._fadeEffect.fadeOut(time);
			
		//	msgBox.addEventListener(FlexMouseEvent.MOUSE_DOWN_OUTSIDE,msgBox.removePopUp);
		//	msgBox.addEventListener(MouseEvent.MOUSE_OVER,msgBox.mouseOver);
			msgBox.addEventListener(MouseEvent.MOUSE_OUT,msgBox.mouseOut);
		}
		
		public function removeAlert():void{
			_fadeEffect.fadeOut(0);
		}
		
		private function removePopUp(e:FlexMouseEvent):void {
			PopUpManager.removePopUp(this);	
		}
		
		private function mouseOver(e:MouseEvent):void {
			if (this._fadeEffect.isPlaying) {
				this._fadeEffect.fadeIn(0);
			}
		}
		
		private function mouseOut(e:MouseEvent):void {
			_fadeEffect.fadeOut(3500);
		}
		
		override protected function measure():void {
			this._label.width = this._label.measureText(_context).width;
			this._label.height = this._label.measureText(_context).height;
			measuredWidth = this._label.width +150;
			measuredHeight = 100;
			_width=measuredWidth;
			_height=measuredHeight;
		}
		public function setWidthHeight():void{
			measure();
		}
		override protected function updateDisplayList(param1:Number, param2:Number) : void {
			super.updateDisplayList(unscaledWidth,unscaledHeight);
			this._label.move(48+(this.width - this._label.width-48)/2 - 5 ,(this.height - this._label.height)/2);
			this._image.x = 25;
			this._image.y = 25;
			
		}
		
		private function createImage():Image {
			_image = new Image();
			if (_imageType == MsgBox.imageType_error) {
				_image.source = icoError;
				_image.width=50;
				_image.height=50;
				_label.setStyle("color","red");
			} else if (_imageType == MsgBox.imageType_right) {
				_image.source = icoRight;	
				_image.width=50;
				_image.height=50;
				_label.setStyle("color","green");
			} else {
				_image.source = icoWarning;		
				_label.setStyle("color","red");
				_image.width=50;
				_image.height=50;
			}
			return _image;
		}
		
		private function createLabel():Label {
			_label = new Label();
			_label.setStyle("fontSize",18);
			
			_label.text = _context;
			return _label;
		}
	}
}
