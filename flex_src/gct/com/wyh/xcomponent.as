package gct.com.wyh
{
	import mx.charts.chartClasses.NumericAxis;
	import mx.collections.ArrayCollection;
	import mx.collections.ArrayList;
	import mx.containers.Canvas;
	import mx.core.UIComponent;
	import mx.messaging.management.Attribute;
	
	import spark.components.BorderContainer;

	public class xcomponent extends BorderContainer
	{
		public var infostr:String=new String();
		public var zuobiao:Array=new Array();//记录在工程图上的坐标
		public var Gpd:Boolean=new Boolean();//G门的判断，true可以连黑线，false不可以
		public var layer:int=0;//层数，合成时布局用
		public var col:int=0;//遍历标记
		public var termpd:Boolean=false;
		
		private var chuyin:Array=new Array();//是否是初因事件,false为非初因，true为初因
		private var cy:Boolean=true;//最终显示初因或非出因
		private var attri:Number=new Number();//类别，1为B，2为X，3为G，4为D，5为虚D，6为BX，7为虚X,8为T
		public var comname:String=new String();//名称
		private var info:String=new String();//文字说明
		private var relattion:Array=new Array();//其所有父变量
		private var num:Number=new Number();//编号
		private var state:Number=new Number();//所处状态
		private var lineout:Array=new Array();//指出去的线
		private var linein:Array=new Array();//指进来的线
		private var grade:Array=new Array();//状态名称
		private var gradeIds:Array=new Array();
		private var colour:Array=new Array();//状态颜色
		private var lineinnum:Number=0;//指入线个数  
		
		private var prelineoutnum:Number = 0; //前一阶段出现数量
		
		private var lineoutnum:Number=0;//指出线个数
		private var firstlineoutnum = 0;	//一步预测之后指出线的数量
		
		private var Disconnected:Boolean=false;//D变量是否已经相连
		private var gradenumber:Number=0;//状态总数
		private var p:Array=new Array();//状态概率，用于B，G，D
		private var issetted:Boolean=false;//是否已经赋值
		private var isadded:Boolean=false;//是否添加
		private var isSequence:Boolean = false;//是否已经排序
		private var isDrawLine:Boolean = false;	//是否划过线
		private var rowNum:int = -1; 			//该结点在第几列
		
		private var isTraversal:Boolean = false;
		
		private var numwrite:Number=new Number();//写txt的编号
		//bxs，是否可检测变量,如果是不可检查变量，那么颜色为灰色，值为0
		private var isCount:int=-1;
		
		public var atstate:ArrayCollection=new ArrayCollection();
		private var properties:Number = 0;
		
		
		public function xcomponent():void
		{
			this.width = 40;
			this.height = 40;
			this.setStyle("borderVisible", false);
		}
		public function addlineinat(lt:line):void
		{
			var canadd:Boolean = false;
			if(lt.startcanvas && lt.startcanvas.getatt() == 4) {
				if(defaultTakenIn[lt.startcanvas.readnum()] !== true) {
					canadd = true;
					defaultTakenIn[lt.startcanvas.readnum()] = true
				}
			} else {
				canadd = true;
			}
			
			if(canadd) {
				var num:int=lt.getnumasin();
				var p:int;
				for(p=lineinnum-1;p>=num;p--)
				{
					linein[p+1]=linein[p];
					line(linein[p+1]).setnumasin(p+1);
				}
				linein[num]=lt;
				lineinnum++;
			}
		}
		
		public function addlineoutat(lt:line):void
		{
			var canadd:Boolean = false;
			if(lt.endcanvas && lt.endcanvas.getatt() == 4) {
				if(defaultTakenOut[lt.endcanvas.readnum()] !== true) {
					defaultTakenOut[lt.endcanvas.readnum()] = true;
					canadd = true;
				}
			} else {
				canadd = true;
			}
			
			if(canadd) {
				var num:int=lt.getnumasout();
				var p:int;
				for(p=lineoutnum-1;p>=num;p--)
				{
					lineout[p+1]=lineout[p];
					line(lineout[p+1]).setnumasout(p+1);
				}
				lineout[num]=lt;
				lineoutnum++;
			}
		}
		
		public function getcopy():xcomponent
		{
			var t:xcomponent=new xcomponent();
			var pp:int;
			t.setatt(attri);
			t.setnum(num);
			t.infostr=infostr;
			t.setinfo(info);
			t.setname(comname);
			t.setDcon(Disconnected);
			t.setIsCount(isCount);
			return t;
		}
		/** 
		 * 设置文字说明
		 */ 
		public function setinfo(str:String):void
		{
			info=str;
		}
		/** 
		 * 读取文字说明
		 */ 
		public function getinfo():String
		{
			return info;
		}
		/** 
		 * 设置是否已经添加到画板上
		 */ 
		public function setadded(chu:Boolean):void
		{
			this.isadded=chu;
		}
		/** 
		 * 读取是否已添加到画板上
		 */ 
		public function getadded():Boolean
		{
			return this.isadded;
		}
		/** 
		 * 读取是否是初因状态
		 */ 
		public function getchuyin(num:Number):Boolean
		{
			return this.chuyin[num];
		}
		/**
		 * 是否被排序过
		 */
		public function getSequence():Boolean
		{
			return this.isSequence;
		}
		/**
		 * 设置被排序过
		 */
		public function setSequence(tem:Boolean):void
		{
			this.isSequence = tem;
		}
		
		
		public function getTraversal():Boolean
		{
			return this.isTraversal;
		}
		
		public function setTraversal(tem:Boolean):void
		{
		    this.isTraversal = tem;
		}
		/**
		 * 设置结点所在的列数
		 */
		public function setRowNum(temrowNum:int):void
		{
			this.rowNum = temrowNum;
		}
		/**
		 * 获得结点所在的列数
		 */
		public function getRowNum():int
		{
			return this.rowNum;
		}
		/**
		 * 获得是否划过线
		 */
		public function getDrawLine():Boolean
		{
			return this.isDrawLine;
		}
		/**
		 * 设置是否划过线
		 */
		public function setDrawLine(tem:Boolean):void
		{
			this.isDrawLine = tem;
		}
		
		/** 
		 * 读取是否是初因变量
		 */ 
		public function getcy():Boolean
		{
			var p:Number = new Number();
			for(p = 0; p < gradenumber; p++)
			{
				cy = cy && chuyin[p];
			}
			return cy;
		}
		/** 
		 * 设定是否赋过值
		 */  
		public function setsetted(setted:Boolean):void
		{
			this.issetted=setted;
		}
		/** 
		 * 读取是否赋过值
		 */  
		public function getsetted():Boolean
		{
			return this.issetted;
		}
		/** 
		 * 添加状态及概率
		 */  
		public function addgrade(gra:String,pp:Number,cc:Boolean,co:String, stateId:Number):void
		{
			while(stateId > this.gradenumber)
			{
				this.grade[this.gradenumber]=gra;
				this.colour[this.gradenumber]=co;
				this.chuyin[this.gradenumber]=cc;
				this.gradenumber ++;
			}
			this.grade[this.gradenumber]=gra;
			this.colour[this.gradenumber]=co;
			if(attri==1)
			{
				while(stateId > this.properties)
				{
					this.p[this.properties]=0;
					this.properties++;
				}
				this.p[this.properties]=pp;
				this.chuyin[this.gradenumber]=cc;
				this.gradenumber++;
				this.properties++;
			} else if(attri==4)
			{
				this.p[0]=pp;
				this.gradenumber=1;
			} else if(attri==2||attri==3||attri==6)
			{
				this.gradenumber++;
			}
		}
		/**
		 *  删除所有状态
		 */
		public function removeallgrade():void
		{
			var t:Number=new Number();
			for(t=0;t<this.gradenumber;t++)
			{
				this.grade[t]=null;
				this.p[t]=null;
			}
			this.gradenumber=0;
		}
		/** 
		 * 获取状态名称
		 */ 
		public function getcolour(pp:Number):String
		{
			return colour[pp];
		}
		/** 
		 * 获取状态名称
		 */  
		public function getgrade(pp:Number):String
		{
			return this.grade[pp];
		}
		/** 
		 * 获取状态的个数
		 */  
		public function getgradenum():Number
		{
			return this.gradenumber;
		}
		
		/**
		 * get maximum even state id
		 */
		public function get_full_states_count() : Number{
			var max : Number = this.gradeIds[this.gradenumber-1];
			return max+1;
		}
		
		private function check_state_id(id : int) : Boolean {
			for (var idx:int = 0; idx < this.gradenumber; idx++)
				if (id == this.gradeIds[idx])
					return true;
			return false;
		}
		
		
		public function get_full_states_properties() : String {
			var last:int = 0, pro:Number;
			var ret: String = "";
			
			if (this.gradenumber == 0)
				return ret;
			
			pro = this.p[0];
			if (isNaN(pro))
				pro = 0;
			ret += pro;
			for(var q:int = 1; q < this.gradenumber; q++) {
				for(var p:int = last+1; p < q ; p++)
					ret += " 0";
				pro = this.getgradep(q);
				if (isNaN(pro))
					pro = 0;
				ret += " "+pro;
			}
			return ret;
		}
		
		
		/** 
		 * 获取某个位置的概率
		 */  
		public function getgradep(pp:Number):Number
		{
			return this.p[pp];
		}
		
		public function hasStateId(stateId:Number):Boolean{
			for(var i:int = 0 ; i < this.gradeIds.length ; i++)
				if(stateId = this.gradeIds[i])
					return true;
			return false;
		}
		
		/** 
		 * 设定D变量是否已经与某个X变量相连
		 */  
		public function setDcon(con:Boolean):void
		{
			this.Disconnected=con;
		}
		/** 
		 * 获取D变量是否已经与X变量相连
		 */  
		public function getDcon():Boolean
		{
			return this.Disconnected;
		}
		/** 
		 * 设定名称
		 */  
		public function setname(name:String):void
		{
			this.comname=name;
		}
		/** 
		 * 读取名称
		 */  
		public function readname():String
		{
			return this.comname;
		}
		/** 
		 * 设定编号
		 */  
		public function setnum(num:Number):void
		{
			this.num=num;
		}
		/** 
		 * 读取编号
		 */  
		public function readnum():Number
		{
			return this.num;
		}
		/** 
		 * 设定写入编号
		 */  
		public function setwritenum(num:Number):void
		{
			this.numwrite=num;
		}
		/** 
		 * 读取写入编号
		 */  
		public function getwritenum():Number
		{
			return this.numwrite;
		}
		/** 
		 * 设定其所在状态
		 */  
		public function setstate(num:Number):void
		{
			this.state=num;
		}
		/** 
		 * 读取其所在状态
		 */  
		public function readstate():Number
		{
			return this.state;
		}
		/** 
		 * 添加指出的线
		 */  
		private var defaultTakenOut:Array = new Array();
		public function addlineout(li:line):void
		{
			var canadd:Boolean = false;
			if(li.endcanvas && li.endcanvas.getatt() == 4 ) {
				if(defaultTakenOut[li.endcanvas.readnum()] !== true) {
					canadd = true;
					defaultTakenOut[li.endcanvas.readnum()] = true;
				}
			} else {
				canadd = true;
			}
			
			if (canadd) {
				li.setnumasout(lineoutnum);
				lineout[lineoutnum]=li;
				lineoutnum++;
			}
		}
		/** 
		 * 删除指出的线
		 */  
		public function dellineout(num:Number):void
		{
			var p:Number=new Number();
			var linetmp:line=new line();
			for(p=num+1;p<lineoutnum;p++)
			{
				linetmp=lineout[p];
				linetmp.setnumasout(p-1);
				lineout[p-1]=linetmp;
			}
			lineout[lineoutnum-1]=null;
			lineoutnum--;
		}
		/** 
		 * 读取指出的线
		 */  
		public function getlineout(num:Number):line
		{
			return lineout[num];
		}
		/** 
		 * 添加指入的线
		 */  
		
		private var defaultTakenIn:Array = new Array();
		public function addlinein(li:line):void
		{
			var canadd:Boolean = false;
			if(li.startcanvas && li.startcanvas.getatt() == 4) {
				if(defaultTakenIn[li.startcanvas.readnum()] !== true) {
					canadd = true;
					defaultTakenIn[li.startcanvas.readnum()] = true	
				}
			} else {
				canadd = true;
			}
			
			if(canadd) {
				li.setnumasin(lineinnum);
				linein[lineinnum]=li;
				lineinnum++;
			}
		}
		/** 
		 * 删除指入的线
		 */  
		public function dellinein(num:Number):void
		{
			var p:Number=new Number();
			var linetmp:line=new line();
			for(p=num+1;p<lineinnum;p++)
			{
				linetmp=linein[p];
				linetmp.setnumasin(p-1);
				linein[p-1]=linetmp;
			}
			linein[lineinnum-1]=null;
			lineinnum--;
		}
		/** 
		 * 读取指入的线
		 */  
		public function getlinein(num:Number):line
		{
			return linein[num];
		}
		/** 
		 * 读取指入的线的个数
		 */  
		public function getlineinnum():Number
		{
			return lineinnum;
		}
		/** 
		 * 读取指出的线的个数
		 */  
		public function getlineoutnum():Number
		{
			return lineoutnum;
		}
		/**
		 * 设定指出线的个数
		 */
		public function setlineoutnum(linoutnu:Number):void
		{
			lineoutnum = linoutnu ;
		}
		/** 
		 * 读取化简预测指出的线的个数
		 */  
		public function getprelineoutnum():Number
		{
			return prelineoutnum;
		}
		/**
		 * 设定化简预测指出线的个数
		 */
		public function setprelineoutnum(linoutnu:Number):void
		{
			prelineoutnum = linoutnu ;
		}
		/** 
		 * 读取一步预测指出的线的个数
		 */  
		public function getfirstlineoutnum():Number
		{
			return firstlineoutnum;
		}
		/**
		 * 设定一步预测指出线的个数
		 */
		public function setfirstlineoutnum(linoutnu:Number):void
		{
			firstlineoutnum = linoutnu ;
		}
		/** 
		 * 设定属性
		 */  
		public function setatt(att:Number):void
		{
			this.attri=att;
			if(att==4) {
				gradenumber=1;
				gradeIds[0] = 0;
			}
			
			if(att!=1)
				layer=1;
		}
		/** 
		 * 读取属性
		 */  
		public function getatt():Number
		{
			return this.attri;
		}
		
		/**
		 * read state ids
		 */
		public function getStateIds():Array{
			return this.gradeIds;
		}
		
		/**
		 * read state id at specified index
		 */
		public function getStateId(index:Number):Number{
			return this.gradeIds[index];
		}
		
		
		/**
		 * set state id at current position
		 */
		public function setStateId(id:Number):void{
			this.gradeIds[this.gradenumber-1] = id;
		}
		
		public function getTypeChar():String{
			if(this.attri == 1)
				return "B";
			else if(this.attri == 2)
				return "X";
			else if(this.attri == 3)
				return "G";
			else if(this.attri == 4)
				return "D";
			else if(this.attri == 6)
				return "U";
			else if(this.attri == 8)
				return "SX";
			return null;
		}
		//isCount的get和set方法
		 public function getIsCount(){
		    return this.isCount;
		 }
		 public function setIsCount(isCount:int){
			  this.isCount=isCount;
		 }
	}
}