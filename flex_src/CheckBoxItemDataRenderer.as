package
{
	import flash.events.Event;	
	import mx.collections.ArrayCollection;
	import mx.controls.CheckBox;
	import mx.controls.DataGrid;
	import mx.controls.Alert;
	
	public class CheckBoxItemDataRenderer extends CheckBox
	{
		private var currentData:Object; //保存当前一行值的对象
		public function CheckBoxItemDataRenderer()
		{
			super();		
			this.maintainProjectionCenter=false;
		
			this.styleName="center";
			this.horizontalCenter="center";
				
			this.addEventListener(Event.CHANGE, changeHandler);
		}
		override public function set data(value:Object):void
		{
			this.currentData= value;
			
			this.selected = value.selected == "true"?true:false;
		
		}
		
		protected function changeHandler(event : Event) : void
		{
			currentData.selected = this.selected.toString();
		}
		/*override public function get data():Object{
		return currentData;
		}*/
	}
}