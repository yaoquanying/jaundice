package edi
{
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	public class fourtree
	{
		public var isright:Boolean=true;//是否是正确的四元式
		public var basic:Boolean=false;//是否是一个基本的状态
		public var lefttree:fourtree;//第一运算数
		public var rightree:fourtree;//第二运算数
		public var cal:int=0;//运算种类,0为非，1为+，2为*
		public var cn:int=0;//若为一个基本状态则基本状态的变量编号
		public var sn:int=0;//若为一个基本状态则基本状态的状态编号
		public var isF:Boolean=false;
		public var cna:ArrayCollection=new ArrayCollection();
		public function getcna():void
		{
			if(basic==true){cna.addItem(cn);}
			else
			{
				if(cal==0){lefttree.getcna();addcna(lefttree.cna);}
				else if(cal==1||cal==2){lefttree.getcna();addcna(lefttree.cna);rightree.getcna();addcna(rightree.cna);}
			}
		}
		private function addcna(arr:ArrayCollection):void
		{
			var p:int;
			for(p=0;p<arr.length;p++)
			{
				var q:int=int(arr.getItemAt(p));
				if(cna.contains(q)==false){cna.addItem(q);}
			}
		}
		public function fourtree()
		{
			
		}
		public function check(c:int,s:int):Boolean
		{
			getcna();
			var b:Boolean=false;
			if(basic==true){if(c==cn&&s==sn){b=true;}}
			else
			{
				if(cal==0){if(lefttree.check(c,s)==false&&cna.contains(c)){b=true;}}
				else if(cal==1){if(lefttree.check(c,s)==true||rightree.check(c,s)==true)b=true;}
				else if(cal==2){if(lefttree.check(c,s)==true&&rightree.check(c,s)==true)b=true;}
			}
			return b;
		}
	}
}