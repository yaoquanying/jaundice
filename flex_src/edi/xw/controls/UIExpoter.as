package edi.xw.controls.utility
{
	import edi.DUCGmodule;
	import edi.line;
	import edi.xcomponent;
	
	import flash.display.BitmapData;
	import flash.display.DisplayObject;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.net.FileReference;
	import flash.utils.ByteArray;
	
	import mx.collections.ArrayCollection;
	import mx.collections.ArrayList;
	import mx.controls.Alert;
	import mx.graphics.codec.PNGEncoder;

	public class UIExpoter
	{		
		public function UIExpoter()
		{
		}
		
		static public function export2Img(d:DisplayObject, name:String) : void {
			var dt:DisplayObject = d;
			var bmpData:BitmapData = new BitmapData(d.width, d.height, true, 0x00ffffff);
			bmpData.draw(d);
			
			var fr:Object = new FileReference();  
			if(fr.hasOwnProperty("save"))
			{  
				var encoder:PNGEncoder = new PNGEncoder();  
				var data:ByteArray = encoder.encode(bmpData);  
				fr.save(data, name+'.jpg');  
			}
			else
			{  
				Alert.show("当前flash player版本不支持此功能,请安装10.0.0以上版本！","提示");  
			}  
		}
		
		static public function export2ImgWithSize(d:DisplayObject, name:String, width:int, height:int) : void {
			var dt:DisplayObject = d;
			var bmpData:BitmapData = new BitmapData(width, height, true, 0x00ffffff);
			bmpData.draw(d);
			
			var fr:Object = new FileReference();  
			if(fr.hasOwnProperty("save"))
			{  
				var encoder:PNGEncoder = new PNGEncoder();  
				var data:ByteArray = encoder.encode(bmpData);  
				fr.save(data, name+'.jpg');  
			}
			else
			{  
				Alert.show("当前flash player版本不支持此功能,请安装10.0.0以上版本！","提示");  
			}  
		}
		
		static public function exportMatrix(dm : DUCGmodule, c:Array) : void {
			var columns:int = 0;
			var data:String = "";
			var matrices : ArrayList = new ArrayList();
			
			for(var p:int=0;p<dm.arrnum;p++)
			{
				var uic:line=dm.arr[p];
				var i:int = 0, j:int = 0;
				
				var parent_node:xcomponent = uic.startcanvas.getatt() == 4 ? uic.startcanvas : c[uic.startcanvas.readnum()];
				var child_node:xcomponent = uic.endcanvas.getatt() == 4 ? uic.endcanvas : c[uic.endcanvas.readnum()];
				
				
				var parent_states_count : Number = parent_node.getgradenum();
				var child_states_count : Number = child_node.getgradenum();
				
				if(parent_states_count > columns)
					columns = parent_states_count;
				
				data = ",";
				for(i = 0 ; i < parent_states_count; i++) {
					var cap:String;
					if(uic.startcanvas.getatt()==1)
					{
						cap="B"+uic.startcanvas.readnum()+"," + parent_node.getStateId(i);
					}
					else if(uic.startcanvas.getatt()==2)
					{
						cap="X"+(uic.startcanvas.readnum()-10000)+","+parent_node.getStateId(i);
					}
					else if(uic.startcanvas.getatt()==3)
					{
						cap="G"+(uic.startcanvas.readnum()-20000)+","+parent_node.getStateId(i);
					}
					else if(uic.startcanvas.getatt()==4)
					{
						cap="D"+(uic.startcanvas.readnum()-10000)+","+parent_node.getStateId(i);
					}
					else if(uic.startcanvas.getatt()==6)
					{
						cap="BX"+(uic.startcanvas.readnum()-30000)+","+parent_node.getStateId(i);
					}
					
					data += '"' + cap + '",';
				}
				
				data += "\n";
				
				for(i = 0 ; i < child_states_count; i++) {
					if(uic.endcanvas.getatt()==1)
					{
						cap="B"+uic.endcanvas.readnum()+","+child_node.getStateId(i);
					}
					else if(uic.endcanvas.getatt()==2)
					{
						cap="X"+(uic.endcanvas.readnum()-10000)+","+child_node.getStateId(i);
					}
					else if(uic.endcanvas.getatt()==3)
					{
						cap="G"+(uic.endcanvas.readnum()-20000)+","+child_node.getStateId(i);
					}
					else if(uic.endcanvas.getatt()==6)
					{
						cap="BX"+(uic.endcanvas.readnum()-30000)+","+child_node.getStateId(i);
					}
					
					data += '"' +cap + '",';
					
					var tmp:String = "";
					
					cap = "";
					for(j = 0; j < parent_states_count; j++) {
						tmp = ""+uic.getlinep(child_node.getStateId(i),parent_node.getStateId(j));
						if(tmp == "NaN")
							tmp = "";
						
						cap += tmp+",";
					}
					
					data += cap + "\n";
				}
				
				matrices.addItem(data);
				
				if(columns < parent_states_count)
					columns = parent_states_count;
			}
			
			data = "";
			var lineCap : String = "";
			for(i = 0; i <= columns ; i++)
				lineCap += ",";
			lineCap += "\n";
			
			for(var q:int=0;q<dm.arrnum;q++)
			{
				data += matrices.getItemAt(q);
				data += lineCap;
			}
			
			var fr:Object = new FileReference();  
			if(fr.hasOwnProperty("save"))
			{
				fr.save(data, 'matrix.csv');  
			}
			else
			{  
				Alert.show("当前flash player版本不支持此功能,请安装10.0.0以上版本！","提示");  
			}  
		}
	}
}