package edi.xw.controls
{  
	import mx.containers.TabNavigator;  
	
	public class ExtTabNavigator extends TabNavigator  
	{  
		public function ExtTabNavigator()  
		{  
			super();  
		}  
		
		override protected function commitSelectedIndex(newIndex:int):void  
		{  
			super.commitSelectedIndex(newIndex);  
			tabBar.selectedIndex = newIndex;  
		}  
		
	}  
}  