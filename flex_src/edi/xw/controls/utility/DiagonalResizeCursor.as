package edi.xw.controls.utility
{

    /**                                                                                                                                                                 
     *  @private                                                                                                                                                        
     */
    public class DiagonalResizeCursor extends HorizontalResizeCursor
    {
	public function DiagonalResizeCursor()
	{
	    super();
            cursor.rotation = 45;
	}
    }

}
