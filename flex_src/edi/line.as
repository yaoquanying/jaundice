package edi
{
	import flash.display.Sprite;
	
	import mx.containers.Canvas;
	import mx.core.UIComponent;
	
	public class line extends UIComponent
	{
		public var startcanvas:xcomponent=new xcomponent;//起始
		public var endcanvas:xcomponent=new xcomponent;//结束
		public var sqlid:int=new int();//数据库中的id
		public var moin:String=new String();
		public var yellow:Boolean=false;
		public var yesp:Sprite;
		public var resp:Sprite;
		public var componentJSP:Array = new Array();
		
		private var number:Number= new Number;//编号
		private var type:Number=new Number;//种类，1红实2红虚3绿实4绿虚5无头6双红线(bxs)7关联性(yao),8双红线(bxs)
		private var issetted:Boolean=false;//是否赋值
		private var linep:Array=new Array();//概率数组
		private var r:Number=new Number();//权重
		private var condition:String=new String();//条件
		private var NumAsIn:Number=new Number();//存放在结束变量中linein数组的编号
		private var NumAsOut:Number=new Number();//存放在结束变量中lineout数组的编号
		public var linewidth:Number=3;//线条粗细
		
		public var data : Matrix;
		
		
		public function initMatrix(lines:Number, columns:Number, rawdata:String):void{
			var raws:Array=rawdata.split(",");
			this.data = new Matrix(lines, columns);
			for(var i:int = 0 ; i < lines; i++)
				for(var j:int = 0 ; j < columns; j++)
					data.setCellAt(i,j, Number(raws[i * columns + j]));
		}
		
		public function removeMatrixColumn(index:int):void{
			this.data.removeColumn(index);
		}
		
		public function addNewMatrixColumn(index:int):void{
			this.data.addNewColumn(index);
		}
		public function removeMatrixLine(index:int):void{
			this.data.removeLineAt(index);
		}
		public function addMatrixLine(index:int):void{
			this.data.addNewColumn(index);
		}
		
		
		public function getcopy():line
		{
			var t:line=new line();
			var p:Number=0;
			var q:Number=0;
			t.moin=moin;
			t.setsetted(issetted);
			t.settype(type, this.componentJSP);
			t.setr(r);
			t.setcon(condition);
			t.sqlid=sqlid;
			t.setNum(number);
			t.setstart(startcanvas);
			t.setend(endcanvas);
			
			//DONE state count restriction
			var parent:Number = startcanvas.getatt() == 4 ? startcanvas.getgradenum():this.componentJSP[startcanvas.readnum()].get_full_states_count();
			var child: Number = this.componentJSP[endcanvas.readnum()].get_full_states_count();
			
			for(p=0;p<child;p++)
			{
				for(q=0;q<parent;q++)
				{
					t.setlinep(p,q,linep[p*parent+q]);
				}
			}
			
			return t;
		}
		/** 
		 * 设定条件
		 */ 
		public function setcon(str:String):void
		{
			if(type==2||type==4)
			{
				this.condition=str;
			}
		}
		/** 
		 * 读取条件
		 */ 
		public function getcon():String
		{
			return this.condition;
		}
		/** 
		 * 权重进行赋值
		 */ 
		public function setr(num:Number):void
		{
			if(type == 1 || type == 2 || type == 6 || type == 7)
			{	
				this.r = num;
			}
		}
		/** 
		 * 读取权重
		 */ 
		public function getr():Number
		{
			return this.r;
		}
		/** 
		 * 进行概率赋值
		 */  
		public function setlinep(x:int,y:int,num:Number):void
		{
			var pnode:xcomponent = startcanvas.getatt() == 4 ? startcanvas:this.componentJSP[startcanvas.readnum()];
			var parent:Number = pnode == null ? 1 : pnode.get_full_states_count();
			//var child:Number = this.endcanvas.getgradenum();
			//this.linep[x*5+y]=num;
			this.linep[x*parent+y]=num;
		}
		/** 
		 * 读取概率
		 */  
		public function getlinep(x:int,y:int):Number
		{
			var pnode:xcomponent = startcanvas.getatt()==4? startcanvas:this.componentJSP[startcanvas.readnum()];
			var parent:Number = pnode.get_full_states_count();
			var ep:Number = linep[x*parent+y]; 
			return ep == NaN ? 0 : ep;
		}
		
//		public function getlinepByStateIndex(x:int, y:int):Number{
//			//
//		}
		
		/** 
		 * 读取是否赋过值
		 */  
		public function setsetted(setted:Boolean):void
		{
			this.issetted=setted;
		}
		/** 
		 * 设定是否赋过值
		 */  
		public function getsetted():Boolean
		{
			return this.issetted;
		}
		/** 
		 * 读取编号
		 */ 
		public function getNum():Number
		{
			return this.number;
		}
		/** 
		 * 设定编号
		 */ 
		public function setNum(num:Number):void
		{
			this.number=num;
		}
		/** 
		 * 设定起始变量
		 */ 
		public function setstart(start:xcomponent):void
		{
			this.startcanvas=start;
		}
		/** 
		 * 设定结束变量
		 */ 
		public function setend(end:xcomponent):void
		{
			this.endcanvas=end;
		}
		/** 
		 * 读取起始变量
		 */ 
		public function getstart():xcomponent
		{
			return this.startcanvas;
		}
		/** 
		 * 读取结束变量
		 */ 
		public function getend():xcomponent
		{
			return this.endcanvas;
		}
		/** 
		 * 设定种类
		 */ 
		public function settype_old(Num:Number):void
		{
			this.type=Num;
		}
		
		public function settype(Num:Number, componentJSP:Array):void{
			this.type = Num;
			this.componentJSP = componentJSP;
		}
		
		/** 
		 * 读取种类，1红实2红虚3绿实4绿虚5无头6双红线(bxs)7关联性(yao),8双红线(bxs)
		 */ 
		public function gettype():Number
		{
			return this.type;
		}
		/** 
		 * 设定在起始变量里linein的编号
		 */ 
		public function setnumasin(num:Number):void
		{
			this.NumAsIn=num;
		}
		/** 
		 * 读取在起始变量里linein的编号
		 */ 
		public function getnumasin():Number
		{
			return this.NumAsIn;
		}
		/** 
		 * 设定在结束变量里lineout的编号
		 */ 
		public function setnumasout(num:Number):void
		{
			this.NumAsOut=num;
		}
		/** 
		 * 读取在结束变量里lineout的编号
		 */ 
		public function getnumasout():Number
		{
			return this.NumAsOut;
		}
		
	}
}