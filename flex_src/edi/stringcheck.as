package edi
{
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	public class stringcheck
	{
		public var componentinJSP:Array=new Array();
		public var Gstr:String;
		public var exp:ArrayCollection=new ArrayCollection();
		public var tr:fourtree;
		private var expnum:int=0;
		private var o1:Object=new Object();
		private var typ:int=0;
		private var cn:int=0;
		private var sn:int=0;
		private var leng:int;
		public var cna:ArrayCollection=new ArrayCollection();
		private function readobj():Boolean
		{
			var b:Boolean=true;
			if(expnum==leng-1)
			{
				b=false;
			}
			else
			{
				expnum++;
				o1=Object(exp.getItemAt(expnum));
				typ=int(o1.typ);
				cn=int(o1.num);
				sn=int(o1.state);
			}
			return b;
		}
		private function getGF(p:int):Boolean
		{
			if(p>=2&&int(Object(exp.getItemAt(p-2)).typ)==6){return true;}
			return false
		}
		public function getchangedstr():String
		{
			var str:String=new String();
			var p:int=0;
			str="";
			for(p=0;p<exp.length;p++)
			{
				var o:Object=Object(exp.getItemAt(p));
				var ty:int=int(o.typ);
				if(ty==0)
				{
					str=str+"(";
				} else if(ty==1) {
					cn=int(o.num);
					sn=int(o.state);
					
					if(cn>=30000)
					{
						str=str+"BX"+(cn-30000)+","+sn;
					} else if(cn>=20000) {
						Alert.show("系统需要，对输入的表达式进行转换");
						var cc:xcomponent=componentinJSP[cn];
						str=str+"("+cc.getgrade(sn)+")";
					} else if(cn>=10000) {
						str=str+"X"+(cn-10000)+","+sn;
					} else if(cn>=0) { 
						str=str+"B"+cn+","+sn;
					} else if(cn<0) {
						str=str+"D"+(cn+10000)+","+sn;
					}
				} else if(ty==2) {
					str=str+")";
				} else if(ty==3) {
					str=str+"!";
				} else if(ty==4) {
					str=str+"+";
				} else if(ty==5) {
					str=str+"*";
				} else if(ty==6) {
					var n1,n2,n3,n4:int;
					n1=int(o.n1);
					n2=int(o.n2);
					n3=int(o.n3);
					n4=int(o.n4);
					str=str+"F"+n1+","+n2+";"+n3+","+n4;
				}
			}
			return str;
		}
		private function getletter(n:int,num:int):String
		{
			//得到第num个状态的第n个字符
			var str:String=Gstr.substring(n,n+1);
			return str;
		}
		private function addwordtoarr(n:int,exparr:ArrayCollection,ssn:int,een:int):void
		{
			//词法分析
			if(n==-1)
			{
				exparr.removeAll();
				var o:Object;
				o={typ:"-1"};
				exparr.addItem(o);
			}
			else
			{
				var o:Object;
				o={typ:String(n),num:String(ssn),state:String(een)};
				exparr.addItem(o);
			}
		}
		public function ana():fourtree
		{
			wordana(0);
			return tr;
		}
		private function wordana(num:int):void
		{
			//词法分析
			var ors:String=Gstr;
			var p:int=0;
			var exparr:ArrayCollection=new ArrayCollection();
			while(p<ors.length)
			{
				var typlet:String=getletter(p,num);
				if(typlet=="B"||typlet=="X"||typlet=="G"||typlet=="D"||typlet=="U")
				{
					var state:int=0;
					p++;
					var ssn:int=0;
					var een:int=0;
					while(state!=3)
					{
						if(state==0)
						{
							if(getletter(p,num)>="0"&&getletter(p,num)<="9"){ssn=10*ssn+int(getletter(p,num));p++;}
							else if(getletter(p,num)==","){state=1;p++;}
							else{p=ors.length;addwordtoarr(-1,exparr,0,0);state=3;}
						}
						else if(state==1)
						{
							if(getletter(p,num)>="0"&&getletter(p,num)<="9"){een=int(getletter(p,num));p++;state=2;}
							else{p=ors.length;state=3;addwordtoarr(-1,exparr,0,0);}
						}
						else if(state==2)
						{
							if(getletter(p,num)>="0"&&getletter(p,num)<="9"){een=10*een+int(getletter(p,num));p++;}
							else if(getletter(p,num)=="+"||getletter(p,num)=="*"||getletter(p,num)==")")
							{
								state=3;
								if(typlet=="B")
								{
									addwordtoarr(1,exparr,ssn,een);
								} else if(typlet=="X") {
									addwordtoarr(1,exparr,ssn+10000,een);
								} else if(typlet=="G") {
									addwordtoarr(1,exparr,ssn+20000,een);
								} else if(typlet=="D") {
									addwordtoarr(1,exparr,ssn-10000,een);
								} else if(typlet=="U") {
									addwordtoarr(1,exparr,ssn+30000,een);
								}
							}
							else if(p==ors.length)
							{
								p++;
								state=3;
								if(typlet=="B")
								{
									addwordtoarr(1,exparr,ssn,een);
								} else if(typlet=="X") {
									addwordtoarr(1,exparr,ssn+10000,een);
								} else if(typlet=="G") {
									addwordtoarr(1,exparr,ssn+20000,een);
								} else if(typlet=="D") {
									addwordtoarr(1,exparr,ssn-10000,een);
								} else if(typlet=="U") {
									addwordtoarr(1,exparr,ssn+30000,een);
								}
							}
							else{p=ors.length;state=3;addwordtoarr(-1,exparr,0,0);}
						}
					}
				}
				else if(getletter(p,num)=="F")
				{
					var state:int=0;
					p++;
					var n1:int=0;
					var n2:int=0;
					var n3:int=0;
					var n4:int=0;
					while(state!=7)
					{
						if(state==0)
						{
							if(getletter(p,num)>="0"&&getletter(p,num)<="9"){n1=10*n1+int(getletter(p,num));p++;}
							else if(getletter(p,num)==","){state=1;p++;}
							else{p=ors.length;addwordtoarr(-1,exparr,0,0);state=7;}
						}
						else if(state==1)
						{
							if(getletter(p,num)>="0"&&getletter(p,num)<="9"){n2=int(getletter(p,num));p++;state=2;}
							else{p=ors.length;state=7;addwordtoarr(-1,exparr,0,0);}
						}
						else if(state==2)
						{
							if(getletter(p,num)>="0"&&getletter(p,num)<="9"){n2=10*n2+int(getletter(p,num));p++;}
							else if(getletter(p,num)==";"){state=3;p++;}
							else{p=ors.length;addwordtoarr(-1,exparr,0,0);state=7;}
						}
						else if(state==3)
						{
							if(getletter(p,num)>="0"&&getletter(p,num)<="9"){n3=int(getletter(p,num));p++;state=4;}
							else{p=ors.length;state=7;addwordtoarr(-1,exparr,0,0);}
						}
						else if(state==4)
						{
							if(getletter(p,num)>="0"&&getletter(p,num)<="9"){n3=10*n3+int(getletter(p,num));p++;}
							else if(getletter(p,num)==","){state=5;p++;}
							else{p=ors.length;addwordtoarr(-1,exparr,0,0);state=7;}
						}
						else if(state==5)
						{
							if(getletter(p,num)>="0"&&getletter(p,num)<="9"){n4=int(getletter(p,num));p++;state=6;}
							else{p=ors.length;state=7;addwordtoarr(-1,exparr,0,0);}
						}
						else if(state==6)
						{
							if(getletter(p,num)>="0"&&getletter(p,num)<="9"){n4=10*n4+int(getletter(p,num));p++;}
							else if(getletter(p,num)=="*")
							{
								state=7;
								var o:Object;
								o={typ:6,n1:String(n1),n2:String(n2),n3:String(n3),n4:String(n4)};
								exparr.addItem(o);
							}
							else{p=ors.length;state=7;addwordtoarr(-1,exparr,0,0);}
						}
					}
					
				}
				else if(getletter(p,num)=="("){p++;addwordtoarr(0,exparr,0,0);}
				else if(getletter(p,num)==")"){p++;addwordtoarr(2,exparr,0,0);}
				else if(getletter(p,num)=="!"){p++;addwordtoarr(3,exparr,0,0);}
				else if(getletter(p,num)=="+"){p++;addwordtoarr(4,exparr,0,0);}
				else if(getletter(p,num)=="*"){p++;addwordtoarr(5,exparr,0,0);}
				else{p=ors.length;addwordtoarr(-1,exparr,0,0);}
			}
			exp=exparr;
			expnum=-1;
			leng=exp.length;
			readobj();
			tr=expression();
		}
		private function expression():fourtree
		{
			var tm:fourtree=new fourtree();
			if(typ==1||typ==0||typ==3||typ==6)
			{
				var tm1:fourtree=new fourtree();
				tm1=factor();
				if(typ==4&&expnum<leng-1)
				{
					var tm2:fourtree=new fourtree();
					tm.lefttree=tm1;
					tm.cal=1;
					var bb:Boolean=readobj();
					if(bb==true)
					{
						tm2=factor();
						tm.rightree=tm2;
					}
					else{tm.isright=false;Alert.show("you4");}
				}
				else{return tm1;}
			}
			else{tm.isright=false;Alert.show("you3");}
			return tm;
		}
		private function factor():fourtree
		{
			var tm:fourtree=new fourtree();
			if(typ==1||typ==3||typ==0||typ==6)
			{
				var tm1:fourtree=new fourtree();
				tm1=item();
				if(typ==5&&expnum<leng-1)
				{
					var tm2:fourtree=new fourtree();
					tm.lefttree=tm1;
					tm.cal=2;
					var bb:Boolean=readobj();
					if(bb==true)
					{
						tm2=item();
						tm.rightree=tm2;
					}
					else{tm.isright=false;Alert.show("you5");return tm;}
				}
				else{return tm1;}
			}
			else{tm.isright=false;Alert.show("you6");}
			return tm;
		}
		private function item():fourtree
		{
			var tm:fourtree=new fourtree();
			if(typ==1)
			{
				tm.basic=true;
				tm.cn=cn;
				tm.sn=sn;
				if(cna.contains(cn)==false){cna.addItem(cn);}
				readobj();
			}
			else if(typ==6)
			{
				var n3,n4:int;
				n3=int(o1.n3);
				n4=int(o1.n4);
				var b:Boolean=readobj();
				if(b==true)
				{
					if(typ==5)
					{
						var bb:Boolean=readobj();
						if(typ==1&&(cn==n3||cn==n3+10000||cn==n3+20000)&&sn==n4)
						{
							tm.basic=true;
							tm.cn=cn;
							tm.sn=sn;
							if(cna.contains(cn)==false){cna.addItem(cn);}
							readobj();
						}
						else if(typ==0)
						{
							var tm2:fourtree=factor();
							tm=tm2;
						}
						else{tm.isright=false;Alert.show("");}
					}
					else{tm.isright=false;Alert.show("2");}
				}
				else{tm.isright=false;Alert.show("3");}
			}
			else if(typ==3)
			{
				var b:Boolean=readobj();
				if(b==true)
				{
					var tm2:fourtree=item();
					tm.cal=0;
					tm.lefttree=tm2;
				}
				else{tm.isright=false;}
			}
			else if(typ==0)
			{
				var b:Boolean=readobj();
				if(b==true)
				{
					var tm2:fourtree=expression();
					if(typ==2){readobj();return tm2;}
					else {tm.isright=false;Alert.show("you");}
				}
				else{tm.isright=false;Alert.show("you2");}
			}
			return tm;
		}
		public function atr():String
		{
			return alerttr(tr);
		}
		private function alerttr(tr:fourtree):String
		{
			var str:String=new String();
			if(tr.basic===true)
			{
				str=""+tr.cn+","+tr.sn;
			}
			else
			{
				if(tr.cal==0){str="!("+alerttr(tr.lefttree)+")";}
				if(tr.cal==1){str="("+alerttr(tr.lefttree)+"+"+alerttr(tr.rightree)+")";}
				if(tr.cal==2){str="("+alerttr(tr.lefttree)+"*"+alerttr(tr.rightree)+")";}
			}
			return str;
		}
	}
}