package edi
{
	import flash.display.Sprite;
	import flash.geom.Point;
	
	import mx.controls.Alert;

	public class drawline
	{
		private var widthh:Number=3;
		
		private function cross_square(from:Point, to:Point):Point{
			//return attech point position
			if(from.x == to.x){
				return (to.y > from.y) ? new Point(from.x, from.y+20) : new Point(from.x, from.y-20);
			}
			else{
				var tan:Number = (to.y - from.y) / (to.x - from.x);
				if(Math.abs(tan) <= 1){
					return (to.x > from.x) ? new Point(from.x + 20, from.y + 20*tan) :
											  new Point(from.x - 20, from.y - 20*tan);
				}
				else{
					return (to.y > from.y) ? new Point(from.x + 20/tan , from.y + 20) :
											  new Point(from.x - 20/tan , from.y - 20);						
				}
			}
		}
				
		private function cross_circle(from:Point, to:Point):Point{
			if(from.x == to.x){
				return (to.y > from.y) ? new Point(from.x, from.y + 20) : new Point(from.x, from.y - 20);
			}
			else{
				var dis:Number = Math.sqrt((to.y - from.y)*((to.y - from.y)) + (to.x - from.x)*((to.x - from.x)));
				return new Point(from.x + 20/dis*(to.x-from.x), from.y + 20/dis*(to.y-from.y));
			}
		}
				
		private function cross_pantagon(from:Point, to:Point):Point{
			var theta:Number = Math.atan2(to.x - from.x, to.y - from.y) / Math.PI;
			var anchors:Array = [0.9, 0.5, 0.1, -0.3, -0.7];
			var min:Number = 1000, min_i:int = 0;
			for(var i:int = 0 ; i < 5 ; i++)
				if(Math.abs(theta-anchors[i]) < min){
					min = Math.abs(theta-anchors[i]);
					min_i = i;
				}
			theta = anchors[min_i] * Math.PI;
			if (min_i == 0){
				theta += Math.PI / 8;
			}
			else if (min_i == 4){
				theta += Math.PI / 15;
			}
			else if (min_i == 3){
				theta += Math.PI / 8;
			}
			else if(theta >= 0)
				theta += Math.PI / 18;
			//Alert.show(''+min_i);
			
			var radius:Number = 19.5 / Math.sin(Math.PI*0.4);
			return new Point(from.x + radius*Math.sin(theta), from.y + radius*Math.cos(theta));
		}
		
		private function cross_gate(from:Point, to:Point):Point{
			var theta:Number = Math.atan2(to.x - from.x, to.y - from.y) / Math.PI;
			if(theta >= 0){
				theta = theta > 0.75 ? 1 : (theta > 0.25 ? 0.5 : 0);
			}
			else{
				theta = theta < -0.75 ? -1 : (theta < -0.25 ? -0.5 : 0);
			}
			
			theta *= Math.PI;
			return new Point(from.x + 20*Math.sin(theta), from.y + 20*Math.cos(theta));
		}
		
		private function rotate(vec:Point, theta:Number):Point{
			var x,y:Number;
			x = vec.x * Math.cos(theta) - vec.y * Math.sin(theta);
			y = vec.x * Math.sin(theta) + vec.y * Math.cos(theta);
			return new Point(x,y);
		}
		
		
		public function draw(scanvas:xcomponent,ecanvas:xcomponent,type:Number,linetmp:line):void
		{
			//线段起始和终止位置坐标
			var sx:Number=new Number;
			var sy:Number=new Number;
			var ex:Number=new Number;
			var ey:Number=new Number;
			var sprite:Sprite=new Sprite();
			sx = scanvas.x;
			sy = scanvas.y;
			ex = ecanvas.x;
			ey = ecanvas.y;
			
			var k:Number=new Number();//斜率
			var leftk:Number=new Number();//箭头一边
			var rightk:Number=new Number();//箭头另一边
			var xushi:Boolean=new Boolean();//虚实线,TRUE为实线,20161028
			var blackornot:Boolean=new Boolean();//黑的不画箭头
			//如果是1、3类型实线。如果类型是2、4类型虚线，类型5黑线
			/**1红实;2红虚;3绿实;4绿虚;5无头;6双线(bxs);7关联性(yao),**/
			if(type == 1 || type == 3 || type == 6 || type == 7)
			{
				xushi = true;
			}
			else if( type == 2 || type == 4)
			{
				xushi = false;
			}
			if (type == 5)
			{
				blackornot = false;
			}
			else
			{
				blackornot = true;
			}
			
			var from:Point,to:Point;
//			线的画出位置都在图像的边沿。所以是中心点+20pix，整个图形是40*40
			var cstart:Point = new Point(sx+20, sy+20); 
			var cend:Point = new Point(ex+20, ey+20);
			
			switch(scanvas.getatt()){
				case 1:
					from = cross_square(cstart, cend);
					break;
				case 6:
				case 2:
				case 8:
					from = cross_circle(cstart, cend);
					break;
				case 3:
				case 9:
					from = cross_gate(cstart, cend);
					break;
				case 4:
					//cstart.x -= 2;
					//cstart.y -= 2;
					from = cross_pantagon(cstart, cend);
					break;
			}
			
			switch(ecanvas.getatt()){
				case 1:
					to = cross_square(cend, cstart);
					break;
				case 6:
				case 2:
				case 8:
					to = cross_circle(cend, cstart);
					break;
				case 3:
				case 9:
					to = cross_gate(cend, cstart);
					break;
				case 4:
					//cend.x -= 2;
					//cend.y -= 2;
					to = cross_pantagon(cend, cstart);
					break;
			}
			
			sprite.graphics.clear();
			// 画实线和虚线的方法
			if (type == 1) 
			{
				sprite.graphics.lineStyle(linetmp.linewidth + 1, 0xff0000);
			}
			else if (type == 2)
			{
				sprite.graphics.lineStyle(linetmp.linewidth + 2, 0xff0000);
			}
			else if (type == 3 || type == 4)
			{
				sprite.graphics.lineStyle(linetmp.linewidth + 1, 0x33ff00);
			}
			else if (type == 5)
			{
				sprite.graphics.lineStyle(linetmp.linewidth, 0x000000);
			}
			else if (type == 6)
			{
				sprite.graphics.lineStyle(linetmp.linewidth + 4, 0xff0000);
			}
			else if (type == 7)
			{
				sprite.graphics.lineStyle(linetmp.linewidth + 1, 0xEE9A00);//1.FFB90F，2.EE9A00，3.EE7621
			}
			if(type == 1 || type == 3 || type == 5 || type == 6 || type == 7)
			{
				sprite.graphics.moveTo(from.x, from.y);
				sprite.graphics.lineTo(to.x, to.y);
			}
			else if(type==2||type==4)
			{
				drawdottedline(from,to,sprite);
			}
			
			if(blackornot)
			{
				var direction:Point = new Point(to.x-from.x, to.y-from.y);
				var destp:Point;
				if (type == 7)//画关联变量的黄色线箭头
				{
					sprite.graphics.lineStyle(linetmp.linewidth+2, 0xEE9A00);//1.FFB90F，2.EE9A00，3.EE7621
				}
				else//画其他变量的红色线箭头
				{
					sprite.graphics.lineStyle(linetmp.linewidth+2, 0xff0000);
				}
				
				direction.normalize(1);
				destp = rotate(direction, 160.0/180*Math.PI);
				destp.x *= 20;
				destp.y *= 20;
				destp.x += to.x;
				destp.y += to.y;
				var triBegx:Number = destp.x;
				var triBegy:Number = destp.y;
				//画箭头
				sprite.graphics.moveTo(destp.x, destp.y);
				sprite.graphics.lineTo(to.x, to.y);
				destp = rotate(direction, -160.0 / 180*Math.PI);
				destp.x *= 20; 
				destp.y *= 20;
				destp.x += to.x;
				destp.y += to.y;
				//
				sprite.graphics.moveTo(destp.x, destp.y);
				sprite.graphics.lineTo(to.x, to.y);
				//画箭头
				//sprite.graphics.lineTo(destp.x, destp.y);
				//如果是关联变量，画三角
				if (type == 7)//画关联变量的黄色线箭头
				{
					sprite.graphics.moveTo(destp.x, destp.y);
					sprite.graphics.lineTo(triBegx, triBegy);
					
					destp = rotate(direction, 160.0/180*Math.PI);
					destp.x *= 16;
					destp.y *= 16;
					destp.x += to.x;
					destp.y += to.y;
					triBegx = destp.x;
					triBegy = destp.y;
					
					destp = rotate(direction, -160.0 / 180*Math.PI);
					destp.x *= 16; 
					destp.y *= 16;
					destp.x += to.x;
					destp.y += to.y;
					
					sprite.graphics.moveTo(destp.x , destp.y);
					sprite.graphics.lineTo(triBegx, triBegy);
				}
			}
			else
			{
				sprite.graphics.beginFill(0x000000);
				sprite.graphics.drawCircle(to.x,to.y,widthh);
			}
						
			linetmp.addChildAt(sprite,0);
			linetmp.resp=sprite;
			if(type==6){
				drawWhite(scanvas,ecanvas,type,linetmp);
			}
			
			drawyellow(scanvas,ecanvas,type,linetmp);	
		}
		private function drawdottedline(xx:Point,yy:Point,sprtmpp:Sprite):void
		{
			
			var k:Number=new Number();
			var n:int=new int();
			var r:Number=new Number();
			var count:Number=new Number();
			var startx:Number=new Number();
			var starty:Number=new Number();
			var endx:Number=new Number();
			var endy:Number=new Number();
			r=Math.sqrt((yy.x-xx.x)*(yy.x-xx.x)+(yy.y-xx.y)*(yy.y-xx.y));
			n=r/40;
			if(yy.x!=xx.x)k=(yy.y-xx.y)/(yy.x-xx.x);
			startx=xx.x;starty=xx.y;
			for(count=0;count<n;count++)
			{
				if(yy.x>xx.x)endx=startx+20/Math.sqrt(1+k*k);
				else if(yy.x<xx.x)endx=startx-20/Math.sqrt(1+k*k);
				else if(yy.x==xx.x)endx=startx;
				if(yy.y>xx.y)endy=starty+20*Math.abs(k)/Math.sqrt(1+k*k);
				else if(yy.y<=xx.y)endy=starty-20*Math.abs(k)/Math.sqrt(1+k*k);
				sprtmpp.graphics.moveTo(startx, starty);
				sprtmpp.graphics.lineTo(endx, endy);
				if(yy.x>xx.x)startx=endx+20/Math.sqrt(1+k*k);
				else if(yy.x<xx.x)startx=endx-20/Math.sqrt(1+k*k);
				else if(yy.x==xx.x)startx=endx;
				if(yy.y>xx.y)starty=endy+20*Math.abs(k)/Math.sqrt(1+k*k);
				else if(yy.y<=xx.y)starty=endy-20*Math.abs(k)/Math.sqrt(1+k*k);//画虚线
			}
			sprtmpp.graphics.moveTo(startx, starty);
			sprtmpp.graphics.lineTo(yy.x, yy.y);
		}
		
		private function drawyellow(scanvas:xcomponent,ecanvas:xcomponent,type:Number,lt:line):void
		{
			var sx:Number=new Number;
			var sy:Number=new Number;
			var ex:Number=new Number;
			var ey:Number=new Number;
			var sprite:Sprite=new Sprite();
			sx = scanvas.x;
			sy = scanvas.y;
			ex = ecanvas.x;
			ey = ecanvas.y;
			
			var k:Number=new Number();//斜率
			var leftk:Number=new Number();//箭头一边
			var rightk:Number=new Number();//箭头另一边
			var xushi:Boolean=new Boolean();//虚实线
			var blackornot:Boolean=new Boolean();//黑的不画箭头
			/*********bxs，type==6，**********/
			if(type==1||type==3||type==6)xushi=true;
			else if(type==2||type==4)xushi=false;
			if(type==5){blackornot=false;}
			else blackornot=true;
			
			sprite.graphics.clear();
			sprite.graphics.lineStyle(lt.linewidth+1,0x0066ff);
			
			var from:Point,to:Point;
			var cstart:Point = new Point(sx+20, sy+20), cend:Point = new Point(ex+20, ey+20);
			
			switch(scanvas.getatt()){
				case 1:
					from = cross_square(cstart, cend);
					break;
				case 6:
				case 2:
				case 8:
					from = cross_circle(cstart, cend);
					break;
				case 3:
				case 9:
					from = cross_gate(cstart, cend);
					break;
				case 4:
					//cstart.x -= 2;
					//cstart.y -= 2;
					from = cross_pantagon(cstart, cend);
					break;
			}
			
			switch(ecanvas.getatt()){
				case 1:
					to = cross_square(cend, cstart);
					break;
				case 6:
				case 2:
				case 8:
					to = cross_circle(cend, cstart);
					break;
				case 3:
				case 9:
					to = cross_gate(cend, cstart);
					break;
				case 4:
				
					//cend.x -= 2;
					//cend.y -= 2;
					to = cross_pantagon(cend, cstart);
					break;
			}
			/*********bxs，type==6，**********/
			if(type==1||type==3||type==5||type==6 || type == 7)
			{
				sprite.graphics.moveTo(from.x, from.y);
				sprite.graphics.lineTo(to.x, to.y);
			}
			else if(type==2||type==4)
			{
				drawdottedline(from,to,sprite);
			}
			
			if(blackornot)
			{
				var direction:Point = new Point(to.x-from.x, to.y-from.y), destp:Point;
				direction.normalize(1);
				destp = rotate(direction, 160.0/180*Math.PI);
				destp.x *= 20; destp.y *= 20;
				destp.x += to.x; destp.y += to.y;
				
				sprite.graphics.moveTo(destp.x, destp.y);
				sprite.graphics.lineTo(to.x, to.y);
				destp = rotate(direction, -160.0/180*Math.PI);
				destp.x *= 20; destp.y *= 20;
				destp.x += to.x; destp.y += to.y;
				sprite.graphics.lineTo(destp.x, destp.y);
			}
			else
			{
				sprite.graphics.beginFill(0x000000);
				sprite.graphics.drawCircle(to.x,to.y,widthh);
			}
			
	
			lt.addChild(sprite);
			lt.yesp=sprite;
			sprite.visible=false;
		}
	//bxs 白线
		private function drawWhite(scanvas:xcomponent,ecanvas:xcomponent,type:Number,lt:line):void
		{
			var sx:Number=new Number;
			var sy:Number=new Number;
			var ex:Number=new Number;
			var ey:Number=new Number;
			var sprite:Sprite=new Sprite();
			sx = scanvas.x;
			sy = scanvas.y;
			ex = ecanvas.x;
			ey = ecanvas.y;
			
			var k:Number=new Number();//斜率
			var leftk:Number=new Number();//箭头一边
			var rightk:Number=new Number();//箭头另一边
			var xushi:Boolean=new Boolean();//虚实线
			var blackornot:Boolean=new Boolean();//黑的不画箭头
			/*********bxs，type==6，**********/
			if(type==1||type==3||type==6)xushi=true;
			else if(type==2||type==4)xushi=false;
			if(type==5){blackornot=false;}
			else blackornot=true;
			
			sprite.graphics.clear();
			sprite.graphics.lineStyle(lt.linewidth-1,0xffffff);
			
			var from:Point,to:Point;
			var cstart:Point = new Point(sx+20, sy+20), cend:Point = new Point(ex+20, ey+20);
			
			switch(scanvas.getatt()){
				case 1:
					from = cross_square(cstart, cend);
					break;
				case 6:
				case 2:
				case 8:
					from = cross_circle(cstart, cend);
					break;
				case 3:
				case 9:
					from = cross_gate(cstart, cend);
					break;
				case 4:
					//cstart.x -= 2;
					//cstart.y -= 2;
					from = cross_pantagon(cstart, cend);
					break;
			}
			
			switch(ecanvas.getatt()){
				case 1:
					to = cross_square(cend, cstart);
					break;
				case 6:
				case 2:
				case 8:
					to = cross_circle(cend, cstart);
					break;
				case 3:
				case 9:
					to = cross_gate(cend, cstart);
					break;
				case 4:
					
					//cend.x -= 2;
					//cend.y -= 2;
					to = cross_pantagon(cend, cstart);
					break;
			}
			/*********bxs，type==6，**********/
			if(type==1||type==3||type==5||type==6)
			{
				//sprite.graphics.lineStyle(lt.linewidth-1,0xff0000);
				sprite.graphics.moveTo(from.x, from.y);
				sprite.graphics.lineTo(to.x, to.y);
			}
			else if(type==2||type==4)
			{
				drawdottedline(from,to,sprite);
			}
			
			if(blackornot)
			{
				var direction:Point = new Point(to.x-from.x, to.y-from.y), destp:Point;
				direction.normalize(1);
				destp = rotate(direction, 160.0/180*Math.PI);
				destp.x *= 20; destp.y *= 20;
				destp.x += to.x; destp.y += to.y;
				
				sprite.graphics.lineStyle(lt.linewidth-1,0xff0000);
				sprite.graphics.moveTo(destp.x, destp.y);
				sprite.graphics.lineTo(to.x, to.y);
				destp = rotate(direction, -160.0/180*Math.PI);
				destp.x *= 20; destp.y *= 20;
				destp.x += to.x; destp.y += to.y;
				sprite.graphics.lineTo(destp.x, destp.y);
				
				sprite.graphics.moveTo(to.x, to.y);
				sprite.graphics.lineStyle(lt.linewidth+2,0xff0000);
				destp = rotate(direction, -180.0/180*Math.PI);
				destp.x *= 16; destp.y *= 16;
				destp.x += to.x; destp.y += to.y;
				sprite.graphics.lineTo(destp.x, destp.y);
				
				
			}
			else
			{
				sprite.graphics.beginFill(0x000000);
				sprite.graphics.drawCircle(to.x,to.y,widthh);
			}
			
			
			lt.addChildAt(sprite,1);
		//	lt.yesp=sprite;
			sprite.visible=true;
		}
		
	}
}