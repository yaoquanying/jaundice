﻿Ext.onReady(function(){
        Ext.state.Manager.setProvider(new Ext.state.CookieProvider());
        
        var viewport = new Ext.Viewport({
            layout: 'border',
            items: [
            // create instance immediately
            // in this instance the TabPanel is not wrapped by another panel
            // since no title is needed, this Panel is added directly
            // as a Container
            new Ext.TabPanel({
                region: 'center', // a center region is ALWAYS required for border layout
                deferredRender: false,
                activeTab: 0,     // first tab initially active
                items: [{
                    contentEl: 'center1',
                    title: '工程图编辑器',
                    closable: false
                },{
                    contentEl: 'center2',
                    title: '知识库编辑器',
                    closable: false,
                    autoScroll: true
                },{
                    contentEl: 'center4',
                    title: '实时监控',
                    closable: false,
                    autoScroll: true
                },{
                    contentEl: 'center5',
                    title: '设置故障',
                    closable: false,
                    autoScroll: true
                }]
            })]
        });
    });
