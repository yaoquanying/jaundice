import java.io.IOException;
import java.io.PrintWriter;
import java.io.File;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;

import javax.servlet.*;
import javax.servlet.http.*;

import java.util.ListIterator;
import java.util.List;

// SAX and JAXP classes.
import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;

import org.xml.sax.*;
import org.xml.sax.helpers.*;

import javax.xml.transform.*;
import javax.xml.transform.stream.*;
import javax.xml.transform.sax.*;

// File upload libraries.
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.FileItem;

import com.flex.demo.GetPathStr;
public class upZip extends HttpServlet 
{
	private static String pathout=GetPathStr.getPath();
	private String uploadPath = pathout;  
	  
	  
	private int maxPostSize = 100 * 1024 * 1024;  
	  
	public upZip() 
    {  
        super();  
    }  
  
    public void destroy() 
    {  
        super.destroy();  
    }  
  
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)  
            throws ServletException, IOException 
    {  
        System.out.println("Access !");  
        response.setContentType("text/html;charset=UTF-8");  
        PrintWriter out = response.getWriter();  
  
  
        DiskFileItemFactory factory = new DiskFileItemFactory();  
        factory.setSizeThreshold(4096);  
        ServletFileUpload upload = new ServletFileUpload(factory);  
        upload.setSizeMax(maxPostSize);  
        try 
        {  
            List fileItems = upload.parseRequest(request);  
            Iterator iter = fileItems.iterator();  
            while (iter.hasNext()) 
            {  
                FileItem item = (FileItem) iter.next();  
                if (!item.isFormField()) 
                {  
                    String name = item.getName();  
                    System.out.println(name);  
                    try 
                    {  
                        item.write(new File(uploadPath + name));  
                    } 
                    catch (Exception e) 
                    {  
                        e.printStackTrace();  
                    }  
                }  
            }  
        } 
        catch (FileUploadException e) 
        {  
            e.printStackTrace();  
            System.out.println(e.getMessage() + "����");  
        }  
    }  
  


    protected void doGet(HttpServletRequest request, HttpServletResponse response)  
            throws ServletException, IOException 
    {  
        processRequest(request, response);  
    }  
  

    protected void doPost(HttpServletRequest request, HttpServletResponse response)  
            throws ServletException, IOException 
    {  
        processRequest(request, response);  
    }  
  

    public String getServletInfo() 
    {  
        return "Short description";  
    }  

}
