package gct.userVal;
import java.io.*;
import java.sql.*;
import java.util.*;
import org.jdom.*;

import db.config.DbConfig;

/**
 * 数据库连接
 * @author SHU
 *
 */
public class UtilsSQL {
	
	/**
	 * 用于数据库连接
	 */
	public static Connection con = connection();

	/**
	 * 数据库连接
	 * @return
	 */
	public static Connection connection() {
		//String dbName = "jaundice";
		try {//        192.168.5.89
			Class.forName("com.mysql.jdbc.Driver");
			String url = "jdbc:mysql://"+DbConfig.host+"/" + DbConfig.dbname
					+ "?useUnicode=true&characterEncoding=UTF-8";
			Connection conn = DriverManager.getConnection(url, DbConfig.usname, DbConfig.dbpwd);
			return conn;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public static int checkCommonUser(String id, String password) {
		if (con == null) {
			return 1;
		}
		try {
			con.setAutoCommit(true);
			String selCode = "SELECT US_PASSWORD FROM US WHERE US_ID='" + id
					+ "'";
			Statement statement = con.createStatement();
			ResultSet result = statement.executeQuery(selCode);// 结果集
			if (result.next()) {
				if (result.getString(1).equals(password)) {
					return 0;
				} else {
					return 2;
				}
			} else {
				return 1;
			}
		} catch (SQLException e) {
			return 1;
		}
	}

}
