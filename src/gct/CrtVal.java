package gct;
import java.io.*;
import java.sql.*;
import java.util.*;

import org.apache.poi.util.StringUtil;
import org.jdom.*;
import org.jdom.output.XMLOutputter;

/**
 * 创建变量/测点信息的类
 * 
 * @author SHU
 * 
 */
public class CrtVal {

	/**
	 * 用于连接数据库
	 */
	private static Connection con = UtilsSQL.con;

	/**
	 * 创建目录
	 */
	public static void crtDict() 
	{
		File dBMain = new File("C:/DUCG");
		if (!dBMain.exists())
			dBMain.mkdir();
		File dB = new File("C:/DUCG/B");
		File dX = new File("C:/DUCG/X");

		if (!dB.exists())
			dB.mkdir();
		if (!dX.exists())
			dX.mkdir();
	}

	/**
	 * 获得变量编号列表
	 * 
	 * @param dir
	 * @return
	 */
	public static int[] serachVals(String dir)
	{
		crtDict();
		File root = new File(dir);
		File[] filesOrDirs = root.listFiles();
		int[] rst = new int[filesOrDirs.length];

		if (filesOrDirs.length == 0)
			return null;

		for (int i = 0; i < filesOrDirs.length; i++) 
		{
			if (filesOrDirs[i].isDirectory())
			{
			} 
			else 
			{
				rst[i] = Integer.parseInt(filesOrDirs[i].getName());
			}
		}

		Arrays.sort(rst);
		return rst;
	}

	/**
	 * 获得所有变量的基本信息
	 * 
	 * @return 所有变量的基本信息
	 */
	public static ArrayList<ValBscInfo> getAllValBscInfoList() {

		/**
		 * 用于存储获得的所有变量的基本信息
		 */
		ArrayList<ValBscInfo> info = new ArrayList<ValBscInfo>();

		if (con == null)
		{
			return null;
		}
		try {
			con.setAutoCommit(true);
			String sqlCode = "select com_id,com_type,com_name,com_des,com_keyword from com where com_type='B' or com_type='X'";
			Statement statement = con.createStatement();
			ResultSet result = statement.executeQuery(sqlCode);
			while (result.next()) 
			{
				ValBscInfo temp = new ValBscInfo();
				temp.setId(result.getString(1));
				temp.setType(result.getString(2));
				temp.setDscp(result.getString(3));
				temp.setvKeys(result.getString(4));
				sqlCode = "select count(*) from state where com_id='"
						+ temp.getId() + "' and com_type='" + temp.type + "'";
				Statement sta = con.createStatement();
				ResultSet tempR = sta.executeQuery(sqlCode);
				tempR.next();
				temp.setStNum(tempR.getInt(1));
				info.add(temp);
			}
			return info;
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * 变量查重处理方法
	 * 
	 * @param typeChk
	 *            查重的变量的类型
	 * @param idChk
	 *            查重的变量的ID
	 * @param vName
	 *            查重的变量的名称
	 * @param vKeys
	 *            查重的变量的关键字
	 * @return 查重的结果（XML）
	 */
	@SuppressWarnings("unchecked")
	public String checkDup(String typeChk, String idChk, String vName,
			String vKeys) 
	{
		if (con == null) 
		{
			return null;
		}

		if (vName == null) 
		{
			return "";
		}

		ArrayList<ComponentT> com = new ArrayList<ComponentT>();
		vName = vName.trim();
		vKeys = vKeys.trim();
		
		Element root = new Element("valDup");
		try {
			con.setAutoCommit(true);
			String sqlCode = "select com_id,com_type,com_des,com_keyword from com where com_type='B' or "
					+ "com_type='X' or com_type='U' and com_type!='"
					+ typeChk
					+ "' and com_id!='" + idChk + "'";
			Statement statement = con.createStatement();
			ResultSet result = statement.executeQuery(sqlCode);
			while (result.next()) 
			{
				ComponentT temp = new ComponentT();
				temp.id = result.getString(1);
				String strType = result.getString(2);
				if (strType.compareTo("U") == 0)
				{
					temp.type = "BX";
				}
				else
				{
					temp.type = strType;
				}
				
				temp.name = result.getString(3);
				temp.keyWord = result.getString(4);
				getR(temp, vName, vKeys);
				if (temp.r > 0) 
				{
					com.add(temp);
				}
			}
			Collections.sort(com, new SortByR());

			ArrayList<ComponentT> comTemp = new ArrayList<ComponentT>();
			for (int i = 0; i < com.size(); i++) 
			{
				if (i > 30) 
				{
					break;
				}
				comTemp.add(com.get(i));
			}
			com = comTemp;
			// System.out.println(com.size());

			for (ComponentT co : com) 
			{
				root.addContent(co.toXml());
			}
			Document slctDoc = new Document(root);
			XMLOutputter outputter = new XMLOutputter();
			// System.out.println(outputter.outputString(slctDoc));
			return outputter.outputString(slctDoc);
		} 
		catch (SQLException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * 辅助变量查重的方法
	 * 
	 * @param temp
	 *            变量的基本属性
	 * @param name
	 *            变量名称
	 * @param keyword
	 *            变量的关键字
	 */
	private void getR(ComponentT temp, String name, String keyword)
	{
		if (temp.name.equals(name))
		{
			temp.r += 5;
		}
		else if (temp.name.contains(name) && !name.equals("")) 
		{
			temp.r++;
		}
		if (keyword.equals(""))
		{
			return;
		}
		String[] k1 = keyword.split(";|；");
		String[] k2 = temp.keyWord.split(";|；");
		for (int i = 0; i < k2.length; i++)
		{
			for (int j = 0; j < k1.length; j++) 
			{
				if (k2[i].trim().equalsIgnoreCase(k1[j].trim()))
				{
					temp.r += 5;
				} 
				else if (k2[i].trim().toLowerCase().contains(k1[j].trim().toLowerCase()))
				{
					temp.r++;
				}
			}
		}
	}

	
	/**
	 * 计算查重的权重
	 * @param vType 变量的类型
	 * @param vName 变量的名称
	 * @param vKeyWords 变量的关键字
	 * @param strCedian 变量的测点
	 * @return 权重的计算结果
	 */
	public static int checkVal(String vType, String vName, String vKeyWords,
			String strCedian) 
	{
		// ////查询变量名称是否相同
		if (con == null) 
		{
			return -1;
		}
		try
		{
			String strSql;
			Statement sta = null;
			ResultSet rSet = null;
            /******bxs,修改，添加T类型变量**********/
			if ((vType.equals("S")||vType.equals("X")) && !strCedian.trim().equals("")) 
			{
				con.setAutoCommit(true);
				strSql = "select * from com where cedian = '" + strCedian + "'";
				sta = con.createStatement();

				rSet = sta.executeQuery(strSql);
				rSet.last();
				if (rSet.getRow() > 0)
					return -4;
			}

			con.setAutoCommit(true);
			strSql = "select * from com where com_name = '" + vName
					+ "' and com_keyword = '" + vKeyWords + "'";
			sta = con.createStatement();
			rSet = sta.executeQuery(strSql);
			rSet.last();
			if (rSet.getRow() > 0) 
			{
				return 2;
			}

			con.setAutoCommit(true);
			strSql = "select * from com where com_name = '" + vName + "'";
			sta = con.createStatement();

			rSet = sta.executeQuery(strSql);
			rSet.last();
			if (rSet.getRow() > 0)
			{
				return 2;
			}

			con.setAutoCommit(true);
			strSql = "select * from com where com_keyword = '" + vKeyWords + "'";
			sta = con.createStatement();

			rSet = sta.executeQuery(strSql);
			rSet.last();
			if (rSet.getRow() > 0)
			{
				return -3;
			}

			con.setAutoCommit(true);
			return 2;
		} catch (SQLException e)
		{
			e.printStackTrace();
			return -1;
		}
	}

	
	/**
	 * 计算查重的权重
	 * @param entityId 实体的ID
	 * @param strCedian 变量的测点
	 * @return 权重的计算结果
	 */
	public static int checkPoint(String entityId, String strCedian) 
	{
		// ////查询变量名称是否相同
		if (con == null) 
		{
			return -1;
		}
		try
		{
			String strSql;
			Statement sta = null;
			ResultSet rSet = null;

			//在同一个实体里，测点名一致即为重复
			con.setAutoCommit(true);
			strSql = "select * from comentity where cedian = '" + strCedian + "' and"
				   + "entity = '" + entityId +"'";
			sta = con.createStatement();

			rSet = sta.executeQuery(strSql);
			rSet.last();
			if (rSet.getRow() > 0)
			{
				return -2; //返回-2 代表该实体中已存在名称为strCedian的测点。
			}
		
			con.setAutoCommit(true);
			return 2;
		} catch (SQLException e)
		{
			// TODO Auto-generated catch block
			return -1;
		}
	}
	
	/**
	 * 根据传入的变量的信息，在数据库中创建变量
	 * @param slctX0Per 变量在工程图X坐标缩放比
	 * @param slctY0Per 变量在工程图Y坐标缩放比
	 * @param slctWidthPer 变量在工程图上的宽度
	 * @param slctHeightPer 变量在工程图上的长度
	 * @param color1 变量表示的框的颜色1
	 * @param color2 变量表示的框的颜色2
	 * @param vType 变量的类型
	 * @param stNumber 变量的状态的数量
	 * @param vName 变量的名称
	 * @param vDscp 变量描述
	 * @param vKeyWords 变量的关键字
	 * @param stGridPrd 变量的状态信息
	 * @param imgId 变量所在的工程图ID
	 * @param ceDian 变量的测点信息
	 * @param isSwitch 变量是否为开关量
	 * @param vOne 属性一
	 * @param vTwo 属性二
	 * @param vThree 属性三
	 * @return
	 */
	public static int crtVal(Number slctX0Per, Number slctY0Per,
			Number slctWidthPer, Number slctHeightPer, 
			String color1, String color2, 
			String vType, String stNumber, String vName,
			String vDscp, String vKeyWords, ArrayList stGridPrd, String imgId,
			String ceDian, boolean isSwitch, Double fillOpacity, 
			Number bpX0Per, Number bpY0Per, Number pointX0Per, Number pointY0Per,
			Number vOne, String vTwo, String vThree,int special,float dVal,int iscount,int cost) 
	{

		String color = color1 + ";" + color2 + ";" + fillOpacity;
		int id = getComponentId( vType );
//		int tmp_id;
		
//		if (vType.startsWith("U")) {
//			tmp_id = getComponentId("X");
//			if (id < tmp_id)
//				id = tmp_id;
//		} else if (vType.startsWith("X")) {
//			tmp_id = getComponentId("U");
//			if (id < tmp_id)
//				id = tmp_id;
//		}
		
		if (con == null) 
		{
			return -1;
		}
		
		// ////查询关键字和变量名称是否相同
		// /////////////////////////////////gengsc/////////////////////////
		try 
		{
			con.setAutoCommit(false);
			/*
			String sqlCode = "insert into com(com_id,com_type,com_name,com_des,com_keyword, table_mark," +
					"graph_mark, cedian, one, two, three, switch,sp) values('"
					+ id + "','" + vType + "','" + vName + "','" + vDscp + "','" + vKeyWords + "','"
					+ "0" + "','" + "0" + "','" + ceDian.trim() + "','" + vOne + "','" + vOne + "','" + vThree + "'," + ((isSwitch ? "1)" : "0)")
					+"','"+special		
					);
			Statement statement = con.createStatement();
			*/
			Statement statement = con.createStatement();
			String sqlCode =null;
			 PreparedStatement pstmt = null;  
			 pstmt = con.prepareStatement(
					 "insert into com(com_id,com_type,com_name,com_des,com_keyword, table_mark," +
						"graph_mark, cedian, one, two, three, switch,sp,dval,iscount,cost) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"
					 );  
			 pstmt.setInt(1, id);
			 pstmt.setString(2, vType);
			 pstmt.setString(3, vName);
			 pstmt.setString(4, vDscp);
			 pstmt.setString(5, vKeyWords);
			 pstmt.setString(6, "0");
			 pstmt.setString(7, "0");
			 pstmt.setString(8, ceDian.trim());
			 pstmt.setInt(9, vOne.intValue());
			 pstmt.setString(10, vTwo);
			 pstmt.setString(11, vThree);
			 pstmt.setInt(12, (isSwitch ? 1 : 0));
			 pstmt.setInt(13,special );
			 pstmt.setFloat(14,dVal );
			 pstmt.setInt(15,iscount );
			 pstmt.setInt(16,cost );
			 pstmt.executeUpdate(); 
			 
			//statement.executeUpdate(pstmt);
			sqlCode = "insert into comimage(com_id,com_type,image_id,location_x,location_y,location_xper,location_yper," +
					"color, biaopai_x, biaopai_y, point_x, point_y,sp) " + "values ("
					+ "'" + id
					+ "','" + vType
					+ "','" + imgId
					+ "','" + slctX0Per
					+ "','"	+ slctY0Per
					+ "','"	+ slctWidthPer
					+ "','" + slctHeightPer
					+ "','" + color
					+ "','" + bpX0Per
					+ "','" + bpY0Per
					+ "','" + pointX0Per
					+ "','" + pointY0Per
					+ "','" + special
					+ "')";
			//System.out.println(sqlCode);
			statement.executeUpdate(sqlCode);
			for (int i = 0; i < stGridPrd.size(); i++) 
			{
				Map ha = (Map) (stGridPrd.get(i));
				String stateId = (String) ha.get("stNumst");
				String quJian = (String) ha.get("stQujian");
				String stDscp = (String) ha.get("stDscp");
				String stType = (String) ha.get("stType");
				String stPPP = (String) ha.get("stPPP");
				String stColor = (String) ha.get("stColor");
				sqlCode = "insert into state(state_id,com_id,com_type,state_qujian,state_des,state_type,state_gailv,state_color) values ("
						+ "'"
						+ stateId
						+ "','"
						+ id
						+ "','"
						+ vType
						+ "','"
						+ quJian
						+ "','"
						+ stDscp
						+ "','"
						+ stType
						+ "','"
						+ stPPP + "','" + stColor + "')";
				statement.executeUpdate(sqlCode);
			}
			con.commit();
			return id;

		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			try 
			{
				con.rollback();
			} 
			catch (SQLException e1) 
			{
				e1.printStackTrace();
			}
			return -1;
		}
	}

	
	/**
	 * 根据传入的变量的信息，在数据库中创建测点
	 * @param COM_ID			测点ID
	 * @param ENTITY_ID 		实体ID
	 * @param TAG 				测点名
	 * @param LABEL1 			测点名
	 * @param LABEL2 			屏显简述
	 * @param COORDINATE 		坐标
	 * @param QUADRANT 			象限
	 * @param BOARD_ANGLE 		标牌角度
	 * @param BOARD_LENGTH 		标牌长度
	 * @param BOARD_STATE 		标牌状态
	 * @param BOARD_VALUE 		标牌值
	 * @param BOARD_DIRECT 		标牌方向
	 * @param BOARD_ROTATIONZ 	镜头旋转的rotationZ值
	 * @return
	 */
	public static int crtPoint(String com_id, String entity_id, String tag, String label1, String label2,
			String zuobiao_x, String zuobiao_y, String zuobiao_z, String xx1, String xx2, String xx3, 
			String xx4,	int board_angle, Number board_length, int board_state,
			Number board_value,	int board_direct, Number board_rotationz)
	{
		if (con == null) 
		{
			return 0;
		}
		
		String coordinate = zuobiao_x + "," + zuobiao_y + "," + zuobiao_z;
		String quadrant = xx1 + "," +xx2 + "," + xx3 + "," + xx4;
		try 
		{
			con.setAutoCommit(false);
			String sqlCode = "insert into comentity(com_id, entity_id, tag, label1, label2, coordinate,"
				    + "quadrant, board_angle, board_length, board_state, board_value,"
				    + "board_direct, board_rotationz) values('" 
				    + com_id 		+ "','"
					+ entity_id		+ "','"
					+ tag			+ "','"
					+ label1		+ "','" 
					+ label2		+ "','" 
					+ coordinate 	+ "','" 
					+ quadrant 		+ "','" 
					+ board_angle	+ "','"
					+ board_length	+ "','"
					+ board_state	+ "','" 
					+ board_value	+ "','" 
					+ board_direct 	+ "','"
					+ board_rotationz+ "')";
			Statement statement = con.createStatement();
			statement.executeUpdate(sqlCode);	
			con.commit();
			return 1;
		} 
		catch (SQLException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			try 
			{
				con.rollback();
			} 
			catch (SQLException e1) 
			{
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			return -1;
		}
	}
	
	/**
	 * 根据传入的变量的信息，
	 * 在数据库中创建
	 * 部件与B变量对应关系
	 * @param entityItemID			部件ID
	 * @param entityExplain 		部件中文标注
	 * @param CorrespondBVal 	        与B变量的对应关系
	 * @return
	 */
	public static int crtBValCor(String entityItemID, String ChineseExplain, String entityID, String CorrespondBVal)
	{
		if (con == null) 
		{
			return 0;
		}
		
		try 
		{
			con.setAutoCommit(false);
			String sqlCode = "select * from bvalcor where entityItemID = '" 
						+ entityItemID + "' and entityID = '"  + entityID + "'";
			Statement statement = con.createStatement();
			ResultSet result = statement.executeQuery(sqlCode);
			if (result.next()) 
			{
				sqlCode = "update bvalcor set CorrespondBVal ='" + CorrespondBVal + "' where entityItemID = '" 
						+ entityItemID + "' and entityID = '"  + entityID + "'";
			}
			else
			{
				sqlCode = "insert into bvalcor(entityItemID, ChineseExplain, entityID, CorrespondBVal) " +
							 "values('" 
				    + entityItemID 		+ "','"
					+ ChineseExplain	+ "','"
					+ entityID			+ "','"
					+ CorrespondBVal	+ "')";
			}
			statement = con.createStatement();
				statement.executeUpdate(sqlCode);	
				con.commit();
				return 1;
		} 
		catch (SQLException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			try 
			{
				con.rollback();
			} 
			catch (SQLException e1) 
			{
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			return -1;
		}
	}
	
	/**
	 * 根据传入的变量的信息，
	 * 在数据库中创建
	 * 部件与B变量对应关系
	 * @param entityItemID			部件ID
	 * @param entityExplain 		部件中文标注
	 * @param CorrespondBVal 	        与B变量的对应关系
	 * @return
	 */
	public static int recoVal(String vType, String vid, String recoMode)
	{
		if (con == null) 
		{
			return 0;
		}
		try 
		{
			if (recoMode.equals("1") || recoMode.equals("2"))
			{
				//恢复删除你内容到com表
				con.setAutoCommit(true);
				String sqlCode = "select * from delcom where com_id='" + vid
							+ "' and com_type='" + vType + "'";
				Statement statement = con.createStatement();
				ResultSet result = statement.executeQuery(sqlCode);
				while (result.next())
				{
					String comIDStr = result.getString(1);
					String comTypeStr = result.getString(2);
					String comNameStr = result.getString(3);
					String comDesStr = result.getString(4);
					String comKeywordStr = result.getString(5);
					String comTableMarkStr = result.getString(6);
					String comGraphMarkStr = result.getString(7);
					String comCedianStr = result.getString(8);
					String comSwitchStr = result.getString(9);
					
					con.setAutoCommit(true);
					sqlCode = "delete from delcom where com_id='" + vid
							+ "' and com_type='" + vType + "'";
					statement = con.createStatement();
					statement.executeUpdate(sqlCode);
					
					con.setAutoCommit(true);
					sqlCode = "insert into com(com_id, com_type, com_name, com_des, com_keyword, " +
							"table_mark, graph_mark, cedian, switch) values('" + comIDStr + "','" + 
							comTypeStr  + "','" + 
							comNameStr + "','" + 
							comDesStr + "','" + 
							comKeywordStr + "','" + 
							comTableMarkStr + "','" + 
							comGraphMarkStr + "','" +
							comCedianStr + "','" + 
							comSwitchStr + "')";
					statement = con.createStatement();
					statement.executeUpdate(sqlCode);
				}
				
				//恢复删除你内容到comimage表
				con.setAutoCommit(true);
				sqlCode = "select *  from delcomimage where com_id='" + vid
						+ "' and com_type='" + vType + "'";
				statement = con.createStatement();
				result = statement.executeQuery(sqlCode);
				while (result.next())
				{
					String comIDStr = result.getString(1);
					String comTypeStr = result.getString(2);
					String imageIDStr = result.getString(3);
					String locationXStr = result.getString(4);
					String locationYStr = result.getString(5);
					String locationXPERStr = result.getString(6);
					String locationYPERStr = result.getString(7);
					String colorStr = result.getString(8);
					String biaopaiXStr = result.getString(9);
					String biaopaiYStr = result.getString(10);
					String pointXStr = result.getString(11);
					String piintYStr = result.getString(12);
					
					con.setAutoCommit(true);
					sqlCode = "delete from delcomimage where com_id='" + vid
							+ "' and com_type='" + vType + "'";
					statement = con.createStatement();
					statement.executeUpdate(sqlCode);
					
					con.setAutoCommit(true);
					sqlCode = "insert into comimage(com_id, com_type, image_id, location_x, location_y, " +
							"location_xper, location_yper, color, biaopai_x, biaopai_y, point_x, point_y) " + 
							"values('" 
							+ comIDStr + "','" 
							+ comTypeStr  + "','" 
							+ imageIDStr + "','" 
							+ locationXStr + "','" 
							+ locationYStr + "','" 
							+ locationXPERStr + "','" 
							+ locationYPERStr + "','" 
							+ colorStr + "','" 
							+ biaopaiXStr + "','" 
							+ biaopaiYStr + "','" 
							+ pointXStr + "','" 
							+ piintYStr + "')";
					statement = con.createStatement();
					statement.executeUpdate(sqlCode);
				}
				
				//恢复删除你内容到state表
				con.setAutoCommit(true);
				sqlCode = "select * from delstate where com_id='" + vid
							+ "' and com_type='" + vType + "'";
				statement = con.createStatement();
				result = statement.executeQuery(sqlCode);
				while (result.next()) 
				{
					String stateIDStr = result.getString(1);
					String comIDStr = result.getString(2);
					String comTypeStr = result.getString(3);
					String stateQujianStr = result.getString(4);
					String stateDesStr = result.getString(5);
					String stateTypeStr = result.getString(6);
					String stateGailvStr = result.getString(7);
					String stateColorStr = result.getString(8);
					
					con.setAutoCommit(true);
					sqlCode = "delete from delstate where com_id='" + vid
							+ "' and com_type='" + vType + "'";
					statement = con.createStatement();
					statement.executeUpdate(sqlCode);
					
					con.setAutoCommit(true);
					sqlCode = "insert into state(state_id, com_id, com_type, state_qujian, state_des, state_type, " +
						"state_gailv, state_color) values('" + stateIDStr + "','" + 
						comIDStr  + "','" + comTypeStr + "','" + stateQujianStr + "','" + 
						stateDesStr + "','" + stateTypeStr + "','" + stateGailvStr + "','" +
						stateColorStr + "')";
					statement = con.createStatement();
					statement.executeUpdate(sqlCode);
				}
			}
			if (recoMode.equals("3"))
			{
				con.setAutoCommit(true);
				String sqlCode = "select * from delcomimage where com_id='" + vid
							+ "' and com_type='" + vType + "'";
				Statement statement = con.createStatement();
				ResultSet result = statement.executeQuery(sqlCode);
				while (result.next())
				{
					String comIDStr = result.getString(1);
					String comTypeStr = result.getString(2);
					String imageIDStr = result.getString(3);
					String locationXStr = result.getString(4);
					String locationYStr = result.getString(5);
					String locationXPERStr = result.getString(6);
					String locationYPERStr = result.getString(7);
					String colorStr = result.getString(8);
					String biaopaiXStr = result.getString(9);
					String biaopaiYStr = result.getString(10);
					String pointXStr = result.getString(11);
					String piintYStr = result.getString(12);
					
					con.setAutoCommit(true);
					sqlCode = "delete from delcomimage where com_id='" + vid
							+ "' and com_type='" + vType + "'";
					statement = con.createStatement();
					statement.executeUpdate(sqlCode);
					
					con.setAutoCommit(true);
					sqlCode = "insert into comimage(com_id, com_type, image_id, location_x, location_y, " +
							"location_xper, location_yper, color, biaopai_x, biaopai_y, point_x, point_y) " + 
							"values('" + 
							comIDStr + "','" + 
							comTypeStr  + "','" + 
							imageIDStr + "','" + 
							locationXStr + "','" + 
							locationYStr + "','" + 
							locationXPERStr + "','" + 
							locationYPERStr + "','" + 
							colorStr + "','" + 
							biaopaiXStr + "','" + 
							biaopaiYStr + "','" + 
							pointXStr + "','" + 
							piintYStr + "')";
					statement = con.createStatement();
					statement.executeUpdate(sqlCode);
				}
			}
			return 1;
		} 
		catch (SQLException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			return -1;
		}
	}
	
	
	/**
	 * 根据给定的变量的类型，得到一个变量的编号
	 * @param type 变量类型
	 * @return 变量编号
	 */
	public static int getComponentId(String type) 
	{
		if (con == null) 
		{
			return -1;
		}

		try
		{
			con.setAutoCommit(true);
			String sqlCode = "select max(com_id)+1 from com where com_type='"
					+ type + "'";
			//System.out.println(sqlCode);
			Statement statement = con.createStatement();
			ResultSet result = statement.executeQuery(sqlCode);
			result.next();
			int iNumCom = result.getInt(1);
			if (iNumCom == 0)
			{
				iNumCom = 1;
			}
			
			con.setAutoCommit(true);
			sqlCode = "select max(com_id)+1 from delcom where com_type='"
					+ type + "'";
			//System.out.println(sqlCode);
			statement = con.createStatement();
			result = statement.executeQuery(sqlCode);
			result.next();

			int iNumDelCom = result.getInt(1);
            if (iNumCom >= iNumDelCom)
            {
            	return iNumCom;
            }
            else
            {
            	return iNumDelCom;
            }
		}
		catch (SQLException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			return -1;
		}
	}

}

/**
 * 存储变量基本信息的类
 * @author SHU
 *
 */
class ComponentT {
	int r = 0;
	
	/**
	 * 变量ID
	 */
	String id;
	/**
	 * 变量名称
	 */
	String name;
	/**
	 * 变量类型
	 */
	String type;
	/**
	 * 变量的关键字
	 */
	String keyWord;

	/**
	 * 将变量的基本信息保存为一个XML字符串
	 * @return 返回保存的XML字符串
	 */
	public Element toXml() {
		Element root = new Element("val");
		Element type = new Element("dupType");
		type.addContent(this.type);
		root.addContent(type);

		Element id = new Element("id");
		id.addContent(this.id);
		root.addContent(id);

		Element name = new Element("dupName");
		name.addContent(this.name);
		root.addContent(name);

		Element dupTypeId = new Element("dupTypeId");
		dupTypeId.addContent(this.type + this.id);
		root.addContent(dupTypeId);

		Element keyWord = new Element("dupKeyWords");
		keyWord.addContent(this.keyWord);
		root.addContent(keyWord);

		Element r = new Element("dupNum");
		r.addContent(String.valueOf(this.r));
		root.addContent(r);

		return root;
	}

}

/**
 * 一个排序的类
 * @author SHU
 *
 */
class SortByR implements Comparator {
	public int compare(Object obj1, Object obj2) {
		ComponentT com1 = (ComponentT) obj1;
		ComponentT com2 = (ComponentT) obj2;
		if (com1.r < com2.r)
			return 1;
		else
			return 0;
	}
}