package gct.database;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.net.SocketException;

/**
 * 用于控制故障回放
 * @author SHU
 *
 */
public class ReviewThread extends Thread {

	private ReadDatabase readDB = null;
	private boolean sendMark = true;

	ReviewThread(ReadDatabase readDB) {
		this.readDB = readDB;
	}

	@Override
	public void run()
	{
		// TODO Auto-generated method stub
		InetSocketAddress isac = new InetSocketAddress(7000);
		DatagramSocket ds = null;
		try
		{
			ds = new DatagramSocket(isac);
			ds.setReceiveBufferSize(2000 * 500 * 300);
		} 
		catch (SocketException e1)
		{
			e1.printStackTrace();
		}
		byte[] buf = new byte[200];

		while (true)
		{
			try
			{
				if (sendMark) 
				{
					DatagramPacket dp = new DatagramPacket(buf, 200);
					ds.receive(dp);// 套接字接受数据包
					// System.out.println("OKBAG");
					readDB.sendToReview();
				}
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
		}
	}

	public void setSendMark(boolean sendMark) 
	{
		this.sendMark = sendMark;
	}

	public boolean isSendMark() 
	{
		return sendMark;
	}
}
