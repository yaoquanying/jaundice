package gct.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Timer;
import java.util.TimerTask;

import db.config.DbConfig;

/**
 * 用于给前台的实时数据显示发送数据,每隔一段时间刷新一次
 * @author Yao
 *
 */
public class RefreshThread {
	/**
	 * 刷新时间间隔
	 */
	private final long timeLen = (long) (1000 * 60 * 20);
	private Timer time = null;

	public static void main(String[] args) 
	{
		RefreshThread test = new RefreshThread();
	}

	RefreshThread()
	{
		time = new Timer();
		time.schedule(new MyTask(), 0, timeLen);
//		while (true)
//			;
	}
}

/**
 * 定时刷新
 * @author SHU
 */
class MyTask extends TimerTask {

	private Connection con = connection();

	@Override
	public void run() 
	{
		refreshComMsg();
		refreshComStateMsg();
		// System.out.println(UDPClient.comMsg.size());
	}

	//每隔timeLen(1000 * 60 * 20)刷新一次
	private void refreshComMsg() 
	{
		UDPClient.comMsg.remove(UDPClient.comMsg);
		if (con == null)
		{
			con = connection();
		}
		try 
		{
			con.setAutoCommit(true);
			String sqlCode = "select com_type,com_id,com_name,com_des from com ";
			Statement statement = con.createStatement();
			ResultSet result = statement.executeQuery(sqlCode);
			while (result.next())
			{
				ComMsg temp = new ComMsg();
				temp.type = result.getString(1);
				temp.id = result.getString(2);
				temp.name = result.getString(3);
				temp.des = result.getString(4);
				UDPClient.comMsg.put(temp.type + temp.id, temp);//每隔timeLen(1000 * 60 * 20)刷新一次
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
	}

	/**
	 * 刷新带有状态的变量信息//每隔timeLen(1000 * 60 * 20)刷新一次
	 */
	private void refreshComStateMsg() 
	{
		UDPClient.comStateMsg.remove(UDPClient.comStateMsg);
		if (con == null)
		{
			con = connection();
		}
		try 
		{
			con.setAutoCommit(true);
			String sqlCode = "select com_type,com_id,com_name,com_des from com ";
			Statement statement = con.createStatement();
			ResultSet result = statement.executeQuery(sqlCode);

			while (result.next())
			{
				String strtype = result.getString(1);
				String strid = result.getString(2);
				String sqlSCode = "select state_id, state_des, state_color from state where com_id='"
							+ strid + "' and com_type='" + strtype + "'";
				
				Statement statementstate = con.createStatement();
				ResultSet resultstate = statementstate.executeQuery(sqlSCode);
				 
				while(resultstate.next())
				{
					ComStateMsg temp = new ComStateMsg();
					temp.type = result.getString(1);
					temp.id = result.getString(2);
					temp.name = result.getString(3);
					temp.des = result.getString(4);
					
					temp.stateID = resultstate.getString(1);
					temp.stateDes = resultstate.getString(2);
					temp.stateColor = resultstate.getString(3);
					UDPClient.comStateMsg.put(temp.stateID + temp.type + temp.id, temp);//每隔timeLen(1000 * 60 * 20)刷新一次
				}
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
	}
	
	/**
	 * 数据库连接
	 * @return
	 */
	public static Connection connection() {
	//	String dbName = "jaundice";
		try {
			Class.forName("com.mysql.jdbc.Driver");
			// 配置数据源				      192.168.5.89
			String url = "jdbc:mysql://"+DbConfig.host+"/" + DbConfig.dbname
					+ "?useUnicode=true&characterEncoding=UTF-8";
			Connection conT = DriverManager.getConnection(url, DbConfig.usname, DbConfig.dbpwd);
			return conT;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
}
