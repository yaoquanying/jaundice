package gct.database;

import java.util.LinkedList;


/**
 * 队列
 * @author SHU
 *
 * @param <T>
 */
public class Queue<T> {
	public LinkedList<T> storage = new LinkedList<T>();
	
	/**
	 * 进入队列
	 * @param v
	 */
	public void push(T v) {
		storage.addFirst(v);
	}

	/**
	 * 出队列
	 * @return
	 */
	public T pop() {
		return storage.removeLast();
	}
	

	/**
	 * 队列是否为空
	 * @return
	 */
	public boolean isEmpty() {
		return storage.isEmpty();
	}
	
	/**
	 * 获得队列第一个
	 * @return
	 */
	public T getFirst(){
		return storage.getLast();
	}
}
