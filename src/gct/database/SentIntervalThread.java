package gct.database;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.SocketException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 * 用于给故障诊断模块发送区间信息
 * @author SHU
 *
 */
public class SentIntervalThread extends Thread{
	
	/**
	 * 发送端口
	 */
	private int port =7200;
	private Connection con = UDPClient.connection();

	@Override
	public void run() {
		// TODO Auto-generated method stub
	
		InetSocketAddress isac = new InetSocketAddress( port);
		DatagramSocket ds = null;
		try {
			ds = new DatagramSocket(isac);
			ds.setReceiveBufferSize(2000 * 500 * 300);
		} catch (SocketException e1) {
			e1.printStackTrace();
		}// 在端口6400接收数据报包的套接字
		byte[] buf = new byte[200];
		DatagramPacket dp = new DatagramPacket(buf, 200);// 创建长度为5000的数据接收包
		while (true) {
			try {
				ds.receive(dp);// 套接字接受数据包
				System.out.println("get");
				sendIntervalMessage();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * 发送区间数据
	 * @param str
	 */
	public void sendIntervalMessage() {

		int bufSize =2000;
		byte[] buf= new byte[bufSize];
		int port=6100;
		try {
			DatagramSocket ds = new DatagramSocket();
			byte[] sendBuf = null;
			DatagramPacket dp = new DatagramPacket(buf, buf.length,
					InetAddress.getByName(UDPClient.targetIp), port);
			
			ArrayList<Node> send = getSendMessage();
			sendBuf=Change.shortToByte((short)send.size());
			dp.setData(sendBuf);
			ds.send(dp);
			for(Node node:send){
				dp.setData(Change.shortToByte(node.type));
				ds.send(dp);
				
				dp.setData(Change.shortToByte(node.id));
				ds.send(dp);
				
				dp.setData(Change.shortToByte(node.countOfInterval));
				ds.send(dp);
				
				for(Interval interval:node.intervals){
					dp.setData(Change.shortToByte(interval.id));
					ds.send(dp);
					
					dp.setData(Change.floatToByte(interval.value1));
					ds.send(dp);
					
					dp.setData(Change.floatToByte(interval.value2));
					ds.send(dp);
				}
				
			}
			ds.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 获得需要发送的节点的信息
	 * @return
	 */
	private ArrayList<Node> getSendMessage(){
		ArrayList<Node> node = new ArrayList<Node>();
		if(con==null){
			con=UDPClient.connection();
		}
		try {
			con.setAutoCommit(true);
			String sqlCode="select com_type,com_id from com where com_type='X' order by com_id asc";
			String sql = "select state_qujian from state where com_id=? and com_type=? order by state_id asc";
			Statement st1 = con.createStatement();
			PreparedStatement st2 = con.prepareStatement(sql);
			ResultSet result1= st1.executeQuery(sqlCode);
			while(result1.next()){
				Node temp= new Node();
				String type =result1.getString(1);
				String id=result1.getString(2);
				temp.type=type.equals("X")?(short)2:1;
				temp.id=Short.valueOf(id);
				
				st2.setString(1, id);
				st2.setString(2, type);
				ResultSet re= st2.executeQuery();
				int i=0;
				while(re.next()){
					Interval test = getIntervals(re.getString(1));
					test.id=(short)i;
					temp.intervals.add(test);
					i++;
				}
				temp.countOfInterval=(short)temp.intervals.size();
				node.add(temp);
			}
			st1.close();
			st2.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return node;
	}
	
	/**
	 * 格式化区间信息
	 * @param input
	 * @return
	 */
	private Interval getIntervals(String input){
		Interval result = new Interval();
		input =input.trim();
		String test = input.substring(1, input.length()-1);
		
		///////////////////////////gengsc
		///////////////////////处理开闭区间
		String strLeft = input.substring(0, 1);
		String strRight = input.substring(input.length()-1, input.length());
		float fLeft = 0.00000001f;
		if(strLeft.trim().equals("("))
		{
			fLeft = -0.00000001f;
		}
		else if(strLeft.trim().equals("]"))
		{
			fLeft = 0.00000001f;
		}
		
		float fRight = 0.00000001f;
		if(strRight.trim().equals("("))
		{
			fRight = -0.00000001f;
		}
		else
		{
			fRight = 0.00000001f;
		}
		//////////////////////////////gengsc
		String[] numT=test.split(",");
		if(numT[0].trim().equals("")){
			result.value1=-99999999.9f;
		}
		else{
			result.value1=Float.valueOf(numT[0]) + fLeft;
		}
		if(numT.length<2){
			result.value2=99999999.9f;
		}
		else if(numT[1].trim().equals("")){
			result.value2=99999999.9f;
		}
		else{
			result.value2=Float.valueOf(numT[1]) + fRight;
		}
		
		return result;
	}

}
