package gct.database;

/**
 * 变量基本信息
 * @author SHU
 *
 */
public class ComMsg {
	
	/**
	 * 变量ID
	 */
	public String id;
	/**
	 * 变量类型
	 */
	public String type;
	/**
	 * 变量名称
	 */
	public String name;
	/**
	 * 变量描述
	 */
	public String des;
}
