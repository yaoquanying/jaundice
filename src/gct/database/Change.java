package gct.database;

import java.util.zip.DataFormatException;

/**
 * 用于各种数据之间的类型转换
 * @author SHU
 *
 */
public class Change {
	
	/**
	* 整型转换到字节数组
	* @param number
	* @return
	*/
	public static byte[] intToByte(int number) {
	    byte[] b = new byte[4];   
	    for (int i = 0; i < 4; i++) {   
	      b[i] = (byte) (number % 256);   
	      number >>= 8;   
	    } 
	   return b;
	}
	
	public static byte[] shortToByte(short i) {
		byte[] result = new byte[2];
		result[1] = (byte) ((i >> 8) & 0xFF);
		result[0] = (byte) (i & 0xFF);
		return result;
	}

	public static byte[] floatToByte(float d) {

		byte[] bytes = new byte[4];
		int l = Float.floatToIntBits(d);
		for (int i = 0; i < bytes.length; i++) {
			bytes[i] = new Integer(l).byteValue();
			l = l >> 8;
		}
		return bytes;
	}

	public static float byteToFloat(byte[] b) throws DataFormatException {
		if (b.length != 4)
			throw new DataFormatException();
		int l;
		l = b[0];
		l &= 0xff;
		l |= ((long) b[1] << 8);
		l &= 0xffff;
		l |= ((long) b[2] << 16);
		l &= 0xffffff;
		l |= ((long) b[3] << 24);
		l &= 0xffffffffl;
		return Float.intBitsToFloat(l);
	}
	
	/**
	* double转换到字节数组
	* @param d
	* @return
	*/
	public static byte[] doubleToByte(double d){
	  
	   byte[] bytes = new byte[8];
	   long l = Double.doubleToLongBits(d);
	   for(int i = 0; i < bytes.length; i++ ){
	    bytes[i]= new Long(l).byteValue();
	    l=l>>8;
	   }
	   return bytes;
	}
	
	/**
	* 字符串到字节数组转换
	* @param s
	* @return
	*/
	public static byte[] stringToByte(String s){
	   return s.getBytes();
	}
	
	/**
	* 字节数组带字符串的转换
	* @param b
	* @return
	*/
	public static String byteToString(byte[] b){
	   return new String(b);
	  
	}
	
	public static int byteToInt(byte[] b) throws DataFormatException {
		if (b.length != 4)
			throw new DataFormatException();
		return (int) ((((b[3] & 0xff) << 24) | ((b[2] & 0xff) << 16)
				| ((b[1] & 0xff) << 8) | ((b[0] & 0xff) << 0)));
	}

	public static short byteToShort(byte[] b) throws DataFormatException {
		if (b.length != 2)
			throw new DataFormatException();

		return (short) ((((b[1] & 0xff) << 8) | b[0] & 0xff));
	}
	
	public static double byteToDouble(byte[] b) throws DataFormatException {
		if (b.length != 8)
			throw new DataFormatException();
		long l;
		l = b[0];
		l &= 0xff;
		l |= ((long) b[1] << 8);
		l &= 0xffff;
		l |= ((long) b[2] << 16);
		l &= 0xffffff;
		l |= ((long) b[3] << 24);
		l &= 0xffffffffl;
		l |= ((long) b[4] << 32);
		l &= 0xffffffffffl;

		l |= ((long) b[5] << 40);
		l &= 0xffffffffffffl;
		l |= ((long) b[6] << 48);
		l &= 0xffffffffffffffl;

		l |= ((long) b[7] << 56);

		return Double.longBitsToDouble(l);
	}
}
