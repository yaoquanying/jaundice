package gct.database;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.net.SocketException;
import java.util.concurrent.locks.ReentrantLock;

/**
 * 接受实时数据的模块
 * @author SHU
 *
 */
public class ReceiveThread implements Runnable 
{

	/**
	 * 存储实时数据的缓冲区
	 */
	private Queue<DatagramPacket> dataBuf = null;
	/**
	 * 接收包的大小
	 */
	public static final int RECEIVEBAG_SIZE = 2000;
	private ReentrantLock lock = null;
	private int port = 0;

	private static int number = 0;

	public void run() 
	{
		// TODO Auto-generated method stub
		while (true) 
		{
			receive();
		}
	}
	
	ReceiveThread(Queue<DatagramPacket> dataBuf, int port, ReentrantLock lock) {
		this.dataBuf = dataBuf;
		this.port = port;
		this.lock = lock;
	}

	/**
	 * 接受数据
	 */
	public void receive() {
		InetSocketAddress isac = new InetSocketAddress(port);
		DatagramSocket ds = null;
		try {
			ds = new DatagramSocket(isac);
			ds.setReceiveBufferSize(RECEIVEBAG_SIZE * 500 * 300);
		} catch (SocketException e1) {
			e1.printStackTrace();
		}
		int conut = 0;
		while (true) {
			try {
				byte[] buf = new byte[RECEIVEBAG_SIZE];
				DatagramPacket dp = new DatagramPacket(buf, RECEIVEBAG_SIZE);// 创建长度为5000的数据接收包
				ds.receive(dp);// 套接字接受数据包
				lock.lock();
				dataBuf.push(dp);
				//System.out.println("BadDataToDB!" + (++number));
				lock.unlock();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

}
