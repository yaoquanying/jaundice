package gct.database;

/**
 * 变量基本信息
 * @author SHU
 *
 */
public class ComStateMsg {
	
	/**
	 * 变量ID
	 */
	public String id;
	/**
	 * 变量类型
	 */
	public String type;
	/**
	 * 变量名称
	 */ 
	public String name;
	/**
	 * 变量描述
	 */
	public String des;
	/**
	 * 变量状态 
	 */
	public String stateID;
	/**
	 * 变量状态简述
	 */
	public String stateDes;
	/**
	 * 变量状态颜色
	 */
	public String stateColor;
}
