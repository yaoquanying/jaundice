package gct.database;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * 故障回放是从数据库中读出历史数据
 * @author SHU
 *
 */
public class ReadDatabase {

	/**
	 * 数据库连接
	 */
	private Connection con = UDPClient.connection();
	/**
	 * 在发送回放数据时每个UDP包中包含的变量的个数
	 */
	private static final int NODESIZE = 144;//112

	/**
	 * 用于存储故障数据的表的名称
	 */
	private String dbName;
	/**
	 * 发生故障的变量的编号
	 */
	private int id = 0;
	/**
	 * 发送的端口号
	 */
	private static final int port = 6000;//6800

	/**
	 * 设置变量编号和表名
	 * @param name 表名
	 * @param id 编号
	 */
	public void setNameAndID(String name, int id) 
	{
		this.dbName = name;
		this.id = id;
	}

	/**
	 * 获得需要发送的数据
	 * @param db 表名
	 * @param id 编号
	 * @return 
	 */
	public ArrayList<byte[]> getSendMessage(String db, String id) {
		if (con == null) 
		{
			con = UDPClient.connection();
		}
		ArrayList<byte[]> sentMsg = new ArrayList<byte[]>();

		String message = "";
		Time time = new Time();

		try {
			con.setAutoCommit(true);
			String sqlCode = "select c_year,c_month,c_day,c_hour,c_minute,c_tzinfo,c_second, message from "
					+ db + " where number=?";
			PreparedStatement statement = con.prepareStatement(sqlCode);
			statement.setString(1, id);
			ResultSet result = statement.executeQuery();
			while (result.next()) 
			{
				time.year = Integer.valueOf(result.getString(1));
				time.month = Integer.valueOf(result.getString(2));
				time.day = Integer.valueOf(result.getString(3));
				time.hour = Integer.valueOf(result.getString(4));
				time.minute = Integer.valueOf(result.getString(5));
				time.tzinfo = Integer.valueOf(result.getString(6));
				time.second = Double.valueOf(result.getString(7));
				message = result.getString(8);
			}
			if (message.equals(""))
			{
				return null;
			}
			result.close();
			statement.close();
			String[] node = message.split("\n");

			int i = 10;
			for (i = 10; i < node.length; i += 10) 
			{
				int index = 0;
				byte[] temp = new byte[NODESIZE * 10 + 1];
				for (int j = 0; j < 10; j++)
				{
					byte[] si = getByteFromMsg(time, node[i - 10 + j]);
					copyByte(temp, si, index);
					index += si.length;
				}
				sentMsg.add(temp);
			}

			int lastNum = node.length - (i - 10);
			byte[] temp = new byte[lastNum * NODESIZE + 1];
			int index = 0;
			for (int k = 0; k < lastNum; k++)
			{
				byte[] si = getByteFromMsg(time, node[i - 10 + k]);
				copyByte(temp, si, index);
				index += si.length;
			}
			sentMsg.add(temp);

		}
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return sentMsg;
	}

	/**
	 * 字节拷贝
	 * @param di
	 * @param si
	 * @param index
	 */
	private void copyByte(byte[] di, byte[] si, int index) 
	{
		for (int i = 0; i < si.length; i++)
		{
			di[index + i] = si[i];
		}
		return;
	}

	/**
	 * 数据处理
	 * @param time
	 * @param node
	 * @return
	 */
	private byte[] getByteFromMsg(Time time, String node) 
	{
		int LEN = 144;  //112
		byte[] result = new byte[LEN];
		byte[] temp = null;
		int index = 0;

		String[] tagMsg = node.split("\t");
		temp = Change.stringToByte(tagMsg[0]);
		copyByte(result, temp, index);
		index += temp.length + 1;

		temp = Change.shortToByte(Short.valueOf(tagMsg[1]));
		copyByte(result, temp, index);
		index += temp.length;
		
		temp = Change.shortToByte(Short.valueOf(tagMsg[2]));
		copyByte(result, temp, index);
		index += temp.length + 2;

		temp = Change.doubleToByte(Double.valueOf(tagMsg[3]));
		copyByte(result, temp, index);
		index += temp.length;

		temp = Change.stringToByte(tagMsg[4]);
		copyByte(result, temp, index);
		index += temp.length;
		
		temp = Change.stringToByte(tagMsg[5]);
		copyByte(result, temp, index);
		index += temp.length;

		temp = Change.stringToByte(tagMsg[6]);
		copyByte(result, temp, index);
		index += temp.length;

		temp = Change.intToByte(time.month);
		copyByte(result, temp, index);
		index += temp.length;

		temp = Change.intToByte(time.year);
		copyByte(result, temp, index);
		index += temp.length;

		temp = Change.intToByte(time.day);//师兄，index的值到这里的时候已经变成了temp最大上限，即144，所以报错。
		copyByte(result, temp, index);
		index += temp.length;

		temp = Change.intToByte(time.hour);
		copyByte(result, temp, index);
		index += temp.length;

		temp = Change.intToByte(time.minute);
		copyByte(result, temp, index);
		index += temp.length;

		temp = Change.intToByte(time.tzinfo);
		copyByte(result, temp, index);
		index += temp.length;

		temp = Change.doubleToByte(time.second);
		copyByte(result, temp, index);
		index += temp.length;

		return result;
	}

	/**
	 * 发送给故障回放模块
	 */
	public void sendToReview() 
	{
		int bufSize = 2000;
		byte[] buf = new byte[bufSize];
		DatagramSocket ds;
		ArrayList<byte[]> sentMsg = getSendMessage(dbName, String.valueOf(id));
		id++;
		if (sentMsg == null)
		{
			return;
		}
		try
		{
			ds = new DatagramSocket();
			DatagramPacket dp = new DatagramPacket(buf, bufSize, InetAddress
					.getByName(UDPClient.targetIp), port);
			for (byte[] temp : sentMsg)
			{
				dp.setData(temp);
				ds.send(dp);
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
}
