package gct.database;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.PrintWriter;
import java.net.DatagramPacket;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;
import java.util.Scanner;
import java.util.concurrent.locks.ReentrantLock;
import java.util.zip.DataFormatException;

import gct.com.wyh.GetPathStr;

/**
 * 将故障数据存储到数据库中
 * @author SHU
 *
 */
public class WriteToDbThread  extends Thread{

	private Connection con = UDPClient.connection();
	private Queue<DatagramPacket> historyBuf = null;
	private Queue<DatagramPacket> curBuf = null;
	ReentrantLock lockHistory = null;
	ReentrantLock lockCur = null;
	private DatagramPacket data = null;
	private int flag = 0;
	private String message = "";
	private PreparedStatement statement = null;
	private boolean newTable = false;

	// private int sqlNum = 0;
	private String sqlName = "";

	private int state = 0;
	
	private int sleepTime = 1000 * 5;

	WriteToDbThread(Queue<DatagramPacket> historyBuf,
			ReentrantLock lockHistory, Queue<DatagramPacket> curBuf,
			ReentrantLock lockCur) 
	{
		this.historyBuf = historyBuf;
		this.curBuf = curBuf;
		this.lockCur = lockCur;
		this.lockHistory = lockHistory;
	}

	public void run() 
	{
		boolean sleepMark= false;
		while (true) 
		{
//			System.out.println(state);
			sleepMark=false;

//			if (state == 0) {
//				lockHistory.lock();
//				if (historyBuf.isEmpty()) {
//				} else if (isBegin(historyBuf.getFirst())) {
//					createNewTable();
//					historyBuf.pop();
//					state = 1;
//				}
//				lockHistory.unlock();
//			}
			if(state == 0)
			{
				lockHistory.lock();
				if(historyBuf.isEmpty())
				{
					sleepMark=true;
				}
				else if(isBegin(historyBuf.getFirst()))
				{
					createNewTable();
					historyBuf.pop();
					state=1;
				}
				lockHistory.unlock();
				if(sleepMark)
				{
					try 
					{
						Thread.sleep(sleepTime);
					} 
					catch (InterruptedException e) 
					{
						
					}
				}
			}
			else if (state == 1)
			{
				lockHistory.lock();
				if (historyBuf.isEmpty())
				{
				} 
				else if (isBegin(historyBuf.getFirst()))
				{
					state = 2;
					historyBuf.pop();
				}
				else
				{
					data = historyBuf.pop();
					getTag(data);
				}
				lockHistory.unlock();
			} 
			else if (state == 2)
			{
				lockCur.lock();
				if (curBuf.isEmpty()) 
				{

				} 
				else if (isBegin(curBuf.getFirst())) 
				{
					curBuf.pop();
					state = 3;
				}
				lockCur.unlock();
			}
			else if (state == 3) 
			{
				lockCur.lock();
				if (curBuf.isEmpty())
				{
				} 
				else if (isBegin(curBuf.getFirst()))
				{
					state = 0;
					curBuf.pop();
				} 
				else
				{
					data = curBuf.pop();
					getTag(data);
				}
				lockCur.unlock();
			}
		}
	}

	/**
	 * 获得每个数据包中的节点数量
	 * 
	 * @param dp
	 * @return
	 */
	private int getTagNumber(DatagramPacket dp) 
	{
//		System.out.println(dp.getLength()); 数据包的长度
		return dp.getLength() / 144;
	}

	/**
	 * 判断是不是一个事故的开始，如果是开始返回true，并新建数据库，否则返回false；
	 * 
	 * @return
	 */
	private boolean isBegin(DatagramPacket data)
	{
		if (data.getLength() < 144)
		{
			return true;
		} 
		else 
		{
			return false;
		}
	}

	/**
	 * 获得存储故障数据的表的名称
	 * @return
	 */
     private String getDbName()
     {
		
		String result="";
		Calendar rightNow = Calendar.getInstance();
		result+=rightNow.get(Calendar.YEAR);
		int number=(rightNow.get(Calendar.MONTH)+1);
		if(number>9)
		{
			result+="_"+number;
		}
		else
		{
			result+="_0"+number;
		}
		number=rightNow.get(Calendar.DATE);
		if(number>9)
		{
			result+="_"+number;
		}
		else
		{
			result+="_0"+number;
		}
		number=rightNow.get(Calendar.HOUR_OF_DAY);
		if(number>9)
		{
			result+="_"+number;
		}
		else
		{
			result+="_0"+number;
		}
		number=rightNow.get(Calendar.MINUTE);
		if(number>9)
		{
			result+="_"+number;
		}
		else{
			result+="_0"+number;
		}
		number=rightNow.get(Calendar.SECOND);
		if(number>9)
		{
			result+="_"+number;
		}
		else
		{
			result+="_0"+number;
		}
		return result;
	}
	
     /**
      * 为新故障数据创建一个新的表
      */
	private void createNewTable() 
	{
		if (con == null)
		{
			con = UDPClient.connection();
		}

		try 
		{
			con.setAutoCommit(true);
//			int sqlNum = getNumber();
//			sqlName = "cisc" + sqlNum;
//			Date data = new Date();
//			String result = data.toString();
//			result=result.replace(" ", "_");
//			result=result.replace(":", "");
//
//
//			sqlName=result;
			sqlName = getDbName();

			UDPClient.revName = sqlName;
			// sqlNum++;
			String sqlCode = "insert into cisctable(name) values(?)";
			PreparedStatement sta1 = con.prepareStatement(sqlCode);
			sta1.setString(1, sqlName);
			sta1.executeUpdate();
			sta1.close();
			sqlCode = "CREATE TABLE "
					+ sqlName
					+ " ( NUMBER INT PRIMARY KEY AUTO_INCREMENT , C_YEAR VARCHAR(25),C_MONTH VARCHAR(25),C_DAY VARCHAR(25),"
					+ "C_HOUR VARCHAR(25),C_MINUTE VARCHAR(25),C_TZINFO VARCHAR(25),C_SECOND VARCHAR(25),MESSAGE MEDIUMBLOB)";
			sta1 = con.prepareStatement(sqlCode);
			sta1.executeUpdate();
			sta1.close();
		} 
		catch (SQLException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.newTable = true;
		try
		{
			if(statement!=null)
			{
				if(!statement.isClosed())
				{
					statement.close();
					System.out.println("close");
				}
				statement=null;
			}
			
		} 
		catch (SQLException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * 序号生成器
	 * @return
	 */
	public static int getNumber()
	{
		int result = 0;

		try 
		{
			Scanner scanner = new Scanner(new FileReader(UDPClient.getPathDb()
					+ "DUCG/NUMBER"));
			result = scanner.nextInt();
			scanner.close();
			PrintWriter write = new PrintWriter(new FileOutputStream(UDPClient
					.getPathDb()
					+ "DUCG/NUMBER"), true);
			write.println(result + 1);
		}
		catch (FileNotFoundException e) 
		{
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * 解析UDP包
	 * @param data
	 * @param begin
	 * @param lenth
	 * @return
	 */
	public static byte[] getInput(byte[] data, int begin, int lenth) 
	{
		byte[] result = new byte[lenth];
		for (int i = 0; i < lenth; i++) 
		{
			result[i] = data[begin + i];
		}
		return result;
	}

	/**
	 * 得到格式化的故障数据
	 * @param dp
	 */
	private void getTag(DatagramPacket dp) 
	{
		try 
		{
			byte[] data = dp.getData();
			int number = getTagNumber(dp);
			byte[] input;
			int index = 0;

			for (int i = 0; i < number; i++) 
			{
				Tag tag = new Tag();

				int lenth = 1;
				input = getInput(data, index, lenth);
				index = index + lenth + 1; 
				tag.cType = Change.byteToString(input);

				lenth = 2;
				input = getInput(data, index, lenth);
				index = index + lenth; 
				tag.iQuality = Change.byteToShort(input);
				
				lenth = 2;
				input = getInput(data, index, lenth);
				index = index + lenth + 2;
				tag.iDValue = Change.byteToShort(input);

				lenth = 8;
				input = getInput(data, index, lenth);
				index += lenth;
				tag.fValue = Change.byteToDouble(input);

				lenth = 32;
				input = getInput(data, index, lenth);
				index += lenth;
				tag.sValue = Change.byteToString(input);

				lenth = 32;
				input = getInput(data, index, lenth);
				index += lenth;
				tag.sName = Change.byteToString(input);
				
				lenth = 32;
				input = getInput(data, index, lenth);
				index += lenth;
				tag.sVarName = Change.byteToString(input);

				lenth = 4;
				input = getInput(data, index, lenth);
				index += lenth;
				tag.time.month = Change.byteToInt(input);

				lenth = 4;
				input = getInput(data, index, lenth);
				index += lenth;
				tag.time.year = Change.byteToInt(input);

				lenth = 4;
				input = getInput(data, index, lenth);
				index += lenth;
				tag.time.day = Change.byteToInt(input);

				lenth = 4;
				input = getInput(data, index, lenth);
				index += lenth;
				tag.time.hour = Change.byteToInt(input);

				lenth = 4;
				input = getInput(data, index, lenth);
				index += lenth;
				tag.time.minute = Change.byteToInt(input);

				lenth = 4;
				input = getInput(data, index, lenth);
				index += lenth;
				tag.time.tzinfo = Change.byteToInt(input);

				lenth = 8;
				input = getInput(data, index, lenth);
				index += lenth;
				tag.time.second = Change.byteToDouble(input);

				if (flag == 0)
				{
					message = tag.getMessage();
				} 
				else 
				{
					message += "\n" + tag.getMessage();
				}
				flag++;
//				System.out.println(message);
				if (flag == UDPClient.NODENUMBER)
				{
					saveToDb(tag, message);
					flag = 0;
					message = "";
				}
				// tag.print();
			}
		} 
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	/**
	 * 将故障数据保存到数据库中
	 * @param tag 
	 * @param message
	 */
	public void saveToDb(Tag tag, String message) 
	{
		if (con == null)
		{
			con = UDPClient.connection();
		}

		try
		{
			con.setAutoCommit(true);
			if (statement == null || newTable)
			{
				newTable = false;
				String sqlCode = "insert into "
						+ sqlName
						+ "(c_year,c_month,c_day,c_hour,c_minute,c_tzinfo,c_second,message)values(?,?,?,?,?,?,?,?)";
				statement = con.prepareStatement(sqlCode);
				System.out.println("new statement");
			}
			statement.setString(1, String.valueOf(tag.time.year));
			statement.setString(2, String.valueOf(tag.time.month));
			statement.setInt(3, tag.time.day);
			statement.setInt(4, tag.time.hour);
			statement.setInt(5, tag.time.minute);
			statement.setInt(6, tag.time.tzinfo);
			statement.setDouble(7, tag.time.second);
			statement.setString(8, message);
//			System.out.println(message);
			
			statement.executeUpdate();
//			System.out.println(statement.toString());
			System.gc();

		} 
		catch (SQLException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
