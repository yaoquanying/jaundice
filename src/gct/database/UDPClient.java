package gct.database;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.net.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;
import java.util.concurrent.locks.ReentrantLock;
import java.util.zip.DataFormatException;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.output.XMLOutputter;

import db.config.DbConfig;

/**
 * 后台运行模块
 * @author SHU
 *
 */
public class UDPClient 
{
	public static int sig = 0;
	private static Connection con = UDPClient.connection();
	public ReentrantLock lockHistory = new ReentrantLock();
	public ReentrantLock lockCur = new ReentrantLock();
	public static HashMap<String, ComMsg> comMsg = new HashMap<String, ComMsg>();
	public static HashMap<String, ComStateMsg> comStateMsg = new HashMap<String, ComStateMsg>();
	public ReadDatabase readDb = new ReadDatabase();
	public int portHistory = 6400;	//将数据存储到数据库用到的端口：转发历史数据端口
	public int portCur = 6600;		//将数据存储到数据库用到的端口：转发实时数据端口
	public static int NODENUMBER = 0;
	public static ArrayList<String> poingList = new ArrayList<String>();

	// public static final String myIp = "127.0.0.1";
	public static final String targetIp = "127.0.0.1";//"192.168.2.244"

	ReviewThread review = null;
 
	// 存放历史数据的缓冲区
	public Queue<DatagramPacket> historyBuf = new Queue<DatagramPacket>();

	// 存放实时数据的缓冲区
	public Queue<DatagramPacket> curBuf = new Queue<DatagramPacket>();

	/*
	 * public static void main(String[] args) {
	 * 
	 * UDPClient test = new UDPClient(); test.createWorkStation();
	 * 
	 * }
	 */

	/**
	 * 数据库连接
	 * 
	 * @return
	 */
	public static Connection connection()
	{
	//	String dbName = "jaundice";
		try 
		{
			Class.forName("com.mysql.jdbc.Driver");
			// 配置数据源
			// 
			// String url = "jdbc:mysql://192.168.137.188/" + dbName  192.168.5.89  
			String url = "jdbc:mysql://"+DbConfig.host+"/" + DbConfig.dbname
					+ "?useUnicode=true&characterEncoding=UTF-8";
			Connection con = DriverManager.getConnection(url, DbConfig.usname, DbConfig.dbpwd);
			return con;
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * 构建工作区
	 */
	public void createWorkStation() 
	{
		sig = 1;
		getNodeNumber();

		// 接受历史数据线程
		Thread receiveHistory = new Thread(new ReceiveThread(historyBuf, portHistory, lockHistory));
		receiveHistory.start();

		// 接受实时数据线程
		Thread receiveCur = new Thread(new ReceiveThread(curBuf, portCur, lockCur));
		receiveCur.start();

		// 将数据存放到数据库线程
		Thread write = new Thread(new WriteToDbThread(historyBuf, lockHistory, curBuf, lockCur));
		write.start();

		// 创建故障回放的线程
		review = new ReviewThread(readDb);
		review.setSendMark(true);
		review.start();

//		SentIntervalThread sentInterval = new SentIntervalThread();
//		sentInterval.start();

		new RefreshThread();

	}

	/**
	 * 获得路径
	 * @return
	 */
	public static String getPathDb()
	{
		String s = UDPClient.class.getResource("").getPath().substring(1)
				.replaceAll("%20", " ");
		// System.out.println(s);
		if (s.charAt(s.length() - 1) == '/') 
		{
			s += "../../../../../../../../";
		} 
		else
		{
			s += "/../../../../../../../../";
		}

		// System.out.println(s);
		return (s);
		// return "C:/";
	}

	/**
	 * 获得节点的数量
	 */
	public static void getNodeNumber() 
	{
		poingList.remove(poingList);
		NODENUMBER = 0;
		try 
		{
			Scanner scanner = new Scanner(new FileReader(getPathDb()
					+ "DUCG/PointList.txt"));
			while (scanner.hasNext())
			{
				String string = scanner.nextLine();
				if (string.equals(""))
				{
					break;
				}
				else
				{
					String[] strTemp = string.split(",");
					poingList.add(strTemp[0]);
//					poingList.add(string);
				}
				NODENUMBER++;
			}
		} 
		catch (FileNotFoundException e) 
		{
			e.printStackTrace();
		}
	}

	public static String revName = "";

	/**
	 * 开始回放
	 * @param name
	 * @return
	 */
	public int beginReview(String name)
	{
		readDb.setNameAndID(name, 1);
		readDb.sendToReview();
		review.setSendMark(true);
		revName = name;
		return 1;
	}

	/**
	 * 继续回放
	 * @return
	 */
	public int continueReview() {
		readDb.sendToReview();
		return 1;
	}

	/**
	 * 结束回放
	 * @return
	 */
	public int endReview() {
		review.setSendMark(false);
		return 1;
	}
	

	/**
	 * 得到故障列表
	 * @return
	 */
	public String getReviewList() {
		if (con == null) {
			con = UDPClient.connection();
		}

		Element root = new Element("vList");

		try {
			con.setAutoCommit(true);
			String sqlCode = "select name from cisctable";
			Statement satatement = con.createStatement();
			ResultSet result = satatement.executeQuery(sqlCode);
			while (result.next()) {
				Element val = new Element("val");
				Element bad = new Element("vBadReplayListName");
				bad.addContent(result.getString(1));
				val.addContent(bad);
				root.addContent(val);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		XMLOutputter outputter = new XMLOutputter();
		return outputter.outputString(new Document(root));
	}

	/**
	 * 获得线条信息
	 * @param id 变量ID
	 * @param type 变量类型
	 * @return
	 */
	public static String getLineMessage(String strTypeID)
	{
		// System.out.println(id + "++++" + type);
		String str[] = strTypeID.split("_");
		Element lineMessage = new Element("lineMessage");
		try 
		{
			for (int i = 0; i < str.length; i++)
			{
				String strOneTypeID[] = str[i].split("-");
				if (strOneTypeID.length > 0)
				{
					String strType = strOneTypeID[0];
					String strID = strOneTypeID[1];
					int index = getIndex(strID, strType);
					if (index == -1)
					{
						return null;
					}
					Element lines = new Element("lines");

					String db1 = revName;
					if (db1 == null || db1 == "") 
					{
						return "";
					}
					int number = 1;
					while (true) 
					{
						String sqlCode = "select c_year,c_month,c_day,c_hour,c_minute,c_second,message from "
								+ db1 + " where number=?";
						PreparedStatement statement = con.prepareStatement(sqlCode);
						statement.setInt(1, number);
						number++;
						ResultSet result = statement.executeQuery();
						Time time = new Time();
						String message = "";
						if (!result.next()) 
						{
							break;
						} 
						else 
						{
							time.year = Integer.valueOf(result.getString(1));
							time.month = Integer.valueOf(result.getString(2));
							time.day = Integer.valueOf(result.getString(3));
							time.hour = Integer.valueOf(result.getString(4));
							time.minute = Integer.valueOf(result.getString(5));
							time.second = Double.valueOf(result.getString(6));
							message = result.getString(7);
							if (message.equals("")) 
							{
								return null;
							}
							result.close();
							String[] node = message.split("\n");

							Element point = new Element("point");

							Element x = new Element("x");

							Element year = new Element("year");
							year.addContent(String.valueOf(time.year));
							x.addContent(year);

							Element month = new Element("month");
							month.addContent(String.valueOf(time.month));
							x.addContent(month);

							Element day = new Element("day");
							day.addContent(String.valueOf(time.day));
							x.addContent(day);

							Element hour = new Element("hour");
							hour.addContent(String.valueOf(time.hour));
							x.addContent(hour);

							Element minute = new Element("minute");
							minute.addContent(String.valueOf(time.minute));
							x.addContent(minute);

							Element second = new Element("second");
							second.addContent(String.valueOf((int) (time.second)));
							x.addContent(second);

							point.addContent(x);
							Element y = new Element("y");

							String[] nodeMsg = node[index].split("\t");
							if (nodeMsg[0].equals("D")) 
							{
								y.addContent(String.valueOf(nodeMsg[2].charAt(0)));
								//y.addContent(String.valueOf(nodeMsg[4].charAt(0)));
							} 
							else 
							{
								y.addContent(String.valueOf(nodeMsg[3]));
								//y.addContent(String.valueOf(nodeMsg[3]));
							}
							point.addContent(y);

							lines.addContent(point);
						}
					}
					lineMessage.addContent(lines);
				}
			}
		}
		
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		XMLOutputter outputter = new XMLOutputter();
		String temsssss = outputter.outputString(new Document(lineMessage));
		return temsssss;
	}

	/**
	 * 获得故障结果
	 * @param db
	 * @return
	 */
	public static String getDbResult(String db) {
		if (con == null) {
			con = connection();
		}

		String db1 = null;

		try {
			con.setAutoCommit(true);
			String sqlCode = "select Max(name) from cisctable";
			Statement sta = con.createStatement();
			ResultSet temp = sta.executeQuery(sqlCode);
			if (temp.next()) {
				db1 = temp.getString(1);
				System.out.println(db1);
			} else {
				return null;
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return db1;
	}

	/**
	 * 获得变量对应的测点信息
	 * @param id 变量ID
	 * @param type 变量类型
	 * @return int
	 */
	public static int getIndex(String id, String type)
	{
		if (con == null) 
		{
			con = connection();
		}

		String cedian = "";

		try 
		{
			con.setAutoCommit(true);
			String sqlCode = "select cedian from com where com_id=? and com_type=?";
			PreparedStatement sta = con.prepareStatement(sqlCode);
			sta.setString(1, id);
			sta.setString(2, type);
			ResultSet result = sta.executeQuery();
			if (result.next()) 
			{
				cedian = result.getString(1);
			}
		} 
		catch (SQLException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		int index = -1;
		for (String temp : UDPClient.poingList) 
		{
			index++;
			if (temp.equals(cedian)) 
			{
				return index;
			}
		}
		return -1;
	}

}

/**
 * 测点类
 * @author SHU
 *
 */
class Tag 
{
	String cType; 	// 测点类型，’R’表示数字量点；’D’表示开关量点
	short iQuality; // 1表示坏点；0表示好点
	short iDValue; 	// cType为’D’时，表示测点的值；cType为’R’时，无效
	double fValue; 	// cType为’R’时，表示测点的值；cType为’D’时，无效
	String sValue; 	// cType为’D’时，表示测点的值；cType为’R’时，无效
	String sName;
	String sVarName;
	Time time = new Time();
	public void print() 
	{
		System.out.println("cType:" + cType);
		System.out.println("iQuality:" + iQuality);
		System.out.println("iDValue:" + iDValue);
		System.out.println("fValue:" + fValue);
		System.out.println("sValue:" + sValue);
		System.out.println("sName:" + sName);
		System.out.println("sVarName:" + sVarName);
		System.out.println("month:" + time.month);
		System.out.println("year:" + time.year);
		System.out.println("day:" + time.day);
		System.out.println("hour:" + time.hour);
		System.out.println("minute:" + time.minute);
		System.out.println("tzinfo:" + time.tzinfo);
		System.out.println("second:" + time.second);
	}

	/**
	 * 获得测点信息
	 * @return
	 */
	public String getMessage() 
	{
		String result = "";
		result = cType + "\t" + iQuality + "\t" + iDValue + "\t" + fValue + "\t" + sValue + "\t"
				+ sName + "\t" + sVarName + "\t" + time.month + "\t" + time.year + "\t" + time.day + "\t"
				+ time.hour + "\t" + time.minute + "\t" + time.tzinfo + "\t" + time.second;
		return result;
	}

}

/**
 * 时间类
 * @author SHU
 *
 */
class Time {
	int month; /* 1-12 */
	int year; /* four digit */
	int day; /* 1-31 */
	int hour; /* 0-23 */
	int minute; /* 0-59 */
	int tzinfo; /* timezone information */
	double second;
}

/**
 * 发送的测点的信息
 * @author SHU
 *
 */
class Node 
{
	short type;
	short id;
	short countOfInterval;
	ArrayList<Interval> intervals = new ArrayList<Interval>();
}

/**
 * 区间信息
 * @author SHU
 *
 */
class Interval 
{
	short id;
	float value1;
	float value2;
}
