package gct;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import java.net.*;
import java.util.ArrayList;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.zip.DataFormatException;

import java.util.*;

import gct.database.Change;

/**
 * 董力编写
 * @author SHU
 *
 */
public class UdpCom extends Thread 
{
	static LinkedBlockingQueue<LinkedList> queueSS = null;
	static HashMap hashPointVal = null;

	static String ipAddrGSC = "127.0.0.1";
	static String ipAddrInf = "127.0.0.1";
	
	static int iReceiveNum = 0;
	public void run() 
	{
		initRec();
	}

	public static void initRec() 
	{
		queueSS = new LinkedBlockingQueue<LinkedList>(16);
		hashPointVal = new HashMap(3000);
		receive();
	}

	// static String ipAddrYZ = "127.0.0.1";
	// static String ipAddrYZ = "192.168.133.100";

	public static void receive() 
	{
		int port = 6200;
		InetSocketAddress isac = new InetSocketAddress(port);
		DatagramSocket ds = null;
		try 
		{
			ds = new DatagramSocket(isac);
			ds.setReceiveBufferSize(2000 * 500 * 300);
		} 
		catch (SocketException e1) 
		{
			e1.printStackTrace();
		}

		
		while (true)
		{
			try 
			{
				byte[] buf = new byte[2000];
				DatagramPacket dp = new DatagramPacket(buf, 2000);// 创建长度为5000的数据接收包
				ds.receive(dp);// 套接字接受数据包
				iReceiveNum = ++iReceiveNum % 100000000;
				//System.out.println(iReceiveNum);
				byte[] data = dp.getData();
				int index = 0;
				for (int i = 0; i < (dp.getLength() / 152); i++) //144
				{
					byte[] input;
					Tag test = new Tag();
					int lenth = 1;
					// System.out.println();
					input = getInput(data, index, lenth);
					index = index + lenth + 1;
					test.cType = byteToString(input);

					lenth = 2;
					input = getInput(data, index, lenth);
					index = index + lenth;
					test.iQuality = byteToShort(input);
					
					lenth = 2;
					input = getInput(data, index, lenth);
					index = index + lenth + 2;
					test.iDValue = Change.byteToShort(input);

					lenth = 8;
					input = getInput(data, index, lenth);
					index += lenth;
					test.fValue = byteToDouble(input);

					lenth = 32;
					input = getInput(data, index, lenth);
					index += lenth;
					test.sValue = byteToString(input);

					lenth = 32;
					input = getInput(data, index, lenth);
					index += lenth;
					test.sName = byteToString(input);
					
					lenth = 32;
					input = getInput(data, index, lenth);
					index += lenth;
					test.sVarName = byteToString(input);
					
					lenth = 4;
					input = getInput(data, index, lenth);
					index += lenth;
					test.time.month = byteToInt(input);

					lenth = 4;
					input = getInput(data, index, lenth);
					index += lenth;
					test.time.year = byteToInt(input);

					lenth = 4;
					input = getInput(data, index, lenth);
					index += lenth;
					test.time.day = byteToInt(input);

					lenth = 4;
					input = getInput(data, index, lenth);
					index += lenth;
					test.time.hour = byteToInt(input);

					lenth = 4;
					input = getInput(data, index, lenth);
					index += lenth;
					test.time.minute = byteToInt(input);

					lenth = 4;
					input = getInput(data, index, lenth);
					index += lenth;
					test.time.tzinfo = byteToInt(input);

					lenth = 8;
					input = getInput(data, index, lenth);
					index += lenth;
					test.time.second = byteToDouble(input);

					lenth = 2;
					input = getInput(data, index, lenth);
					index += lenth + 6;
					test.iState = byteToShort(input);
					// test.print();
					// System.out.println(String.valueOf(hashPointVal.toString()));
					
					hashPointVal.put(test.sVarName, test);
				//	System.out.println(test.sName);
					/*
					 * if (hashPointVal.put(test.sName, test) == null)
					 * //System.out.println(test.sName + "++" // +
					 * String.valueOf(hashPointVal.size())); else
					 * System.out.println(String.valueOf(hashPointVal.size()) +
					 * "------" + test.sName + "***" + String.valueOf(numTest));
					 */
				}

				// 这里接收完成，我们给发送者一条消息，告诉他发送成功了
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
		}

	}

	public static void send() 
	{
		try 
		{
			DatagramSocket ds = new DatagramSocket();
			byte[] test1 = shortToByte((short) 10);
			DatagramPacket dp = new DatagramPacket(test1, test1.length,
					InetAddress.getByName(ipAddrGSC), 7700);
			ds.send(dp);

			ds.close();

		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}

	public static byte[] getInput(byte[] data, int begin, int lenth) 
	{
		byte[] result = new byte[lenth];
		for (int i = 0; i < lenth; i++) 
		{
			result[i] = data[begin + i];
		}
		return result;
	}

	public static String byteToString(byte[] b)
	{
		StringBuffer s1=new StringBuffer(32);
		for (int i=0;i<b.length ;i++ )
		{
			if(b[i]== 0)
			{
				break;
			}
			
		    s1.append((char)b[i]);

		}
		return s1.toString();
	}

	public static int byteToInt(byte[] b) throws DataFormatException 
	{
		if (b.length != 4)
			throw new DataFormatException();
		return (int) ((((b[3] & 0xff) << 24) | ((b[2] & 0xff) << 16)
				| ((b[1] & 0xff) << 8) | ((b[0] & 0xff) << 0)));
	}

	public static short byteToShort(byte[] b) throws DataFormatException 
	{
		if (b.length != 2)
			throw new DataFormatException();

		return (short) ((((b[1] & 0xff) << 8) | b[0] & 0xff));
	}

	public static byte[] shortToByte(int i) 
	{
		byte[] result = new byte[2];
		result[1] = (byte) ((i >> 8) & 0xFF);
		result[0] = (byte) (i & 0xFF);
		return result;
	}

	public static double byteToDouble(byte[] b) throws DataFormatException 
	{
		if (b.length != 8)
			throw new DataFormatException();
		long l;
		l = b[0];
		l &= 0xff;
		l |= ((long) b[1] << 8);
		l &= 0xffff;
		l |= ((long) b[2] << 16);
		l &= 0xffffff;
		l |= ((long) b[3] << 24);
		l &= 0xffffffffl;
		l |= ((long) b[4] << 32);
		l &= 0xffffffffffl;

		l |= ((long) b[5] << 40);
		l &= 0xffffffffffffl;
		l |= ((long) b[6] << 48);
		l &= 0xffffffffffffffl;

		l |= ((long) b[7] << 56);

		return Double.longBitsToDouble(l);
	}

	public static byte[] floatToByte(float d) 
	{

		byte[] bytes = new byte[4];
		int l = Float.floatToIntBits(d);
		for (int i = 0; i < bytes.length; i++) {
			bytes[i] = new Integer(l).byteValue();
			l = l >> 8;
		}
		return bytes;
	}

	public static float byteToFloat(byte[] b) throws DataFormatException {
		if (b.length != 4)
			throw new DataFormatException();
		int l;
		l = b[0];
		l &= 0xff;
		l |= ((long) b[1] << 8);
		l &= 0xffff;
		l |= ((long) b[2] << 16);
		l &= 0xffffff;
		l |= ((long) b[3] << 24);
		l &= 0xffffffffl;
		return Float.intBitsToFloat(l);
	}

	static byte[] getMessage(String[][] nodeMessage) {
		ArrayList<Node> node = new ArrayList<Node>();
		try {
			ByteArrayOutputStream buf = new ByteArrayOutputStream();
			DataOutputStream out = new DataOutputStream(buf);

			short test = 1;
			float test1 = (float) 0.5;
			out.writeShort((short) (1));
			out.writeShort(((short) (1)));
			out.writeShort(test);
			out.writeShort(test);
			out.writeShort(test);
			out.writeFloat(test1);
			test1 = (float) 1.5;
			out.writeFloat(test1);
			// out.writeShort(9999);

			byte[] result = buf.toByteArray();
			out.close();
			buf.close();
			return result;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}

	/*
	 * public static void main(String[] args) { openInf(); }
	 */

	static int infState = 0;

	public static int getInfState() {
		return infState;
	}

	public static void openInf() {
		if (infState == 0) {
			Process process = null;
			try {
				process = Runtime.getRuntime().exec(
						GetPathStr.getPath()
								+ "/DUCG/Inference/Run.bat");
				// for (int i = 0; i < 1; i++)
				// process = Runtime.getRuntime().exec(
				// "java " + "D:/Program Files/DUCG/Inference/ducg.jar");
				

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			infState = 1;
		}
	}

	public static void closeInf() {
		if (infState == 1) {
			try {
				DatagramSocket ds = new DatagramSocket();
				byte[] test1 = shortToByte((short) (-111));
				DatagramPacket dp = new DatagramPacket(test1, test1.length,	new InetSocketAddress("127.0.0.1", 6300));
				ds.send(dp);

				System.out.println("SEND OK");
//				ds.close();

			} catch (Exception e) {
				e.printStackTrace();
			}
			infState = 0;
		}
	}
}

class Tag 
{
	String cType; // 测点类型，’R’表示数字量点；’D’表示开关量点
	short iQuality; // 1表示坏点；0表示好点
	short iDValue; // cType为’D’时，表示测点的值；cType为’R’时，无效
	double fValue; // cType为’R’时，表示测点的值；cType为’D’时，无效
	String sValue; // cType为’D’时，表示测点的值；cType为’R’时，无效
	String sName;
	String sVarName;
	Time time = new Time();
	short iState;
	public void print() 
	{
		System.out.println("cType:" + cType);
		System.out.println("iQuality:" + iQuality);
		System.out.println("iDValue:" + iDValue);
		System.out.println("fValue:" + fValue);
		System.out.println("sValue:" + sValue);
		System.out.println("sName:" + sName);
		System.out.println("sVarName:" + sVarName);
		System.out.println("month:" + time.month);
		System.out.println("year:" + time.year);
		System.out.println("day:" + time.day);
		System.out.println("hour:" + time.hour);
		System.out.println("minute:" + time.minute);
		System.out.println("tzinfo:" + time.tzinfo);
		System.out.println("second:" + time.second);
	}
}

class Time 
{
	int month; /* 1-12 */
	int year; /* four digit */
	int day; /* 1-31 */
	int hour; /* 0-23 */
	int minute; /* 0-59 */
	int tzinfo; /* timezone information */
	double second;
}

class Node
{
	short type;
	short id;
	short countOfInterval;
	ArrayList<Interval> intervals = new ArrayList<Interval>();
}

class Interval 
{
	short id;
	float value1;
	float value2;
}
