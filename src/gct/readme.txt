EditVal.java
获取基本信息：public static String getBasicInfo(String vType, String vid)
获取状态信息：public static String getStGrid(String vType, String vid)
========================
VList.java
选区信息：public static String getSlctStr(String vType, String vid)

==============================
搜索目录下所有变量编号：public static int[] serachVals(String dir)
用法：
String pathB = GetPathStr.getPath() + "DUCG/B/";
String pathX = GetPathStr.getPath() + "DUCG/X/";
int[] vB = serachVals(pathB);
int[] vX = serachVals(pathX);
=================================
引jdom.jar这个包就可以读写xml，具体使用方法可以看这些类里面的例子或者直接看http://www.jdom.org/downloads/docs.html的IBM developerWorks的几篇文章，很简单很简单…稍微自己简单看下就肯定没问题。
======================================
xml文件结构样例：
<?xml version="1.0" encoding="UTF-8"?>
<val type="B" id="2" imgId="2"><basicInfo><vName>V2阀门状态</vName><vDscp>V2阀门的开关状态</vDscp><stNumber>3</stNumber><vKeyWords>状态;开关;V2;阀门</vKeyWords></basicInfo><stGridPrd stNumber="3"><state><stNumst>0</stNumst><stQujian>阀门关闭</stQujian><stDscp>v2阀门正常关闭</stDscp><stType>0</stType><stPPP /><stColor /></state><state><stNumst>1</stNumst><stQujian>阀门开启</stQujian><stDscp>v2阀门故障打开</stDscp><stType>1</stType><stPPP>0.0001</stPPP><stColor /></state><state><stNumst>2</stNumst><stQujian>阀门开启</stQujian><stDscp>v2阀门打开后不能回座</stDscp><stType>2</stType><stPPP>0.005</stPPP><stColor /></state></stGridPrd><slct><slctX0Per>0.34585288</slctX0Per><slctY0Per>0.36099586</slctY0Per><slctWidthPer>0.061032865</slctWidthPer><slctHeightPer>0.07676349</slctHeightPer><color1>65484</color1><color2>13369497</color2></slct></val>


董力12/24