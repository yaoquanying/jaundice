package gct;

import flex.messaging.io.ArrayCollection;
import flex.messaging.io.amf.ASObject;
import flex.messaging.io.amf.translator.ASTranslator;
import gct.UdpCom;

import java.io.*;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.SocketException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import gct.database.*;
import java.sql.*;
import java.util.*;
import java.util.HashMap;

import javax.mail.Flags.Flag;

import org.apache.xalan.xsltc.runtime.Hashtable;
import org.jdom.*;
import org.jdom.input.SAXBuilder;
import org.jdom.output.XMLOutputter;

import com.sun.org.apache.bcel.internal.classfile.Field;
import com.sun.org.apache.xerces.internal.dom.ParentNode;
import com.sun.org.apache.xpath.internal.operations.And;
import com.sun.org.apache.xpath.internal.operations.Bool;

import gct.database.UDPClient;

/**
 * 处理变量列表/测点列表的类
 * @author SHU
 *
 */
public class VList {
	
	/**
	 * 用于数据库连接
	 */
	private static Connection con = UtilsSQL.con;
	private static int oldComNum = 0;
	//bxs 清空recommend.txt
    /**
     * 删除单个文件
     * @param   sPath    被删除文件的文件名
     * @return 单个文件删除成功返回true，否则返回false
     */
    public boolean deleteFile(String patientID,String fileName) {
       boolean flag = false;
       File   file = new File(GetPathStr.getPathWeb()  + "KBGraph/"+patientID+"/"+fileName);
        // 路径为文件且不为空则进行删除
        if (file.isFile() && file.exists()) {
            file.delete();
            
            flag = true;
        }
       
        return flag;
    }
    public boolean writeFile(String patientID,String fileName) {
        boolean flag = false;
        File   file = new File(GetPathStr.getPathWeb()  + "KBGraph/"+patientID+"/"+fileName);
         // 路径为文件且不为空则进行删除
         if (file.isFile() && file.exists()) {
        	 
             try {
				FileWriter fw =new FileWriter(file);
				fw.write("stop");
				  fw.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
             flag = true;
         }
       
         return flag;
     }
	//bxs 与推理机通讯
	public  String receive(int flag){
		//UDP连接的Socket
		byte[] buffer = new byte[1000];
		try {
		InetSocketAddress isac = new InetSocketAddress("127.0.0.1", 6301);
		DatagramSocket dsoc = new DatagramSocket(isac);
		dsoc.setReceiveBufferSize(2000000);

		DatagramPacket dpack = new DatagramPacket(buffer, buffer.length);

		try {
		dsoc.receive(dpack);
		dsoc.close();
		} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		}
		} catch (SocketException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		}

		int count = pacHandler_f(buffer);
		System.out.print(count);
		return Integer.toString(count);
	}
	
	public int pacHandler_f(byte[] bytbuffer) {
		ByteBuffer bytbuf = ByteBuffer.wrap(bytbuffer);
		bytbuf.order(ByteOrder.LITTLE_ENDIAN);
		return bytbuf.getShort();
		}
	
	public  void sent(int flag,int port){
		System.out.println(flag);
		System.out.println(port);
		DatagramSocket dataSocket;
		DatagramPacket dataPacket;
		byte sendDataByte[];
		String sendStr;
		try {
			// 指定端口号，避免与其他应用程序发生冲突
			
			dataSocket = new DatagramSocket(port);
			sendDataByte = new byte[1024];

			ByteBuffer chearFlag = ByteBuffer.allocate(10);
			chearFlag.order(ByteOrder.LITTLE_ENDIAN);
			short flagData = (short)flag;
			chearFlag.putShort(flagData);
			byte[] fuyi = chearFlag.array();
			dataPacket = new DatagramPacket(fuyi, fuyi.length,
					InetAddress.getByName("127.0.0.1"), port);

			dataSocket.send(dataPacket);
		  	dataSocket.close();
   
		} 
		catch (SocketException se) 
		{
			se.printStackTrace();
		} 
		catch (IOException ie) 
		{
			ie.printStackTrace();
		}
	}
	//
	/** 
	 * 一步预测
	 * @return
	 */
	public String firstYuce()
	{
		if (!checkFileHasContent("KBGraph/FirstPredict"))
		{
			return "";
		}
		FileReader fr;
		List listNodes = null;
		try 
		{
			Element root = new Element("vList");
			fr = new FileReader(GetPathStr.getPathWeb() + "KBGraph/FirstPredict");
			BufferedReader br = new BufferedReader(fr);
			Document docGraphWYH;
			try 
			{
				SAXBuilder builder = new SAXBuilder();
				String pathF = GetPathStr.getPathWeb() + "KBGraph/graph.xml";
				// System.out.println(pathF);
				docGraphWYH = builder.build(new File(pathF));
				listNodes = docGraphWYH.getRootElement().getChildren("nodes");
			}
			catch (JDOMException e) 
			{
				e.printStackTrace();
			}
			catch (NullPointerException e)
			{
				e.printStackTrace();
			}
			String allYuceVarStr = "";
			while (br.ready())
			{
				Element val = new Element("val");
				String strRd = br.readLine();
				if (strRd == null || strRd.compareTo("") == 0) 
				{
					continue;
				}
				// System.out.println(strRd + "--");
				String[] rstArr = strRd.split(" ");
				String numFK = rstArr[0].split(":")[1];
				// System.out.println(rstArr.length);
				String typeFK = "";
				String stateFK = rstArr[1].split(":")[1];

				Element eleT;
				if (listNodes == null)
				{
					return "";
				}  
				// System.out.println("--"+listNodes.size());
				for (int i = 0; i < listNodes.size(); i++)
				{
					eleT = (Element) (listNodes.get(i));
					// System.out.println(eleT.getAttributeValue("id")+"++");
					if (eleT.getAttributeValue("id").compareTo(numFK) == 0)
					{
						String strCnum = eleT.getAttributeValue("cnum");
						// System.out.println(strCnum+'-');
						int intCnum = Integer.valueOf(strCnum);
						if (intCnum >= 30000)
						{
							intCnum -= 30000;
						}
						else if (intCnum >= 20000) 
						{
							intCnum -= 20000;
						} 
						else if (intCnum >= 10000) 
						{
							intCnum -= 10000;
						}
						numFK = String.valueOf(intCnum);

						typeFK = eleT.getAttributeValue("type");
						break;
					}

				}

				if (typeFK == "")
				{
					System.out.println("don't find the YuCe");
				}
				String strVarExist[] = allYuceVarStr.split("_");
				boolean isFind = false;
				for (int k = 0; k < strVarExist.length; k++)
				{
					String strtemp = typeFK + numFK;
					if (strVarExist[k].compareTo(strtemp) == 0)
					{
						isFind = true;
						break;
					}
				}
				if (isFind)
				{
					continue;
				}

				Element vBadListType = new Element("vBadListType");
				vBadListType.addContent(typeFK);
				val.addContent(vBadListType);

				Element vBadListState = new Element("vBadListState");
				vBadListState.addContent(stateFK);
				val.addContent(vBadListState);

				Element vBadListNum = new Element("vBadListNum");
				vBadListNum.addContent(numFK);
				val.addContent(vBadListNum);

				try 
				{
					con.setAutoCommit(true);
					Statement statement = con.createStatement();
					String sqlCode = "select com_des, com_name from com where com_id='"
							+ numFK + "' and com_type='" + typeFK + "'";
					ResultSet result = statement.executeQuery(sqlCode);
					if (result.next()) 
					{
						Element vBadListName = new Element("vBadListName");
						vBadListName.addContent(result.getString(1));
						val.addContent(vBadListName);

						Element vBadListDscp = new Element("vBadListDscp");
						vBadListDscp.addContent(result.getString(2));
						val.addContent(vBadListDscp);
					}

					statement = con.createStatement();
					sqlCode = "select state_des " + "from state where com_id='"	+ numFK + "' and com_type='" + typeFK
							+ "' and state_id='" + stateFK + "'";
					result = statement.executeQuery(sqlCode);
					if (result.next()) {
						Element vBadListStateDscp = new Element("vBadListStateDscp");
						String strT = result.getString(1);

						if (strT == null || strT.compareTo("") == 0)
						{
							vBadListStateDscp.addContent("_");
						}
						else
						{
							vBadListStateDscp.addContent(strT);
						}
						val.addContent(vBadListStateDscp);
					}
				} 
				catch (SQLException e) 
				{
					e.printStackTrace();
				}

				if (typeFK.compareTo("U") == 0)
				{
					typeFK = "BX";
				}
				String strTypeID = typeFK + numFK;
				Element vBadListTypeID = new Element("vBadListTypeID");
				vBadListTypeID.addContent(strTypeID);
				val.addContent(vBadListTypeID);
				
				allYuceVarStr = allYuceVarStr + strTypeID + "_";
				root.addContent(val);
			}
			// System.out.println(strRet);
			// return strRet;
			Document slctDoc = new Document(root);
			XMLOutputter outputter = new XMLOutputter();
			// System.out.println(outputter.outputString(slctDoc));
			return outputter.outputString(slctDoc);
		} catch (IOException e) {
			e.printStackTrace();
			return "";
		}
	}

	/**
	 * 两步预测
	 * @return
	 */
	public String secondYuce() {
		if (!checkFileHasContent("KBGraph/SecondPredict")) {
			return "";
		}
		FileReader fr;
		List listNodes = null;
		try 
		{
			Element root = new Element("vList");
			String strRet = "";
			fr = new FileReader(GetPathStr.getPathWeb() + "KBGraph/SecondPredict");
			BufferedReader br = new BufferedReader(fr);
			Document docGraphWYH;
			try
			{
				SAXBuilder builder = new SAXBuilder();
				String pathF = GetPathStr.getPathWeb() + "KBGraph/graph.xml";
				// System.out.println(pathF);
				docGraphWYH = builder.build(new File(pathF));
				listNodes = docGraphWYH.getRootElement().getChildren("nodes");
			}
			catch (JDOMException e)
			{
				e.printStackTrace();
			} 
			catch (NullPointerException e)
			{
				e.printStackTrace();
			}
			String allYuceVarStr = "";
			while (br.ready()) 
			{
				Element val = new Element("val");
				String strRd = br.readLine();
				if (strRd == null || strRd.compareTo("") == 0) 
				{
					continue;
				}
				// System.out.println(strRd + "--");
				String[] rstArr = strRd.split(" ");
				String numFK = rstArr[0].split(":")[1];
				// System.out.println(rstArr.length);
				String typeFK = "";
				String stateFK = rstArr[1].split(":")[1];

				Element eleT;
				if (listNodes == null) 
				{
					return "";
				}
				// System.out.println("--"+listNodes.size());
				for (int i = 0; i < listNodes.size(); i++) 
				{
					eleT = (Element) (listNodes.get(i));
					// System.out.println(eleT.getAttributeValue("id")+"++");
					if (eleT.getAttributeValue("id").compareTo(numFK) == 0) 
					{
						String strCnum = eleT.getAttributeValue("cnum");
						// System.out.println(strCnum+'-');
						int intCnum = Integer.valueOf(strCnum);
						if (intCnum >= 30000)
						{
							intCnum -= 30000;
						}
						else if (intCnum >= 20000) 
						{
							intCnum -= 20000;
						} 
						else if (intCnum >= 10000) 
						{
							intCnum -= 10000;
						}
						numFK = String.valueOf(intCnum);

						typeFK = eleT.getAttributeValue("type");
						break;
					}

				}

				if (typeFK == "") 
				{
					System.out.println("don't find the YuCe");
				}
				String strVarExist[] = allYuceVarStr.split("_");
				boolean isFind = false;
				for (int k = 0; k < strVarExist.length; k++)
				{
					String strtemp = typeFK + numFK;
					if (strVarExist[k].compareTo(strtemp) == 0)
					{
						isFind = true;
						break;
					}
				}
				if (isFind)
				{
					continue;
				}
				
				
				Element vBadListType = new Element("vBadListType");
				vBadListType.addContent(typeFK);
				val.addContent(vBadListType);

				Element vBadListState = new Element("vBadListState");
				vBadListState.addContent(stateFK);
				val.addContent(vBadListState);

				Element vBadListNum = new Element("vBadListNum");
				vBadListNum.addContent(numFK);
				val.addContent(vBadListNum);

				try
				{
					con.setAutoCommit(true);
					Statement statement = con.createStatement();
					String sqlCode = "select com_des, com_name from com where com_id='"
							+ numFK + "' and com_type='" + typeFK + "'";
					ResultSet result = statement.executeQuery(sqlCode);
					if (result.next())
					{
						Element vBadListName = new Element("vBadListName");
						vBadListName.addContent(result.getString(1));
						val.addContent(vBadListName);

						Element vBadListDscp = new Element("vBadListDscp");
						vBadListDscp.addContent(result.getString(2));
						val.addContent(vBadListDscp);
					}

					statement = con.createStatement();
					sqlCode = "select state_des " + "from state where com_id='"
							+ numFK + "' and com_type='" + typeFK
							+ "' and state_id='" + stateFK + "'";
					result = statement.executeQuery(sqlCode);
					if (result.next()) {
						Element vBadListStateDscp = new Element(
								"vBadListStateDscp");
						String strT = result.getString(1);

						if (strT == null || strT.compareTo("") == 0)
							vBadListStateDscp.addContent("_");
						else
							vBadListStateDscp.addContent(strT);
						val.addContent(vBadListStateDscp);
					}
				} catch (SQLException e) {
					e.printStackTrace();
				}
				
				if (typeFK.compareTo("U") == 0)
				{
					typeFK = "BX";
				}
				String strTypeID = typeFK + numFK;
				Element vBadListTypeID = new Element("vBadListTypeID");
				vBadListTypeID.addContent(strTypeID);
				val.addContent(vBadListTypeID);

				allYuceVarStr = allYuceVarStr + strTypeID + "_";
				root.addContent(val);
			}
			// System.out.println(strRet);
			// return strRet;
			Document slctDoc = new Document(root);
			XMLOutputter outputter = new XMLOutputter();
			// System.out.println(outputter.outputString(slctDoc));
			return outputter.outputString(slctDoc);
		} catch (IOException e) {
			e.printStackTrace();
			return "";
		}

	}

	/**
	 *  检查文件是否为空
	 * @param filePath 
	 * @return
	 */
	public static boolean checkFileHasContent(String filePath) {
		InputStream in = null;
		try {
			in = new FileInputStream(GetPathStr.getPathWeb()  + filePath);
			
			if (in.read() == -1) {
				return false;
			} else {
				return true;
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return false;   
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
	}
	/**
	 * bxs
	 * 2016.6.29
	 * 将医生选择的带检测结果写到recommend.txt
	 * 
	 * **/
	public static Boolean saveRecommend(ArrayCollection array,String patientID){
	
		//
		File file = new File(GetPathStr.getPathWeb()  + "KBGraph/"+patientID+"/recommend.txt");	
		try {
			FileWriter fw = new FileWriter(file);
			
			for(int i=0;i<array.size();i++){
				ASObject asObj = (ASObject) array.get(i);
				String temp=asObj.get("variable")+";"
				+asObj.get("testvalue")+";"
				+asObj.get("probability")+";"
				+asObj.get("structure")+";"
				+asObj.get("cost")+";"
				+asObj.get("selected")+"\r\n";
				//System.out.print(temp);
				fw.write(temp);
			//	System.out.println(asObj.get("variable"));
			//	System.out.println(asObj.get("testvalue"));
			//	System.out.println(asObj.get("probability"));
			//	System.out.println(asObj.get("structure"));
			//	System.out.println(asObj.get("cost"));
			//	System.out.println(asObj.get("selected"));
			}
			
			fw.close();
			return true;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
		return false;
	}
	/****
	 * bxs
	 * 2016.6.29
	 * 读取推荐检查的数据 
	 * */
	public static String getRecommend(String patientID){
		File file = new File(GetPathStr.getPathWeb()  + "KBGraph/" + patientID + "/recommend.txt");	
		if(file.length() == 0){
			return "noContent";
		}
		try {
			FileReader fr = new FileReader(file);
			
			BufferedReader br = new BufferedReader (fr);
			
			Element root = new Element("vList");
			while(br.ready()){
				String temp =br.readLine();
				
				if (temp == null || temp.compareTo("") == 0||temp.trim().equals("yes")) 
				{
					continue;
				}else if(temp.trim().equals("no")){
					br.close();
					fr.close();
				return "no";	
				}else if(temp.trim().equals("stop")){
					br.close();
					fr.close();
				return "stop";	
				}
				Element val = new Element("val");
				String[] arr = temp.split(";");
				//变量
				String variable = arr[0];
				Element evariable = new Element("variable");
				evariable.addContent(variable);
				val.addContent(evariable);
				//拆解变量，分为类型和number作查询
				String vtype=variable.substring(0, 1);
				if(variable.substring(0, 1).equals("S")){
					vtype="X";
				}
				String vnum=variable.substring(1);
				Element evnum = new Element("vListID");
				evnum.addContent(vnum);
				val.addContent(evnum);
				  
				//查询变量具体名称
				Statement statement = con.createStatement();
				String sqlCode = "select com_des, com_name,one,two from com where com_id='"
						+ vnum + "' and com_type='" + vtype + "'";
				ResultSet result = statement.executeQuery(sqlCode);
				while(result.next()){
					//节点名称
					String vname=result.getString(2);
					Element evName = new Element("vListValName");
					evName.addContent(vname);
					val.addContent(evName);
					//节点描述
					Element evListDes = new Element("vListDes");
					evListDes.addContent(result.getString(1));
					val.addContent(evListDes);
					//类型2
					Element vListTwo = new Element("vListTwo");
					vListTwo.addContent(result.getString(4));
					val.addContent(vListTwo);
					//类型1
					Element vListOne = new Element("vListOne");
					vListOne.addContent(result.getString(3));
					val.addContent(vListOne);
				}
												
				//检测价值
				String testvalue =arr[1];
				double value=Double.parseDouble(testvalue);
				if(value<0.01){
					testvalue="<0.01";
				}else{
				java.text.DecimalFormat   df=new   java.text.DecimalFormat("###.##");   
				testvalue=df.format(value);
				
				}
				
				Element etestvalue = new Element("testvalue");
				etestvalue.addContent(testvalue);
				val.addContent(etestvalue);
				//症状初始状态
				Element vListNumst = new Element("vListNumst");
				vListNumst.addContent("-1");
				val.addContent(vListNumst);
				//症状初始显示
				Element vListstDes = new Element("vListstDes");
				vListstDes.addContent("未知");
				val.addContent(vListstDes);
				
				//概率重要度
				String probability =arr[2];
				double valpro=Double.parseDouble(probability);
				if(valpro<0.001){
					probability="<0.001";
				}else{
				java.text.DecimalFormat   df=new   java.text.DecimalFormat("#.###");   
				probability=df.format(value);
				
				}
				//
				Element eprobability = new Element("probability");
				eprobability.addContent(probability);
				val.addContent(eprobability);
				
				//结构重要度
				String structure =arr[3];
				Element estructure = new Element("structure");
				estructure.addContent(structure);
				val.addContent(estructure);
				
				//检测代价
				String cost=arr[4];
				Element ecost = new Element("cost");
				ecost.addContent(cost);
				val.addContent(ecost);
				
				//是否已检测
				String check=arr[5];
				Element echeck = new Element("selected");
				
				if(check.equals("0")||check==""){
					check="false";
					
				}else if(check.equals("1")){
					check="true";
				}else{}
				
				echeck.addContent(check);
				val.addContent(echeck);
				
				//封装每一行成为一个val
				root.addContent(val);
			}
			
			br.close();
	        fr.close();
	        
	        Document slctDoc = new Document(root);
	        XMLOutputter outputter = new XMLOutputter();
	        return outputter.outputString(slctDoc);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.print("file no found");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "hello";
		
	}
	
	public static void CleanResult(String patientID)
	{
		try {
			File file = new File(GetPathStr.getPathWeb()  + "KBGraph/"+patientID+"/result.txt");	
			if(file.exists()){
			FileWriter fw;
			fw = new FileWriter(file);
			fw.write("");
			fw.close();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * 从Result.txt中获取推理出来的故障信息
	 * @param strImageId
	 * @return
	 */
	public static String getInfResult(String strImageId, String strVarType, String strVarID, String strCurState,String patientID)
	{
		// System.out.println("infRst");
		
		String path="KBGraph/"+patientID+"/result.txt";
		//System.out.println(GetPathStr.getPathWeb()+path);
	    File file = new File(GetPathStr.getPathWeb()+path);
	    if(!file.exists()){
	    	
	    	return "";
	    }
			
		FileReader fr;
		FileReader frIgnore;
		List listNodes = null;
		try 
		{
			Element root = new Element("vList");
			
			if (strVarType.compareTo("B") == 0)
			{
				fr = new FileReader(GetPathStr.getPathWeb() + path);
				
			}
			else
			{
				String strFilePath = "KBGraph/"+patientID+"/Result/U" + strVarID + "," + strCurState + ".txt";
				
				/*String strFilePath = "KBGraph/Result/U" + strVarID + "," + strCurState + ".txt";*/
				//添加住院号
				String strCheckPath = strFilePath.substring(5);
				if (!checkFileHasContent(strFilePath))
				{
					return "-1";
				}
				fr = new FileReader(GetPathStr.getPathWeb() + strFilePath);
			}

			frIgnore = new FileReader(GetPathStr.getPathWeb() + "KBGraph/"+patientID+"/ignore");
			BufferedReader br = new BufferedReader(fr);
			BufferedReader brIgnore = new BufferedReader(frIgnore);
			Document docGraphWYH;
			try 
			{
				SAXBuilder builder = new SAXBuilder();
				String pathF = GetPathStr.getPathWeb() + "KBGraph/graph.xml";
				// System.out.println(pathF);
				docGraphWYH = builder.build(new File(pathF));
				listNodes = docGraphWYH.getRootElement().getChildren("nodes");
			} 
			catch (JDOMException e) 
			{
				e.printStackTrace();
			}
			catch (NullPointerException e) 
			{
				e.printStackTrace();
			}
			int lineNum = 0;
			while (br.ready()) 
			{
				
				String strIgnoreInfo = "";
				String strRd = br.readLine();
				if (strRd == null || strRd.compareTo("") == 0) 
				{
					continue;
				}
				String strIgnore = "";
				if(brIgnore.ready())
				{
					strIgnore = brIgnore.readLine().split(" ")[0];
				}
				// System.out.println(strRd + "--");
				String[] rstArr = strRd.split(" ");
				String numFK = rstArr[0];
				// System.out.println(rstArr.length);
				String typeFK = rstArr[1];
				String stateFK = rstArr[2];
				String probFK = rstArr[3];
				Double changStr=Double.parseDouble(probFK);
				if(changStr>0.001){
				java.text.DecimalFormat   df=new   java.text.DecimalFormat("#0.000");   
					     
				probFK=  df.format(changStr).toString()+"%";
					
				}else{
					
					probFK =probFK+"%";
				}
				
				
				for (int iNum = 3; iNum < rstArr.length; iNum++)
				{
					Element val = new Element("val");
					if (iNum == 3)
					{
						numFK = rstArr[0];
						stateFK = rstArr[2];
					}
					else
					{
						numFK = rstArr[iNum].split(",")[0];
						stateFK = rstArr[iNum].split(",")[1];
					}
					
					Element varGraphID = new Element("varGraphID");	//记录在graph.XML文件中的ID。
					varGraphID.addContent(numFK);
					val.addContent(varGraphID);
					
					if (typeFK.compareTo("D") == 0)
					{
					} 
					else
					{
						Element eleT;
						if (listNodes == null) 
						{
							return "";
						}
						// System.out.println("--"+listNodes.size());
						
						if(!strIgnore.equals(""))
						{
							String strTemp[] = strIgnore.split(",");
							if (strTemp.length > 1)
							{
								strIgnore = strTemp[0];
								for(int i = 0; i < listNodes.size(); i++)
						    	{
							    	Element eleIgnore = (Element)(listNodes.get(i));
							    	if(eleIgnore.getAttributeValue("id").compareTo(strIgnore) == 0)
						    		{
							    		strIgnoreInfo = eleIgnore.getAttributeValue("name");
						    		}
						    	}
								strIgnore = strTemp[1];
								for(int i = 0; i < listNodes.size(); i++)
						    	{
							    	Element eleIgnore = (Element)(listNodes.get(i));
							    	if(eleIgnore.getAttributeValue("id").compareTo(strIgnore) == 0)
						    		{
							    		strIgnoreInfo = strIgnoreInfo + "和" + eleIgnore.getAttributeValue("name");
						    		}
						    	}
							}
							else
							{
								strIgnore = strTemp[0];
								for(int i = 0; i < listNodes.size(); i++)
						    	{
							    	Element eleIgnore = (Element)(listNodes.get(i));
							    	if(eleIgnore.getAttributeValue("id").compareTo(strIgnore) == 0)
						    		{
							    		strIgnoreInfo = eleIgnore.getAttributeValue("name");
						    		}
						    	}
							} 
						}
						for (int i = 0; i < listNodes.size(); i++) 
						{
							eleT = (Element) (listNodes.get(i));
							// System.out.println(eleT.getAttributeValue("id")+"++");
							if (eleT.getAttributeValue("id").compareTo(numFK) == 0) 
							{
								String strCnum = eleT.getAttributeValue("cnum");
								// System.out.println(strCnum+'-');
								int intCnum = Integer.valueOf(strCnum);
								if (intCnum >= 30000)
								{
									intCnum -= 30000;
								}
								else if (intCnum >= 20000)
								{
									intCnum -= 20000;
								}
								else if (intCnum >= 10000)
								{
									intCnum -= 10000;
								}
								numFK = String.valueOf(intCnum);
								break;
							}
						}
					}
					
					String strtype = typeFK;
					if (strtype.compareTo("U") == 0)
					{
						strtype = "BX";
					}
					String strTypeID = strtype + numFK;

					Element vBadListTypeID = new Element("vBadListTypeID");
					vBadListTypeID.addContent(strTypeID);
					val.addContent(vBadListTypeID);
					
					Element vBadListType = new Element("vBadListType");
					vBadListType.addContent(strtype);
					val.addContent(vBadListType);

					Element vBadListNum = new Element("vBadListNum");
					vBadListNum.addContent(numFK);
					val.addContent(vBadListNum);
					
					Element vBadListState = new Element("vBadListState");
					vBadListState.addContent(stateFK);
					val.addContent(vBadListState);

					Element vBadListPrb = new Element("vBadListPrb");
					vBadListPrb.addContent(probFK);
					val.addContent(vBadListPrb);

					Element lineNumEle = new Element("lineNum");
					lineNumEle.addContent(String.valueOf(lineNum));
					val.addContent(lineNumEle);
					
					try
					{
						con.setAutoCommit(true);
						Statement stat = con.createStatement();
						String sqlCode = "select image_name from image, comimage where image.image_id = comimage.image_id" 
								+ " and comimage.com_id='"
								+ numFK + "' and comimage.com_type='" + typeFK + "' and comimage.image_id!='"
								+ strImageId + "'";
						ResultSet result = stat.executeQuery(sqlCode);
						String strResult = "";
						while(result.next())
						{
							if(strResult.equals(""))
							{
								strResult = result.getString(1);
							}
							else
							{
								strResult = strResult + ";" + result.getString(1);
							}
						}
						if(strResult.equals(""))
						{
							strResult = "在其它图上无此变量选框";
						}
						Element vOtherImage= new Element("vOtherImage");
						vOtherImage.addContent(strResult);
						val.addContent(vOtherImage);
					}
					catch (SQLException e) 
					{
						e.printStackTrace();
					}

					try 
					{
						con.setAutoCommit(true);
						Statement statement = con.createStatement();
						String sqlCode = "select com_des ,com_name from com where com_id='"
								+ numFK + "' and com_type='" + typeFK + "'";
						ResultSet result = statement.executeQuery(sqlCode);
						if (result.next())
						{
							Element vBadListName = new Element("vBadListName");
							vBadListName.addContent(result.getString(1));
							val.addContent(vBadListName);

							Element vBadListDscp = new Element("vBadListDscp");
							if(strIgnoreInfo.equals(""))
							{
							    vBadListDscp.addContent(result.getString(2));
							}
							else
							{
								vBadListDscp.addContent(result.getString(2) + "(问题信号：" + strIgnoreInfo + ")");
							}
							val.addContent(vBadListDscp);
						}

						statement = con.createStatement();
						sqlCode = "select state_des from state where com_id='" + numFK
								+ "' and com_type='" + typeFK
								+ "' and state_id='" + stateFK + "'";
						result = statement.executeQuery(sqlCode);
						if (result.next())
						{
							Element vBadListStateDscp = new Element("vBadListStateDscp");
							String strT = result.getString(1);
							
							if (strT == null || strT.compareTo("") == 0)
							{
								vBadListStateDscp.addContent("_");
							}
							else
							{	
								vBadListStateDscp.addContent(strT);
							}
							
							val.addContent(vBadListStateDscp);
						}
					} 
					catch (SQLException e)
					{
						e.printStackTrace();
					}

					root.addContent(val);
				}
				lineNum++;
			}
			//bxs，修改bug，文件读取后未关闭的bug
             br.close();
             fr.close();
			Document slctDoc = new Document(root);
			XMLOutputter outputter = new XMLOutputter();
			// System.out.println(outputter.outputString(slctDoc));
			return outputter.outputString(slctDoc);
		}
		catch (IOException e) 
		{
			e.printStackTrace();
			 
            
			return "";
		}
	}

	/**
	 * 每当在2维模式下更改工程图的时候，重新设置一下BX故障列表
	 */
	public String ReGetVarList(String strImageId, String vBXTypeID)
	{
		if (con == null) 
		{
			return null;
		}
		try 
		{
			con.setAutoCommit(true);
			Statement statement = con.createStatement();
			String []vBCount = vBXTypeID.split("-");
			String otherImage = "";
			for (int i = 0; i < vBCount.length; i++)
			{
				String strTypeID[] = vBCount[i].split("_");//存放变量编号和类型的数组，直接分开即可，不用循环
				String sqlCode = "select image_name from image, comimage where image.image_id = comimage.image_id" 
									+ " and comimage.com_id='" + strTypeID[1] + "' and comimage.com_type='" + strTypeID[0] 
									+ "' and comimage.image_id!='" + strImageId + "'";
				ResultSet result = statement.executeQuery(sqlCode);
				String strResult = "";
				while(result.next())
				{
					if(strResult.equals(""))
					{
						strResult = result.getString(1);
					}
					else
					{
						strResult = strResult + ";" + result.getString(1);
					}
				}
				if(strResult.equals(""))
				{
					strResult = "在其它图上无此变量选框";
				}			
				otherImage = otherImage + strResult + "_";
			}
			return otherImage;
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			return "";
		}
	}
	
	/**
	 * 姚全营编写
	 * 获得预测信息
	 * @param strImageId
	 * @return
	 */
	public static String getForcastInfResult(String strImageId)
	{
		// System.out.println("infRst");
		if (!checkFileHasContent("KBGraph/fresult.txt"))
		{
			return "";
		}
		FileReader fr;
		List listNodes = null;
		try 
		{
			Element root = new Element("vList");
			fr = new FileReader(GetPathStr.getPathWeb() + "KBGraph/fresult.txt");
			
			BufferedReader br = new BufferedReader(fr);
			Document docGraphWYH;
			try 
			{
				SAXBuilder builder = new SAXBuilder();
				String pathF = GetPathStr.getPathWeb() + "KBGraph/graph.xml";
				// System.out.println(pathF);
				docGraphWYH = builder.build(new File(pathF));
				listNodes = docGraphWYH.getRootElement().getChildren("nodes");
			} catch (JDOMException e) {
				e.printStackTrace();
			} catch (NullPointerException e) {
				e.printStackTrace();
			}
			int lineNum = 0;
			while (br.ready()) 
			{
				
				String strIgnoreInfo = "";
				String strRd = br.readLine();
				if (strRd == null || strRd.compareTo("") == 0) 
				{
					continue;
				}
				String strIgnore = "";
//				if(brIgnore.ready())
//				{
//					strIgnore = brIgnore.readLine().split(" ")[0];
//				}
				// System.out.println(strRd + "--");
				String[] rstArr = strRd.split(" ");
				String numFK = rstArr[0];
				// System.out.println(rstArr.length);
				String typeFK = rstArr[1];
				String stateFK = rstArr[2];
				String probFK = rstArr[3] + "倍";
				for (int iNum = 3; iNum < rstArr.length; iNum++)
				{
					Element val = new Element("val");
					if (iNum == 3)
					{
						numFK = rstArr[0];
						stateFK = rstArr[2];
					}
					else
					{
						numFK = rstArr[iNum].split(",")[0];
						stateFK = rstArr[iNum].split(",")[1];
					}

					if (typeFK.compareTo("D") == 0)
					{
					} 
					else
					{
						Element eleT;
						if (listNodes == null) 
						{
							return "";
						}
						// System.out.println("--"+listNodes.size());
						
						if(!strIgnore.equals(""))
						{
							String strTemp[] = strIgnore.split(",");
							if (strTemp.length > 1)
							{
								strIgnore = strTemp[0];
								for(int i = 0; i < listNodes.size(); i++)
						    	{
							    	Element eleIgnore = (Element)(listNodes.get(i));
							    	if(eleIgnore.getAttributeValue("id").compareTo(strIgnore) == 0)
						    		{
							    		strIgnoreInfo = eleIgnore.getAttributeValue("name");
						    		}
						    	}
								strIgnore = strTemp[1];
								for(int i = 0; i < listNodes.size(); i++)
						    	{
							    	Element eleIgnore = (Element)(listNodes.get(i));
							    	if(eleIgnore.getAttributeValue("id").compareTo(strIgnore) == 0)
						    		{
							    		strIgnoreInfo = strIgnoreInfo + "和" + eleIgnore.getAttributeValue("name");
						    		}
						    	}
							}
							else
							{
								strIgnore = strTemp[0];
								for(int i = 0; i < listNodes.size(); i++)
						    	{
							    	Element eleIgnore = (Element)(listNodes.get(i));
							    	if(eleIgnore.getAttributeValue("id").compareTo(strIgnore) == 0)
						    		{
							    		strIgnoreInfo = eleIgnore.getAttributeValue("name");
						    		}
						    	}
							}
						}
						for (int i = 0; i < listNodes.size(); i++) 
						{
							eleT = (Element) (listNodes.get(i));
							// System.out.println(eleT.getAttributeValue("id")+"++");
							if (eleT.getAttributeValue("id").compareTo(numFK) == 0) 
							{
								String strCnum = eleT.getAttributeValue("cnum");
								// System.out.println(strCnum+'-');
								int intCnum = Integer.valueOf(strCnum);
								if (intCnum >= 20000)
								{
									intCnum -= 20000;
								}
								else if (intCnum >= 10000)
								{
									intCnum -= 10000;
								}
								numFK = String.valueOf(intCnum);
								break;
							}
						}
					}

					Element vBadListType = new Element("vBadListType");
					vBadListType.addContent(typeFK);
					val.addContent(vBadListType);

					Element vBadListState = new Element("vBadListState");
					vBadListState.addContent(stateFK);
					val.addContent(vBadListState);

					Element vBadListPrb = new Element("vBadListPrb");
					vBadListPrb.addContent(probFK);
					val.addContent(vBadListPrb);

					Element vBadListNum = new Element("vBadListNum");
					vBadListNum.addContent(numFK);
					val.addContent(vBadListNum);

					Element lineNumEle = new Element("lineNum");
					lineNumEle.addContent(String.valueOf(lineNum));
					val.addContent(lineNumEle);
					
					//////////////变量所在工程图信息//////////////////////
					try
					{
						con.setAutoCommit(true);
						Statement stat = con.createStatement();
						String sqlCode = "select image_name from image, comimage where image.image_id = comimage.image_id" 
							+ " and comimage.com_id='"
								+ numFK + "' and comimage.com_type='" + typeFK + "' and comimage.image_id!='"
								+strImageId + "'";
						ResultSet result = stat.executeQuery(sqlCode);
						String strResult = "";
						while(result.next())
						{
							if(strResult.equals(""))
							{
								strResult = result.getString(1);
							}
							else
							{
								strResult = strResult + ";" + result.getString(1);
							}
						}
						if(strResult.equals(""))
						{
							strResult = "在其它图上无此变量选框";
						}
						Element vOtherImage= new Element("vOtherImage");
						vOtherImage.addContent(strResult);
						val.addContent(vOtherImage);
					}
					catch (SQLException e)
					{
						e.printStackTrace();
					}
					
					///////////////状态描述////////////////
					try 
					{
						con.setAutoCommit(true);
						Statement statement = con.createStatement();
						String sqlCode = "select com_des, com_name from com where com_id='"
								+ numFK + "' and com_type='" + typeFK + "'";
						ResultSet result = statement.executeQuery(sqlCode);
						if (result.next())
						{
							Element vBadListName = new Element("vBadListName");
							vBadListName.addContent(result.getString(1));
							val.addContent(vBadListName);

							Element vBadListDscp = new Element("vBadListDscp");
							if(strIgnoreInfo.equals(""))
							{
							    vBadListDscp.addContent(result.getString(2));
							}
							else
							{
								vBadListDscp.addContent(result.getString(2) + "(问题信号：" + strIgnoreInfo + ")");
							}
							val.addContent(vBadListDscp);
						}

						statement = con.createStatement();
						sqlCode = "select state_des from state where com_id='" + numFK
								+ "' and com_type='" + typeFK
								+ "' and state_id='" + stateFK + "'";
						result = statement.executeQuery(sqlCode);
						if (result.next()) 
						{
							Element vBadListStateDscp = new Element("vBadListStateDscp");
							String strT = result.getString(1);

							if (strT == null || strT.compareTo("") == 0)
							{
								vBadListStateDscp.addContent("_");
							}
							else
							{
								vBadListStateDscp.addContent(strT);
							}
							val.addContent(vBadListStateDscp);
						}
					} 
					catch (SQLException e) 
					{
						e.printStackTrace();
					}
					
					////////////////新增预报信息与3维部件的对应信息 by 2013-3-7
					try 
					{
						con.setAutoCommit(true);
						Statement statement = con.createStatement();
						String TypeIDString = typeFK + numFK;
						String sqlCode = "select entityItemID, entityID, CorrespondBVal "
										+ "from bvalcor where CorrespondBVal like '%" + TypeIDString + "%'";
						ResultSet result;
						result = statement.executeQuery(sqlCode);
						String entityitemIDAllString = "";
						while (result.next())
						{
							boolean isFind = false;
							String strCorBVal[] = result.getString(3).split(";");
							for (int j = 0; j < strCorBVal.length; j++)
							{
								if (strCorBVal[j].compareTo(TypeIDString) == 0)
								{
									isFind = true;
									break;
								}
							}
							if (!isFind)
							{
								continue;
							}
							
							entityitemIDAllString = entityitemIDAllString + result.getString(1) + "," + result.getString(2) + ";";
						}
						if (entityitemIDAllString != "")
						{
							entityitemIDAllString = entityitemIDAllString.substring(0, entityitemIDAllString.length() - 1);
						}
						
						Element entityItemID = new Element("entityItemID");
						entityItemID.addContent(entityitemIDAllString);
						val.addContent(entityItemID);
						
						Element vOtherEntity = new Element("vOtherEntity");
						vOtherEntity.addContent(entityitemIDAllString);
						val.addContent(vOtherEntity);
						
						Element valTypeID = new Element("valTypeID");
						valTypeID.addContent(TypeIDString);
						val.addContent(valTypeID);
					} 
					catch (SQLException e)
					{
						// TODO Auto-generated catch block
						e.printStackTrace();
						return null;
					}
					root.addContent(val);
				}
				lineNum++;
			}
			// System.out.println(strRet);
			// return strRet;
			Document slctDoc = new Document(root);
			XMLOutputter outputter = new XMLOutputter();
			// System.out.println(outputter.outputString(slctDoc));
			return outputter.outputString(slctDoc);
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
			return "";
		}
	}

	/**
	 * 设置表关注
	 * @param type 变量类型
	 * @param id 变量ID
	 * @param mark 关注
	 */
	public static void setTableAttention(String type, String id, int mark) {
		if (con == null) {
			return;
		}
		System.out.println(type + "id" + id);
		try {
			con.setAutoCommit(true);
			String sqlCode = "update com set table_mark=? where com_id=? and com_type=?";
			PreparedStatement statement = con.prepareStatement(sqlCode);
			statement.setString(1, String.valueOf(mark));
			statement.setString(2, id);
			statement.setString(3, type);
			statement.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 设置图关注
	 * @param type 变量类型
	 * @param id 变量ID
	 * @param mark 关注
	 */
	public static void setImgAttention(String strTypeID, int mark) 
	{
		if (con == null) 
		{
			return;
		}
		try
		{
			con.setAutoCommit(true);
			String str[] = strTypeID.split("_");
			if (str.length > 0)
			{
				for (int i = 0; i < str.length; i++)
				{
					String strOneTypeID[] = str[i].split("-");
					if (strOneTypeID.length > 0)
					{
						String strType = strOneTypeID[0];
						String strID = strOneTypeID[1];
						String sqlCode = "update com set graph_mark=? where com_id=? and com_type=? and com_name != ''";
						PreparedStatement statement = con.prepareStatement(sqlCode);
						statement.setString(1, String.valueOf(mark));
						statement.setString(2, strID);
						statement.setString(3, strType);
						statement.executeUpdate();
			
						System.out.println(String.valueOf(mark) + "**" + strID + strType);
					}
				}
			}
			
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
	}

	/**
	 * 根据工程图编号获得变量关注
	 * @param imId 工程图编号
	 * @return
	 */
	public String getAttsByImIdTable(String imId) {
		if (con == null) {
			return null;
		}

		Element root = new Element("vList");

		try {
			con.setAutoCommit(true);
			String sqlCode = "select com.com_id,com.com_type from com,"
					+ "comimage where com.com_id=comimage.com_id and"
					+ " com.com_type=comimage.com_type and table_mark='1' and "
					+ "image_id='" + imId + "'";
			// System.out.println(sqlCode);
			Statement statement = con.createStatement();
			ResultSet result = statement.executeQuery(sqlCode);

			while (result.next()) {
				Element val = new Element("val");

				Element vId = new Element("vListNum");
				vId.addContent(result.getString(1));
				val.addContent(vId);

				Element vType = new Element("vListType");
				vType.addContent(result.getString(2));
				val.addContent(vType);

				root.addContent(val);

			}

			XMLOutputter outputter = new XMLOutputter();
			return outputter.outputString(new Document(root));

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * 设置图关注
	 * @param type 变量类型
	 * @param id 变量ID
	 * @param mark 关注
	 */
	public void setGraphAttention(String type, String id, int mark) {
		if (con == null) {
			return;
		}
		try {
			con.setAutoCommit(true);
			String sqlCode = "update com set graph_mark=? where com_id=? and com_type=?";
			PreparedStatement statement = con.prepareStatement(sqlCode);
			statement.setString(1, String.valueOf(mark));
			statement.setString(2, id);
			statement.setString(3, type);
			statement.executeUpdate();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * 根据工程图编号获得关注变量
	 * @param imId 工程图编号
	 * @return
	 */
	public String getAttsByImIdGraphTu(String imId) {
		if (con == null) {
			return null;
		}

		Element root = new Element("vList");

		try {
			con.setAutoCommit(true);
			String sqlCode = "select com.com_id,com.com_type from com,"
					+ "comimage where com.com_id=comimage.com_id and"
					+ " com.com_type=comimage.com_type and graph_mark='1'"
					+ " and " + "image_id='" + imId + "'";
			// System.out.println(sqlCode);
			Statement statement = con.createStatement();
			ResultSet result = statement.executeQuery(sqlCode);

			StringBuffer strb = new StringBuffer();
			while (result.next()) {
				// System.out.println(result.getString(2));
				strb.append(result.getString(2));
				strb.append(result.getString(1));
				strb.append(",");
			}

			// System.out.println(imId + "-");
			if (strb.length() == 0)
				return "";

			// System.out.println(strb.substring(0, strb.length() - 1));
			return strb.substring(0, strb.length() - 1);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * 获得关注变量
	 * @return
	 */
	public static String getAttsByImIdGraphBiao() {
		if (con == null) {
			return null;
		}

		Element root = new Element("vList");

		try {
			con.setAutoCommit(true);
			String sqlCode = "select com.com_id,com.com_type from com,"
					+ "comimage where com.com_id=comimage.com_id and"
					+ " com.com_type=comimage.com_type and table_mark='1'";
			// +" and " + "image_id='" + imId + "'";
			// System.out.println(sqlCode);
			Statement statement = con.createStatement();
			ResultSet result = statement.executeQuery(sqlCode);

			StringBuffer strb = new StringBuffer();
			while (result.next()) 
			{
				// System.out.println(result.getString(2));
				strb.append(result.getString(2));
				strb.append(result.getString(1));
				strb.append(",");
			}

			if (strb.length() == 0)
				return "";

			// System.out.println(strb.substring(0, strb.length() - 1));
			return strb.substring(0, strb.length() - 1);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * 根据变量编号和类型获得变量在所有工程图上的信息
	 * @param vType 变量类型
	 * @param vid 变量ID
	 * @return
	 */
	public String getSlctStrWithoutImgId(String vType, String vid) 
	{
		if (con == null) 
		{
			return null;
		}
		// System.out.println(vType+vid);
		Element root = new Element("root");

		try 
		{
			con.setAutoCommit(true);
			Statement statement = con.createStatement();
			if (vType.compareTo("BX") == 0)
			{
				vType = "U";
			}
			
			String sqlCode = "select image_id,location_x,location_y,location_xper,"
					+ "location_yper,color, biaopai_x, biaopai_y, point_x, point_y  from comimage where com_id='"
					+ vid
					+ "' and com_type='" + vType + "'";
			ResultSet result = statement.executeQuery(sqlCode);
			while (result.next()) 
			{
				Element slct = new Element("slct");

				Element imageId = new Element("imageId");
				imageId.addContent(result.getString(1));
				slct.addContent(imageId);

				Element slctX0Per = new Element("slctX0Per");
				slctX0Per.addContent(result.getString(2));
				slct.addContent(slctX0Per);

				Element slctY0Per = new Element("slctY0Per");
				slctY0Per.addContent(result.getString(3));
				slct.addContent(slctY0Per);

				Element slctWidthPer = new Element("slctWidthPer");
				slctWidthPer.addContent(result.getString(4));
				slct.addContent(slctWidthPer);

				Element slctHeightPer = new Element("slctHeightPer");
				slctHeightPer.addContent(result.getString(5));
				slct.addContent(slctHeightPer);

				String[] color = result.getString(6).split(";");

				Element color1 = new Element("color1");
				color1.addContent(color[0]);
				slct.addContent(color1);

				Element color2 = new Element("color2");
				color2.addContent(color[1]);
				slct.addContent(color2);

				//By Su Chen
				if(color.length > 2)
				{
					Element fillOpacity = new Element("fillOpacity");
					fillOpacity.addContent(color[2]);
					slct.addContent(fillOpacity);
				}
				
			/*2012 03 30 新增读取标牌的位置信息<<*/
				Element bpX0Per = new Element("bpX0Per");
				bpX0Per.addContent(result.getString(7));
				slct.addContent(bpX0Per);
				
				Element bpY0Per = new Element("bpY0Per");
				bpY0Per.addContent(result.getString(8));
				slct.addContent(bpY0Per);
				
				Element pointX0Per = new Element("pointX0Per");
				pointX0Per.addContent(result.getString(9));
				slct.addContent(pointX0Per);
				
				Element pointY0Per = new Element("pointY0Per");
				pointY0Per.addContent(result.getString(10));
				slct.addContent(pointY0Per);
			/*>>新增完成*/
				
				root.addContent(slct);
			}

			Document slctDoc = new Document(root);
			XMLOutputter outputter = new XMLOutputter();
			// System.out.println(outputter.outputString(slctDoc));
			return outputter.outputString(slctDoc);

		} 
		catch (SQLException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * 根据变量编号和类型获得变量在所有工程图上的信息
	 * @param vType 变量类型
	 * @param vid 变量ID
	 * @return
	 */
	public String getBValCorItemId(String vBadTypeIDStr)
	{
		if (con == null) 
		{
			return null;
		}
		// System.out.println(vType+vid);
		String []vBadTypeIDArr = vBadTypeIDStr.split(";");
		Element root = new Element("root");
		try 
		{
			con.setAutoCommit(true);
			Statement statement = con.createStatement();
			String sqlCode = "select entityItemID, ChineseExplain, entityID, CorrespondBVal "
					+ "from bvalcor where ";
			ResultSet result;
			for (int i = 0; i < vBadTypeIDArr.length; i++)
			{
				String sqlCode1 = sqlCode + " CorrespondBVal like '%" + vBadTypeIDArr[i] + "%'";
				result = statement.executeQuery(sqlCode1);
				while (result.next())
				{
					boolean isFind = false;
					String strCorBVal[] = result.getString(4).split(";");
					for (int j = 0; j < strCorBVal.length; j++)
					{
						if (strCorBVal[j].compareTo(vBadTypeIDArr[i]) == 0)
						{
							isFind = true;
							break;
						}
					}
					if (!isFind)
					{
						continue;
					}
					
					Element comInfo = new Element("comInfo");

					Element entityItemID = new Element("entityItemID");
					entityItemID.addContent(result.getString(1));
					comInfo.addContent(entityItemID);

					Element ChineseExplain = new Element("ChineseExplain");
					ChineseExplain.addContent(result.getString(2));
					comInfo.addContent(ChineseExplain);

					Element entityID = new Element("entityID");
					entityID.addContent(result.getString(3));
					comInfo.addContent(entityID);

					Element CorrespondBVal = new Element("CorrespondBVal");
					CorrespondBVal.addContent(result.getString(4));
					comInfo.addContent(CorrespondBVal);
					
					Element valTypeID = new Element("valTypeID");
					valTypeID.addContent(vBadTypeIDArr[i]);
					comInfo.addContent(valTypeID);
					
					root.addContent(comInfo);
				}
			}
			
			Document slctDoc = new Document(root);
			XMLOutputter outputter = new XMLOutputter();
			// System.out.println(outputter.outputString(slctDoc));
			return outputter.outputString(slctDoc);
			
		} 
		catch (SQLException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * 获得选区信息
	 * @param vType 变量类型
	 * @param vid 变量信息
	 * @param imaId 工程图编号
	 * @return
	 */
	public String getSlctStr(String vType, String vid, String imaId) 
	{
		if (con == null) 
		{
			return null;
		}
		Element root = new Element("root");

		try {
			con.setAutoCommit(true);
			Statement statement = con.createStatement();
			String sqlCode = "select com_id, com_type, image_id,location_x,location_y,location_xper,"
					+ "location_yper,color, biaopai_x, biaopai_y, point_x, point_y  from comimage where com_id='"
					+ vid
					+ "' and com_type='"
					+ vType
					+ "' and image_id='"
					+ imaId
					+ "'";
			ResultSet result = statement.executeQuery(sqlCode);
			while (result.next())
			{
				Element slct = new Element("slct");

				Element vId = new Element("vListNum");
				vId.addContent(result.getString(1));
				slct.addContent(vId);

				Element vType1 = new Element("vListType");
				vType1.addContent(result.getString(2));
				slct.addContent(vType1);
				
				Element imageId = new Element("imageId");
				imageId.addContent(result.getString(3));
				slct.addContent(imageId);

				Element slctX0Per = new Element("slctX0Per");
				slctX0Per.addContent(result.getString(4));
				slct.addContent(slctX0Per);

				Element slctY0Per = new Element("slctY0Per");
				slctY0Per.addContent(result.getString(5));
				slct.addContent(slctY0Per);

				Element slctWidthPer = new Element("slctWidthPer");
				slctWidthPer.addContent(result.getString(6));
				slct.addContent(slctWidthPer);

				Element slctHeightPer = new Element("slctHeightPer");
				slctHeightPer.addContent(result.getString(7));
				slct.addContent(slctHeightPer);

				String[] color = result.getString(8).split(";");

				Element color1 = new Element("color1");
				color1.addContent(color[0]);
				slct.addContent(color1);

				Element color2 = new Element("color2");
				color2.addContent(color[1]);
				slct.addContent(color2);
				
				//By Su Chen
				if(color.length > 2)
				{
					Element fillOpacity = new Element("fillOpacity");
					fillOpacity.addContent(color[2]);
					slct.addContent(fillOpacity);
				}
			/*2012 03 30 新增读取标牌的位置信息<<*/
				Element bpX0Per = new Element("bpX0Per");
				bpX0Per.addContent(result.getString(9));
				slct.addContent(bpX0Per);
				
				Element bpY0Per = new Element("bpY0Per");
				bpY0Per.addContent(result.getString(10));
				slct.addContent(bpY0Per);
				
				Element pointX0Per = new Element("pointX0Per");
				pointX0Per.addContent(result.getString(11));
				slct.addContent(pointX0Per);
				
				Element pointY0Per = new Element("pointY0Per");
				pointY0Per.addContent(result.getString(12));
				slct.addContent(pointY0Per);
			/*>>新增完成*/
				
				root.addContent(slct);
			}

			Document slctDoc = new Document(root);
			XMLOutputter outputter = new XMLOutputter();
			return outputter.outputString(slctDoc);

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * 根据实体编号获得变量信息
	 * @param entityId 实体图编号
	 * @return
	 */
	public static String getVListEntityIdStr(String entityId) 
	{
		if (con == null) 
		{
			return null;
		}

		Element root = new Element("vBoardList");
		try
		{
			con.setAutoCommit(true);
			Statement statement = con.createStatement();
			String sqlCode = "select com_id, tag, label1, label2,"
					+ "coordinate, quadrant, board_angle, board_length, board_state,"
					+ "board_value, board_direct, board_rotationz from comentity "
					+ "where entity_id='" + entityId + "'";
			ResultSet result = statement.executeQuery(sqlCode);
			while (result.next()) 
			{
				Element val = new Element("val");

				String vNum = result.getString(1);
				
				Element tag = new Element("tag");
				tag.addContent(result.getString(2));
				val.addContent(tag);

				Element label1 = new Element("label1");
				label1.addContent(result.getString(3));
				val.addContent(label1);

				Element label2 = new Element("label2");
				label2.addContent(result.getString(4));
				val.addContent(label2);

				//获得测点的坐标信息
				String[] coordinate = result.getString(5).split(",");

				Element coordinate_x = new Element("coordinate_x");
				coordinate_x.addContent(coordinate[0]);
				val.addContent(coordinate_x);

				Element coordinate_y = new Element("coordinate_y");
				coordinate_y.addContent(coordinate[1]);
				val.addContent(coordinate_y);
				
				Element coordinate_z = new Element("coordinate_z");
				coordinate_z.addContent(coordinate[2]);
				val.addContent(coordinate_z);
				
				//获得测点的象限信息
				String[] quanrant = result.getString(6).split(",");
				
				Element quanrant1 = new Element("quanrant1");
				quanrant1.addContent(quanrant[0]);
				val.addContent(quanrant1);

				Element quanrant2 = new Element("quanrant2");
				quanrant2.addContent(quanrant[1]);
				val.addContent(quanrant2);
				
				Element quanrant3 = new Element("quanrant3");
				quanrant3.addContent(quanrant[2]);
				val.addContent(quanrant3);
				
				Element quanrant4 = new Element("quanrant4");
				quanrant4.addContent(quanrant[3]);
				val.addContent(quanrant4);
				
				Element board_angle = new Element("board_angle");
				board_angle.addContent(result.getString(7));
				val.addContent(board_angle);

				Element board_length = new Element("board_length");
				board_length.addContent(result.getString(8));
				val.addContent(board_length);
				
				Element board_state = new Element("board_state");
				board_state.addContent(result.getString(9));
				val.addContent(board_state);
				
				Element board_value = new Element("board_value");
				board_value.addContent(result.getString(10));
				val.addContent(board_value);
				
				Element board_direct = new Element("board_direct");
				board_direct.addContent(result.getString(11));
				val.addContent(board_direct);
				
				Element board_rotationz = new Element("board_rotationz");
				board_rotationz.addContent(result.getString(12));
				val.addContent(board_rotationz);
								
				
//				sqlCode = "select com_name, cedian from com where com_type='" + "X"
//						   + "' and com_id='" + vNum + "'";
//				Statement temp = con.createStatement();
//				ResultSet tempResult = temp.executeQuery(sqlCode);
//				if (tempResult.next()) 
//				{
//					Element com_name = new Element("com_name");
//					com_name.addContent(tempResult.getString(1));
//					val.addContent(com_name);
//					
//					Element cedian = new Element("cedian");
//					cedian.addContent(tempResult.getString(1));
//					val.addContent(cedian);
//				}
				
				root.addContent(val);
			}

			Document slctDoc = new Document(root);
			XMLOutputter outputter = new XMLOutputter();
			// System.out.println(outputter.outputString(slctDoc));
			return outputter.outputString(slctDoc);

		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * 取出所有测点信息
	 * 	 * @return
	 */
	public static String getVListAllEntity() 
	{ 
		if (con == null) 
		{
			return null;
		}

		Element root = new Element("vPointList");
		try
		{
			con.setAutoCommit(true);
			Statement statement = con.createStatement();
			String sqlCode = "select com_id, entity_id, tag, label1, label2,"
					+ "coordinate, quadrant, board_angle, board_length, board_state,"
					+ "board_value, board_direct, board_rotationz from comentity ";
			ResultSet result = statement.executeQuery(sqlCode);
			while (result.next()) 
			{
				Element point = new Element("point");

				Element com_id = new Element("com_id");
				com_id.addContent(result.getString(1));
				point.addContent(com_id);

				Element entity_id = new Element("entity_id");
				entity_id.addContent(result.getString(2));
				point.addContent(entity_id);
				
				Element tag = new Element("tag");
				tag.addContent(result.getString(3));
				point.addContent(tag);

				Element label1 = new Element("label1");
				label1.addContent(result.getString(4));
				point.addContent(label1);

				Element label2 = new Element("label2");
				label2.addContent(result.getString(5));
				point.addContent(label2);

				//获得测点的坐标信息
				String[] coordinate = result.getString(6).split(",");

				Element coordinate_x = new Element("coordinate_x");
				coordinate_x.addContent(coordinate[0]);
				point.addContent(coordinate_x);

				Element coordinate_y = new Element("coordinate_y");
				coordinate_y.addContent(coordinate[1]);
				point.addContent(coordinate_y);
				
				Element coordinate_z = new Element("coordinate_z");
				coordinate_z.addContent(coordinate[2]);
				point.addContent(coordinate_z);
				
				//获得测点的象限信息
				String[] quanrant = result.getString(7).split(",");
				
				Element quanrant1 = new Element("quanrant1");
				quanrant1.addContent(quanrant[0]);
				point.addContent(quanrant1);

				Element quanrant2 = new Element("quanrant2");
				quanrant2.addContent(quanrant[1]);
				point.addContent(quanrant2);
				
				Element quanrant3 = new Element("quanrant3");
				quanrant3.addContent(quanrant[2]);
				point.addContent(quanrant3);
				
				Element quanrant4 = new Element("quanrant4");
				quanrant4.addContent(quanrant[3]);
				point.addContent(quanrant4);
				
				Element board_angle = new Element("board_angle");
				board_angle.addContent(result.getString(8));
				point.addContent(board_angle);

				Element board_length = new Element("board_length");
				board_length.addContent(result.getString(9));
				point.addContent(board_length);
				
				Element board_state = new Element("board_state");
				board_state.addContent(result.getString(10));
				point.addContent(board_state);
				
				Element board_value = new Element("board_value");
				board_value.addContent(result.getString(11));
				point.addContent(board_value);
				
				Element board_direct = new Element("board_direct");
				board_direct.addContent(result.getString(12));
				point.addContent(board_direct);
				
				Element board_rotationz = new Element("board_rotationz");
				board_rotationz.addContent(result.getString(13));
				point.addContent(board_rotationz);
								
				root.addContent(point);
			}

			Document slctDoc = new Document(root);
			XMLOutputter outputter = new XMLOutputter();
			// System.out.println(outputter.outputString(slctDoc));
			return outputter.outputString(slctDoc);

		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * 取出被图关注的测点信息
	 * 	 * @return
	 */
	public static String getVListGuanZhuEntity() 
	{ 
		if (con == null) 
		{
			return null;
		}

		Element root = new Element("vPointList");
		try
		{   
			con.setAutoCommit(true);
			Statement statement = con.createStatement();
			String sqlCode = "SELECT CEDIAN FROM com where GRAPH_MARK = '1'";
			
			ResultSet resultCom = statement.executeQuery(sqlCode);
			while (resultCom.next()) 
			{
					String CedianString = resultCom.getString(1);
					String sqlCodeCom = "select com_id, entity_id, tag, label1, label2,"
					+ "coordinate, quadrant, board_angle, board_length, board_state,"
					+ "board_value, board_direct, board_rotationz from comentity "
					+ "where label1 = '" + CedianString +"'";
					Statement statementEntity = con.createStatement();
					ResultSet result = statementEntity.executeQuery(sqlCodeCom);
					
					while (result.next())
					{
						Element point = new Element("point");
		
						Element com_id = new Element("com_id");
						com_id.addContent(result.getString(1));
						point.addContent(com_id);
		
						Element entity_id = new Element("entity_id");
						entity_id.addContent(result.getString(2));
						point.addContent(entity_id);
						
						Element tag = new Element("tag");
						tag.addContent(result.getString(3));
						point.addContent(tag);
		
						Element label1 = new Element("label1");
						label1.addContent(result.getString(4));
						point.addContent(label1);
		
						Element label2 = new Element("label2");
						label2.addContent(result.getString(5));
						point.addContent(label2);
		
						//获得测点的坐标信息
						String[] coordinate = result.getString(6).split(",");
		
						Element coordinate_x = new Element("coordinate_x");
						coordinate_x.addContent(coordinate[0]);
						point.addContent(coordinate_x);
		
						Element coordinate_y = new Element("coordinate_y");
						coordinate_y.addContent(coordinate[1]);
						point.addContent(coordinate_y);
						
						Element coordinate_z = new Element("coordinate_z");
						coordinate_z.addContent(coordinate[2]);
						point.addContent(coordinate_z);
						
						//获得测点的象限信息
						String[] quanrant = result.getString(7).split(",");
						
						Element quanrant1 = new Element("quanrant1");
						quanrant1.addContent(quanrant[0]);
						point.addContent(quanrant1);
		
						Element quanrant2 = new Element("quanrant2");
						quanrant2.addContent(quanrant[1]);
						point.addContent(quanrant2);
						
						Element quanrant3 = new Element("quanrant3");
						quanrant3.addContent(quanrant[2]);
						point.addContent(quanrant3);
						
						Element quanrant4 = new Element("quanrant4");
						quanrant4.addContent(quanrant[3]);
						point.addContent(quanrant4);
						
						Element board_angle = new Element("board_angle");
						board_angle.addContent(result.getString(8));
						point.addContent(board_angle);
		
						Element board_length = new Element("board_length");
						board_length.addContent(result.getString(9));
						point.addContent(board_length);
						
						Element board_state = new Element("board_state");
						board_state.addContent(result.getString(10));
						point.addContent(board_state);
						
						Element board_value = new Element("board_value");
						board_value.addContent(result.getString(11));
						point.addContent(board_value);
						
						Element board_direct = new Element("board_direct");
						board_direct.addContent(result.getString(12));
						point.addContent(board_direct);
						
						Element board_rotationz = new Element("board_rotationz");
						board_rotationz.addContent(result.getString(13));
						point.addContent(board_rotationz);
										
						root.addContent(point);
					}
			}

			Document slctDoc = new Document(root);
			XMLOutputter outputter = new XMLOutputter();
			// System.out.println(outputter.outputString(slctDoc));
			return outputter.outputString(slctDoc);

		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * 取出所有测点名称信息
	 * 	 * @return
	 */
	public static String getPointNameList(String pointName) 
	{ 
		if (con == null) 
		{
			return null;
		}

		Element root = new Element("vPointNameList");
		try
		{
			con.setAutoCommit(true);
			Statement statement = con.createStatement();
			String sqlCode = "select com_name, cedian from com "
				  			 + "where cedian LIKE '%" + pointName +"%' AND COM_TYPE='X' AND com_name != ''";
			ResultSet result = statement.executeQuery(sqlCode);
			while (result.next()) 
			{
				Element point = new Element("pointName");

				Element com_name = new Element("com_name");
				com_name.addContent(result.getString(1));
				point.addContent(com_name);

				Element cedian = new Element("cedian");
				cedian.addContent(result.getString(2));
				point.addContent(cedian);
								
				root.addContent(point);
			}

			Document slctDoc = new Document(root);
			XMLOutputter outputter = new XMLOutputter();
			return outputter.outputString(slctDoc);

		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * 取出所有测点名称信息
	 * 	 * @return
	 */
	public static String getBValCorList() 
	{ 
		if (con == null) 
		{
			return null;
		}

		Element root = new Element("vBValCorList");
		try
		{
			con.setAutoCommit(true);
			Statement statement = con.createStatement();
			String sqlCode = "select entityItemID, entityID, ChineseExplain, CorrespondBVal from BValCor";
			ResultSet result = statement.executeQuery(sqlCode);
			while (result.next()) 
			{
				Element BValCor = new Element("BValCor");
				
				Element entityItemID = new Element("entityItemID");
				entityItemID.addContent(result.getString(1));
				BValCor.addContent(entityItemID);
				
				Element entityID = new Element("entityID");
				entityID.addContent(result.getString(2));
				BValCor.addContent(entityID);

				Element ChineseExplain = new Element("ChineseExplain");
				ChineseExplain.addContent(result.getString(3));
				BValCor.addContent(ChineseExplain);

				Element CorrespondBVal = new Element("CorrespondBVal");
				CorrespondBVal.addContent(result.getString(4));
				BValCor.addContent(CorrespondBVal);
				
				root.addContent(BValCor);
			}

			Document slctDoc = new Document(root);
			XMLOutputter outputter = new XMLOutputter();
			return outputter.outputString(slctDoc);

		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * 取出所有B变量信息
	 * 	 * @return
	 */
	public static String getBValList() 
	{ 
		if (con == null) 
		{
			return null;
		}

		Element root = new Element("vBValList");
		try
		{
			con.setAutoCommit(true);
			Statement statement = con.createStatement();
			String sqlCode = "select com_id, com_type, com_des from com "
				             + "where com_type = 'B' or com_type = 'U'";
			ResultSet result = statement.executeQuery(sqlCode);
			while (result.next()) 
			{
				Element BVal = new Element("BVal");
				
				String bValid = result.getString(1);
				String bValType = result.getString(2);
				if (bValType.compareTo("U") == 0)
				{
					bValType = "BX";
				}
				
				Element BValTypeID = new Element("BValTypeID");
				String str = bValType + bValid;
				BValTypeID.addContent(str);
				BVal.addContent(BValTypeID);

				Element com_des = new Element("com_des");
				com_des.addContent(result.getString(3));
				BVal.addContent(com_des);
				
//				Element isSelected = new Element("isSelected");
//				isSelected.addContent("false");
//				BVal.addContent(isSelected);
				
				root.addContent(BVal);
			}

			Document slctDoc = new Document(root);
			XMLOutputter outputter = new XMLOutputter();
			return outputter.outputString(slctDoc);

		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * 根据工程图编号获得变量信息
	 * @param imgIdFunc 工程图编号
	 * @return
	 */
	public static String getVListImgIdStr(String imgIdFunc) 
	{
		if (con == null) 
		{
			return null;
		}

		Element root = new Element("vList");
		try
		{
			con.setAutoCommit(true);
			Statement statement = con.createStatement();
			String sqlCode = "select location_x,location_y,location_xper, location_yper,color ,com_id,"
					+ "com_type, biaopai_x, biaopai_y, point_x, point_y from comimage where image_id='"
					+ imgIdFunc + "'";
			ResultSet result = statement.executeQuery(sqlCode);
			while (result.next()) 
			{
				Element val = new Element("val");

				Element slctX0Per = new Element("slctX0Per");
				slctX0Per.addContent(result.getString(1));
				val.addContent(slctX0Per);

				Element slctY0Per = new Element("slctY0Per");
				slctY0Per.addContent(result.getString(2));
				val.addContent(slctY0Per);

				Element slctWidthPer = new Element("slctWidthPer");
				slctWidthPer.addContent(result.getString(3));
				val.addContent(slctWidthPer);

				Element slctHeightPer = new Element("slctHeightPer");
				slctHeightPer.addContent(result.getString(4));
				val.addContent(slctHeightPer);

				// System.out.println(result.getString(4));
				String[] color = result.getString(5).split(";");

				Element color1 = new Element("color1");
				color1.addContent(color[0]);
				val.addContent(color1);

				Element color2 = new Element("color2");
				color2.addContent(color[1]);
				val.addContent(color2);
				
				//By Su Chen
				if(color.length > 2)
				{
					Element fillOpacity = new Element("fillOpacity");
					fillOpacity.addContent(color[2]);
					val.addContent(fillOpacity);
				}

				Element vListNum = new Element("vListNum");
				vListNum.addContent(result.getString(6));
				val.addContent(vListNum);

				Element vListType = new Element("vListType");
				vListType.addContent(result.getString(7));
				val.addContent(vListType);

			/*2012 03 30 新增读取标牌的位置信息<<*/
				Element bpX0Per = new Element("bpX0Per");
				bpX0Per.addContent(result.getString(8));
				val.addContent(bpX0Per);
				
				Element bpY0Per = new Element("bpY0Per");
				bpY0Per.addContent(result.getString(9));
				val.addContent(bpY0Per);
				
				Element pointX0Per = new Element("pointX0Per");
				pointX0Per.addContent(result.getString(10));
				val.addContent(pointX0Per);
				
				Element pointY0Per = new Element("pointY0Per");
				pointY0Per.addContent(result.getString(11));
				val.addContent(pointY0Per);
			/*>>新增完成*/
				
				Element valShow = new Element("valShow");
				valShow.addContent(new String("0"));
				val.addContent(valShow);
								
				root.addContent(val);
			}

			Document slctDoc = new Document(root);
			XMLOutputter outputter = new XMLOutputter();
			// System.out.println(outputter.outputString(slctDoc));
			return outputter.outputString(slctDoc);

		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * 获取某张工程图上所有变量的变量列表
	 * @param imgIdFunc 工程图编号
	 * @return
	 */
	public static String getVListByImgIdStr(String imgIdFunc)
	{
		if (con == null) 
		{
			return null;
		}

		Element root = new Element("vList");
		try 
		{
			con.setAutoCommit(true);
			Statement statement = con.createStatement();
			String sqlCode = "select location_x,location_y,location_xper, location_yper,color ,com_id,"
							+ "com_type, biaopai_x, biaopai_y, point_x, point_y, sp from comimage where image_id='"
							+ imgIdFunc + "'";
	
			// vListStNumber
			// vListName
			ResultSet result = statement.executeQuery(sqlCode);
			while (result.next()) 
			{
				Element val = new Element("val");

				Element slctX0Per = new Element("slctX0Per");
				slctX0Per.addContent(result.getString(1));
				val.addContent(slctX0Per);

				Element slctY0Per = new Element("slctY0Per");
				slctY0Per.addContent(result.getString(2));
				val.addContent(slctY0Per);

				Element slctWidthPer = new Element("slctWidthPer");
				slctWidthPer.addContent(result.getString(3));
				val.addContent(slctWidthPer);

				Element slctHeightPer = new Element("slctHeightPer");
				slctHeightPer.addContent(result.getString(4));
				val.addContent(slctHeightPer);

				String[] color = result.getString(5).split(";");

				Element color1 = new Element("color1");
				color1.addContent(color[0]);
				val.addContent(color1);

				Element color2 = new Element("color2");
				color2.addContent(color[1]);
				val.addContent(color2);
				
				//By Su Chen
				if(color.length > 2)
				{
					Element fillOpacity = new Element("fillOpacity");
					fillOpacity.addContent(color[2]);
					val.addContent(fillOpacity);
				}

				String vNum = result.getString(6);
				//类型
				String vType = result.getString(7);
                String flag=result.getString(12);
                /*
                String sxType="";
                if(vType=="X"&&flag=="1"){
                	sxType="SX";
                }else 
                if(vType=="X"&&flag=="0"){
                	sxType="X";
                }else{
                	sxType=vType;
                }
                */
				Element vListNum = new Element("vListNum");
				vListNum.addContent(result.getString(6));
				val.addContent(vListNum);

				Element vListType = new Element("vListType");
				vListType.addContent(vType);
				val.addContent(vListType);

			/*2012 03 30 新增读取标牌的位置信息<<*/
				Element bpX0Per = new Element("bpX0Per");
				bpX0Per.addContent(result.getString(8));
				val.addContent(bpX0Per);
				
				Element bpY0Per = new Element("bpY0Per");
				bpY0Per.addContent(result.getString(9));
				val.addContent(bpY0Per);
				
				Element pointX0Per = new Element("pointX0Per");
				pointX0Per.addContent(result.getString(10));
				val.addContent(pointX0Per);
				
				Element pointY0Per = new Element("pointY0Per");
				pointY0Per.addContent(result.getString(11));
				val.addContent(pointY0Per);
				//
				Element sp = new Element("sp");
				sp.addContent(result.getString(12));
				val.addContent(sp);
			/*>>新增完成*/
				
				sqlCode = "select count(*) from state where com_type='" + vType
						+ "' and com_id='" + vNum + "'";
				Statement temp = con.createStatement();
				ResultSet tempResult = temp.executeQuery(sqlCode);
				if (tempResult.next()) 
				{
					Element vListStNum = new Element("vListStNumber");
					vListStNum.addContent(tempResult.getString(1));
					val.addContent(vListStNum);
				}

				sqlCode = "select com_des from com where com_type='" + vType
						+ "' and com_id='" + vNum + "'";
				temp = con.createStatement();
				tempResult = temp.executeQuery(sqlCode);
				if (tempResult.next()) 
				{
					Element vListStNum = new Element("vListName");
					vListStNum.addContent(tempResult.getString(1));
					val.addContent(vListStNum);
				}

				root.addContent(val);
			}

			Document slctDoc = new Document(root);
			XMLOutputter outputter = new XMLOutputter();
			// System.out.println(outputter.outputString(slctDoc));
			return outputter.outputString(slctDoc);

		}
		catch (SQLException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * 获取所有变量的变量列表XML
	 * @return
	 */
	public static String getVListStr() 
	{
		if (con == null) 
		{
			return null;
		}
		try 
		{
			Statement temp = con
					.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
							ResultSet.CONCUR_READ_ONLY);
			Element components = new Element("components");
			String sqlCode = "select com_id,com_type from com where com_type='B' or com_type='X'";
			ResultSet result = temp.executeQuery(sqlCode);
			while (result.next()) 
			{
				components.addContent(new Component(result.getString(1), result
						.getString(2)).writeCom());
			}
			Document slctDoc = new Document(components);
			XMLOutputter outputter = new XMLOutputter();
			return outputter.outputString(slctDoc);
		} 
		catch (SQLException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
   

	/**
	 * 产生随机的曲线数据 10min
	 * @param id 变量ID
	 * @param type 变量类型
	 * @return
	 */
	public String getLineMessage(String id, String type) {
		Element lines = new Element("lines");

		int temp;
		for (int i = 0; i < 600; i += 5) {
			temp = i / 60;
			Element point = new Element("point");

			Element x = new Element("x");

			/*
			 * Element year = new Element("year"); year.addContent("2010");
			 * x.addContent(year);
			 * 
			 * Element month = new Element("month"); month.addContent("4");
			 * x.addContent(month);
			 * 
			 * Element day = new Element("day"); day.addContent("3");
			 * x.addContent(day);
			 * 
			 * Element hour = new Element("hour"); hour.addContent("12");
			 * x.addContent(hour);
			 * 
			 * Element minute = new Element("minute");
			 * minute.addContent(String.valueOf(temp)); x.addContent(minute);
			 * 
			 * Element second = new Element("second");
			 * second.addContent(String.valueOf(i % 60)); x.addContent(second);
			 */
			// x.addContent("12-12-2012 04:"+ ((temp < 10) ? "0" +
			// String.valueOf(temp) : String.valueOf(temp))+ ":"+ ((i % 60 < 10)
			// ? "0" + String.valueOf(i % 60) : String.valueOf(i % 60)));
			x.addContent(((temp < 10) ? "0" + String.valueOf(temp) : String
					.valueOf(temp))
					+ ":"
					+ ((i % 60 < 10) ? "0" + String.valueOf(i % 60) : String
							.valueOf(i % 60)));
			point.addContent(x);
			Element y = new Element("y");
			Random random = new Random();
			int r = random.nextInt(10);
			// System.out.println(r);
			y.addContent(String.valueOf(r));

			point.addContent(y);

			lines.addContent(point);
		}

		XMLOutputter outputter = new XMLOutputter();
		// System.out.println(outputter.outputString(new Document(lines)));
		return outputter.outputString(new Document(lines));
	}

	public static UDPClient udpc;

	/**
	 * 显示通信的各变量信息
	 * 变量列表
	 */
	public static String getVListViewGCT() 
	{
		if (con == null) 
		{
			return null;
		}
		try 
		{
			con.setAutoCommit(true);
			Statement statement = con.createStatement();
			
			if (UdpCom.hashPointVal == null) 
			{
				UdpCom rec = new UdpCom();
				rec.start();
			}

			if (UDPClient.sig == 0)
			{
				UDPClient.sig = 1;
				udpc = (new UDPClient());
				udpc.createWorkStation();
			}

			Iterator<String> itStr = UdpCom.hashPointVal.keySet().iterator();//构建hsahPointVal的key值集合
			Element root = new Element("vList");
   
			String strTemp;
			while (itStr.hasNext()) 
			{
				strTemp = new String(itStr.next());

				Tag tagTemp = (Tag) (UdpCom.hashPointVal.get(strTemp));

				Element val = new Element("val");
				
				int iEnd = 0;
				for (iEnd = 0; iEnd < strTemp.length(); iEnd++) 
				{
					if (!Character.isLetterOrDigit(strTemp.charAt(iEnd))) 
					{
						break;
					}
				}
				String strvListNum = strTemp.substring(1, iEnd);
				String strvListType = strTemp.substring(0, 1);
				String strCurState = String.valueOf(tagTemp.iState);
				Element vListNum = new Element("vListNum");
				vListNum.addContent(strvListNum);
				val.addContent(vListNum);

				Element vListType = new Element("vListType");
				vListType.addContent(strvListType);
				val.addContent(vListType);

				Element vListValNum = new Element("vListValNum");

				if (tagTemp.cType.charAt(0) == 'D') 
				{
					String str = tagTemp.sValue;
					vListValNum.addContent(str);
				} 
				else
				{
					vListValNum.addContent(String.valueOf(tagTemp.fValue));
				}
				val.addContent(vListValNum);
				
				Element vListValState = new Element("vListValState");
				vListValState.addContent(String.valueOf(tagTemp.iState));
				val.addContent(vListValState);

				gct.database.ComStateMsg cmT; 
				if (UDPClient.comStateMsg.containsKey(tagTemp.iState + strTemp.substring(0, 1) + String.valueOf(Integer.valueOf(strTemp.substring(1, iEnd)))))
				{
					cmT = UDPClient.comStateMsg.get(tagTemp.iState + strTemp.substring(0, 1) + String.valueOf(Integer.valueOf(strTemp.substring(1, iEnd))));
				}
				else
				{
					cmT = new gct.database.ComStateMsg();
					cmT.des = strTemp.substring(0, 1) + String.valueOf(Integer.valueOf(strTemp.substring(1, iEnd)));
					cmT.name = strTemp.substring(0, 1) + String.valueOf(Integer.valueOf(strTemp.substring(1,iEnd)));
					cmT.stateDes = "没有说明";
					cmT.stateColor = "0x000000";
				}
				Element vListDes = new Element("vListDes");
				vListDes.addContent(cmT.des);
				val.addContent(vListDes);

				Element vListValName = new Element("vListValName");
				vListValName.addContent(cmT.name);
				val.addContent(vListValName);
				
				String strColor = "0x000000";
				String strState = "未知状态";
				if (!cmT.stateDes.equals(""))
				{
					strState = cmT.stateDes;
				}
				if (!cmT.stateColor.equals(""))
				{
					strColor = cmT.stateColor;		//异常状态下的颜色
				}
				
				Element stColor = new Element("stColor");
				stColor.addContent(strColor);	
				val.addContent(stColor);
				
				Element vListStateDscp = new Element("vListStateDscp");
				vListStateDscp.addContent(strState);				
				val.addContent(vListStateDscp);
				
				Element stDisplayType = new Element("stDisplayType");
				stDisplayType.addContent("标牌显示");				
				val.addContent(stDisplayType);
				 
				root.addContent(val);
			}

			Document slctDoc = new Document(root);
			XMLOutputter outputter = new XMLOutputter();
			return outputter.outputString(slctDoc);
		}
		catch (SQLException e)
		{
			e.printStackTrace();
			return null;
		}
	}
	
	public int IsCommunicat()
	{
		int iresult = 0;
		int newComnum = UdpCom.iReceiveNum;
		if (newComnum != oldComNum) 
		{	
			iresult = 1;
		}
		oldComNum = newComnum;
		return iresult;
	}
	
	// 董力
	// 526
	public static String getComMsg()
	{
		Iterator<String> it = UDPClient.comMsg.keySet().iterator();
		String keyRet = "";

		while (it.hasNext()) 
		{
			String keyStr = it.next();
			String keyAdd = keyStr + "`" + (UDPClient.comMsg.get(keyStr)).name + "^";
			keyRet = keyRet.concat(keyAdd);
		}

		if(keyRet.length() > 0)
		{
			keyRet = keyRet.substring(0, keyRet.length() - 1);
		}

		// System.out.println(keyRet);
		return keyRet;
	}
	
	public static String getComDes()
	{
		Iterator<String> it = UDPClient.comMsg.keySet().iterator();
		String keyRet = "";

		while (it.hasNext()) 
		{
			String keyStr = it.next();
			String keyAdd = keyStr + "`" + (UDPClient.comMsg.get(keyStr)).des + "^";
			keyRet = keyRet.concat(keyAdd);
		}

		if(keyRet.length() > 0)
		{
			keyRet = keyRet.substring(0, keyRet.length() - 1);
		}

		//System.out.println(keyRet);
		return keyRet;
	}

	/**
	 * 获得一个变量在一张工程图上的父变量
	 * @param imageId 工程图编号
	 * @return
	 */
	public String getParentComByImgId(String imageId)
	{
		if (con == null) {
			return null;
		}

		Element root = new Element("vals");

		try {
			con.setAutoCommit(true);
			/*
			 * String sqlCode =
			 * "select com.com_id,com.com_type,com.com_name,com.com_des" +
			 * " from comimage,com  where com.com_id = comimage.com_id " +
			 * " and com.com_type=comimage.com_type" +
			 * " and image_id=(select image_parent" +
			 * " from image where image_id=? ) and " +
			 * " com.com_id not in (select com_id from comimage where image_id=?)"
			 * ;
			 */
			String sqlCode = "select com.com_id,com.com_type,com.com_des,com.com_name"
					+ " from comimage,com  where com.com_id = comimage.com_id "
					+ " and com.com_type=comimage.com_type"
					+ " and com.com_id not in (select com_id from comimage where image_id=? and com.com_type=comimage.com_type)";

			PreparedStatement statement = con.prepareStatement(sqlCode);

			statement.setString(1, imageId);
			System.out.println(statement.toString());
			// System.out.println(imageId);
			ResultSet result = statement.executeQuery();
			Hashtable hashT = new Hashtable();
			while (result.next()) {
				if (hashT
						.containsKey(result.getString(1) + result.getString(2))) {
					continue;
				}
				hashT.put(result.getString(1) + result.getString(2), "");

				Element val = new Element("val");
				
				Element vId = new Element("vId");
				vId.addContent(result.getString(1));
				val.addContent(vId);
				
				Element vType = new Element("vType");
				vType.addContent(result.getString(2));
				val.addContent(vType);
				
				String strTypeID = result.getString(2);
				if (strTypeID.compareTo("U") == 0)
				{
					strTypeID = "BX" + result.getString(1);
				}
				else
				{
					strTypeID = strTypeID + result.getString(1);
				}
				Element vTypeID = new Element("vTypeID");
				vTypeID.addContent(strTypeID);
				val.addContent(vTypeID);

				Element vName = new Element("vName");
				vName.addContent(result.getString(3));
				val.addContent(vName);

				Element vDes = new Element("vDes");
				vDes.addContent(result.getString(4));
				val.addContent(vDes);

				root.addContent(val);
			}

			XMLOutputter outputter = new XMLOutputter();
			return outputter.outputString(new Document(root));

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 将变量添加到工程图上
	 * @param imageId 工程图编号
	 * @param typeF 变量类型
	 * @param idF 变量ID
	 */
	public void addComToImage(String imageId, String typeF, String idF) 
	{
		if (con == null) 
		{
			return;
		} 
		if (typeF.contains("B"))
		{
			typeF = "B";
		}
		else if(typeF.contains("X"))
		{
			typeF = "X";
		}
		else if(typeF.contains("U"))
		{
			typeF = "U";
		}
		// System.out.println(idF + "-" + typeF + ":" + imageId);
		try {
			con.setAutoCommit(true);
			String sqlCode = "insert into comimage(com_id,com_type,image_id,location_x,location_y,location_xper,"
					+ "location_yper,color) values(?,?,?,'0','0','0','0','0;0')";
			PreparedStatement statememt = con.prepareStatement(sqlCode);
			statememt.setString(1, idF);
			statememt.setString(2, typeF);
			statememt.setString(3, imageId);
			statememt.executeUpdate();
		} catch (SQLException e) {
			// e.printStackTrace();
		}
	}

	
	public int beginReview(String name) 
	{
		System.out.println("review name: " + name);
		return udpc.beginReview(name);
	}

	public int continueReview() 
	{
		return udpc.continueReview();
	}

	public int endReview() {
		return udpc.endReview();
	}

	public String getReviewList() {
		// System.out.println("review");
		if(udpc == null)
			return "";
		System.out.println(udpc.getReviewList());
		return udpc.getReviewList();
	}

	public void exportCeDian() 
	{
		if (con == null) 
		{
			return;
		}

		// System.out.println(GetPathStr.getPath() + "DUCG/PointList.txt");
		PrintWriter write = null;
		PrintWriter write1 = null;
		PrintWriter write2 = null;
//		PrintWriter write3 = null;
		try {
			write = new PrintWriter(new FileOutputStream(GetPathStr.getPath()
					+ "DUCG/PointList.txt"), true);
			write1 = new PrintWriter(new FileOutputStream(GetPathStr.getPathWeb()
					+ "PointList.txt"), true);
			write2 = new PrintWriter(new FileOutputStream(GetPathStr.getPath()
					+ "DUCG/Communicate/PointList.txt"), true);
//			write3 = new PrintWriter(new FileOutputStream(GetPathStr.getPath()
//					+ "DUCG/Communicate/故障回放通信/PointList.txt"), true);
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		}

		try {
			con.setAutoCommit(true);
			String sqlCode = "select cedian, com_id,com_type from com where COM_TYPE = 'X' and cedian is not null and cedian <> ''";
			Statement sta = con.createStatement();
			ResultSet result = sta.executeQuery(sqlCode);
			while (result.next()) {
				String output = result.getString(1) + "," + result.getString(3) + result.getString(2);
				write.println(output);
				write1.println(output);
				write2.println(output);
//				write3.println(output);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		write.close();
		write1.close();
		write2.close();
//		write3.close();

		UDPClient.getNodeNumber();

	}
	
	/**
	 * 判断一个变量是否存在于这张图上
	 * @param strComId 变量ID
	 * @param strComType 变量类型
	 * @param strImgId 工程图编号
	 * @return
	 */
	public Boolean isExist(String strComId, String strComType, String strImgId)
	{
		try 
		{
			con.setAutoCommit(true);
			String sqlCode = "select * from comimage where com_id = '" + strComId + "' and com_type = '"
			    + strComType +"' and image_id = '" + strImgId + "'";
			Statement sta = con.createStatement();
			ResultSet result = sta.executeQuery(sqlCode);
			if (result.next()) 
			{
				return true;
			}

		} catch (SQLException e) 
		{
			e.printStackTrace();
			return false;
		}
		return false;
	}
	
	/**
	 * 获取没有测点的X变量列表
	 */
	/**
	 * 从offlineevidence.txt中获取信息
	 * @param strImageId
	 * @return
	 */
	public static String getNoPointList()
	{
		FileReader fr;
		Element root = new Element("vals");
		try 
		{
			String strRet = "";
			fr = new FileReader(GetPathStr.getPathWeb() + "KBGraph/offlineevidence");
			BufferedReader br = new BufferedReader(fr);
			String allVarState = "";
			String strRd = "";
			
			if (br.ready())
			{
				strRd = br.readLine();
			}
			String[] rstArr = strRd.split(" ");
			try 
			{
				con.setAutoCommit(true);
				Statement statement = con.createStatement();
				String sqlCode = "select com_id, com_type, com_des, CEDIAN from com where com_type = 'X' and (cedian is null or cedian = '')";
				ResultSet result = statement.executeQuery(sqlCode);
				
				while (result.next()) 
				{
					String strID = result.getString(1);
					String strType = result.getString(2);
					String strTypeID = strType + strID;
					String varState = "-1";
					
					for (int i = 0; i < rstArr.length; i++)
					{
						String []strTypeIDState = rstArr[i].split(",");
						if (strTypeID.equals(strTypeIDState[0]))
						{
							varState = strTypeIDState[1];
							break;
						}
					}
					Element val = new Element("val");

					Element vOffLineTypeID = new Element("vOffLineTypeID");
					vOffLineTypeID.addContent(strTypeID);
					val.addContent(vOffLineTypeID);
					
					Element vOffLineID = new Element("vOffLineID");
					vOffLineID.addContent(result.getString(1));
					val.addContent(vOffLineID);

					Element vOffLineType = new Element("vOffLineType");
					vOffLineType.addContent(result.getString(2));
					val.addContent(vOffLineType);

					Element vOffLineDes = new Element("vOffLineDes");
					vOffLineDes.addContent(result.getString(3));
					val.addContent(vOffLineDes);
					
					Element vOffLineState = new Element("vOffLineState");
					vOffLineState.addContent(varState);
					val.addContent(vOffLineState);
					
					Statement temp = con.createStatement();
					String sqlstr = "select state_color, state_des from state where com_id='" + strID +
							  "' and com_type='" + strType + "' and state_id='" + varState + "'";

					ResultSet res1 = temp.executeQuery(sqlstr);
					Element stColor = new Element("stColor");
					String strStateString = "未知状态";
					if (res1.next())
					{
						stColor.addContent(res1.getString(1));
						strStateString = res1.getString(2);
					}
					else
					{      
						stColor.addContent("0x000000");	//异常状态下的颜色
					}   
					val.addContent(stColor);
					
					Element vListStateDscp = new Element("vListStateDscp");
					vListStateDscp.addContent(strStateString);
					val.addContent(vListStateDscp);
					
					root.addContent(val);
				}	
			} 
			catch (SQLException e) 
			{
				e.printStackTrace();
			}
			Document slctDoc = new Document(root);
			XMLOutputter outputter = new XMLOutputter();
			return outputter.outputString(slctDoc);
		}
		catch (IOException e) 
		{
			e.printStackTrace();
			return "";
		}
	}
	
	public String SaveOffLineState(String strSavState)
	{

		// System.out.println(GetPathStr.getPath() + "DUCG/PointList.txt");
		PrintWriter write = null;
		try 
		{
			write = new PrintWriter(new FileOutputStream(GetPathStr.getPathWeb() + "KBGraph/offlineevidence"), true);
			write.println(strSavState);
			
			//给推理机发送-2消息。
			ByteBuffer fuerFlag = ByteBuffer.allocate(10);
			fuerFlag.order(ByteOrder.LITTLE_ENDIAN);
			short flagData = -2;
			fuerFlag.putShort(flagData);
			byte[] fuer = fuerFlag.array();
			DatagramPacket dpsendCount;
			try 
			{
				DatagramSocket dsoc = null;
				try
				{
					dsoc = new DatagramSocket();
				} 
				catch (SocketException e) 
				{
					e.printStackTrace();
					return "0";
				}
				dpsendCount = new DatagramPacket(fuer, fuer.length, new InetSocketAddress("127.0.0.1", 6300));
				dsoc.send(dpsendCount);
				try
				{
				    Thread.sleep(500);
				}
				catch (InterruptedException e)
				{
				    return "0";
				}
			} 
			catch (Exception e) 
			{
				return "0";
			}
		} 
		catch (FileNotFoundException e1) 
		{
			e1.printStackTrace();
			return "-1";
		}

		write.close();
		return "1";
	}
	
	public String getInitVList()
	{
		Element root = new Element("vals");
		
		try 
		{
			con.setAutoCommit(true);
			Statement statement = con.createStatement();
			String sqlCode = "select com_id, com_type, com_des, com_name, two, three from com where com_type = 'X' ";
			ResultSet result = statement.executeQuery(sqlCode);
			
			while (result.next()) 
			{
				Element val = new Element("val");

				Element vListDes = new Element("vListDes");
				vListDes.addContent(result.getString(3));
				val.addContent(vListDes);
				
				Element vListValName = new Element("vListValName");
				vListValName.addContent(result.getString(4));
				val.addContent(vListValName);
				
				Element vListTwo = new Element("vListTwo");
				vListTwo.addContent(result.getString(5));
				val.addContent(vListTwo);
				
				Element vListThree = new Element("vListThree");
				vListThree.addContent(result.getString(6));
				val.addContent(vListThree);
				
				Element vListValNum = new Element("vListValNum");
				vListValNum.addContent("*");
				val.addContent(vListValNum);
				
				Element vListStateDscp = new Element("vListStateDscp");
				vListStateDscp.addContent("正常");
				val.addContent(vListStateDscp);
				
				Element vListValState = new Element("vListValState");
				vListValState.addContent("0");
				val.addContent(vListValState);
				
				Element stColor = new Element("stColor");
				stColor.addContent("6750003");
				val.addContent(stColor);
				
				Element stDisplayType = new Element("stDisplayType");
				stDisplayType.addContent("标牌显示");
				val.addContent(stDisplayType);

				root.addContent(val);
			}	
		} 
		
		
		
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		Document slctDoc = new Document(root);
		XMLOutputter outputter = new XMLOutputter();
		return outputter.outputString(slctDoc);
	}
	
	
	/**
	 * 获得所有X变量信息
	 * @return
	 */
	public String getPatientMessage() {
		if (con == null) {
			return null;
		}

		Element vals = new Element("vals");

		try {
			con.setAutoCommit(true);
			
			String sqlCode = "select com_id,com_type,com_des,com_name,two,one" +
								" from com where com_type='X' AND isshow='1' AND com_des != '测试'";

			Statement st1 = con.createStatement();
			ResultSet re1 = st1.executeQuery(sqlCode);
			while (re1.next()) {
				Element val = new Element("val");

				String id = re1.getString(1);
				String type = re1.getString(2);
				
				Element vType = new Element("vType");
				vType.addContent(type);
				val.addContent(vType);

				Element vListID = new Element("vListID");
				vListID.addContent(id);
				val.addContent(vListID);

				Element vListDes = new Element("vListDes");
				vListDes.addContent(re1.getString(3));
				val.addContent(vListDes);
				
				Element vListValName = new Element("vListValName");
				vListValName.addContent(re1.getString(4));
				val.addContent(vListValName);
				
				Element vListTwo = new Element("vListTwo");
				vListTwo.addContent(re1.getString(5));
				val.addContent(vListTwo);
				
				Element vListOne = new Element("vListOne");
				vListOne.addContent(re1.getString(6));
				val.addContent(vListOne);
			
				Element vListNumst = new Element("vListNumst");
				vListNumst.addContent("-1");
				val.addContent(vListNumst);

				Element vListstDes = new Element("vListstDes");
				vListstDes.addContent("未知");
				val.addContent(vListstDes);

				vals.addContent(val);
			}

			XMLOutputter outputter = new XMLOutputter();
			return outputter.outputString(new Document(vals));

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}

	}
	
	public String getPatientEvidence(String strFileName) 
	{
		if (con == null) {
			return null;
		}
		
		Element vals = new Element("vals");
		
		try 
		{
			con.setAutoCommit(true);
			String sqlCode = "select Num, State from evidence where FileName = '" + strFileName.trim() + "'";
			Statement st1 = con.createStatement();
			ResultSet re1 = st1.executeQuery(sqlCode);
			int i=0;
			while (re1.next()) 
			{
				String Num = re1.getString(1);
				String State = re1.getString(2);
				
				Element val = new Element("val");
				Element vListID = new Element("vListID");
				vListID.addContent(Num);
				val.addContent(vListID);
				Element vListNumst = new Element("vListNumst");
				vListNumst.addContent(State);
				val.addContent(vListNumst);
				
				sqlCode = "select state_des from state where com_id='"
							+ Num + "' and state_id='" + State + "'";
				Statement st2 = con.createStatement();
				ResultSet re2 = st2.executeQuery(sqlCode);
				int number = 0;
				String message = "";
				if (re2.next()) {
					number++;
					message += re2.getString(1);
				}
				while (re2.next()) {
					number++;
					message += "\r" + re2.getString(1);
				}

				Element vListstDes = new Element("vListstDes");
				vListstDes.addContent(message);
				val.addContent(vListstDes);
				
				//获得该变量的关注度
				sqlCode = "select dval from com where com_id='" + Num + "'";
				Statement st3 = con.createStatement();
				ResultSet re3 = st3.executeQuery(sqlCode);
				if (re3.next()) {
					Element vListDval = new Element("vListDval");
					vListDval.addContent(re3.getString(1));
					val.addContent(vListDval);
				}
				
				vals.addContent(val);
				i++;
			}
			
			XMLOutputter outputter = new XMLOutputter();
			if(i==0)
			{
			    return "";
			}
			else
			{
			    return outputter.outputString(new Document(vals));
			}
		}
		catch (SQLException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	
	public String getPatientName(String strFileName) 
	{
		if (con == null) {
			return null;
		}
		
		Element root = new Element("vals");
		try 
		{
			con.setAutoCommit(true);
			Statement statement = con.createStatement();
			String sqlCode = "select Name, Sex, Age from patient where FileName = '" + strFileName.trim() + "'";
			ResultSet result = statement.executeQuery(sqlCode);
			
			while (result.next()) 
			{
				Element val = new Element("val");
				
				Element vName = new Element("vName");
				vName.addContent(result.getString(1));
				val.addContent(vName);

				Element vSex = new Element("vSex");
				vSex.addContent(result.getString(2));
				val.addContent(vSex);
				
				Element vAge = new Element("vAge");
				vAge.addContent(result.getString(3));
				val.addContent(vAge);
								
				root.addContent(val);
			}	
			
		
		} 
		
		catch (SQLException e) 
		{
			e.printStackTrace();
			return null;
		}
		Document slctDoc = new Document(root);
		XMLOutputter outputter = new XMLOutputter();
		return outputter.outputString(slctDoc);
		
	}
	
	public String getInitVListZhengzhuangNumst(String comID)
	{
		if (con == null) {
			return null;
		}
		Element root = new Element("vals");
		try 
		{
			con.setAutoCommit(true);
			Statement statement = con.createStatement();
			String sqlCode = "select state_id, state_des from state where com_id = '" + comID.trim() + "' and com_type = 'X' ";
			ResultSet result = statement.executeQuery(sqlCode);
			
			while (result.next()) 
			{
				Element val = new Element("val");
				
				Element vListNumst = new Element("vListNumst");
				vListNumst.addContent(result.getString(1));
				val.addContent(vListNumst);

				Element vListID = new Element("vListID");
				vListID.addContent(comID);
				val.addContent(vListID);
				
				Element vListstDes = new Element("vListstDes");
				vListstDes.addContent(result.getString(2));
				val.addContent(vListstDes);
											
				root.addContent(val);
			}	
			
		
		} 
		
		catch (SQLException e) 
		{
			e.printStackTrace();
			return null;
		}
		Document slctDoc = new Document(root);
		XMLOutputter outputter = new XMLOutputter();
		return outputter.outputString(slctDoc);
	}
	
	
	
	public int vListSave(ArrayCollection stGridPrd, String strFileName)
	{
		if (con == null) {
			return 0;
		}
		
		try 
		{
			con.setAutoCommit(true);
			String strDelete = "delete from evidence where FileName = '" + strFileName + "'";
			Statement st = con.createStatement();
			st.execute(strDelete);
			st.close();
		}
		catch (SQLException e) 
		{
			e.printStackTrace();
			return 0;
		}
		ASTranslator asT = new ASTranslator();
		String strAdd = "INSERT INTO evidence (FileName, Num, State) VALUES";
		for(int i=0; i<stGridPrd.size(); i++)
		{
			ASObject asObj = (ASObject) stGridPrd.get(i);
			asObj.setType("gct.NewnodeStatus");
			NewnodeStatus nodeSt = (NewnodeStatus) asT.convert(asObj,
					NewnodeStatus.class);
			
			String strNum = nodeSt.vListID;
			String strState = nodeSt.vListNumst;
			if(i==stGridPrd.size() -1)
			{
				strAdd = strAdd + "('" + strFileName + "','" + strNum + "','" + strState + "'); ";
			}
			else
			{
			    strAdd = strAdd + "('" + strFileName + "','" + strNum + "','" + strState + "'), ";
			}
			
		}
		
		//System.out.println(strAdd);
		
		try 
		{
		    Statement st1 = con.createStatement();
		    st1.execute(strAdd);
		    st1.close();
		}
		catch (SQLException e) 
		{
			e.printStackTrace();
			return 0;
		}
		
		
		return 1;
	}

	public int vListSavePatient(String strFileName, String strName, String strSex, String strAge)
	{
		if (con == null) {
			return 0;
		}
		
		try 
		{
			con.setAutoCommit(true);
			String strDelete = "delete from patient where FileName = '" + strFileName + "'";
			Statement st = con.createStatement();
			st.execute(strDelete);
			st.close();
		}
		catch (SQLException e) 
		{
			e.printStackTrace();
			return 0;
		}
		
		String strAdd = "INSERT INTO patient (FileName, Name, Sex, Age) VALUES";
		strAdd = strAdd + "('" + strFileName + "','" + strName + "','" + strSex+ "','" + strAge + "'); ";
		
		try 
		{
		    Statement st1 = con.createStatement();
		    st1.execute(strAdd);
		    st1.close();
		}
		catch (SQLException e) 
		{
			e.printStackTrace();
			return 0;
		}
		
		
		return 1;
	}

	
	public String getInitVListPatient() {
		if (con == null) {
			return null;
		}

		Element vals = new Element("vals");

		try {
			con.setAutoCommit(true);
			
			String sqlCode = "select FileName, Name, Sex, Age from patient ";

			Statement st1 = con.createStatement();
			ResultSet re1 = st1.executeQuery(sqlCode);
			while (re1.next()) {
				
				Element val = new Element("val");
				
				Element vID = new Element("vID");
				vID.addContent(re1.getString(1));
				val.addContent(vID);
				
				Element vName = new Element("vName");
				vName.addContent(re1.getString(2));
				val.addContent(vName);
				
				Element vSex = new Element("vSex");
				vSex.addContent(re1.getString(3));
				val.addContent(vSex);
				
				Element vAge = new Element("vAge");
				vAge.addContent(re1.getString(4));
				val.addContent(vAge);
				
				vals.addContent(val);
			}

			XMLOutputter outputter = new XMLOutputter();
			return outputter.outputString(new Document(vals));

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}

	}
		
	public String getInitVListZhengzhuang() {
		if (con == null) {
			return null;
		}

		Element vals = new Element("vals");

		try {
			con.setAutoCommit(true);
			
			String sqlCode = "select com_id, com_des, com_name, two, dval from com where com_type = 'X' and one = '0' AND isshow='1'";

			Statement st1 = con.createStatement();
			ResultSet re1 = st1.executeQuery(sqlCode);
			while (re1.next()) {
				Element val = new Element("val");
			
				String Num = re1.getString(1);
				Element vListID = new Element("vListID");
				vListID.addContent(Num);
				val.addContent(vListID);
				
				Element vListDes = new Element("vListDes");
				vListDes.addContent(re1.getString(2));
				val.addContent(vListDes);
				
				Element vListValName = new Element("vListValName");
				vListValName.addContent(re1.getString(3));
				val.addContent(vListValName);
				
				Element vListTwo = new Element("vListTwo");
				vListTwo.addContent(re1.getString(4));
				val.addContent(vListTwo);
				
				Element vListOne = new Element("vListOne");
				vListOne.addContent("0");
				val.addContent(vListOne);
				
				Element vListValNum = new Element("vListValNum");
				vListValNum.addContent("*");
				val.addContent(vListValNum);
				
				Element vListStateDscp = new Element("vListStateDscp");
				vListStateDscp.addContent("正常");
				val.addContent(vListStateDscp);
				
				Element vListValState = new Element("vListValState");
				vListValState.addContent("0");
				val.addContent(vListValState);
				
				Element stColor = new Element("stColor");
				stColor.addContent("6750003");
				val.addContent(stColor);
				
				Element stDisplayType = new Element("stDisplayType");
				stDisplayType.addContent("标牌显示");
				val.addContent(stDisplayType);
				
				Element vListNumst = new Element("vListNumst");
				vListNumst.addContent("-1");
				val.addContent(vListNumst);

				Element vListstDes = new Element("vListstDes");
				vListstDes.addContent("未知");
				val.addContent(vListstDes);
				
				Element vListDval = new Element("vListDval");
				vListDval.addContent(re1.getString(5));
				val.addContent(vListDval);
				
				vals.addContent(val);
			}

			XMLOutputter outputter = new XMLOutputter();
			return outputter.outputString(new Document(vals));

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
		
	public String getInitVListTizheng()
	{
		if (con == null) {
			return null;
		}
		Element root = new Element("vals");
		try 
		{
			con.setAutoCommit(true);
			Statement statement = con.createStatement();
			String sqlCode = "select com_id, com_des, com_name, two from com where com_type = 'X' and one = '1' AND isshow='1'";
			
			ResultSet result = statement.executeQuery(sqlCode);
			
			while (result.next()) 
			{
				Element val = new Element("val");
				
				Element vListID = new Element("vListID");
				vListID.addContent(result.getString(1));
				val.addContent(vListID);

				Element vListDes = new Element("vListDes");
				vListDes.addContent(result.getString(2));
				val.addContent(vListDes);
				
				Element vListValName = new Element("vListValName");
				vListValName.addContent(result.getString(3));
				val.addContent(vListValName);
				
				Element vListTwo = new Element("vListTwo");
				vListTwo.addContent(result.getString(4));
				val.addContent(vListTwo);
				
				Element vListOne = new Element("vListOne");
				vListOne.addContent("1");
				val.addContent(vListOne);
			
				
				Element vListValNum = new Element("vListValNum");
				vListValNum.addContent("*");
				val.addContent(vListValNum);
				
				Element vListStateDscp = new Element("vListStateDscp");
				vListStateDscp.addContent("正常");
				val.addContent(vListStateDscp);
				
				Element vListValState = new Element("vListValState");
				vListValState.addContent("0");
				val.addContent(vListValState);
				
				Element stColor = new Element("stColor");
				stColor.addContent("6750003");
				val.addContent(stColor);
				
				Element stDisplayType = new Element("stDisplayType");
				stDisplayType.addContent("标牌显示");
				val.addContent(stDisplayType);
				
				Element vListNumst = new Element("vListNumst");
				vListNumst.addContent("-1");
				val.addContent(vListNumst);

				Element vListstDes = new Element("vListstDes");
				vListstDes.addContent("未知");
				val.addContent(vListstDes);


				root.addContent(val);
			}	
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			return null;
		}
		Document slctDoc = new Document(root);
		XMLOutputter outputter = new XMLOutputter();
		return outputter.outputString(slctDoc);
	}
	
	
	public String getInitVListYingxiangxuejc()
	{
		if (con == null) {
			return null;
		}
		Element root = new Element("vals");
		try 
		{
			con.setAutoCommit(true);
			Statement statement = con.createStatement();
			String sqlCode = "select com_id, com_des, com_name, two from com where com_type = 'X' and one = '2' AND isshow='1'";
			
			ResultSet result = statement.executeQuery(sqlCode);
			
			while (result.next()) 
			{
				Element val = new Element("val");
				
				Element vListID = new Element("vListID");
				vListID.addContent(result.getString(1));
				val.addContent(vListID);

				Element vListDes = new Element("vListDes");
				vListDes.addContent(result.getString(2));
				val.addContent(vListDes);
				
				Element vListValName = new Element("vListValName");
				vListValName.addContent(result.getString(3));
				val.addContent(vListValName);
				
				Element vListTwo = new Element("vListTwo");
				vListTwo.addContent(result.getString(4));
				val.addContent(vListTwo);
				
				Element vListOne = new Element("vListOne");
				vListOne.addContent("2");
				val.addContent(vListOne);
				
				Element vListValNum = new Element("vListValNum");
				vListValNum.addContent("*");
				val.addContent(vListValNum);
				
				Element vListStateDscp = new Element("vListStateDscp");
				vListStateDscp.addContent("正常");
				val.addContent(vListStateDscp);
				
				Element vListValState = new Element("vListValState");
				vListValState.addContent("0");
				val.addContent(vListValState);
				
				Element stColor = new Element("stColor");
				stColor.addContent("6750003");
				val.addContent(stColor);
				
				Element stDisplayType = new Element("stDisplayType");
				stDisplayType.addContent("标牌显示");
				val.addContent(stDisplayType);

				Element vListNumst = new Element("vListNumst");
				vListNumst.addContent("-1");
				val.addContent(vListNumst);

				Element vListstDes = new Element("vListstDes");
				vListstDes.addContent("未知");
				val.addContent(vListstDes);

				
				root.addContent(val);
			}	
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			return null;
		}
		Document slctDoc = new Document(root);
		XMLOutputter outputter = new XMLOutputter();
		return outputter.outputString(slctDoc);
	}
	
	
	public String getInitVListShiyanshijc()
	{
		if (con == null) {
			return null;
		}
		Element root = new Element("vals");
		try 
		{
			con.setAutoCommit(true);
			Statement statement = con.createStatement();
			String sqlCode = "select com_id, com_des, com_name, two from com where com_type = 'X' and one = '3' AND isshow='1'";
			ResultSet result = statement.executeQuery(sqlCode);
			
			while (result.next()) 
			{
				Element val = new Element("val");
				
				Element vListID = new Element("vListID");
				vListID.addContent(result.getString(1));
				val.addContent(vListID);

				Element vListDes = new Element("vListDes");
				vListDes.addContent(result.getString(2));
				val.addContent(vListDes);
				
				Element vListValName = new Element("vListValName");
				vListValName.addContent(result.getString(3));
				val.addContent(vListValName);
				
				Element vListTwo = new Element("vListTwo");
				vListTwo.addContent(result.getString(4));
				val.addContent(vListTwo);
				
				Element vListOne = new Element("vListOne");
				vListOne.addContent("3");
				val.addContent(vListOne);
				
				Element vListValNum = new Element("vListValNum");
				vListValNum.addContent("*");
				val.addContent(vListValNum);
				
				Element vListStateDscp = new Element("vListStateDscp");
				vListStateDscp.addContent("正常");
				val.addContent(vListStateDscp);
				
				Element vListValState = new Element("vListValState");
				vListValState.addContent("0");
				val.addContent(vListValState);
				
				Element stColor = new Element("stColor");
				stColor.addContent("6750003");
				val.addContent(stColor);
				
				Element stDisplayType = new Element("stDisplayType");
				stDisplayType.addContent("标牌显示");
				val.addContent(stDisplayType);
				
				Element vListNumst = new Element("vListNumst");
				vListNumst.addContent("-1");
				val.addContent(vListNumst);

				Element vListstDes = new Element("vListstDes");
				vListstDes.addContent("未知");
				val.addContent(vListstDes);


				root.addContent(val);
			}	
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			return null;
		}
		Document slctDoc = new Document(root);
		XMLOutputter outputter = new XMLOutputter();
		return outputter.outputString(slctDoc);
	}
	
		/**
	 * 获得变量的状态表
	 * @param vType 变量类型
	 * @param vid 变量编号
	 * @return 存储变量状态信息的XML字符串
	 */
	public String getVarPolicyList(String vType, String vid, String vState) 
	{
		if (con == null) 
		{
			return null;
		}

		Element root = new Element("val");
		try 
		{
			con.setAutoCommit(true);
			Statement statement = con.createStatement();
			String sqlCode = "";
			if (vState.equals(""))
			{
				sqlCode = "select state_id, state_des, policy_pro, policy_event, policy_method, "
					+ "policy_result, policy_ratio from policy where com_id='"
					+ vid + "' and com_type='"
					+ vType + "' order by state_id";
			}
			else 
			{
				sqlCode = "select state_id, state_des, policy_pro, policy_event, policy_method, "
				+ "policy_result, policy_ratio from policy where com_id='"
				+ vid + "' and com_type='"
				+ vType + "' and state_id='" + vState + "'";
			}
			ResultSet result = statement.executeQuery(sqlCode);
			if (result.next())
			{
				Element varStateID = new Element("varStateID");
				Element varDscp = new Element("varDscp");
				Element policyPro = new Element("policyPro");
				Element policyEvent = new Element("policyEvent");
				Element policyMethod = new Element("policyMethod");
				Element policyResult = new Element("policyResult");
				Element policyRatio = new Element("policyRatio");
				Element varPolicy = new Element("varPolicy");
				varStateID.addContent(result.getString(1));
				varPolicy.addContent(varStateID);
				
				varDscp.addContent(result.getString(2));
				varPolicy.addContent(varDscp);
				
				policyPro.addContent(result.getString(3));
				varPolicy.addContent(policyPro);
				
				policyEvent.addContent(result.getString(4));
				varPolicy.addContent(policyEvent);
				
				policyMethod.addContent(result.getString(5));
				varPolicy.addContent(policyMethod);
				
				policyResult.addContent(result.getString(6));
				varPolicy.addContent(policyResult);
				
				policyRatio.addContent(result.getString(7));
				varPolicy.addContent(policyRatio);
				
				root.addContent(varPolicy);
				
				while (result.next())
				{
					varPolicy = new Element("varPolicy");
					varStateID.addContent(result.getString(1));
					varPolicy.addContent(varStateID);
					
					varDscp.addContent(result.getString(2));
					varPolicy.addContent(varDscp);
					
					policyPro.addContent(result.getString(3));
					varPolicy.addContent(policyPro);
					
					policyEvent.addContent(result.getString(4));
					varPolicy.addContent(policyEvent);
					
					policyMethod.addContent(result.getString(5));
					varPolicy.addContent(policyMethod);
					
					policyResult.addContent(result.getString(6));
					varPolicy.addContent(policyResult);
					
					policyRatio.addContent(result.getString(7));
					varPolicy.addContent(policyRatio);
					
					root.addContent(varPolicy);
				}
			}
			else 
			{
				sqlCode = "select state_id, state_des, state_gailv from state where com_id='"
						+ vid + "' and com_type='"
						+ vType + "' and state_id != '0' order by state_id";
				result = statement.executeQuery(sqlCode);
				while (result.next()) 
				{
					Element varStateID = new Element("varStateID");
					Element varDscp = new Element("varDscp");
					Element policyPro = new Element("policyPro");
					Element policyEvent = new Element("policyEvent");
					Element policyMethod = new Element("policyMethod");
					Element policyResult = new Element("policyResult");
					Element policyRatio = new Element("policyRatio");
					Element varPolicy = new Element("varPolicy");
					varStateID.addContent(result.getString(1));
					varPolicy.addContent(varStateID);
	
					varDscp.addContent(result.getString(2));
					varPolicy.addContent(varDscp);
					
					policyPro.addContent(result.getString(3));
					varPolicy.addContent(policyPro);
					
					policyEvent.addContent("");//
					varPolicy.addContent(policyEvent);
					
					policyMethod.addContent("");
					varPolicy.addContent(policyMethod);
					
					policyResult.addContent("");
					varPolicy.addContent(policyResult);
					
					policyRatio.addContent("");
					varPolicy.addContent(policyRatio);
					
					root.addContent(varPolicy);
				}
			}
			
			Document slctDoc = new Document(root);
			XMLOutputter outputter = new XMLOutputter();
			return outputter.outputString(slctDoc);

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * 保存决策库信息
	 */
	public String saveVarPolicy(String varID, String varType, ArrayList varPolicyGridPrd)
	{
		if(con == null)
		{
			return "-2";
		}
		
		try
		{
			con.setAutoCommit(true);
			String sqlCode = "delete from policy where com_id='" + varID + "' and com_type='" + varType + "'";
			Statement statements = con.createStatement();
			statements.execute(sqlCode);

			for (int i = 0; i < varPolicyGridPrd.size(); i++) 
			{
				Map ha = (Map) (varPolicyGridPrd.get(i));
				String varStateID 	= (String) ha.get("varStateID");
				String varDscp 		= (String) ha.get("varDscp");
				String policyPro 	= (String) ha.get("policyPro");
				String policyEvent 	= (String) ha.get("policyEvent");
				String policyMethod = (String) ha.get("policyMethod");
				String policyResult = (String) ha.get("policyResult");
				String policyRatio 	= (String) ha.get("policyRatio");
				sqlCode = "insert into policy(state_id,com_id,com_type,state_des,policy_pro,policy_event,policy_method,policy_result, policy_ratio) values(?,?,?,?,?,?,?,?,?)";
				PreparedStatement statement = con.prepareStatement(sqlCode);
				statement.setString(1, varStateID);
				statement.setString(2, varID);
				statement.setString(3, varType);
				statement.setString(4, varDscp);
				statement.setString(5, policyPro);
				statement.setString(6, policyEvent);
				statement.setString(7, policyMethod);
				statement.setString(8, policyResult);
				statement.setString(9, policyRatio);
				statement.executeUpdate();
			}
		}
		catch (SQLException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "-1";
		}
		return "1";
	}
	
	/**
	 * 获得指定变量的关联变量，即上下游变量，如果上下游变量为检查类变量（com表中one为2、3），
	 * 继续寻找这个变量的上下游变量
	 * @param COM_ID，COM_TYPE, DVAL
	 * @return 上下游变量的变量描述，状态，关注度（输入变量的关注度+上下游变量的关注度）, String dval
	 */
	public String getAssociateVariable(String com_id, String com_type) {
		Element assVarlEle = new Element("vals");
		String[]	comidStr = com_id.split("_");//已选择需要检查变量的iD
		
		for (int m = 0; m < comidStr.length; m++){
			
			//1.查找当前变量的B或者BX父变量；
			StringBuffer parentVarStrBuf = new StringBuffer("");
			getParentVar(comidStr[m], com_type, parentVarStrBuf);
			String parentVarStr = parentVarStrBuf.toString();
			
			//如果当前变量存在父变量
			if (parentVarStr.compareTo("") != 0){
				System.out.println("\n====>>\n当前变量" + com_type + comidStr[m] + "的父变量是：" + parentVarStrBuf);
				
				//2.查找当前变量的下游变量；
				StringBuffer childVarStrBuf = new StringBuffer("");
				HashMap<String, Element> hashChildInfor = new HashMap<String, Element>();
				getChildVar(comidStr[m], com_type, childVarStrBuf, hashChildInfor);
				System.out.println("当前变量" + com_type + comidStr[m] + "的下游变量是：" + childVarStrBuf);
				
				//3.查找下游变量的B或者BX父变量，将子变量和父变量存入哈希列表；
				String childVarStr = childVarStrBuf.toString();
				if (childVarStr.compareTo("") != 0)
				{
					String[] childVarArr = childVarStrBuf.substring(0, childVarStrBuf.length() - 1).toString().split("_");
					HashMap<String, String> hashChildPar = new HashMap<String, String>();
					System.out.println("共 " + String.valueOf(childVarArr.length) + "个子变量");
					
					for (int i = 0; i < childVarArr.length; i++)
					{
						String type = childVarArr[i].substring(0, 1);
						String id = childVarArr[i].substring(1, childVarArr[i].length());
						StringBuffer childParentVarStrBuf = new StringBuffer("");
						getParentVar(id, type, childParentVarStrBuf);
						hashChildPar.put(type + id, childParentVarStrBuf.toString());
						System.out.println("下游变量" + type + id + "的父变量是：" + childParentVarStrBuf.toString());
					}
							
					//4.比较两种父变量，是否出现了新的父变量
					String parentStr = parentVarStrBuf.toString();
					//采用Iterator遍历HashMap  
			        Iterator it = hashChildPar.keySet().iterator();  
			        while(it.hasNext()) 
			        {  
			            String hashKeyChildStr = (String)it.next();  
			            String hashValueParentStr = hashChildPar.get(hashKeyChildStr);
			            String[] parentArr = hashValueParentStr.split("_");
			            boolean isExistNewParNode = false;
			            
			            for (int j = 0; j < parentArr.length; j++)
			            {
			            	//如果子变量出现了新的父变量，则保留该子变量作为关联变量
				            if (parentStr.indexOf(parentArr[j]) < 0){
								System.out.println("查找出来符合要求的子变量 " + hashKeyChildStr + " 的父变量是-> " + parentArr[j]);
								isExistNewParNode = true;
								break;
				            }
			            }
			            //如果没有出现新的父变量，则删除掉
			            if (!isExistNewParNode){
			            	hashChildInfor.remove(hashKeyChildStr);
			            }
			        } 
			        
			        it = hashChildInfor.keySet().iterator(); 
			        while(it.hasNext()) {  
			            String hashKeyChildStr = (String)it.next();  
			            assVarlEle.addContent(hashChildInfor.get(hashKeyChildStr));
			        }
				}
			}
			//5.直接查找该变量的关联变量
			getAssVarInfo(com_id, com_type, assVarlEle);
		}
		
		
			
		XMLOutputter outputter = new XMLOutputter();
		return outputter.outputString(new Document(assVarlEle));
	}
	/**
	 * 2016-10-10
	 * 递归查找当前变量的父变量B或者BX类型
	 */
	public void getParentVar(String com_id, String com_type, StringBuffer parentVar) {
		if (con == null) {
			return;
		}
		try {
			//查找父变量
			String sqlCode = "select REL_BEGIN_ID, REL_BEGIN_TYPE from relation where REL_END_ID='"
							+ com_id + "' and REL_END_TYPE='" + com_type + "'";
			
			con.setAutoCommit(true);
			Statement statement = con.createStatement();
			ResultSet result = statement.executeQuery(sqlCode);
			while (result.next()) {
				String strID = result.getString(1);
				String strTYPE = result.getString(2);
				if (strTYPE.compareTo("B") == 0 || strTYPE.compareTo("U") == 0)
				{
					if (parentVar.indexOf(strTYPE + strID + "_") < 0){
						parentVar.append(strTYPE + strID + "_");
					}
				}
				else if(strTYPE.compareTo("D") != 0) {
					getParentVar(strID, strTYPE, parentVar);
				}
			}
		}
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return;
		}
	}
	
	/**
	 * 2016-10-10
	 * 查找子变量，为的是查找这些子变量是否有新的B、BX类型的变量
	 */
	public void getChildVar(String com_id, String com_type, StringBuffer childVar, HashMap<String, Element> hashChildInfor) {
		if (con == null) {
			return;
		}
		try {
			//查找子变量
			String sqlCode = "select REL_END_ID, REL_END_TYPE, REL_R from relation where REL_BEGIN_ID='"
							+ com_id + "' and REL_BEGIN_TYPE='" + com_type +  "' and REL_TYPE != '7'";
			con.setAutoCommit(true);
			Statement statement = con.createStatement();
			ResultSet result = statement.executeQuery(sqlCode);
			while (result.next()) {
				String strID = result.getString(1);
				String strTYPE = result.getString(2);
				
				//子变量必须是X类型的变量
				if (strTYPE.compareTo("X") == 0)
				{
					String sqlCodefindVar = "select com_id, com_des, two, isshow from com where com_id ='"
											+ strID + "' and com_type='" + strTYPE + "'";
					
					Statement smsqlCodefindVar = con.createStatement();
					ResultSet refindVar = smsqlCodefindVar.executeQuery(sqlCodefindVar);
					
					if (refindVar.next()) {
						String strIsShow = refindVar.getString(4); 	//1：可检测变量；0：不可检测变量
						//检索可检测变量
						if (strIsShow.equals("1")) {
							childVar.append(strTYPE + strID + "_");
							
							Element val = new Element("val");							
							
							Element vListID = new Element("vListID");
							vListID.addContent(refindVar.getString(1));
							val.addContent(vListID);
							
							Element vListTypeID = new Element("vListTypeID");
							vListTypeID.addContent(strTYPE + refindVar.getString(1));
							val.addContent(vListTypeID);
							
							Element vListDes = new Element("vListDes");
							vListDes.addContent(refindVar.getString(2));
							val.addContent(vListDes);
							
							Element vListTwo = new Element("vListTwo");
							vListTwo.addContent(refindVar.getString(3));
							val.addContent(vListTwo);
							
							Element vListAttention = new Element("vListAttention");
							vListAttention.addContent(String.valueOf(result.getInt(3)));
							val.addContent(vListAttention);
							
							//症状初始状态
							Element vListNumst = new Element("vListNumst");
							vListNumst.addContent("-1");
							val.addContent(vListNumst);
							//症状初始显示
							Element vListstDes = new Element("vListstDes");
							vListstDes.addContent("未知");
							val.addContent(vListstDes);
							
							Element isSelected = new Element("isSelected");
							isSelected.addContent("");
							val.addContent(isSelected);
							
							hashChildInfor.put(strTYPE + strID, val);
						}
						else {
							getChildVar(strID, strTYPE, childVar, hashChildInfor);
						}
					}
				}
			}
		}
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return;
		}
	}

	/**HashMap<String, Element> hashChildInfor
	 * 2016-11-2
	 * 直接查找该变量的关联变量
	 */
	String strAssVarTypeIDSum = "";
	public void getAssVarInfo(String com_id, String com_type, Element assVarlEle) {
		if (con == null) {
			return;
		}
		try {
			//查找子变量
			String sqlCode = "select REL_END_ID, REL_END_TYPE, REL_R, REL_TYPE from relation where REL_BEGIN_ID='"
							+ com_id + "' and REL_BEGIN_TYPE='" + com_type +  "'";
			con.setAutoCommit(true);
			Statement statement = con.createStatement();
			ResultSet result = statement.executeQuery(sqlCode);
			
			while (result.next()) {
				String strID = result.getString(1);
				String strTYPE = result.getString(2);
				String strvListAttention = String.valueOf(result.getInt(3));
				String strRelType = result.getString(4);
				int isIncludeIndex = strAssVarTypeIDSum.indexOf(strTYPE + strID);
				if (isIncludeIndex >= 0)
				{
					continue;
				}
				strAssVarTypeIDSum = strAssVarTypeIDSum + strTYPE + strID + "_"; 
				//关联变量必须是X类型的变量
				if (strTYPE.compareTo("X") == 0 && strRelType.equals("7") && (isIncludeIndex < 0))
				{
					//获得该变量的详细信息
					String sqlCodefindVar = "select com_id, com_des, two from com where com_id ='"
											+ strID + "' and com_type='" + strTYPE + "'";
					
					Statement smsqlCodefindVar = con.createStatement();
					ResultSet refindVar = smsqlCodefindVar.executeQuery(sqlCodefindVar);
					
					if (refindVar.next()) {
						Element val = new Element("val");							
						
						Element vListID = new Element("vListID");
						vListID.addContent(refindVar.getString(1));
						val.addContent(vListID);
						
						Element vListTypeID = new Element("vListTypeID");
						vListTypeID.addContent(strTYPE + refindVar.getString(1));
						val.addContent(vListTypeID);
						
						Element vListDes = new Element("vListDes");
						vListDes.addContent(refindVar.getString(2));
						val.addContent(vListDes);
						
						Element vListTwo = new Element("vListTwo");
						vListTwo.addContent(refindVar.getString(3));
						val.addContent(vListTwo);
						
						Element vListAttention = new Element("vListAttention");
						vListAttention.addContent(strvListAttention);
						val.addContent(vListAttention);
						
						//症状初始状态
						Element vListNumst = new Element("vListNumst");
						vListNumst.addContent("-1");
						val.addContent(vListNumst);
						//症状初始显示
						Element vListstDes = new Element("vListstDes");
						vListstDes.addContent("未知");
						val.addContent(vListstDes);
						
						Element isSelected = new Element("isSelected");
						isSelected.addContent("");
						val.addContent(isSelected);
						
						//hashChildInfor.put(strTYPE + strID, val);
						assVarlEle.addContent(val);
					}
				}
				else if (strTYPE.equals("G"))
				{
					getAssVarInfo(strID, strTYPE, assVarlEle);
				}
			}
		}
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return;
		}
	}
	
	/**
	 * 获得非B类型变量列表，现出到决策库的建造中
	 */
	public String getXNode()
	{
		if(con == null)
		{
			return null;
		}
		
		Element root = new Element("vals");
		try 
		{
			con.setAutoCommit(true);
			String sqlCode = "select state_id, com_id, com_type from state where " +
							 "com_type != 'B' and state_id != '0' order by com_id";
			Statement state = con.createStatement();
			
			ResultSet result = state.executeQuery(sqlCode);
			while(result.next())
			{
				Element val = new Element("val");
				String strStateID = result.getString(1);
				String strComID = result.getString(2);
				String strType = result.getString(3);
				String strVar = strType + strComID + "," + strStateID;
				Element vType = new Element("strVar");
				vType.addContent(strVar);
				val.addContent(vType);
				
				root.addContent(val);
			}
			XMLOutputter outputter = new XMLOutputter();
			return outputter.outputString(root);
			
		}
		catch (SQLException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
}
/**
 * 变量基本信息
 * @author SHU
 *
 */
class ComMsg {
	/**
	 * 变量Id
	 */
	public String id;
	/**
	 * 变量类型
	 */
	public String type;
	/**
	 * 变量名称
	 */
	public String name;
	/**
	 * 变量描述
	 */
	public String des;
}
