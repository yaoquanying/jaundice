package gct;
/**
 * 表示变量基本信息的类
 * @author SHU
 *
 */
public class ValBscInfo {
	/**
	 * 变量类型
	 */
	String type;
	/**
	 * 变量ID
	 */
	String id;
	/**
	 * 变量名称
	 */
	String name;
	/**
	 * 变量描述
	 */
	String dscp;
	/**
	 * 变量关键字
	 */
	String vKeys;
	/**
	 * 变量状态数量
	 */
	int stNum;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDscp() {
		return dscp;
	}

	public void setDscp(String dscp) {
		this.dscp = dscp;
	}

	public String getvKeys() {
		return vKeys;
	}

	public void setvKeys(String vKeys) {
		this.vKeys = vKeys;
	}

	public int getStNum() {
		return stNum;
	}

	public void setStNum(int stNum) {
		this.stNum = stNum;
	}
}
