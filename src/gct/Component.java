package gct;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.jdom.Document;
import org.jdom.Element;

/**
 * 描述各种变量的类
 * @author SHU
 *
 */
public class Component {
	
	/**
	 * 变量的id
	 */
	public String id="";
	
	/**
	 * 变量类型
	 */
	public String type="";
	
	/**
	 * 连接数据库的接口
	 */
	private Connection con;
	
	/**
	 * 通过statement来执行对数据库的访问
	 */
	private Statement statement;
	
	
	/**
	 * 构造方法，传入变量的编号和类型，通过本方法，从数据库中取出本变量的所有属性，赋值给本类中的变量的属性值
	 * @param id 变量的编号
	 * @param type 变量的类型
	 */
	Component(String id,String type){
		con=UtilsSQL.con;
		try {
			statement =con.createStatement();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		this.id=id;
		this.type=type;
		if (con == null) {
			return;
		}
		
		//从数据库中取出边路的基本属性
		try {
			con.setAutoCommit(true);
			String sqlCode = "select com_name,com_des,com_keyword,cedian from com where com_id='"+id+"' and com_type='"+type+"'";
			ResultSet result = statement.executeQuery(sqlCode);
			if(result.next()){
				this.name=result.getString(1);
				this.des=result.getString(2);
				this.keyWord=result.getString(3);
				this.ceDian=result.getString(4);
			}
			else{
				return;
			}
			
			//从数据库中取出变量的状态属性
			sqlCode = "select state_id,state_qujian,state_des,state_type,state_gailv,state_color from state where com_id='"
				+id+"' and com_type='"+type+"' order by state_id desc";
			result = statement.executeQuery(sqlCode);
			
			//从数据库返回的结果中解析变量的状态属性
			while(result.next()){
				State temp = new State();
				temp.stateId=result.getString(1);
				temp.quJian=result.getString(2);
				temp.stateDes=result.getString(3);
				temp.stateType=result.getString(4);
				temp.gaiLv=result.getString(5);
				temp.stateColor=result.getString(6);
				state.add(temp);
			}
			
			stateNum=state.size();
//			System.out.println("状态数:"+stateNum);
			
			
			//获得变量在工程图中的具体的位置信息
			sqlCode = "select image_id,location_x,location_y,location_xper,location_yper,color from comimage where com_id='"+id+"' and com_type='"+type+"'";
			result = statement.executeQuery(sqlCode);
			//从数据库返回的结果中解析变量在工程图中的信息
			while(result.next()){
				Location temp=new Location();
				temp.imageId=result.getString(1);
				temp.plocationX=result.getString(2);
				temp.plocationY=result.getString(3);
				temp.plocationXPer=result.getString(4);
				temp.plocationYPer=result.getString(5);
				temp.color=result.getString(6);
				location.add(temp);
			}
			locationNum=location.size();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 变量的名称
	 */
	public String name;
	
	/**
	 * 变量描述
	 */
	public String des;
	
	/**
	 * 变量的关键字
	 */
	public String keyWord;
	
	/**
	 * 变量所关联的测点
	 */
	public String ceDian;
	
	/**
	 * 变量在工程图中出现的次数
	 */
	public int locationNum=0;
	/**
	 * 存储变量在工程图中的位置的信息的集合
	 */
	public ArrayList<Location> location = new ArrayList<Location>();
	
	/**
	 * 表示变量在工程图中具体信息的类
	 * @author SHU
	 *
	 */
	class Location{
		
		/**
		 * 变量在工程图中的位置的X坐标
		 */
		public String plocationX;
		/**
		 * 变量在工程图中的位置的Y坐标
		 */
		public String plocationY;
		/**
		 * 变量在工程图中X位置的缩放比
		 */
		public String plocationXPer;
		/**
		 * 变量在工程图中Y位置的缩放比
		 */
		public String plocationYPer;
		/**
		 * 变量所处的工程图的id
		 */
		public String imageId;
		/**
		 * 变量在工程图中所用框的颜色
		 */
		public String color;
	}
	
	/**
	 * 变量的状态的数量
	 */
	public int stateNum=0;
	
	/**
	 * 用来存储变量的状态属性的集合
	 */
	public ArrayList<State> state= new ArrayList<State>();
	
	/**
	 * 用来描述变量的状态属性的类
	 * @author SHU
	 *
	 */
	class State{
		/**
		 * 变量的状态编号
		 */
		public String stateId;
		/**
		 * 变量的状态区间
		 */
		public String quJian;
		/**
		 * 变量的状态描述
		 */
		public String stateDes;
		/**
		 * 变量的状态类型
		 */
		public String stateType;
		/**
		 * 变量在该状态的概率
		 */
		public String gaiLv;
		/**
		 * 表示该变量处在该状态的颜色
		 */
		public String stateColor;
	}
	
	
	/**
	 * 将变量信息存储到数据库中
	 */
	public void writeToDB()
	{
		
		if (con == null) 
		{
			return;
		}
		try 
		{
			//存储变量的基本信息
			con.setAutoCommit(true);
			String sqlCode = "insert into com(com_id,com_type,com_name,com_des,com_keyword,cedian) values (" +
					"'"+id+"','"+type+"','"+name+"','"+des+"','"+keyWord+"','"+ceDian+"')";
			statement.executeUpdate(sqlCode);
			//存储变量的状态信息
			for(State temp:state)
			{
				sqlCode = "insert into state(state_id,com_id,com_type,state_qujian,state_des,state_type,state_gailv,state_color)" +
					"values('"+temp.stateId+"','"+id+"','"+type+"','"+temp.quJian+"','"+temp.stateDes+"','"+temp.stateType+"','"+temp.gaiLv+"','"+temp.stateColor+"')";
				statement.executeUpdate(sqlCode);
			}
			//存储变量在工程图上的信息
			for(Location temp:location)
			{
				sqlCode ="insert into comimage(image_id,location_x,location_y,location_xper,location_yper,color,com_id,com_type)values(?,?,?,?,?,?,?,?)";
				PreparedStatement tempState = con.prepareStatement(sqlCode);
				tempState.setString(1, temp.imageId);
				tempState.setString(2, temp.plocationX);
				tempState.setString(3, temp.plocationY);
				tempState.setString(4, temp.plocationXPer);
				tempState.setString(5, temp.plocationYPer);
				tempState.setString(6, temp.color);
				tempState.setString(7, id);
				tempState.setString(8, type);
				tempState.executeUpdate();
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	
	public Element writeCom(){
		Element root = new Element("val");
		root.setAttribute("type",type );// 设置根节点属性
		root.setAttribute("id", id);
		Element basicInfo = new Element("basicInfo");
		Element vName = new Element("vName");
		vName.addContent(name);
		basicInfo.addContent(vName);
		Element vDes = new Element("vDscp");
		vDes.addContent(des);
		basicInfo.addContent(vDes);
		Element vKeyWord = new Element("vKeyWords");
		vKeyWord.addContent(keyWord);
		basicInfo.addContent(vKeyWord);
		
		Element vLocations = new Element("vLocations");
		vLocations.setAttribute("vLocationNum",String.valueOf(locationNum));
		Element vPlocationX=null;
		Element vPlocationY=null;
		Element vPlocationXPer=null;
		Element vImageId=null;
		Element vColor=null;
		
		for(Location temp:location){
			Element vLocation=new Element("vLocation");
			vPlocationX = new Element("vLocationX").addContent(temp.plocationX);
			vLocation.addContent(vPlocationX);
			vPlocationY = new Element("vLocationY").addContent(temp.plocationY);
			vLocation.addContent(vPlocationY);
			vPlocationXPer = new Element("vLocationXPer").addContent(temp.plocationXPer);
			vLocation.addContent(vPlocationXPer);
			Element vPlocatonYPer = new Element("vLocationYPer").addContent(temp.plocationYPer);
			vLocation.addContent(vPlocatonYPer);
			vImageId = new Element("vImageId").addContent(temp.imageId);
			vLocation.addContent(vImageId);
			vColor = new Element("vColor").addContent(temp.color);
			vLocation.addContent(vColor);
			vLocations.addContent(vLocation);
		}
		
		basicInfo.addContent(vLocations);
		
		Element vStates = new Element("vStates").setAttribute("vStatesNum", String.valueOf(stateNum));
		
		Element vStateId = null;
		Element vQuJian = null;
		Element vStateDes = null;
		Element vStateType =null;
		Element vGaiLv = null;
		Element vStateColor = null;
		
		for(State temp:state){
			Element vState = new Element("vState");
			vStateId = new Element("vStateId").addContent(temp.stateId);
			vState.addContent(vStateId);
			vQuJian = new Element("vQuJian").addContent(temp.quJian);
			vState.addContent(vQuJian);
			vStateDes = new Element("vDes").addContent(temp.stateDes);
			vState.addContent(vStateDes);
			vStateType = new Element("vStateType").addContent(temp.stateType);
			vState.addContent(vStateType);
			vGaiLv = new Element("vGaiLv").addContent(temp.gaiLv);
			vState.addContent(vGaiLv);
			vStateColor = new Element("vColor").addContent(temp.stateColor);
			vState.addContent(vStateColor);
			vStates.addContent(vState);
		}
		root.addContent(basicInfo);
		root.addContent(vStates);
		return root;
	}
}
