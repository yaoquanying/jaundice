package gct;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
/**
 * ������д
 * @author SHU
 *
 */
public class JspServlet extends HttpServlet {  
	   public void doGet(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException {  
	       response.setContentType("text/html");  
	         
	       String scope = request.getParameter("scope");  
	       String param = request.getParameter("param");  
	       String result = null;  
	         
	       if(scope.equals("session")){  
	            result = (String)request.getSession().getAttribute(param);  
	        }else if(scope.equals("application")){  
	            result = (String)getServletContext().getAttribute(param);  
	        }  
	          
	        PrintWriter out = response.getWriter();  
	        out.print(result);  
	        out.flush();  
	        out.close();  
	    }  
	  
	    public void doPost(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException {  
	        doGet(request, response);  
	    }  
	}  
