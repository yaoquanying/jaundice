package gct;
import java.awt.Image;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import javax.imageio.ImageIO;

/**
 * @author SHU
 *
 */
public class GetImage 
{
	public ImageInfo getImage(String imgId)
	{
		ImageInfo imageInfo = new ImageInfo();
		imageInfo.imageID = imgId;
		
		int byteReads;
		imageInfo.imgInfoArr = new byte[20480];
		BufferedInputStream by_is=null;
		ByteArrayOutputStream bao_os=null;
	    String imgPath = GetPathStr.getPathWeb() + "imgUp/" + imgId;
	    
	    try
	    {
	    	by_is = new BufferedInputStream(new FileInputStream(imgPath));
	    	bao_os = new ByteArrayOutputStream();
	    	
	    	byte[] imgBuf = new byte[20480];
	    	while((byteReads = by_is.read(imgBuf, 0, imgBuf.length)) != -1)
	    	{
	    		bao_os.write(imgBuf, 0, byteReads);
	    	}
	    	if(by_is != null)
	    	{
	    		by_is.close();
	    	}
	    }
	    catch(IOException e)
	    {
	    	e.printStackTrace();
	    }
	    if(bao_os != null)
	    {
	    	imageInfo.imgInfoArr = bao_os.toByteArray();
	    }
	    else
	    {
	    	imageInfo.imgInfoArr = null;
	    }
	    return imageInfo;
	}
	//获得缩略图
	public ImageInfo getThumbnails(String imgId)
	{
		ImageInfo imageInfo = new ImageInfo();
		imageInfo.imageID = imgId;
		
		int byteReads;
		imageInfo.imgInfoArr = new byte[20480];
		BufferedInputStream by_is=null;
		ByteArrayOutputStream bao_os=null;
	    String imgPath = GetPathStr.getPathWeb() + "imgUp/Thumb/" + imgId;
	    
	    try
	    {
	    	File fileThumb = new File(imgPath);
			if (!fileThumb.exists())
			{
				autoZoom(imgId);
			}
	    	by_is = new BufferedInputStream(new FileInputStream(imgPath));
	    	bao_os = new ByteArrayOutputStream();
	    	
	    	byte[] imgBuf = new byte[20480];
	    	while((byteReads = by_is.read(imgBuf, 0, imgBuf.length)) != -1)
	    	{
	    		bao_os.write(imgBuf, 0, byteReads);
	    	}
	    	if(by_is != null)
	    	{
	    		by_is.close();
	    	}
	    }
	    catch(IOException e)
	    {
	    	e.printStackTrace();
	    }
	    if(bao_os != null)
	    {
	    	imageInfo.imgInfoArr = bao_os.toByteArray();
	    }
	    else
	    {
	    	imageInfo.imgInfoArr = null;
	    }
	    return imageInfo;
	}
	
	/**
	 * @param imgF
	 * @param maxW
	 * @param maxH
	 * @return
	 */
	public static int autoZoom(String imgF) 
	{
		String fileName = GetPathStr.getPathWeb() + "imgUp/" + imgF;
		String ThumbfilePath = GetPathStr.getPathWeb() + "imgUp/Thumb/";
		String ThumbfileName = GetPathStr.getPathWeb() + "imgUp/Thumb/" + imgF;
		int maxW = 200;
		int maxH = 200;
		try
		{
			double Ratio = 0.0;
			File F = new File(fileName);
			File ThF = new File(ThumbfilePath);

			if (!F.exists())
			{
				return 1;
			}
			if (!ThF.exists())
			{
				if(!ThF.mkdir())
				{
					throw new Exception("目录不存在，创建失败！");
				}
			}
			ThF = new File(ThumbfileName);
			if(!ThF.exists())
			{
				if(!ThF.createNewFile())
                {
                	throw new Exception("文件不存在，创建失败！");
                }
			}

			BufferedImage Bi = ImageIO.read(F);

			// 假设图片宽 高 最大为200 200
			
			if ((Bi.getHeight() > maxH) || (Bi.getWidth() > maxW)) 
			{
				if (Bi.getHeight() > Bi.getWidth())
					Ratio = (float) (maxH) / Bi.getHeight();
				else
					Ratio = (float) (maxW) / Bi.getWidth();
			}
			else 
			{
				return 1;
			}
			Image Itemp = Bi.getScaledInstance(maxW, maxH, Bi.SCALE_REPLICATE);

			//System.out.println(44);2012-6-14

			AffineTransformOp op = new AffineTransformOp(AffineTransform
					.getScaleInstance(Ratio, Ratio), null);
			Itemp = op.filter(Bi, null);

			ImageIO.write((BufferedImage) Itemp, "jpg", ThF);
		} 
		catch (Exception ex) 
		{
			ex.printStackTrace();
		}
		return 1;
	}
}