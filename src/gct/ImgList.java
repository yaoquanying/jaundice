package gct;
import java.io.*;
import java.sql.*;
import java.util.*;
import org.jdom.*;
import org.jdom.output.XMLOutputter;
/**
 * 处理工程图列表的类
 * @author SHU
 *
 */
public class ImgList 
{
	private static Connection con = UtilsSQL.con;

	public static void crtDictImg()
	{
		File dBMain = new File(GetPathStr.getPath() + "DUCG");
		if (!dBMain.exists())
			dBMain.mkdir();
		File dB = new File(GetPathStr.getPath() + "DUCG/img");
		if (!dB.exists())
			dB.mkdir();
		dB = new File(GetPathStr.getPathWeb() + "imgUp");
		if (!dB.exists())
			dB.mkdir();
	}

	/**
	 * 获得工程图类表的方法
	 * @return
	 */
	public static String getList()
	{

		if (con == null)
		{
			return null;
		}
		Element rootRet = new Element("imgs");

		try {
			con.setAutoCommit(true);
			Statement statement = con.createStatement();
			String sqlCode = "select image_name,image_id from image";
			ResultSet result = statement.executeQuery(sqlCode);
			while (result.next()) {
				Element eleImg = new Element("img");
				Element v1 = new Element("name");
				Element v2 = new Element("id");
				v1.addContent(result.getString(1));
				v2.addContent(result.getString(2));
				eleImg.addContent(v1);
				eleImg.addContent(v2);
				rootRet.addContent(eleImg);
			}

			XMLOutputter outputter = new XMLOutputter();
			//System.out.println(outputter.outputString(new Document(rootRet)));
			return outputter.outputString(new Document(rootRet));

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * 获取工程图信息
	 * @param idFunc 工程图编号
	 * @return
	 */
	public String getImgViewInfo(String idFunc) {

		Element rootRet = new Element("info");

		Element v1 = new Element("name");
		Element v2 = new Element("dscp");
		if (con == null) {
			return null;
		}
		try {
			con.setAutoCommit(true);
			Statement statement = con.createStatement();
			String sqlCode = "select image_name,image_des from image where image_id='"
					+ idFunc + "'";
			ResultSet result = statement.executeQuery(sqlCode);
			if (result.next()) {
				v1.addContent(result.getString(1));
				v2.addContent(result.getString(2));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		rootRet.addContent(v1);
		rootRet.addContent(v2);
		XMLOutputter outputter = new XMLOutputter();
		return outputter.outputString(new Document(rootRet));
	}
	
	/**
	 * 获得工程图类表
	 * @return
	 */
	public String getImageList()
	{
		Element imgs = new Element("imgs");
		Element eleImg = new Element("img");
		Element v1 = new Element("name");
		Element v2 = new Element("id");
		v1.addContent("顶层图");
		v2.addContent("1");
		eleImg.addContent(v1);
		eleImg.addContent(v2);
		imgs.addContent(eleImg);
		addImage(imgs, "1", 1);
		Document slctDoc = new Document(imgs);
		XMLOutputter outputter = new XMLOutputter();
		return outputter.outputString(slctDoc);
	}
	
	/**
	 * 添加工程图
	 * @param root
	 * @param id
	 * @param iRank
	 */
	private void addImage(Element root, String id, int iRank)
	{
		if (con == null) 
		{
			return;
		}
		try 
		{
			con.setAutoCommit(true);
			Statement statement = con.createStatement();
			String sqlCode = "select image_name,image_id,image_parent,image_des from image where image_parent='"
					+ id + "'";
			ResultSet result = statement.executeQuery(sqlCode);
			while (result.next()) 
			{
				Element img = new Element("img");
				String[] message = new String[4];
				for (int i = 1; i <= 4; i++) 
				{
					message[i - 1] = result.getString(i);
					if (message[i - 1] == null) 
					{
						message[i - 1] = "";
					}
				}
//				String str = "(" + String.valueOf(iRank + 1) + "级)";
				String str = "";
				for(int i=0; i<iRank; i++)
				{
					str = str + "—";
				}
				Element v1 = new Element("name");
				Element v2 = new Element("id");
				v1.addContent(str + message[0]);
				v2.addContent(message[1]);
				img.addContent(v1);
				img.addContent(v2);
//				img.setAttribute("name", str + message[0]);
//				img.setAttribute("id", message[1]);
				root.addContent(img);
				addImage(root, message[1], iRank+1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	

	/**
	 * 获得工程图父子关系树
	 * @return 
	 */
	public String getTree() {

		Element imgs = new Element("imgs");
		imgs.setAttribute("name", "顶层图");
		imgs.setAttribute("id", "1");
		addTree(imgs, "1");
		Document slctDoc = new Document(imgs);
		XMLOutputter outputter = new XMLOutputter();
		return outputter.outputString(slctDoc);
	}

	/**
	 * 添加父子关系树
	 * @param root
	 * @param id
	 */
	private void addTree(Element root, String id) {
		if (con == null) {
			return;
		}
		try {
			con.setAutoCommit(true);
			Statement statement = con.createStatement();
			String sqlCode = "select image_name,image_id,image_parent,image_des from image where image_parent='"
					+ id + "'";
			ResultSet result = statement.executeQuery(sqlCode);
			while (result.next()) {
				Element img = new Element("img");
				String[] message = new String[4];
				for (int i = 1; i <= 4; i++) {
					message[i - 1] = result.getString(i);
					if (message[i - 1] == null) {
						message[i - 1] = "";
					}
				}
				img.setAttribute("name", message[0]);
				img.setAttribute("id", message[1]);
				img.setAttribute("parent", message[2]);
				img.setAttribute("dscp", message[3]);
				root.addContent(img);
				addTree(img, message[1]);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
