package gct.com.wyh;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.jdom.Document;
import org.jdom.Element;


public class Component {
	
	public String id="";
	public String type="";
	public String sp="";
	public int iscount;
	
	private Connection con;
	private Statement statement;
	
	Component(String id,String type,String sp,int iscount){
		con=DucgSql.con;
		statement =DucgSql.statement;
		this.id=id;
		this.type=type;
		this.sp=sp;
		this.iscount=iscount;
		if (con == null) {
			return;
		}
		try {
			con.setAutoCommit(true);
			String sqlCode = "select com_name,com_des,com_keyword from com where com_id='"+id+"' and com_type='"+type+"'";
			ResultSet result = statement.executeQuery(sqlCode);
			if(result.next()){
				this.name=result.getString(1);
				this.des=result.getString(2);
				this.keyWord=result.getString(3);
			}
			else{
				return;
			}
			sqlCode = "select state_id,state_qujian,state_des,state_type,state_gailv,state_color from state where com_id='"
				+id+"' and com_type='"+type+"' order by state_id desc";
			result = statement.executeQuery(sqlCode);
			while(result.next()){
				State temp = new State();
				temp.stateId=result.getString(1);
				temp.quJian=result.getString(2);
				temp.stateDes=result.getString(3);
				temp.stateType=result.getString(4);
				temp.gaiLv=result.getString(5);
				temp.stateColor=result.getString(6);
				state.add(temp);
			}
			stateNum=state.size();
			
			sqlCode = "select image_id,location_x,location_y,location_xper,location_yper from comimage where com_id='"+id+"' and com_type='"+type+"'";
			result = statement.executeQuery(sqlCode);
			while(result.next()){
				Location temp=new Location();
				temp.imageId=result.getString(1);
				temp.plocationX=result.getString(2);
				temp.plocationY=result.getString(3);
				temp.plocationXPer=result.getString(4);
				temp.plocationYPer=result.getString(5);
				location.add(temp);
			}
			locationNum=location.size();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	Component(String id,String type,String sp){
		con=DucgSql.con;
		statement =DucgSql.statement;
		this.id=id;
		this.type=type;
		this.sp=sp;
		if (con == null) {
			return;
		}
		try {
			con.setAutoCommit(true);
			String sqlCode = "select com_name,com_des,com_keyword from com where com_id='"+id+"' and com_type='"+type+"'";
			ResultSet result = statement.executeQuery(sqlCode);
			if(result.next()){
				this.name=result.getString(1);
				this.des=result.getString(2);
				this.keyWord=result.getString(3);
			}
			else{
				return;
			}
			sqlCode = "select state_id,state_qujian,state_des,state_type,state_gailv,state_color from state where com_id='"
				+id+"' and com_type='"+type+"' order by state_id desc";
			result = statement.executeQuery(sqlCode);
			while(result.next()){
				State temp = new State();
				temp.stateId=result.getString(1);
				temp.quJian=result.getString(2);
				temp.stateDes=result.getString(3);
				temp.stateType=result.getString(4);
				temp.gaiLv=result.getString(5);
				temp.stateColor=result.getString(6);
				state.add(temp);
			}
			stateNum=state.size();
			
			sqlCode = "select image_id,location_x,location_y,location_xper,location_yper from comimage where com_id='"+id+"' and com_type='"+type+"'";
			result = statement.executeQuery(sqlCode);
			while(result.next()){
				Location temp=new Location();
				temp.imageId=result.getString(1);
				temp.plocationX=result.getString(2);
				temp.plocationY=result.getString(3);
				temp.plocationXPer=result.getString(4);
				temp.plocationYPer=result.getString(5);
				location.add(temp);
			}
			locationNum=location.size();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	//baseInformation
	public String name;
	public String des;
	public String keyWord;
	
	public int locationNum=0;
	public ArrayList<Location> location = new ArrayList<Location>();
	
	class Location{
		public String plocationX;
		public String plocationY;
		public String plocationXPer;
		public String plocationYPer;
		public String imageId;
		public String color;
	}
	
	//状态信息
	public int stateNum=0;
	public ArrayList<State> state= new ArrayList<State>();
	
	class State{
		public String stateId;
		public String quJian;
		public String stateDes;
		public String stateType;
		public String gaiLv;
		public String stateColor;
	}
	
	public void writeToDB(){
		
		if (con == null) {
			return;
		}
		try {
			con.setAutoCommit(false);
			String sqlCode = "insert into com(com_id,com_type,com_name,com_des,com_keyword) values (" +
					"'"+id+"','"+type+"','"+name+"','"+des+"','"+keyWord+"')";
			statement.executeUpdate(sqlCode);
			for(State temp:state){
				sqlCode = "insert into state(state id,com_id,com_type,state_qujian,state_des,state_type,state_gailv,state_color)" +
					"values('"+temp.stateId+"','"+id+"','"+type+"','"+temp.quJian+"','"+temp.stateDes+"','"+temp.stateType+"','"+temp.gaiLv+"','"+temp.stateColor+"')";
			}
			con.commit();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			try {
				con.rollback();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		
	}
	
	private String path="F:/shu/MySql/Xml/";
	
	public Element writeCom(){
		Element root = new Element("val");
		root.setAttribute("type",type );// 设置根节点属性
		root.setAttribute("id", id);
		root.setAttribute("sp", sp);
		if(iscount!=0){iscount=1;}
		root.setAttribute("iscount",Integer.toString(iscount));
		
		
		Element basicInfo = new Element("basicInfo");
		Element vName = new Element("vName");
		vName.addContent(name);
		basicInfo.addContent(vName);
		Element vDes = new Element("vDscp");
		vDes.addContent(des);
		basicInfo.addContent(vDes);
		Element vKeyWord = new Element("vKeyWords");
		vKeyWord.addContent(keyWord);
		basicInfo.addContent(vKeyWord);
		
		Element vLocations = new Element("vLocations");
		vLocations.setAttribute("vLocationNum",String.valueOf(locationNum));
		Element vPlocationX=null;
		Element vPlocationY=null;
		Element vPlocationXPer=null;
		Element vImageId=null;
		Element vColor=null;
		
		for(Location temp:location){
			Element vLocation=new Element("vLocation");
			vPlocationX = new Element("vLocationX").addContent(temp.plocationX);
			vLocation.addContent(vPlocationX);
			vPlocationY = new Element("vLocationY").addContent(temp.plocationY);
			vLocation.addContent(vPlocationY);
			vPlocationXPer = new Element("vLocationXPer").addContent(temp.plocationXPer);
			vLocation.addContent(vPlocationXPer);
			Element vPlocatonYPer = new Element("vLocationYPer").addContent(temp.plocationYPer);
			vLocation.addContent(vPlocatonYPer);
			vImageId = new Element("vImageId").addContent(temp.imageId);
			vLocation.addContent(vImageId);
			vColor = new Element("vColor").addContent(temp.color);
			vLocation.addContent(vColor);
			vLocations.addContent(vLocation);
		}
		
		basicInfo.addContent(vLocations);
		
		Element vStates = new Element("vStates").setAttribute("vStatesNum", String.valueOf(stateNum));
		
		Element vStateId = null;
		Element vQuJian = null;
		Element vStateDes = null;
		Element vStateType =null;
		Element vGaiLv = null;
		Element vStateColor = null;
		
		for(State temp:state){
			Element vState = new Element("vState");
			vStateId = new Element("vStateId").addContent(temp.stateId);
			vState.addContent(vStateId);
			vQuJian = new Element("vQuJian").addContent(temp.quJian);
			vState.addContent(vQuJian);
			vStateDes = new Element("vDes").addContent(temp.stateDes);
			vState.addContent(vStateDes);
			vStateType = new Element("vStateType").addContent(temp.stateType);
			vState.addContent(vStateType);
			vGaiLv = new Element("vGaiLv").addContent(temp.gaiLv);
			vState.addContent(vGaiLv);
			vStateColor = new Element("vColor").addContent(temp.stateColor);
			vState.addContent(vStateColor);
			vStates.addContent(vState);
		}
		root.addContent(basicInfo);
		root.addContent(vStates);
		return root;
	}
}
