package gct.com.wyh;

import flex.messaging.io.ArrayCollection;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;

import db.config.DbConfig;

public class DucgSql {
	public static Connection con;
	public static Statement statement;
	private static gct.GetPathStr getpath = new gct.GetPathStr();
	private static String pathout = getpath.getPathWeb();

	/**
	 * 连接数据库
	 * 
	 * @return
	 */
	public boolean connection() {
	//	String dbName = "jaundice";
		try {
			Class.forName("com.mysql.jdbc.Driver");
			// 配置数据源                 //    192.168.5.89
			String url = "jdbc:mysql://"+DbConfig.host+"/" + DbConfig.dbname
					+ "?useUnicode=true&characterEncoding=UTF-8";
			con = DriverManager.getConnection(url,DbConfig.usname, DbConfig.dbpwd);
			statement = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_READ_ONLY);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	/**
	 * 从COM表中获得所有变量的信息，返回给前端。yqy 16.11.01
	 * @return
	 */
	//bxs 2016.6
	public String getAllComponent() {
		if (con == null) {
			return null;
		}
		try {
			Statement temp = con
					.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
							ResultSet.CONCUR_READ_ONLY);
			Element components = new Element("components");
			String sqlCode = "select com_id,com_type,sp,iscount from com";
			ResultSet result = temp.executeQuery(sqlCode);
			while (result.next()) {
				components.addContent(new Component(result.getString(1), result
						.getString(2), Integer.toString(result.getInt(3)),result.getInt(4) ).writeCom());
			}
			Document slctDoc = new Document(components);
			XMLOutputter outputter = new XMLOutputter();
			return outputter.outputString(slctDoc);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * 关闭连接
	 */
	public void close() {
		if (con == null) {
			return;
		}
		try {
			con.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public String getfirstpre() throws IOException {
		File directory = new File(pathout + "KBGraph//FirstPredict");
		File[] files = directory.listFiles();
		String returnstr = new String();
		String strtmp = new String();
		returnstr = "";
		FileInputStream fileinstream = new FileInputStream(pathout
				+ "KBGraph//FirstPredict");
		InputStreamReader inputreader = new InputStreamReader(fileinstream);
		BufferedReader bufreader = new BufferedReader(inputreader);
		while ((strtmp = bufreader.readLine()) != null) {
			returnstr = returnstr + strtmp + ",";
		}
		return returnstr;
	}

	public String getsecondpre() throws IOException {
		File directory = new File(pathout + "KBGraph//SecondPredict");
		File[] files = directory.listFiles();
		String returnstr = new String();
		String strtmp = new String();
		returnstr = "";
		FileInputStream fileinstream = new FileInputStream(pathout
				+ "KBGraph//SecondPredict");
		InputStreamReader inputreader = new InputStreamReader(fileinstream);
		BufferedReader bufreader = new BufferedReader(inputreader);
		while ((strtmp = bufreader.readLine()) != null) {
			returnstr = returnstr + strtmp + ",";
		}
		return returnstr;
	}
	
	public boolean updateLine(int id, double range, int line_count, int column_count, String condition, String data){
		if(con==null){
			return false;
		}
		try {
			con.setAutoCommit(false);
			String sqlCode = "delete from juzhen where rel_name='"+id+"'";
			statement.executeUpdate(sqlCode);
			sqlCode = "insert into juzhen(rel_name,line,arrange,message)values('"+id+"','"+line_count+"','"+column_count+"','"+data+"')";
			statement.executeUpdate(sqlCode);
			sqlCode = "update relation set rel_r='"+range+"', rel_message='"+condition+"' where rel_name='"+id+"'";
			statement.executeUpdate(sqlCode);
			con.commit();
			//con.setAutoCommit(true);
			//System.out.println("Line updated: ID="+id+" Data="+data+" DIM="+line_count+"*"+column_count);
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
			try {
				con.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			return false;
		}
	}
	
	public void updatePartialLine(int id, int line_count, int column_count, String data){
		if(con==null){
			return;
		}
		try {
			con.setAutoCommit(false);
			String sqlCode = "delete from juzhen where rel_name='"+id+"'";
			statement.executeUpdate(sqlCode);
			sqlCode = "insert into juzhen(rel_name,line,arrange,message)values('"+id+"','"+line_count+"','"+column_count+"','"+data+"')";
			statement.executeUpdate(sqlCode);
			//sqlCode = "update relation set rel_r='"+range+"', rel_message='"+condition+"' where rel_name='"+id+"'";
			//statement.executeUpdate(sqlCode);
			con.commit();
			//System.out.println("Line updated: ID="+id+" Data="+data+" DIM="+line_count+"*"+column_count);
		} catch (SQLException e) {
			e.printStackTrace();
			try {
				con.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}
	}
	
	public String getRelatedLines(int id, String type){
		try{
			con.setAutoCommit(false);
			PreparedStatement stmt = con.prepareStatement("select * from relation where REL_END_ID=? and REL_END_TYPE=?");
			stmt.setInt(1, id);
			stmt.setString(2, type);
			ResultSet result = stmt.executeQuery();
			
			Element in_lines = new Element("lineins");
			Element out_lines = new Element("lineouts");
			while(result.next()){
				int line_id = result.getInt("REL_NAME");
				PreparedStatement stmtline = con.prepareStatement("select * from juzhen where REL_NAME=?");
				stmtline.setInt(1, line_id);
				ResultSet lineResult = stmtline.executeQuery();
				if(lineResult.next()){
					Element line = new Element("line");
					line.setAttribute("id", Integer.toString(line_id));
					Element lineData = new Element("LineData");
					lineData.addContent(lineResult.getString("MESSAGE"));
					lineData.setAttribute("lines", lineResult.getString("LINE"));
					lineData.setAttribute("columns", lineResult.getString("ARRANGE"));
					
					
					Element rEndId = new Element("rEndId");
					rEndId.addContent(result.getString("REL_END_ID"));
					Element rBeginId = new Element("rBeginId");
					rBeginId.addContent(result.getString("REL_BEGIN_ID"));
					Element rEndType = new Element("rEndType");
					Element rBeginType = new Element("rBeginType");
					rEndType.addContent(result.getString("REL_END_TYPE"));
					rBeginType.addContent(result.getString("REL_BEGIN_TYPE"));
					
					line.addContent(lineData);
					line.addContent(rBeginId);
					line.addContent(rBeginType);
					line.addContent(rEndId);
					line.addContent(rEndType);
					
					in_lines.addContent(line);
				}
			}
			
			stmt = con.prepareStatement("select * from relation where REL_BEGIN_ID=? and REL_BEGIN_TYPE=?");
			stmt.setInt(1, id);
			stmt.setString(2, type);
			result = stmt.executeQuery();
			while(result.next()){
				int line_id = result.getInt("REL_NAME");
				PreparedStatement stmtline = con.prepareStatement("select * from juzhen where REL_NAME=?");
				stmtline.setInt(1, line_id);
				ResultSet lineResult = stmtline.executeQuery();
				if(lineResult.next()){
					Element line = new Element("line");
					line.setAttribute("id", Integer.toString(line_id));
					Element lineData = new Element("LineData");
					lineData.addContent(lineResult.getString("MESSAGE"));
					lineData.setAttribute("lines", lineResult.getString("LINE"));
					lineData.setAttribute("columns", lineResult.getString("ARRANGE"));
					
					Element rEndId = new Element("rEndId");
					rEndId.addContent(result.getString("REL_END_ID"));
					Element rBeginId = new Element("rBeginId");
					rBeginId.addContent(result.getString("REL_BEGIN_ID"));
					Element rEndType = new Element("rEndType");
					Element rBeginType = new Element("rBeginType");
					rEndType.addContent(result.getString("REL_END_TYPE"));
					rBeginType.addContent(result.getString("REL_BEGIN_TYPE"));
					
					line.addContent(lineData);
					line.addContent(rBeginId);
					line.addContent(rBeginType);
					line.addContent(rEndId);
					line.addContent(rEndType);
					
					out_lines.addContent(line);
				}
			}
			
			Element root = new Element("RelatedLines");
			root.addContent(in_lines);
			root.addContent(out_lines);
			Document doc = new Document(root);
			XMLOutputter outputter = new XMLOutputter();
			return outputter.outputString(doc);
		}
		catch(SQLException e){
			e.printStackTrace();
			Element in_lines = new Element("lineins");
			Element out_lines = new Element("lineouts");
			Element root = new Element("RelatedLines");
			root.addContent(in_lines);
			root.addContent(out_lines);
			Document doc = new Document(root);
			XMLOutputter outputter = new XMLOutputter();
			return outputter.outputString(doc);
		}
	}

	public RelatedLines getRelatedLinesObject(int id, String type){
		try{
			con.setAutoCommit(false);
			PreparedStatement stmt = con.prepareStatement("select * from relation where REL_END_ID=? and REL_END_TYPE=?");
			stmt.setInt(1, id);
			stmt.setString(2, type);
			ResultSet result = stmt.executeQuery();
						
			RelatedLines related = new RelatedLines();
			
			while(result.next()){
				int line_id = result.getInt("REL_NAME");
				PreparedStatement stmtline = con.prepareStatement("select * from juzhen where REL_NAME=?");
				stmtline.setInt(1, line_id);
				ResultSet lineResult = stmtline.executeQuery();
				if(lineResult.next()){
					Line line = new Line(line_id, 
							Integer.parseInt(result.getString("REL_BEGIN_ID")),result.getString("REL_BEGIN_TYPE"), 
							Integer.parseInt(result.getString("REL_END_ID")), result.getString("REL_END_TYPE"));
					
					line.initMatrix(Integer.parseInt(lineResult.getString("LINE")), Integer.parseInt(lineResult.getString("ARRANGE")), 
							lineResult.getString("MESSAGE"));
					
					related.addInLine(line);
				}
			}
			
			stmt = con.prepareStatement("select * from relation where REL_BEGIN_ID=? and REL_BEGIN_TYPE=?");
			stmt.setInt(1, id);
			stmt.setString(2, type);
			result = stmt.executeQuery();
			while(result.next()){
				int line_id = result.getInt("REL_NAME");
				PreparedStatement stmtline = con.prepareStatement("select * from juzhen where REL_NAME=?");
				stmtline.setInt(1, line_id);
				ResultSet lineResult = stmtline.executeQuery();
				if(lineResult.next()){
					Line line = new Line(line_id, 
							Integer.parseInt(result.getString("REL_BEGIN_ID")),result.getString("REL_BEGIN_TYPE"), 
							Integer.parseInt(result.getString("REL_END_ID")), result.getString("REL_END_TYPE"));
					
					line.initMatrix(Integer.parseInt(lineResult.getString("LINE")), Integer.parseInt(lineResult.getString("ARRANGE")), 
							lineResult.getString("MESSAGE"));
					
					related.addOutLine(line);
				}
			}
			
			return related;
		}
		catch(SQLException e){
			e.printStackTrace();
			return new RelatedLines();
		}
	}
	
	/**
	 * checkIsIncludeAddVar
	 */
	public String checkIsIncludeAddVar(ArrayCollection varArray)
	{
		String returnstr = new String();
		returnstr = "";
		for(int i = 0; i < varArray.size(); i++){
			Object obj = varArray.get(i);
			String varInfo = obj.toString();
			if (varInfo.contains(":")){
				//取每一行中变量的信息，并按照类型和编号拆分父、子变量。
				String []varArr = varInfo.split(":");
				String varFarName = varArr[0];
				String varChiName = varArr[1];
				String varFarTypeString = varFarName.substring(0, 1);
				String varFarIDString = varFarName.substring(1, varFarName.length());
				String varChiTypeString = varChiName.substring(0, 1);
				String varChiIDString = varChiName.substring(1, varChiName.length());
				//查找这些变量是否有关联变量
				String parAssVar = getAssVarInfo(varFarIDString, varFarTypeString);
				String chiAssVar = getAssVarInfo(varChiIDString, varChiTypeString);
				if (!parAssVar.equals("")){
					returnstr = returnstr + parAssVar;
				}
				if (!chiAssVar.equals("")){
					returnstr = returnstr + chiAssVar;
				}
			}
			returnstr = returnstr + varInfo + " ";
		}
		return returnstr;
	}
	
	/**
	 * 2016-11-2
	 * 获得关联变量的信息
	 */
	public String getAssVarInfo(String com_id, String com_type) {
		if (con == null) {
			return "";
		}
		try {
			//查找子变量
			String sqlCode = "select REL_END_ID, REL_END_TYPE, REL_R from relation where REL_BEGIN_ID='"
							+ com_id + "' and REL_BEGIN_TYPE='" + com_type +  "' and REL_TYPE = '7'";
			con.setAutoCommit(false);
			Statement statement = con.createStatement();
			ResultSet result = statement.executeQuery(sqlCode);
			String strLineString = "";
			while (result.next()) {
				String strChiID = result.getString(1);
				String strParType = result.getString(2);
					
				strLineString = strLineString + com_type + com_id + ":" + strParType + strChiID + ":7 "; 
			}
			return strLineString;
		}
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "";
		}
	}
}
