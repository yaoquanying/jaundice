package gct.com.wyh;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import org.apache.xalan.templates.VarNameCollector;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.output.XMLOutputter;

import com.sun.corba.se.impl.resolver.SplitLocalResolverImpl;
import com.sun.org.apache.xpath.internal.operations.Bool;

import flex.messaging.io.ArrayCollection;
import flex.messaging.io.amf.ASObject;

public class readSimple 
{
	private gct.GetPathStr getpath;
	//private String circlepath = getpath.getPathWeb() + "KBGraph\\firstshow\\";
	private String BXcirclepath = getpath.getPathWeb() + "KBGraph\\secondshow\\";
	private String simplepath = getpath.getPathWeb() + "KBGraph\\simple\\";
	//private String savpath = getpath.getPathWeb() + "KBGraph\\sav.xml";

	/**
	 * 某个病例推理结果中化简图下面的文件个数
	 */
	public Number getcirclenum(int simpleType,String patientID) //被【查看化简】引用
	{   
		String circlepath = getpath.getPathWeb() + "KBGraph\\"+patientID+"\\firstshow\\";
		File directory;
		if (simpleType == 1)
		{
			directory = new File(circlepath);
		}
		else
		{
			directory = new File(BXcirclepath);
		}
		
		File[] files = directory.listFiles();
		return files.length;
	}

	public Number getsimplenum()
	{
		File directory = new File(simplepath);
		File[] files = directory.listFiles();
		return files.length;
	}

	/**
	 * 根据某个化简图文件的信息
	 * @param num
	 * @param simpleType
	 * @param simpleString
	 * @param patientID
	 * @return
	 * @throws IOException
	 */
	public String getcircle(int num, int simpleType, String simpleString,String patientID) throws IOException //被getcirclenum_resultHandler引用, num为第几个文件
	{   
		String circlepath = getpath.getPathWeb() + "KBGraph\\"+patientID+"\\firstshow\\";
		String returnstr = new String();
		File directory;
		if (simpleType == 1)
		{
			directory = new File(circlepath);
		}
		else 
		{
			directory = new File(BXcirclepath);
		}
		File[] files = directory.listFiles();
		String strtmp = new String();
		String strFileName = files[num].getName();
		String strTemp = strFileName.substring(0, strFileName.length()-4);
 		String[] strTemp1 = strTemp.split("_");
 		boolean isExit = false;
 		if (simpleType == 2)
 		{
 			String[] strSimpleID = simpleString.split("-");
 			for (int i = 0; i < strSimpleID.length; i++)
	 		{
	 			if (strSimpleID[i].compareTo(strTemp1[0]) == 0)
	 			{
	 				isExit = true;
	 				break;
	 			}
	 		} 
	 		if (!isExit)
	 		{
	 			return "FileNoMatch";
	 		}
 		}
 		
 		int numTemp = num + 1;
		if(strTemp1.length > 1)
		{
			// new add 2012-4-17
			String str = strTemp1[1].substring(1, strTemp1[1].length()-1);
			if (str.length() > 0)
			{
				String[] strTemp2 = str.split(",");
				if (strTemp2.length > 1)
				{
					returnstr = "" + numTemp + "(问题信号:" + getNodes(strTemp2[0]) + 
									         "和" + getNodes(strTemp2[1]) + ")" + " ";
				}
				else
				{
					strTemp1[1] = strTemp2[0];
					returnstr = "" + numTemp + "(问题信号:" + getNodes(strTemp1[1]) + ")" + " ";
				}
			}
			else
			{
				returnstr = "" + numTemp + " ";
			}
			// add end
		}
		else
		{
		    returnstr = "" + numTemp + " ";
		}
		
		FileInputStream fileinstream;
		if (simpleType == 1)
		{
			fileinstream = new FileInputStream(circlepath + files[num].getName());
		}
		else 
		{
			fileinstream = new FileInputStream(BXcirclepath + files[num].getName());
		}
		
		InputStreamReader inputreader = new InputStreamReader(fileinstream);
		BufferedReader bufreader = new BufferedReader(inputreader);
		while ((strtmp = bufreader.readLine()) != null)
		{
			returnstr = returnstr + strtmp;
			returnstr = returnstr + " ";
		}
		fileinstream.close();
		return returnstr;
	}
	
	public String getNodes(String strValues)
	{
		List listNodes = null;
		String strName = "";
		try
		{
			Document docGraphWYH;
			try
			{
				SAXBuilder builder = new SAXBuilder();
				String pathF = gct.GetPathStr.getPathWeb() + "KBGraph/graph.xml";
				// System.out.println(pathF);
				docGraphWYH = builder.build(new File(pathF));
				listNodes = docGraphWYH.getRootElement().getChildren("nodes");
			}
			catch (JDOMException e)
			{
				e.printStackTrace();
			}
			catch (NullPointerException e)
			{
				e.printStackTrace();
			}
			
			Element eleT;
			if (listNodes == null)
			{
				return "";
			}
			
			strValues = strValues.trim();
			
			for (int i = 0; i < listNodes.size(); i++)
			{
				eleT = (Element) (listNodes.get(i));
				// System.out.println(eleT.getAttributeValue("id")+"++");
				if (eleT.getAttributeValue("id").compareTo(strValues) == 0)
				{
					strName = eleT.getAttributeValue("name");
				}
			}
		}
		catch (IOException e)
		{
			e.printStackTrace();
			return "";
		}
		return strName;
	}

	public String getsimple(int num) throws IOException 
	{
		String returnstr = new String();
		File directory = new File(simplepath);
		File[] files = directory.listFiles();
		String strtmp = new String();
		returnstr = "" + num + " ";
		FileInputStream fileinstream = new FileInputStream(simplepath
				+ files[num].getName());
		InputStreamReader inputreader = new InputStreamReader(fileinstream);
		BufferedReader bufreader = new BufferedReader(inputreader);
		while ((strtmp = bufreader.readLine()) != null)
		{
			returnstr = returnstr + strtmp;
			returnstr = returnstr + " ";
		}
		fileinstream.close();
		return returnstr;
	}
	// bxs getuser modify
	public String getsav() throws IOException	//被getAllComponent_resultHandler引用1次。（初始化用）
	{   
		String savpath = getpath.getPathWeb() + "KBGraph\\sav.xml";
		
		File directory = new File(savpath);
		File[] files = directory.listFiles();
		String returnstr = new String();
		String strtmp = new String();
		returnstr = "";
		FileInputStream fileinstream = new FileInputStream(savpath);
		InputStreamReader inputreader = new InputStreamReader(fileinstream);
		BufferedReader bufreader = new BufferedReader(inputreader);
		while ((strtmp = bufreader.readLine()) != null)
		{
			returnstr = returnstr + strtmp;
		}
		return returnstr;
	}
	
	/**
	 * 获得某个病例下的化简图信息（总图）
	 * @param simpleType
	 * @param simpleString
	 * @param patientID
	 * @return
	 * @throws IOException
	 */
	public String getcircleAll(int simpleType, String simpleString,String patientID) throws IOException
	{   
		String circlepath = getpath.getPathWeb() + "KBGraph\\"+patientID+"\\firstshow\\";
		String returnstr = new String();
		returnstr = "" + " ";
		File directory;
		if (simpleType == 1)
		{
			directory = new File(circlepath);
		}
		else 
		{
			directory = new File(BXcirclepath);
		}
		List<String> listRelation = new ArrayList<String>();
		List<String> listRelationD = new ArrayList<String>();
		List<String> listVariable = new ArrayList<String>();
		File[] filesExit = directory.listFiles();
		if(filesExit.length<1){
			return "nofile";
		}
		if(simpleType == 1)
		{
			File[] files = directory.listFiles();
			for(int i = 0; i<files.length; i++)
			{
				FileInputStream fileinstream = new FileInputStream(circlepath + files[i].getName());
				InputStreamReader inputreader = new InputStreamReader(fileinstream);
				BufferedReader bufreader = new BufferedReader(inputreader);
				String strtmp = new String();
				while ((strtmp = bufreader.readLine()) != null)
				{
					if(strtmp.equals("0"))
					{
						continue;
					}
					if(strtmp.contains(":"))
					{
						if(!listRelation.contains(strtmp))
						{
							if(!strtmp.contains("D")){
								listRelation.add(strtmp);
							}
							else{
								listRelationD.add(strtmp);
							}
						}
					}
					else
					{
						if(!listVariable.contains(strtmp))
						{
							listVariable.add(strtmp);
						}
					}
				}
				fileinstream.close();
			}
		}
		else if(simpleType == 2)
		{
			String[] strSimpleID = simpleString.split("-");
			List<String> listSimpleId = new ArrayList<String>();
			for(int i=0; i<strSimpleID.length; i++)
			{
				if(!strSimpleID[i].equals(""))
				{
					listSimpleId.add(strSimpleID[i]);
				}
			}
			
			File[] files = directory.listFiles();
			for(int i = 0; i<files.length; i++)
			{
				String strName = files[i].getName().split("_")[0];
				if(!listSimpleId.contains(strName))
				{
					continue;
				}
				FileInputStream fileinstream = new FileInputStream(BXcirclepath + files[i].getName());
				InputStreamReader inputreader = new InputStreamReader(fileinstream);
				BufferedReader bufreader = new BufferedReader(inputreader);
				String strtmp = new String();
				while ((strtmp = bufreader.readLine()) != null)
				{
					if(strtmp.equals("0"))
					{
						continue;
					}
					if(strtmp.contains(":"))
					{
						if(!listRelation.contains(strtmp))
						{
							listRelation.add(strtmp);
						}
					}
					else
					{
						if(!listVariable.contains(strtmp))
						{
							listVariable.add(strtmp);
						}
					}
				}
				fileinstream.close();
			}
		}
		for(int i = 0; i < listRelation.size(); i++)
		{
			returnstr = returnstr + listRelation.get(i);
			returnstr = returnstr + " ";
		}
		for(int i = 0; i < listRelationD.size(); i++)
		{
			String temp=listRelationD.get(i);
			StringTokenizer st = new StringTokenizer(temp,":");
			String firstSt =st.nextToken();
			String secondSt =st.nextToken();
			if(!returnstr.contains(":"+secondSt+":")){
				returnstr = returnstr + listRelationD.get(i);
				returnstr = returnstr + " ";
			}
		}
		returnstr = returnstr + "0";
		returnstr = returnstr + " ";
		for(int i=0; i<listVariable.size(); i++)
		{
			returnstr = returnstr + listVariable.get(i);
			returnstr = returnstr + " ";
		}

		if (returnstr.equals(" 0 ") )
		{
			returnstr = "NoBadResult";
		}
		return returnstr;
	}

	/**
	 * @param strVarIDType
	 * @param strState
	 * @return
	 */
	public String getBXFatherNode(String strVarIDType, String strState)
	{
		try 
		{
			String BXpath = getpath.getPathWeb() + "KBGraph\\result\\";
			String simpleString = new String(); //存放BX变量的父节点序号。

			String strpath = strVarIDType + "," + strState + ".txt";
			FileInputStream fileinstream = new FileInputStream(BXpath + strpath);
			InputStreamReader inputreader = new InputStreamReader(fileinstream);
			BufferedReader bufreader = new BufferedReader(inputreader);
			String strtmp = new String();
			while(bufreader.ready()) 
			{
				strtmp = bufreader.readLine();
				String[] rstArr = strtmp.split(" ");
				String numFK = rstArr[0];
				simpleString = simpleString + numFK + "-";
			}
			
			return simpleString;
		}
		catch (IOException e) 
		{
			e.printStackTrace();
			return "";
		}
	}
	
	/**
	 * 获取推理机推理出来的BX变量列表
	 * @param strVarIDType
	 * @return
	 */
	
	public String getBXState(String strVarIDType)
	{
		String strBXState = "";
		String BXpath = getpath.getPathWeb() + "KBGraph\\result\\";
		File directory = new File(BXpath);
		File[] files = directory.listFiles();

		for (int i = 0; i < files.length; i++)
		{
			String strName = files[i].getName().split(",")[0];
			if(!strName.equals(strVarIDType))
			{
				continue;
			}
			String strState = files[i].getName().split(",")[1];
			strState = strState.substring(0, strState.length() - 4);
			strBXState = strBXState + strState + "-";
		}
		return strBXState;
	}
}