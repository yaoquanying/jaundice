package gct.com.wyh;

public class Line {
	public int lineId;
	public int startId, endId;
	public String startType, endType;
	public Matrix mData;
	
	public Line(int lineId, int startId, String startType, int endId, String endType){
		this.lineId = lineId;
		this.startId = startId;
		this.startType = startType;
		this.endId = endId;
		this.endType = endType;
	}
	
	public void initMatrix(int lineCount, int columnCount, String rawdata){
		String[] data = new String[lineCount*columnCount];
		int index = 0;
		do{
			int comma = rawdata.indexOf(',');
			
			if(comma >= 0)
				data[index++] = rawdata.substring(0, comma);
			else if(rawdata.length() > 0){
				data[index++] = rawdata;
				rawdata = "";
			}
			
			if(comma < rawdata.length()-1)
				rawdata = rawdata.substring(comma+1);
			else
				rawdata = "";
		}while(!rawdata.equals(""));
		
		mData = new Matrix(lineCount, columnCount);
		for(int i = 0 ; i < lineCount; i++)
			for(int j = 0 ; j < columnCount; j++){
				mData.setCellAt(i,j, Double.parseDouble(data[i*columnCount + j]));
			}
	}
}
