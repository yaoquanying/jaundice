package gct;
import java.io.*;
import java.sql.*;
import java.util.*;
import org.jdom.*;

/**
 * 在这个类中主要做变量拷贝的工作
 * @author SHU
 *
 */
public class CopyVal {
	/**
	 * 连接数据库的接口
	 */
	private static Connection con = UtilsSQL.con;

	/*
	 * 创建目录
	 */
//	public static void crtDict() {
//		File dBMain = new File(GetPathStr.getPath() + "DUCG");
//		if (!dBMain.exists())
//			dBMain.mkdir();
//		File dB = new File(GetPathStr.getPath() + "DUCG/B");
//		File dX = new File(GetPathStr.getPath() + "DUCG/X");
//
//		if (!dB.exists())
//			dB.mkdir();
//		if (!dX.exists())
//			dX.mkdir();
//	}

	/**
	 * 根据变量类型申请变量ID
	 */
	public int getComponentId(String type) {
		if (con == null) 
		{
			return -1;
		}
		try 
		{
			//首先查看ID表中是否有未用的ID
			con.setAutoCommit(true);
			String sqlCode = "select max(com_id)+1 from com where com_type='"
					+ type + "'";
			System.out.println(sqlCode);
			Statement statement = con.createStatement();
			ResultSet result = statement.executeQuery(sqlCode);
			result.next();
			int iNumCom = result.getInt(1);
			if (iNumCom == 0)
			{
				iNumCom = 1;
			}
			
			con.setAutoCommit(true);
			sqlCode = "select max(com_id)+1 from delcom where com_type='"
					+ type + "'";
			System.out.println(sqlCode);
			statement = con.createStatement();
			result = statement.executeQuery(sqlCode);
			result.next();

			int iNumDelCom = result.getInt(1);
            if (iNumCom >= iNumDelCom)
            {
            	return iNumCom;
            }
            else
            {
            	return iNumDelCom;
            }

		} 
		catch (SQLException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;
	}

	/**
	 * 拷贝变量
	 * @param type 变量类型
	 * @param id 变量编号
	 * @return 返回拷贝后变量的编号
	 */
	public String copyVal(String type, String id) 
	{
		Component temp = new Component(id, type);
	
        int idTemp = getComponentId(type);
//        int tmp_id;
//		
//		if (type.startsWith("U")) {
//			tmp_id = getComponentId("X");
//			if (idTemp < tmp_id)
//				idTemp = tmp_id;
//		} else if (type.startsWith("X")) {
//			tmp_id = getComponentId("U");
//			if (idTemp < tmp_id)
//				idTemp = tmp_id;
//		}
		temp.id = String.valueOf(idTemp);
		temp.writeToDB();
		return String.valueOf(temp.id);
	}
}