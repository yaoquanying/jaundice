package gct;
import java.io.*;
import java.sql.*;
import java.util.*;
import org.jdom.*;

/**
 * 用于删除工程图的类
 * @author SHU
 *
 */
public class DelImg {
	
	/**
	 * 用于连接数据库
	 */
	private static Connection con = UtilsSQL.con;

	/**
	 * 董力编写
	 */
	public static void crtDictImg() {
		File dBMain = new File(GetPathStr.getPath() + "DUCG");
		if (!dBMain.exists())
			dBMain.mkdir();
		File dB = new File(GetPathStr.getPath() + "DUCG/img");
		if (!dB.exists())
			dB.mkdir();
		dB = new File(GetPathStr.getPathWeb() + "imgUp");
		if (!dB.exists())
			dB.mkdir();
	}

	/**
	 * 删除工程图
	 * @param imgNm 工程图名称
	 * @return 返回删除的结果
	 */
	public int delImg(String imgNm) {

		if (con == null) {
			return 1;
		}
		
		if(!imageIsEmpty(imgNm)){
			return 12;
		}
		
		if(imgNm.equals("1"))
		{
			return 13;
		}
		
		try {
			con.setAutoCommit(true);
			String sqlCode = "delete from image where image_id='" + imgNm
					+ "'";
			Statement statement = con.createStatement();
			statement.executeUpdate(sqlCode);
			return 0;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return 1;
		}
	}
	
	/**
	 * 检查工程图的名称是否已经使用
	 * @param imgNum 工程图名称
	 * @return 检查的结果
	 */
	public boolean imageIsEmpty(String imgNum){
		if (con == null) {
			return true;
		}
		
		try {
			con.setAutoCommit(true);
			String sqlCode = "select count(*) from comimage where image_id=?";
			PreparedStatement sta = con.prepareStatement(sqlCode);
			sta.setString(1, imgNum);
			ResultSet result = sta.executeQuery();
			if(result.next()){
				if(result.getInt(1)>0){
					return false;
				}
			}
			return true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return true;
		}
	}
}
