package gct;
import java.io.Serializable;
/**
 * 姚全营编写
 * 返回图片的ID和二进制流
 * @author Tony
 */

public class ImageInfo  implements Serializable
{
	public String imageID;
	public byte[] imgInfoArr;
}
