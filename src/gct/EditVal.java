package gct;
import java.io.*;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.net.SocketException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.sql.*;
import java.util.*;
import java.lang.Thread;

import org.jdom.*;
import org.jdom.output.XMLOutputter;

import gct.Component.State;
import gct.com.wyh.DucgSql;
import gct.com.wyh.Line;
import gct.com.wyh.Matrix;
import gct.com.wyh.RelatedLines;

import flex.messaging.io.ArrayCollection;
import flex.messaging.io.amf.ASObject;
import flex.messaging.io.amf.translator.ASTranslator;


/**
 * 用于编辑变量信息/测点信息的类
 * @author SHU
 *
 */
public class EditVal {
	
	/**
	 * 用于连接数据库
	 */
	private static Connection con = UtilsSQL.con;

	
	/**
	 * 根据变量的类型和编号获得变量的基本信息
	 * @param vType 变量类型
	 * @param vid 变量编号
	 * @return 用于存储变量基本信息的XML字符串
	 */
	//bxs 2016.6增加x默认概率
	public static String getBasicInfo(String vType, String vid) {

		if (con == null) {
			return null;
		}

		Element root = new Element("val");
		Element basicInfo = new Element("basicInfo");
		Element vName = new Element("vName");
		Element vDscp = new Element("vDscp");
		Element stNumber = new Element("stNumber");
		Element vKeyWords = new Element("vKeyWords");
		Element ceDian = new Element("vCeDian");
		Element isSwitch = new Element("isSwitch");
		Element vOne = new Element("vOne");
		Element vTwo = new Element("vTwo");
		Element vThree = new Element("vThree");
		Element dval = new Element("dval");
		Element cost = new Element("cost");
		Element isCount = new Element("iscount");
		try {
			con.setAutoCommit(true);
			Statement statement = con.createStatement();
			String sqlCode = "select com_name ,com_des,com_keyword,one,two,three,cedian,switch,dval,iscount,cost from com where com_id='"
					+ vid + "' and com_type='" + vType + "'";
			ResultSet result = statement.executeQuery(sqlCode);
			if (result.next()) {
				vName.addContent(result.getString(1));
				vDscp.addContent(result.getString(2));
				vKeyWords.addContent(result.getString(3));
				vOne.addContent(Integer.toString(result.getInt(4)));
				vTwo.addContent(result.getString(5));
				vThree.addContent(result.getString(6));
				ceDian.addContent(result.getString(7));
				isSwitch.addContent(Integer.toString(result.getInt(8)));
				dval.addContent(Float.toString(result.getFloat(9)));
				isCount.addContent(Integer.toString(result.getInt(10)));
				cost.addContent(Integer.toString(result.getInt(11)));
				//System.out.println("test"+Integer.toString(result.getInt(10)));
				//System.out.println("test"+Integer.toString(result.getInt(11)));
				sqlCode = "select count(*) from state where com_id='" + vid
						+ "' and com_type='" + vType + "'";
				ResultSet re = statement.executeQuery(sqlCode);
				re.next();
				stNumber.addContent(re.getString(1));
			} else {
				return null;
			}
			basicInfo.addContent(vName);
			basicInfo.addContent(vDscp);
			basicInfo.addContent(stNumber);
			basicInfo.addContent(vKeyWords);
			basicInfo.addContent(vOne);
			basicInfo.addContent(vTwo);
			basicInfo.addContent(vThree);
			basicInfo.addContent(ceDian);
			basicInfo.addContent(isSwitch);
			basicInfo.addContent(dval);
			basicInfo.addContent(isCount);
			basicInfo.addContent(cost);
			root.addContent(basicInfo);
			Document slctDoc = new Document(root);
			XMLOutputter outputter = new XMLOutputter();
			// System.out.println(outputter.outputString(slctDoc));
			return outputter.outputString(slctDoc);

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
   
	/**
	 * 获得变量的状态表
	 * @param vType 变量类型
	 * @param vid 变量编号
	 * @return 存储变量状态信息的XML字符串
	 */
	public String getStGrid(String vType, String vid) {

		if (con == null) {
			return null;
		}

		Element root = new Element("val");

		try {
			con.setAutoCommit(true);
			Statement statement = con.createStatement();
			String sqlCode = "select state_id,state_qujian,state_des,state_type,state_gailv,state_color "
					+ "from state where com_id='"
					+ vid
					+ "' and com_type='"
					+ vType + "' order by state_id";
			ResultSet result = statement.executeQuery(sqlCode);
			while (result.next()) {
				Element state = new Element("state");
				Element stNumst = new Element("stNumst");
				stNumst.addContent(result.getString(1));
				state.addContent(stNumst);

				Element stQujian = new Element("stQujian");
				stQujian.addContent(result.getString(2));
				state.addContent(stQujian);

				Element stDscp = new Element("stDscp");
				stDscp.addContent(result.getString(3));
				state.addContent(stDscp);

				Element stType = new Element("stType");
				stType.addContent(result.getString(4));
				state.addContent(stType);

				Element stPPP = new Element("stPPP");
				stPPP.addContent(result.getString(5));
				state.addContent(stPPP);

				Element stColor = new Element("stColor");
				stColor.addContent(result.getString(6));
				state.addContent(stColor);

				Element stValID = new Element("stValID");
				stValID.addContent(vid);
				state.addContent(stValID);
				
				Element stValType = new Element("stValType");
				stValType.addContent(vType);
				state.addContent(stValType);
				
				root.addContent(state);
			}

			Document slctDoc = new Document(root);
			XMLOutputter outputter = new XMLOutputter();
			return outputter.outputString(slctDoc);

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * 获得变量的状态信息表
	 * @param vType 变量类型
	 * @param vid 变量编号
	 * @return 存储变量状态信息的XML字符串
	 */
	public String getBXStGrid(String vType, String vid, String strState)
	{

		if (con == null)
		{
			return null;
		}

		Element root = new Element("val");

		try 
		{
			con.setAutoCommit(true);
			Statement statement = con.createStatement();
			String strStateArr[] = strState.split("-"); 
			for (int i = 0; i < strStateArr.length; i++)
			{
				String sqlCode = "select state_id,state_des,state_color from state where com_id='"
						+ vid + "' and com_type='" + vType + "'" 
						+ "and state_id = '" + strStateArr[i] + "' order by state_id";
				ResultSet result = statement.executeQuery(sqlCode);
				while (result.next())
				{
					Element state = new Element("state");
					
					Element stType = new Element("stType");
					stType.addContent(result.getString(1));
					state.addContent(stType);
					
					Element stDscp = new Element("stDscp");
					stDscp.addContent(result.getString(2));
					state.addContent(stDscp);
	
					Element stColor = new Element("stColor");
					stColor.addContent(result.getString(3));
					state.addContent(stColor);
					
					root.addContent(state);
				}
			}
			
			Document slctDoc = new Document(root);
			XMLOutputter outputter = new XMLOutputter();
			return outputter.outputString(slctDoc);

		}
		catch (SQLException e)
		{
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * 检查变量是否重复
	 * @param vType 变量类型
	 * @param vName 变量名称
	 * @param vKeyWords 关键字
	 * @param strCedian 测点
	 * @param strVid 变量的ID 
	 * @param vTypeOld 变量的旧类型
	 * @return
	 */
	public static int checkVal(String vType,String vName, String vKeyWords, String strCedian, String strVid, String vTypeOld)
	{
	    //////
		if (con == null) {
			return -1;
		}
		try
		{
			
			String strSql;
			Statement sta = null;
			ResultSet rSet = null;
			
			if ((vType.equals("X")||vType.equals("S"))&&!strCedian.trim().equals(""))
			{
				con.setAutoCommit(true);
				strSql = "select * from com where cedian = '" + strCedian + "'and com_id != '" 
			    + strVid + "' and com_type = '" + vTypeOld + "'";
				sta = con.createStatement();
				
				rSet = sta.executeQuery(strSql);
				rSet.last();
				if (rSet.getRow() > 0) return -4;

			}
			
			con.setAutoCommit(true);
			strSql = "select * from com where com_name = '" + vName 
			    + "' and com_keyword = '" + vKeyWords + "' and com_id != '" 
			    + strVid + "' and com_type = '" + vTypeOld + "'";
			sta = con.createStatement();
			rSet = sta.executeQuery(strSql);
			rSet.last();
			if(rSet.getRow() > 0)
			{
				return 2;
			}
			
			con.setAutoCommit(true);
			strSql = "select * from com where com_name = '" + vName + "' and com_id != '" 
			    + strVid + "' and com_type = '" + vTypeOld + "'";
			sta = con.createStatement();
			
			rSet = sta.executeQuery(strSql);
			rSet.last();
			if(rSet.getRow() > 0)
			{
				return 2;
			}
			
			con.setAutoCommit(true);
			strSql = "select * from com where com_keyword = '" + vKeyWords + "'and com_id != '" 
			    + strVid + "' and com_type = '" + vTypeOld + "'";
			sta = con.createStatement();
			
			rSet = sta.executeQuery(strSql);
			rSet.last();
			if(rSet.getRow() > 0)
			{
				return -3;
			}
			
			return 2;
		}
		catch (SQLException e)
		{
			// TODO Auto-generated catch block
			return -1;
		}
		
	    //////查询关键字是否相同
		
	}

	
	/**
	 * 编辑变量工程图信息(修改变量工程图)
	 * @param vType 变量类型
	 * @param vid 变量ID
	 * @param slctX0Per 变量在工程图的X坐标
	 * @param slctY0Per 变量在工程图的Y坐标
	 * @param slctWidthPer 变量在工程图的X坐标的缩放比 
	 * @param slctHeightPer 变量在工程图的Y坐标的缩放比
	 * @param color1 表示变量框的颜色1
	 * @param color2 表示变量框的颜色2
	 * @param imageid 工程图的ID
	 * @param fillOpacity 表示变量框的透明度
	 * @return
	 */
	public int scltAg(String vType, String vid, Number slctX0Per, Number slctY0Per, Number slctWidthPer, 
			Number slctHeightPer, String color1, String color2, int imageid, Double fillOpacity,
			Number bpX0Per, Number bpY0Per, Number pointX0Per, Number pointY0Per) 
	{
		if (con == null) 
		{
			return 0;
		}
		try 
		{
			String color = color1 + ";" + color2 + ";" + fillOpacity;
			con.setAutoCommit(true);
			String sqlCode = "update comimage set location_x='" + slctX0Per	+ "',location_y='" 
					+ slctY0Per + "',location_xper='" + slctWidthPer + "'," + "location_yper='"
					+ slctHeightPer	+ "',color='" + color + "',biaopai_x='" + bpX0Per	+ "',biaopai_y='" 
					+ bpY0Per + "',point_x='" + pointX0Per + "',point_y='" + pointY0Per
					+ "' where com_type='" + vType
					+ "' and com_id='" + vid + "' and image_id='" + imageid
					+ "'";
			Statement statement = con.createStatement();
			statement.executeUpdate(sqlCode);
			return 1;

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return 0;
		}
	}

	/**
	 * 调整变量在工程图上的位置
	 * @param vType 变量类型
	 * @param vid 变量的ID
	 * @param slctX0Per 变量在工程图的X坐标
	 * @param slctY0Per 变量在工程图的Y坐标
	 * @param slctWidthPer 变量在工程图的X坐标的缩放比 
	 * @param slctHeightPer 变量在工程图的Y坐标的缩放比
	 * @param imageid 工程图的ID
	 * @param pointX0Per 变量标牌拐点的X坐标
	 * @param pointY0Per 变量标牌拐点的Y坐标
	 * @return 0.2
	 */
	public int scltAgTuo(String vType, String vid, Number slctWidthPer, Number slctHeightPer,int imageid, Integer iDragKind,
						 Number X0Per, Number Y0Per)/**/
	{
		if (con == null) 
		{
			return 0;
		}
		try 
		{
			con.setAutoCommit(true);
			String sqlCode = "";
			if (iDragKind == 1)
			{
				sqlCode = "update comimage set location_x='" + X0Per	+ "',location_y='" + Y0Per
				+ "',location_xper='" + slctWidthPer + "',location_yper='" + slctHeightPer 
				+ "' where com_type ='" + vType + "' and com_id='" + vid + "' and image_id='" + imageid + "'";
			}
			if (iDragKind == 2)
			{
				sqlCode = "update comimage set biaopai_x='" + X0Per	+ "',biaopai_y='" + Y0Per
					+ "',location_xper='" + slctWidthPer + "',location_yper='" + slctHeightPer 
					+ "' where com_type ='" + vType + "' and com_id='" + vid + "' and image_id='" + imageid + "'";
			}
			if (iDragKind == 3)
			{
				sqlCode = "update comimage set point_x='" + X0Per	+ "',point_y='" + Y0Per
				+ "',location_xper='" + slctWidthPer + "',location_yper='" + slctHeightPer 
				+ "' where com_type ='" + vType + "' and com_id='" + vid + "' and image_id='" + imageid + "'";
			}
			Statement statement = con.createStatement();
			statement.executeUpdate(sqlCode);
			return 1;

		} 
		catch (SQLException e)
		{
			e.printStackTrace();
			return 0;
		}
	}
	
	/**
	 * 根据传入的变量的信息，修改数据库中测点的信息
	 * @param COM_ID			测点ID
	 * @param ENTITY_ID 		实体ID
	 * @param TAG 				测点名
	 * @param LABEL1 			测点名
	 * @param LABEL2 			屏显简述
	 * @param COORDINATE 		坐标
	 * @param QUADRANT 			象限
	 * @param BOARD_ANGLE 		标牌角度
	 * @param BOARD_LENGTH 		标牌长度
	 * @param BOARD_STATE 		标牌状态
	 * @param BOARD_VALUE 		标牌值
	 * @param BOARD_DIRECT 		标牌方向
	 * @param BOARD_ROTATIONZ 	镜头旋转的rotationZ值
	 * @return
	 */
	public int editPoint(ArrayCollection changeArr)
	{
		if (con == null) 
		{
			return 0;
		}
		int i = 0;
		ASTranslator asT = new ASTranslator();
		try 
		{
			while (i < changeArr.size())
			{
				ASObject asObj = (ASObject)changeArr.get(i);
				asObj.setType("gct.ChangeList");
				ChangeList changeList = new ChangeList();
				changeList = (ChangeList) asT.convert(asObj, ChangeList.class);
				
				String coordinate = changeList.zuobiao_x + "," + changeList.zuobiao_y + "," + changeList.zuobiao_z;
				String quadrant = changeList.xx1 + "," +changeList.xx2 + "," + changeList.xx3 + "," + changeList.xx4;
		
				con.setAutoCommit(true);
				String sqlCode = "update comentity set tag='" + changeList.tag	
				+ "',label1='" + changeList.label1
				+ "',label2='" + changeList.label2 + "',coordinate='" + coordinate 
				+ "',quadrant='" + quadrant	
				+ "',board_angle='" + changeList.board_angle 
				+ "',coordinate='" + coordinate 
				+ "',board_length='" + changeList.board_length 
				+ "',board_state='" + changeList.board_state 
				+ "',board_value='" + changeList.board_value 
				+ "',board_direct='" + changeList.board_direct 
				+ "',board_rotationz='" + changeList.board_rotationz
				+ "' where com_id ='" + changeList.com_id 
				+ "' and entity_id='" + changeList.entity_id + "'";
				
				Statement statement = con.createStatement();
				statement.executeUpdate(sqlCode);
				i++;
			}
			return 1;
		} 
		catch (SQLException e)
		{
			e.printStackTrace();
			return -1;
		}
	}
		
	/**
	 * 调整变量在工程图上的颜色、透明度
	 * @param vType 变量类型
	 * @param vid 变量ID
	 * @param color1 表示变量框的颜色1
	 * @param color2 表示变量框的颜色2
	 * @param imageid 工程图的ID
	 * @param fillOpacity 表示变量框的透明度
	 * @return
	 */
	public int editScltColor(String vType, String vid, 
			String color1, String color2, int imageid, Double fillOpacity) {
		if (con == null) {
			return 0;
		}
		try {
			String color = color1 + ";" + color2 + ";" + fillOpacity;
			con.setAutoCommit(true);
			String sqlCode = "update comimage set color='" + color + 
					"' where com_type ='" + vType
					+ "' and com_id='" + vid + "' and image_id='" + imageid
					+ "'";
			Statement statement = con.createStatement();
			statement.executeUpdate(sqlCode);
			return 1;

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return 0;
		}
	}

	/**
	 * 根据传入的参数编辑变量的信息
	 * @param vTypeOld 变量旧的类型
	 * @param vType 变量新的类型
	 * @param vid 变量ID
	 * @param stNumber 变量的状态的数量
	 * @param vName 变量名称
	 * @param vDscp 变量描述
	 * @param vKeyWords 变量关键字
	 * @param stGridPrd  变量在工程图上的信息
	 * @param ceDian 测点 
	 * @param isSwitch 变量是否为开关量
	 * @param vOne 分类一
	 * @param vTwo 分类二
	 * @param vThree 分类三
	 * @param sp 
	 * @param dval 
	 * @param iscount 
	 * @param cost 
	 * @return
	 */
	public int editVal(String vTypeOld, String vType, String vid,
			String stNumber, String vName, String vDscp, String vKeyWords,
			ArrayList stGridPrd, String ceDian, boolean isSwitch,
			Number vOne, String vTwo, String vThree,int sp,float dval,int iscount,int cost) {

		if (con == null) {
			return -1;
		}
        
        System.out.println(vTypeOld+vType);
		try {
			if (vTypeOld.compareTo(vType) == 0) {
			//	con.setAutoCommit(true);
				String sqlCode = "delete from state where com_id='" + vid
						+ "' and com_type='" + vTypeOld + "'";
				Statement statements = con.createStatement();
				statements.execute(sqlCode);
				sqlCode = "update com set com_type=?,com_name=?,com_des=?,com_keyword=?,cedian=?," +
							"one=?,two=?,three=?,switch=?,sp=?,dval=?,iscount=?,cost=? " +
							"where com_id=? and com_type=?";
				PreparedStatement statement = con.prepareStatement(sqlCode);
				statement.setString(1, vType);
				statement.setString(2, vName);
				statement.setString(3, vDscp);
				statement.setString(4, vKeyWords);
				statement.setString(14, vid);
				statement.setString(15, vTypeOld);
				statement.setString(5, ceDian.trim());
				statement.setInt(6, vOne.intValue());
				statement.setString(7, vTwo);
				statement.setString(8, vThree);
				statement.setBoolean(9, isSwitch);
				statement.setInt(10, sp);
				statement.setFloat(11, dval);
				statement.setInt(12, iscount);
				statement.setInt(13, cost);
				 
				statement.executeUpdate();
				//bxs update the attritude of sp in table cominage
				sqlCode="update comimage set sp=? where com_id=? and com_type=? ";
				PreparedStatement statement1 = con.prepareStatement(sqlCode);
				statement1.setInt(1, sp);
		
				statement1.setInt(2, Integer.parseInt(vid));
				statement1.setString(3, vTypeOld);
				
				statement1.executeUpdate();
				//bxs 添加，修改X类型变量为SX类型变量
				//修改comgraph中的sp
				sqlCode="update comgraph set sp=? where com_id=? and com_type=? ";
				PreparedStatement statement2 = con.prepareStatement(sqlCode);
				statement2.setInt(1, sp);
				statement2.setInt(2, Integer.parseInt(vid));
				statement2.setString(3, vTypeOld);
				statement2.executeUpdate();
				//
				//修改relation中的bsp或esp
				
				sqlCode="update relation set bsp=? where REL_BEGIN_ID=? and REL_BEGIN_TYPE=? ";
				PreparedStatement statement3 = con.prepareStatement(sqlCode);
				statement3.setInt(1, sp);
				statement3.setInt(2, Integer.parseInt(vid));
				statement3.setString(3, vTypeOld);
				statement3.executeUpdate();
				
				sqlCode="update relation set esp=? where REL_END_ID=? and REL_END_TYPE=? ";
				PreparedStatement statement4 = con.prepareStatement(sqlCode);
				statement4.setInt(1, sp);
				statement4.setInt(2, Integer.parseInt(vid));
				statement4.setString(3, vTypeOld);
				statement4.executeUpdate();
				
				Map<Integer, Integer> stateMap = new HashMap<Integer, Integer>();
				for (int i = 0; i < stGridPrd.size(); i++) {
					Map ha = (Map) (stGridPrd.get(i));
					String stateId = (String) ha.get("stNumst");
					String quJian = (String) ha.get("stQujian");
					String stDscp = (String) ha.get("stDscp");
					String stType = (String) ha.get("stType");
					String stPPP = (String) ha.get("stPPP");
					String stColor = (String) ha.get("stColor");
					sqlCode = "insert into state(state_id,com_id,com_type,state_qujian,state_des,state_type,state_gailv,state_color) values(?,?,?,?,?,?,?,?)";
					statement = con.prepareStatement(sqlCode);
					statement.setString(1, stateId);
					statement.setString(2, vid);
					statement.setString(3, vType);
					statement.setString(4, quJian);
					statement.setString(5, stDscp);
					statement.setString(6, stType);
					statement.setString(7, stPPP);
					statement.setString(8, stColor);
					statement.executeUpdate();
					stateMap.put(Integer.valueOf((String) ha.get("stNumst")), Integer.valueOf((String) ha.get("stOldNumst")));
				}
				updateRelatedLines(Integer.parseInt(vid), vType, stateMap);
				return Integer.valueOf(vid);
			} else {
				return -1;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			try {
				con.rollback();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			e.printStackTrace();
			return 0;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return 0;
		}
	}
	
	/**
	 * @author Su Chen
	 * @param id
	 * @param type
	 * @param stateMap
	 */
	public void updateRelatedLines(int id, String type, Map<Integer, Integer> stateMap) {
		DucgSql ds = new DucgSql();
		ds.connection();
		RelatedLines rl = ds.getRelatedLinesObject(id, type);
		int maxStateId = -1;
		for(Integer k : stateMap.keySet())
		{
			if(k > maxStateId)
				maxStateId = k;
		}
		for(Line l : rl.inLines)
		{			
			Matrix m = new Matrix(maxStateId + 1, l.mData.columsCount);
			for(int i = 0; i <= maxStateId; i++)
			{
				if(stateMap.containsKey(i) && stateMap.get(i) >= 0)
				{
					for(int j = 0; j < l.mData.columsCount; j++)
					{
						m.setCellAt(i, j, l.mData.getCellAt(stateMap.get(i), j));
					}
				}
			}
			ds.updatePartialLine(l.lineId, m.linesCount, m.columsCount, m.toString());
		}
		for(Line l : rl.outLines)
		{
			Matrix m = new Matrix(l.mData.linesCount, maxStateId + 1);
			for(int i = 0; i <= maxStateId; i++)
			{
				if(stateMap.containsKey(i) && stateMap.get(i) >= 0)
				{
					for(int j = 0; j < l.mData.linesCount; j++)
					{
						m.setCellAt(j, i, l.mData.getCellAt(j, stateMap.get(i)));
					}
				}
			}
			ds.updatePartialLine(l.lineId, m.linesCount, m.columsCount, m.toString());
		}
	}

	/**
	 * 董力编写
	 * @param stGridPrd
	 * @return
	 * DN20160519修改
	 */
	public int setAllSt(ArrayCollection stGridPrd, String strFileName) {
		//public int setAllSt(ArrayCollection stGridPrd) {
			DatagramSocket dsoc = null;
			ASTranslator asT = new ASTranslator();
			int sendCount = -2;
			try
			{
				dsoc = new DatagramSocket();
			} 
			catch (SocketException e) 
			{
				e.printStackTrace();
				return 0;
			}
			
			ByteBuffer chearFlag = ByteBuffer.allocate(10);
			chearFlag.order(ByteOrder.LITTLE_ENDIAN);
			short flagData = -3;
			chearFlag.putShort(flagData);
			byte[] fuyi = chearFlag.array();
			DatagramPacket dpsendCount;
			try
			{
				dpsendCount = new DatagramPacket(fuyi, fuyi.length, new InetSocketAddress("127.0.0.1", 6300));
				dsoc.send(dpsendCount);
				try
				{
				    Thread.sleep(500);
				}
				catch (InterruptedException e)
				{
				    return 0;
				}
			}
			catch (IOException e) 
			{
				e.printStackTrace();
				return 0;
			}
			
			
			while (sendCount < stGridPrd.size()) 
			{
				ByteBuffer tempBuf = ByteBuffer.allocate(1024);
				tempBuf.order(ByteOrder.LITTLE_ENDIAN);
				if (sendCount >= 0) 
				{
					ASObject asObj = (ASObject) stGridPrd.get(sendCount);
					asObj.setType("gct.nodeStatus");
					nodeStatus nodeSt = (nodeStatus) asT.convert(asObj,
							nodeStatus.class);
					// System.out.println(nodeSt.vType + "%%%%%%%%%");
					short type = (short) (nodeSt.vType.equals("B") ? 1 : 2);
					short index = (short) Integer.parseInt(nodeSt.vNum);
					short status1 = (short) Integer.parseInt(nodeSt.vStSlct);
					try 
					{
						tempBuf.putShort(type);
						tempBuf.putShort(index);
						tempBuf.putFloat(0);
						tempBuf.putShort((short) 0);
						tempBuf.putShort(status1);
						tempBuf.putShort((short) 0);
						tempBuf.putFloat(0);
						tempBuf.putFloat(0);
						byte[] buffer = tempBuf.array();
						DatagramPacket dp = new DatagramPacket(buffer,
								buffer.length, new InetSocketAddress("127.0.0.1",
										6300));
						dsoc.send(dp);
						tempBuf.clear();
					}
					catch (IOException e)
					{
						e.printStackTrace();
						return 0;
					}
				} 
				else 
				{
					
					if (sendCount== -2)//发送住院号
					{
						
						String  id = strFileName;
						
						//tempBuf.put
						
						;//tempBuf.array();
						DatagramPacket dp;
						try 
						{   
							byte[] buffer = id.getBytes("gbk");
							//int len=buffer.length;
							dp = new DatagramPacket(buffer, buffer.length,
									new InetSocketAddress("127.0.0.1", 6300));
							dsoc.send(dp);
						} 
						catch (IOException e) 
						{
							e.printStackTrace();
							return 0;
						}
						tempBuf.clear();
						}
					
					if(sendCount== -1)
					{
					short data = (short) 
					stGridPrd.size();
					System.out.println(data + ">>>>");
					tempBuf.putShort(data);
					byte[] buffer = tempBuf.array();
					DatagramPacket dp;
					try 
					{
						dp = new DatagramPacket(buffer, buffer.length,
								new InetSocketAddress("127.0.0.1", 6300));
						dsoc.send(dp);
					} 
					catch (IOException e) 
					{
						e.printStackTrace();
						return 0;
					}
					tempBuf.clear();
					}
				}
				sendCount++;
			}
			if (dsoc != null)
				dsoc.close();
			return sendCount;
		}
     
	/**
	 * @param stGridPrd
	 * @param valStateArr
	 * @return
	 */
	public int changeValState(ArrayCollection stGridPrd, ArrayCollection valStateArr, int badReplayCount)
	{    
		DatagramSocket dsoc = null;
		ASTranslator asT = new ASTranslator();
		ASTranslator asTNewState = new ASTranslator();
		int sendCount = -1;
		int newStateCount = 0;
		try
		{
			dsoc = new DatagramSocket();
		} 
		catch (SocketException e) 
		{
			e.printStackTrace();
			return 0;
		}
		
		ByteBuffer chearFlag = ByteBuffer.allocate(10);
		chearFlag.order(ByteOrder.LITTLE_ENDIAN);
		short flagData = -1;
		chearFlag.putShort(flagData);
		byte[] fuyi = chearFlag.array();
		DatagramPacket dpsendCount;
		try
		{
			dpsendCount = new DatagramPacket(fuyi, fuyi.length, new InetSocketAddress("127.0.0.1", 6300));
			dsoc.send(dpsendCount);
			try
			{
			    Thread.sleep(500);
			}
			catch (InterruptedException e)
			{
			    return 0;
			}
		}
		catch (IOException e) 
		{
			e.printStackTrace();
			return 0;
		}
		for (int subCount = 1; subCount <= badReplayCount; subCount++)	//根据故障回放的次数进行故障数据重新发送
		{
			while (sendCount < stGridPrd.size()) 
			{
				ByteBuffer tempBuf = ByteBuffer.allocate(1024);
				tempBuf.order(ByteOrder.LITTLE_ENDIAN);
				if (sendCount >= 0) 
				{
					ASObject asObj = (ASObject)stGridPrd.get(sendCount);
					asObj.setType("gct.NodeListStatus");
					NodeListStatus nodeSt = new NodeListStatus();
					nodeSt = (NodeListStatus) asT.convert(asObj, NodeListStatus.class);
					         
					short type = (short) (nodeSt.vListType.equals("B") ? 1 : 2);
					short index = (short) Integer.parseInt(nodeSt.vListNum);
					short status1 = (short) Integer.parseInt(nodeSt.vListValState);
					
					while(newStateCount < valStateArr.size())
					{
						ASObject asNewStateObj = (ASObject) valStateArr.get(newStateCount++);
						asNewStateObj.setType("gct.NodeNewStatus");
						NodeNewStatus nodeNewSt = new NodeNewStatus();
						nodeNewSt =	(NodeNewStatus) asT.convert(asNewStateObj, NodeNewStatus.class);
						
						short typeNew = (short) (nodeNewSt.stValType.equals("B") ? 1 : 2);
						short indexNew = (short) Integer.parseInt(nodeNewSt.stValID);
						short statusNew = (short) Integer.parseInt(nodeNewSt.stNumst);
						
						if (type == typeNew && index == indexNew) 
						{
							status1 = statusNew;
							break;
						}
					}
									
					try
					{
						tempBuf.putShort(type);
						tempBuf.putShort(index);
						tempBuf.putFloat(0);
						tempBuf.putShort((short) 0);
						tempBuf.putShort(status1);
						tempBuf.putShort((short) 0);
						tempBuf.putFloat(0);
						tempBuf.putFloat(0);
						byte[] buffer = tempBuf.array();
						DatagramPacket dp = new DatagramPacket(buffer,
								buffer.length, new InetSocketAddress("127.0.0.1", 6300));
						dsoc.send(dp);
						tempBuf.clear();
					}
					catch (IOException e)
					{
						e.printStackTrace();
						return 0;
					}
				}            
				else                     
				{
					short data = (short) stGridPrd.size();
					System.out.println("The " + subCount + "/" + badReplayCount + " times bad repaly, " + "valNum: " + data );
					tempBuf.putShort(data);
					byte[] buffer = tempBuf.array();
					
					DatagramPacket dp;
					try 
					{
						dp = new DatagramPacket(buffer, buffer.length, new InetSocketAddress("127.0.0.1", 6300));
						dsoc.send(dp);
					} 
					catch (IOException e) 
					{
						e.printStackTrace();
						return 0;
					}
					tempBuf.clear();
				}
				sendCount++;
			}
			//////////////////////////////////////////////////////////////////
			sendCount = -1;				//每一次提交故障数据后，变量的发送个数复位。
			try 
			{
			    Thread.sleep(500);
			}
			catch (InterruptedException e) 
			{
			    return 0;
			}
		}
		
		if (dsoc != null)
		{
			dsoc.close();
		}
		return sendCount;
	}
	
	/**
	 * 获得所有X，B变量信息
	 * @return
	 */
	public String getBOrXMessage() {
		if (con == null) {
			return null;
		}

		Element vals = new Element("vals");

		try {
			con.setAutoCommit(true);

			String sqlCode = "select com_id,com_type,com_des from com where com_type='X' AND isshow='1' AND com_des != '测试'";

			Statement st1 = con.createStatement();
			ResultSet re1 = st1.executeQuery(sqlCode);
			while (re1.next()) {
				Element val = new Element("val");

				String id = re1.getString(1);
				String type = re1.getString(2);

				Element vType = new Element("vType");
				vType.addContent(type);
				val.addContent(vType);

				Element vNum = new Element("vNum");
				vNum.addContent(id);
				val.addContent(vNum);

				Element vName = new Element("vName");
				vName.addContent(re1.getString(3));
				val.addContent(vName);

				sqlCode = "select state_id,state_des from state where com_id='"
						+ id + "' and com_type='" + type + "'";
				Statement st2 = con.createStatement();
				ResultSet re2 = st2.executeQuery(sqlCode);
				int number = 0;
				String message = "";
				if (re2.next()) {
					number++;
					message += re2.getString(1) + "-" + re2.getString(2);
				}
				while (re2.next()) {
					number++;
					message += "\r" + re2.getString(1) + "-" + re2.getString(2);
				}
				// System.out.println(message);

				Element vStNum = new Element("vStNum");
				vStNum.addContent(String.valueOf(number));
				val.addContent(vStNum);

				Element vStStr = new Element("vStStr");
				vStStr.addContent(message);
				val.addContent(vStStr);

				vals.addContent(val);
			}

			XMLOutputter outputter = new XMLOutputter();
			return outputter.outputString(new Document(vals));

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}

	}
	
	public String getEvidence(String strFileName) 
	{
		if (con == null) {
			return null;
		}
		
		Element vals = new Element("vals");
		
		try 
		{
			con.setAutoCommit(true);
			String sqlCode = "select Num, State from evidence where FileName = '" + strFileName.trim() + "'";
			Statement st1 = con.createStatement();
			ResultSet re1 = st1.executeQuery(sqlCode);
			int i=0;
			while (re1.next()) 
			{
				String Num = re1.getString(1);
				String State = re1.getString(2);
				
				Element val = new Element("val");
				Element vNum = new Element("vNum");
				vNum.addContent(Num);
				val.addContent(vNum);
				Element vStStr = new Element("vStStr");
				vStStr.addContent(State);
				val.addContent(vStStr);
				
				vals.addContent(val);
				i++;
			}
			
			XMLOutputter outputter = new XMLOutputter();
			if(i==0)
			{
			    return "";
			}
			else
			{
			    return outputter.outputString(new Document(vals));
			}
		}
		catch (SQLException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	
	public int saveEvidence(ArrayCollection stGridPrd, String strFileName)
	{
		if (con == null) {
			return 0;
		}
		
		try 
		{
			con.setAutoCommit(true);
			String strDelete = "delete from evidence where FileName = '" + strFileName + "'";
			Statement st = con.createStatement();
			st.execute(strDelete);
			st.close();
		}
		catch (SQLException e) 
		{
			e.printStackTrace();
			return 0;
		}
		ASTranslator asT = new ASTranslator();
		String strAdd = "INSERT INTO evidence (FileName, Num, State) VALUES";
		for(int i=0; i<stGridPrd.size(); i++)
		{
			ASObject asObj = (ASObject) stGridPrd.get(i);
			asObj.setType("gct.nodeStatus");
			nodeStatus nodeSt = (nodeStatus) asT.convert(asObj,
					nodeStatus.class);
			
			String strNum = nodeSt.vNum;
			String strState = nodeSt.vStSlct;
			if(i==stGridPrd.size() -1)
			{
				strAdd = strAdd + "('" + strFileName + "','" + strNum + "','" + strState + "'); ";
			}
			else
			{
			    strAdd = strAdd + "('" + strFileName + "','" + strNum + "','" + strState + "'), ";
			}
			
		}
		
		
		
		try 
		{
		    Statement st1 = con.createStatement();
		    st1.execute(strAdd);
		    st1.close();
		}
		catch (SQLException e) 
		{
			e.printStackTrace();
			return 0;
		}
		
		
		return 1;
	}
}