package gct;
import java.io.*;
import java.util.ResourceBundle;

import javax.imageio.stream.FileImageInputStream;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.omg.CORBA.Request;

/**
 * ������д
 * @author SHU
 *
 */
public class readImg extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor of the object.
	 */
	public readImg() {
		super();
	}

	/**
	 * Destruction of the servlet. <br>
	 */
	public void destroy() {
		super.destroy(); // Just puts "destroy" string in log
		// Put your code here
	}

	/**
	 * The doGet method of the servlet. <br>
	 * 
	 * This method is called when a form has its tag value method equals to get.
	 * 
	 * @param request
	 *            the request send by the client to the server
	 * @param response
	 *            the response send by the server to the client
	 * @throws ServletException
	 *             if an error occurred
	 * @throws IOException
	 *             if an error occurred
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
//		try {
//			String fileNum = request.getParameter("id");
//			FileImageInputStream inputStream = new FileImageInputStream(
//					new File(GetPathStr.getPath() + "DUCG/imgUp/" + fileNum));
//			byte data[] = new byte[(int) inputStream.length()];
//			inputStream.read(data);
//			inputStream.close();
//			response.setContentType("image/*");
//			ServletOutputStream outputStream = response.getOutputStream();
//			outputStream.write(data);
//			outputStream.close();
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			String fileNum = request.getParameter("id");
			FileImageInputStream inputStream = new FileImageInputStream(
					new File(GetPathStr.getPathWeb() + "imgUp/" + fileNum));
			byte data[] = new byte[(int) inputStream.length()];
			inputStream.read(data);
			inputStream.close();
			response.setContentType("image/*");
			ServletOutputStream outputStream = response.getOutputStream();
			outputStream.write(data);
			outputStream.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	protected void processRequest(HttpServletRequest request,  
            HttpServletResponse response) throws ServletException, IOException {  
          
        String imgName = request.getParameter("id");
        String strrequestName = request.getServerName();
//        ResourceBundle resourceBundel = ResourceBundle.getBundle("Application");  
//        String imgPath = resourceBundel.getString("img_root");  
          
        File imgFile = new File(GetPathStr.getPathWeb() + "imgUp/" + imgName);
        if(imgFile.exists()) {  
              
            response.reset();  
            response.setContentType("application/octet-stream");  
            response.setCharacterEncoding("utf-8");  
            response.setHeader("Content-disposition", "filename=" + imgName);  
              
            InputStream is = new FileInputStream(imgFile);  
            OutputStream outputStream = response.getOutputStream();  
            int data = -1;    
            while((data = is.read()) != -1) {  
                outputStream.write(data);    
            }  
            outputStream.flush();  
            outputStream.close();
            
        }
        this.destroy();
    }  

	/**
	 * Initialization of the servlet. <br>
	 * 
	 * @throws ServletException
	 *             if an error occurs
	 */
	public void init() throws ServletException {
		// Put your code here
	}

}
