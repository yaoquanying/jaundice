package gct;
import java.io.*;
import java.sql.*;
import java.util.*;
import org.jdom.*;

/**
 * 用于删除变量的类
 * @author SHU
 *
 */
public class DelVal {
	
	/**
	 * 用于连接数据库
	 */
	private static Connection con = UtilsSQL.con;

	/**
	 * 董力编写
	 */
	public static void crtDict() {
		File dBMain = new File(GetPathStr.getPath() + "DUCG");
		if (!dBMain.exists())
			dBMain.mkdir();
		File dB = new File(GetPathStr.getPath() + "DUCG/B");
		File dX = new File(GetPathStr.getPath() + "DUCG/X");

		if (!dB.exists())
			dB.mkdir();
		if (!dX.exists())
			dX.mkdir();
	}

	/** 变量是否被占用
	 * @param vType
	 * @param vid
	 * @param imageNoId
	 * @param type
	 * @return
	 */
	public static int valIsUse(String vType, String vid, String imageNoId, int type)
	{
		if (con == null)
		{
			return 0;
		}
		try
		{
			con.setAutoCommit(true);
			String sqlCode = "select *  from comgraph where com_id='" + vid + "' and com_type='" + vType + "'";
			Statement statement = con.createStatement();
			ResultSet rSetRut = statement.executeQuery(sqlCode);
			rSetRut.last();
			if (rSetRut.getRow() > 0)
			{
				return 4; //该变量已经在知识库中使用，不能删除
			}
		} 
		catch (SQLException e)
		{
			e.printStackTrace();
			return -1;
		}
		return 1;
	}
	/**
	 * 删除变量
	 * @param vType 变量类型
	 * @param vid 变量的编号
	 * @param imageNoId 变量所在的工程图的工程图编号
	 * @param type 工程图的类型学
	 * @return 删除的结果
	 */
	public static int delVal(String vType, String vid, String imageNoId, int type, String vDelId, String vDelType)
	{
		if (con == null)
		{
			return 0;
		}
		
		//删除变量信息
		if (type == 1)
		{
			try
			{
				con.setAutoCommit(true);
				String sqlCode = "select *  from comgraph where com_id='" + vid + "' and com_type='" + vType + "'";
				Statement statement = con.createStatement();
				ResultSet rSetRut = statement.executeQuery(sqlCode);
				rSetRut.last();
				if (rSetRut.getRow() > 0)
				{
					return 4; //该变量已经在知识库中使用，不能删除
				}
				
				//先将变量进行备份--------------------------------------
				delComVal(vType, vid, imageNoId, type, vDelId, vDelType);
				delComImageVal(vType, vid, imageNoId, type, vDelId, vDelType);
				delStateVal(vType, vid, imageNoId, type, vDelId, vDelType);
				//----------------------------------------------------
				
				sqlCode = "delete from com where com_id='" + vid
						+ "' and com_type='" + vType + "'";
				statement = con.createStatement();
				// System.out.println(sqlCode);
				statement.executeUpdate(sqlCode);
				
//				sqlCode = "insert into idtable(id,id_type) values('" + vid
//						+ "','" + vType + "')";
//				statement.executeUpdate(sqlCode);
				
				return 1;  //仅从com表中删除 
			} 
			catch (SQLException e)
			{
				e.printStackTrace();
				return -1;
			}
		} 
		else   //不删除其他工程图
		{
			try
			{
				con.setAutoCommit(true);
				String sqlCode = "select * from comimage where com_id='" + vid + "' and com_type='" + vType + "'";
				Statement sta1 = con.createStatement();
			
				ResultSet rSet = sta1.executeQuery(sqlCode);
				rSet.last();
				if(rSet.getRow() == 1)
				{
					con.setAutoCommit(true);  //算例
				    String sqlCode1 = "select * from comgraph where com_id='" + vid + "' and com_type='" + vType + "'";
				    Statement sta2 = con.createStatement();
					
					ResultSet rSet1 = sta2.executeQuery(sqlCode1);
					if(rSet1.next())
					{
						return -1;
					}
					//------------------先备份，后删除---------------------------------
					delComImageVal(vType, vid, imageNoId, type, vDelId, vDelType);
					delComVal(vType, vid, imageNoId, type, vDelId, vDelType);
					delStateVal(vType, vid, imageNoId, type, vDelId, vDelType);
					//-----------------------------------------------------------------------
					
					con.setAutoCommit(true);
					sqlCode = "delete from comimage where com_id=? and com_type=? and image_id=?";
					PreparedStatement sta = con.prepareStatement(sqlCode);
					sta.setString(1, vid);
					sta.setString(2, vType);
					sta.setString(3, imageNoId);
					sta.executeUpdate();
					sta.close();
					
					con.setAutoCommit(true);
					sqlCode = "delete from com where com_id='" + vid
							+ "' and com_type='" + vType + "'";
					Statement statement = con.createStatement();
					statement.executeUpdate(sqlCode);
					
//					sqlCode = "insert into idtable(id,id_type) values('" + vid
//							+ "','" + vType + "')";
//					statement.executeUpdate(sqlCode);
					
					System.out.println("OK");
					return 2;		//com,comimage全被删除
				}
				else
				{
					delComImageVal(vType, vid, imageNoId, type, vDelId, vDelType);
					con.setAutoCommit(true);
					sqlCode = "delete from comimage where com_id=? and com_type=? and image_id=?";
					PreparedStatement sta = con.prepareStatement(sqlCode);
					sta.setString(1, vid);
					sta.setString(2, vType);
					sta.setString(3, imageNoId);
					sta.executeUpdate();
					sta.close();
					return 3;		//从comimage表中删除
				}
			} 
			catch (SQLException e)
			{
				e.printStackTrace();
				return -1;
			}
		}
	}
	/**
	 * 备份从COM表中删除的变量
	 * @param vType 变量类型
	 * @param vid 变量的编号
	 * @param imageNoId 变量所在的工程图的工程图编号
	 * @param type 工程图的类型学
	 * @return 删除的结果
	 */
	public static int delComVal(String vType, String vid, String imageNoId, int type, String vDelId, String vDelType)
	{
		if (con == null)
		{
			return 0;
		}
		try
		{
			//删除原先在delcom表中保存临时恢复信息
			con.setAutoCommit(true);
			String sqlCode = "delete from delcom where com_id='" + vDelId
					+ "' and com_type='" + vDelType + "'";
			Statement statement = con.createStatement();
			statement.executeUpdate(sqlCode);
			//删除可能存在的同编号的变量
			sqlCode = "delete from delcom where com_id='" + vid
					+ "' and com_type='" + vType + "'";
			statement = con.createStatement();
			statement.executeUpdate(sqlCode);
			
			con.setAutoCommit(true);
			sqlCode = "select *  from com where com_id='" + vid
				+ "' and com_type='" + vType + "'";
			statement = con.createStatement();
			ResultSet result = statement.executeQuery(sqlCode);
			while (result.next())
			{
				String comIDStr = result.getString(1);
				String comTypeStr = result.getString(2);
				String comNameStr = result.getString(3);
				String comDesStr = result.getString(4);
				String comKeywordStr = result.getString(5);
				String comTableMarkStr = result.getString(6);
				String comGraphMarkStr = result.getString(7);
				String comCedianStr = result.getString(8);
				String comSwitchStr = result.getString(9);
				if (comTableMarkStr == null)
				{
					comTableMarkStr = "0";
				}
				if (comGraphMarkStr == null)
				{
					comGraphMarkStr = "0";
				}
				if (comSwitchStr == null)
				{
					comSwitchStr = "0";
				}
				
				con.setAutoCommit(true);
				sqlCode = "insert into delcom(com_id, com_type, com_name, com_des, com_keyword, " +
						"table_mark, graph_mark, cedian, switch) values('" + comIDStr + "','" + 
						comTypeStr  + "','" + comNameStr + "','" + comDesStr + "','" + 
						comKeywordStr + "','" + comTableMarkStr + "','" + comGraphMarkStr + "','" +
						comCedianStr + "','" + comSwitchStr + "')";
				statement = con.createStatement();
				statement.executeUpdate(sqlCode);
			}
		return 1;	
		} 
		catch (SQLException e)
		{
			e.printStackTrace();
			return -1;
		}
	}
	/**
	 * 备份从COM_IMAGE表中删除的变量
	 * @param vType 变量类型
	 * @param vid 变量的编号
	 * @param imageNoId 变量所在的工程图的工程图编号
	 * @param type 工程图的类型学
	 * @return 删除的结果
	 */
	public static int delComImageVal(String vType, String vid, String imageNoId, int type, String vDelId, String vDelType)
	{
		if (con == null)
		{
			return 0;
		}
		try
		{
			//删除原先在delcomimage表中保存临时恢复信息
			con.setAutoCommit(true);
			String sqlCode = "delete from delcomimage where com_id='" + vDelId
					+ "' and com_type='" + vDelType + "'";
			Statement statement = con.createStatement();
			statement.executeUpdate(sqlCode);
			//删除可能存在的同编号的变量
			sqlCode = "delete from delcomimage where com_id='" + vid
					+ "' and com_type='" + vType + "'";
			statement = con.createStatement();
			statement.executeUpdate(sqlCode);
			
			con.setAutoCommit(true);
			sqlCode = "select *  from comimage where com_id='" + vid
					+ "' and com_type='" + vType;
			if (type == 1)//删除其他功能图中的变量信息
			{
				sqlCode +=  "'";
			}
			else //只删除当前工程图中的变量
			{
				sqlCode +=  "' and image_id ='" + imageNoId + "'";
			}
			statement = con.createStatement();
			ResultSet result = statement.executeQuery(sqlCode);
			while (result.next())
			{
				String comIDStr 		= result.getString(1);
				String comTypeStr		= result.getString(2);
				String imageIDStr 		= result.getString(3);
				String locationXStr 	= result.getString(4);
				String locationYStr 	= result.getString(5);
				String locationXPERStr 	= result.getString(6);
				String locationYPERStr 	= result.getString(7);
				String colorStr 		= result.getString(8);
				String biaopaiXStr 		= result.getString(9);
				String biaopaiYStr 		= result.getString(10);
				String pointXStr 		= result.getString(11);
				String pointYStr 		= result.getString(12);

				con.setAutoCommit(true);
				sqlCode = "insert into delcomimage(com_id, com_type, image_id, location_x, location_y, " +
						"location_xper, location_yper, color, biaopai_x, biaopai_y, point_x, point_y) " + 
						"values('" 			+ 
						comIDStr 			+ "','" + 
						comTypeStr  		+ "','" + 
						imageIDStr 			+ "','" + 
						locationXStr 		+ "','" + 
						locationYStr 		+ "','" + 
						locationXPERStr 	+ "','" + 
						locationYPERStr 	+ "','" + 
						colorStr 			+ "','" + 
						biaopaiXStr 		+ "','" + 
						biaopaiYStr 		+ "','" + 
						pointXStr 			+ "','" + 
						pointYStr 			+ "')"; 
				statement = con.createStatement();
				statement.executeUpdate(sqlCode);
			}
			return 1;
		}
		catch (SQLException e)
		{
			e.printStackTrace();
			return -1;
		}
	}
	/**
	 * 备份从COM表中删除的变量
	 * @param vType 变量类型
	 * @param vid 变量的编号
	 * @param imageNoId 变量所在的工程图的工程图编号
	 * @param type 工程图的类型学
	 * @return 删除的结果
	 */
	public static int delStateVal(String vType, String vid, String imageNoId, int type, String vDelId, String vDelType)
	{
		if (con == null)
		{
			return 0;
		}
		try
		{
			//删除原先在delstate表中保存临时恢复信息
			con.setAutoCommit(true);
			String sqlCode = "delete from delstate where com_id='" + vDelId
					+ "' and com_type='" + vDelType + "'";
			Statement statement = con.createStatement();
			statement.executeUpdate(sqlCode);
			
			sqlCode = "delete from delstate where com_id='" + vid
					+ "' and com_type='" + vType + "'";
			statement = con.createStatement();
			statement.executeUpdate(sqlCode);
			
			con.setAutoCommit(true);
			sqlCode = "select *  from state where com_id='" + vid
				+ "' and com_type='" + vType + "'";
			statement = con.createStatement();
			ResultSet result = statement.executeQuery(sqlCode);
			while (result.next())
			{
				String stateIDStr = result.getString(1);
				String comIDStr = result.getString(2);
				String comTypeStr = result.getString(3);
				String stateQujianStr = result.getString(4);
				String stateDesStr = result.getString(5);
				String stateTypeStr = result.getString(6);
				String stateGailvStr = result.getString(7);
				String stateColorStr = result.getString(8);

				con.setAutoCommit(true);
				sqlCode = "insert into delstate(state_id, com_id, com_type, state_qujian, state_des, state_type, " +
						"state_gailv, state_color) values('" + stateIDStr + "','" + 
						comIDStr  + "','" + comTypeStr + "','" + stateQujianStr + "','" + 
						stateDesStr + "','" + stateTypeStr + "','" + stateGailvStr + "','" +
						stateColorStr + "')";
				statement = con.createStatement();
				statement.executeUpdate(sqlCode);
			}
		return 1;	
		} 
		catch (SQLException e)
		{
			e.printStackTrace();
			return -1;
		}
	}
	
	/**
	 * 删除测点信息
	 * @param com_id 		变量ID
	 * @param entity_id		实体ID
	 * @return 删除的结果
	 */
	public static int delPoint(String com_id, String entity_id)
	{
		if (con == null)
		{
			return 0;
		}

		try
		{
			con.setAutoCommit(true);
			String sqlCode = "delete from comentity where com_id='" + com_id
							+ "' and entity_id='" + entity_id + "'";
			
			Statement statement = con.createStatement();
							
			statement.executeUpdate(sqlCode);
			
			return 1;
		} 
		catch (SQLException e)
		{
			e.printStackTrace();
			return -1;
		}
	}
}
