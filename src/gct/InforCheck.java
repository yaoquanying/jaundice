package gct;	
	/**
	 * 用来获取硬件信息并检测系统的注册信息
	 * @author Tony
	 **/
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Iterator;
import java.util.Vector;

public class InforCheck
{	 
	static 
	{
		System.setProperty("java.library.path", System.getProperty("java.library.path"));
		System.loadLibrary("InforCheckJNI");
	}

	public native static int CheckLocationInfor(String fileName);
	public static int checkResult = -1;	//检查结果，保证在tomcat开始的时候只check一次。
	public int getRegisterInfor()
	{
		if (checkResult != -1)
		{
			return checkResult;
		}
		else
		{
			String path = GetPathStr.getPath().substring(0, GetPathStr.getPath().length() - 3);
			//System.out.println("loading  identity verification " + path + "Register");
			checkResult = CheckLocationInfor(path + "Register");
			checkResult = 10;
		}
		freeDll("InforCheckJNI");
		return checkResult;
	}
	
	 private synchronized void freeDll(String InforCheckJNI) 
	 {
		 try 
		 {
			 ClassLoader classLoader = this.getClass().getClassLoader();
			 Field field = ClassLoader.class.getDeclaredField("nativeLibraries");
			 field.setAccessible(true);
			 Vector<Object> libs = (Vector<Object>) field.get(classLoader);
			 Iterator<Object> it = libs.iterator();
			 Object o;
			 while (it.hasNext()) 
			 {
				 o = it.next();
				 Field[] fs = o.getClass().getDeclaredFields();
				 boolean hasInit = false;
				 for (int k = 0; k < fs.length; k++) 
				 {
					 if (fs[k].getName().equals("name")) 
					 {
						 fs[k].setAccessible(true);
						 String dllPath = fs[k].get(o).toString();
						 if (dllPath.endsWith(InforCheckJNI)) 
						 {
							 hasInit = true;
						 }
					 }
				 }
				 if (hasInit)
				 {
					 Method finalize = o.getClass().getDeclaredMethod("finalize", new Class[0]);
					 finalize.setAccessible(true);
					 finalize.invoke(o, new Object[0]);
					 it.remove();        
					 libs.remove(o);
				 }
			 }
		 } 
		 catch (Exception e)
		 {
			 e.printStackTrace();
		 }
	}
}