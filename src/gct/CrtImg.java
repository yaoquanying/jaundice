﻿package gct;
import java.awt.Image;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.*;
import java.sql.*;
import java.util.*;

import javax.imageio.ImageIO;

import org.jdom.*;

import com.sun.image.codec.jpeg.JPEGCodec;
import com.sun.image.codec.jpeg.JPEGImageEncoder;

/**
 * 创建工程图信息的类
 * @author SHU
 *
 */
public class CrtImg {
	private static Connection con = UtilsSQL.con;

	/**
	 * 创建目录
	 */
	public static void crtDictImg() {
		File dBMain = new File(GetPathStr.getPath() + "DUCG");
		if (!dBMain.exists())
			dBMain.mkdir();
		File dB = new File(GetPathStr.getPath() + "DUCG/img");
		if (!dB.exists())
			dB.mkdir();
		dB = new File(GetPathStr.getPathWeb() + "imgUp");
		if (!dB.exists())
			dB.mkdir();
	}

	/**
	 * 根据传入的信息，在数据库中创建对应的工程图信息
	 * @param imgNm 工程图名称
	 * @param imgDscp 对工程图的描述
	 * @param imgType 工程图的图片类型
	 * @param imgParentId 工程图的父图的ID
	 * @return 返回创建的工程图的ID
	 */
	public String crtImg(String imgNm, String imgDscp, String imgType, String imgParentId)
	{
		if (con == null) 
		{
			return null;
		}
		
		if(hasNamBefore(imgNm))
		{
			return "-1";
		}
		
		if (imgParentId.equals("R")) 
		{
			imgParentId = "1";
		}
		try {
			con.setAutoCommit(true);
			String sqlCode = "";
			if (imgParentId != null) 
			{
				sqlCode = "insert into image(image_name,image_des,image_type,image_parent) "
						+ "values('"
						+ imgNm
						+ "','"
						+ imgDscp
						+ "','"
						+ imgType + "','" + imgParentId + "')";
			} 
			else 
			{
				sqlCode = "insert into image(image_name,image_des,image_type) "
						+ "values('" + imgNm + "','" + imgDscp + "','"
						+ imgType + "')";

			}
			Statement statement = con.createStatement();
			statement.executeUpdate(sqlCode);
			sqlCode = "select max(image_id) from image";
			ResultSet result = statement.executeQuery(sqlCode);
			if (result.next())
			{
				return result.getString(1);
			}
		}
		catch (SQLException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		return null;
	}
	
	/**
	 * 检查工程图的名称是否已经被使用
	 * @param name 工程图名称
	 * @return 返回检查的结果，已经被使用的话返回true，否则返回false
	 */
	public boolean hasNamBefore(String name){
		if (con == null) {
			return false;
		}
		
		try {
			con.setAutoCommit(true);
			String sqlCode = "select count(*) from image where image_name=?";
			PreparedStatement sta = con.prepareStatement(sqlCode);
			sta.setString(1, name);
			ResultSet result = sta.executeQuery();
			if(result.next()){
				if(result.getInt(1)>0){
					return true;
				}
			}
			return false;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * 编辑工程图信息
	 * @param id 工程图id
	 * @param name 工程图名称
	 * @param dscp 工程图描述
	 * @return 编辑的结果，成功编辑返回1，失败返回0
	 */
	public String editImgInfo(String id, String name, String dscp) {
		if (con == null) {
			return "0";
		}
		try {
			con.setAutoCommit(true);
			String sqlCode = "update image set image_name='" + name
					+ "',image_des='" + dscp + "' where image_id='" + id + "'";
			Statement statement = con.createStatement();
			statement.executeUpdate(sqlCode);
			return "1";
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "0";
		}
	}

	/**
	 * 董力书写
	 * @param imgsrc
	 * @param imgdist
	 * @param widthdist
	 * @param heightdist
	 */
	public static void reduceImg(String imgsrc, String imgdist, int widthdist,
			int heightdist) {
		try {
			File srcfile = new File(imgsrc);
			if (!srcfile.exists()) {
				return;
			}
			Image src = ImageIO.read(srcfile);

			BufferedImage tag = new BufferedImage((int) widthdist,
					(int) heightdist, BufferedImage.TYPE_INT_RGB);

			tag.getGraphics().drawImage(
					src.getScaledInstance(widthdist, heightdist,
							Image.SCALE_SMOOTH), 0, 0, null);

			FileOutputStream out = new FileOutputStream(imgdist);
			JPEGImageEncoder encoder = JPEGCodec.createJPEGEncoder(out);
			encoder.encode(tag);
			out.close();

		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

	/**
	 * 董力书写
	 * @param imgF
	 * @param maxW
	 * @param maxH
	 * @return
	 */
	public static int autoZoom(String imgF, int maxW, int maxH) 
	{
		String fileName = GetPathStr.getPathWeb() + "imgUp/" + imgF;
		// String fileName = "D:/Program Files/" + "DUCG/imgUp/" + "27" +
		// ".jpg";
		// System.out.println(fileName);
		//maxW *= 1.05;
		//maxH *= 1.05;
		maxW = 4000;
		maxH = 4000;
		try
		{
			double Ratio = 0.0;
			File F = new File(fileName);
			File ThF = new File(fileName);

			if (!F.exists())
			{
				return 1;
			}
			cretThumb(imgF);
			BufferedImage Bi = ImageIO.read(F);

			// 假设图片宽 高 最大为5000 5000
			
			if ((Bi.getHeight() > maxH) || (Bi.getWidth() > maxW)) 
			{
				if (Bi.getHeight() > Bi.getWidth())
					Ratio = (float) (maxH) / Bi.getHeight();
				else
					Ratio = (float) (maxW) / Bi.getWidth();
			}
			else 
			{
				return 1;
			}
			Image Itemp = Bi.getScaledInstance(maxW, maxH, Bi.SCALE_REPLICATE);

			//System.out.println(44);2012-6-14

			AffineTransformOp op = new AffineTransformOp(AffineTransform
					.getScaleInstance(Ratio, Ratio), null);
			Itemp = op.filter(Bi, null);

			ImageIO.write((BufferedImage) Itemp, "jpg", ThF);
		} 
		catch (Exception ex) 
		{
			ex.printStackTrace();
		}
		return 1;
	}
	/**
	 * @param imgF
	 * @param maxW
	 * @param maxH
	 * @return
	 */
	public static int cretThumb(String imgF) 
	{
		String fileName = GetPathStr.getPathWeb() + "imgUp/" + imgF;
		String ThumbfilePath = GetPathStr.getPathWeb() + "imgUp/Thumb/";
		String ThumbfileName = GetPathStr.getPathWeb() + "imgUp/Thumb/" + imgF;
		int maxW = 200;
		int maxH = 200;
		try
		{
			double Ratio = 0.0;
			File F = new File(fileName);
			File ThF = new File(ThumbfilePath);

			if (!F.exists())
			{
				return 1;
			}
			if (!ThF.exists())
			{
				if(!ThF.mkdir())
				{
					throw new Exception("目录不存在，创建失败！");
				}
			}
			ThF = new File(ThumbfileName);
			if(!ThF.exists())
			{
				if(!ThF.createNewFile())
                {
                	throw new Exception("文件不存在，创建失败！");
                }
			}

			BufferedImage Bi = ImageIO.read(F);

			// 假设图片宽 高 最大为200 200
			
			if ((Bi.getHeight() > maxH) || (Bi.getWidth() > maxW)) 
			{
				if (Bi.getHeight() > Bi.getWidth())
					Ratio = (float) (maxH) / Bi.getHeight();
				else
					Ratio = (float) (maxW) / Bi.getWidth();
			}
			else 
			{
				return 1;
			}
			Image Itemp = Bi.getScaledInstance(maxW, maxH, Bi.SCALE_REPLICATE);

			//System.out.println(44);2012-6-14

			AffineTransformOp op = new AffineTransformOp(AffineTransform
					.getScaleInstance(Ratio, Ratio), null);
			Itemp = op.filter(Bi, null);

			ImageIO.write((BufferedImage) Itemp, "jpg", ThF);
		} 
		catch (Exception ex) 
		{
			ex.printStackTrace();
		}
		return 1;
	}
}
