package com.flex.demo;

import org.jdom.Document;
import org.jdom.Element;

public class Relation {
	
	public String name="";
	public String beginId;
	public String beginType;
	public String endId;
	public String endType;
	public String type="1";
	public String r;
	public String message;
	//bxs,2016.6
	public String bsp;
	public String esp;
	public JuZhen juZhen=new JuZhen();
	
	
	class JuZhen{
		int line=0;
		int arrange=0;
		String message="";
	}
	
	private String path="F:/shu/MySql/Xml/";
	
	public boolean writeRelation(){
		Element root = new Element("relation");
		root.setAttribute("type", type);
		Element basicInfo = new Element("basicInfo");
		Element rName = new Element("rName");
		rName.addContent(name);
		basicInfo.addContent(rName);
		Element rBeginId = new Element("rBeginId");
		rBeginId.addContent(beginId);
		basicInfo.addContent(rBeginId);
		Element rBeginType = new Element("rBeginType");
		rBeginId.addContent(beginType);
		basicInfo.addContent(rBeginType);
		Element rEndId = new Element("rEndId");
		rBeginId.addContent(endId);
		basicInfo.addContent(rEndId);
		Element rEndType = new Element("rEndType");
		rBeginId.addContent(endType);
		basicInfo.addContent(rEndType);
		Element rR = new Element("rR");
		rR.addContent(r);
		basicInfo.addContent(rR);
		Element rMessage = new Element("rMessage");
		rMessage.addContent(message);
		basicInfo.addContent(rMessage);
		Element rJuZhen = new Element("rJuZhen");
		rJuZhen.setAttribute("line", String.valueOf(juZhen.line));
		rJuZhen.setAttribute("arrange", String.valueOf(juZhen.arrange));
		Element jMessage = new Element("jMessage");
		jMessage.addContent(juZhen.message);
		rJuZhen.addContent(jMessage);
		basicInfo.addContent(rJuZhen);
		root.addContent(basicInfo);
		Document Doc = new Document(root);
		return DucgSql.WriteXMLDoc(Doc, path, name);
	}
	
}
