package com.flex.demo;

public class GetPathStr {
	public static String getPath() {
		String s = GetPathStr.class.getResource("").getPath().substring(1).replaceAll("%20", " ");
		if (s.charAt(s.length() - 1) == '/') {
			s += "../../../../../../../../";
		} else {
			s += "/../../../../../../../../";
		}
		
		//System.out.println(s);
		return (s);
		//return "C:/";
	}
}
