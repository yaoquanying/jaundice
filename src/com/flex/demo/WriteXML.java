package com.flex.demo;

import java.io.*;
import java.util.List;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.output.XMLOutputter;
public class WriteXML {
	private static Element root=new Element("ducg");
	private static int rn=1;
	private static gct.GetPathStr getpath;
	private static String pathout=getpath.getPathWeb();
	public void XMLinit(){
		root.removeChildren("*");
		root.removeAttribute("*");
		root=new Element("ducg");
		root.setAttribute("name","EditDUCG");
		rn=1;
		System.out.println("init xml service");
	}
	public void addcom(String name,int num,int state,int type,String pro,int x,int y,int cnum,float dval,int iscount,int cost){
		Element com=new Element("nodes");
		com.setAttribute("id","n"+num);
		com.setAttribute("name",name);

		if(type==1){
			com.setAttribute("type","B");
		}
		else if(type==2){
			com.setAttribute("type","X");
			com.setAttribute("dval",Float.toString(dval));
			com.setAttribute("iscount",Integer.toString(iscount));
			com.setAttribute("cost",Integer.toString(cost));
		}
		else if(type==3||type==9){
			com.setAttribute("type","G");
		}
		else if(type==4){
			com.setAttribute("type","D");
		}
		else if(type==5){
			com.setAttribute("type","B2");
		}
		else if(type==6) {
			com.setAttribute("type","X2");
		}/*else if(type==8) {
			com.setAttribute("type","S");
			
		}*/
		//重新修改过的类型，当目标为S时，类型也为X，但到时候加一个特殊标记
		else if(type==8) {
			com.setAttribute("type","X");
			com.setAttribute("dval",Float.toString(dval));
			com.setAttribute("iscount",Integer.toString(iscount));
			com.setAttribute("cost",Integer.toString(cost));
			
		}
		
		if (name.startsWith("U")) {
			com.setAttribute("type","U");
		}
		
		com.setAttribute("states",""+state);
		if(type==1||type==5){
			com.setAttribute("probabilities",pro);
		}
		//重新设置属性，区分X和SX
		if(type==2){
			com.setAttribute("special","0");
		}
		if(type==8){
			com.setAttribute("special","1");
		}
		com.setAttribute("x",""+x);
		com.setAttribute("y",""+y);
		if(cnum>50000){
			com.setAttribute("cnum",""+(cnum-30000));
		}else{
			com.setAttribute("cnum",""+cnum);
		}
		
		root.addContent(com);
	}

	public void addX2father(int num,int father,float r,int arrnum)
	{
		try
		{
			List all=root.getChildren("nodes");
			for(int i=0;i<all.size();i++)
			{
				if(((Element)all.get(i)).getAttribute("type").getValue().equals("U"))
				{
					if(((Element)all.get(i)).getAttribute("id").getValue().equals("n"+num))
					{
						Element toadd=(Element)all.get(i);
						Element add=new Element("parents");
						add.setAttribute("id","p"+arrnum);
						add.setAttribute("sourceRef","n"+father);
						add.setAttribute("weight",""+r);
						add.setAttribute("matrixRef","m"+arrnum);
						toadd.addContent(add);
					}
				}
			}
		} catch (Exception exp) {
			exp.printStackTrace();
		}
	}
	
	public void addXfather(int num,int father,float r,int arrnum)
	{
		try
		{
			List all=root.getChildren("nodes");
			for(int i=0;i<all.size();i++)
			{
				if(((Element)all.get(i)).getAttribute("type").getValue().equals("X")&&((Element)all.get(i)).getAttribute("special").getValue().equals("0"))
				{
					if(((Element)all.get(i)).getAttribute("id").getValue().equals("n"+num))
					{
						Element toadd=(Element)all.get(i);
						Element add=new Element("parents");
						add.setAttribute("id","p"+arrnum);
						add.setAttribute("sourceRef","n"+father);
						add.setAttribute("weight",""+r);
						add.setAttribute("matrixRef","m"+arrnum);
						toadd.addContent(add);
					}
				}
			}
		} catch (Exception exp) {
			exp.printStackTrace();
		}
	}
	// bxs modify T add T father
	public void addTfather(int num,int father,float r,int arrnum)
	{
		try
		{
			List all=root.getChildren("nodes");
			for(int i=0;i<all.size();i++)
			{
				if(((Element)all.get(i)).getAttribute("type").getValue().equals("X")&&((Element)all.get(i)).getAttribute("special").getValue().equals("1"))
				{
					if(((Element)all.get(i)).getAttribute("id").getValue().equals("n"+num))
					{
						Element toadd=(Element)all.get(i);
						Element add=new Element("parents");
						add.setAttribute("id","p"+arrnum);
						add.setAttribute("sourceRef","n"+father);
						add.setAttribute("weight",""+r);
						add.setAttribute("matrixRef","m"+arrnum);
						toadd.addContent(add);
					}
				}
			}
		} catch (Exception exp) {
			exp.printStackTrace();
		}
	}
	
	
	
	
	public void addGfather(int num,int father,int arrnum)
	{
		List all=root.getChildren("nodes");
		for(int i=0;i<all.size();i++)
		{
			if(((Element)all.get(i)).getAttribute("type").getValue().equals("G"))
			{
				if(((Element)all.get(i)).getAttribute("id").getValue().equals("n"+num))
				{
					Element toadd=(Element)all.get(i);
					Element add=new Element("parents");
					add.setAttribute("id","p"+arrnum);
					add.setAttribute("sourceRef","n"+father);
					toadd.addContent(add);
				}
			}
		}
	}
	public void addGgate(int num,int sn,String ex)
	{
		List all=root.getChildren("nodes");
		List all2=root.getChildren("expression");
		int p=all2.size()+1;
		for(int i=0;i<all.size();i++)
		{
			if(((Element)all.get(i)).getAttribute("type").getValue().equals("G"))
			{
				if(((Element)all.get(i)).getAttribute("id").getValue().equals("n"+num))
				{
					Element toadd=(Element)all.get(i);
					List g=toadd.getChildren("gate");
					boolean finded=false;
					for(int q=0;q<g.size();q++)
					{
						if(((Element)g.get(q)).getAttribute("sn").getValue().equals(""+sn))
						{
							finded=true;
							((Element)g.get(q)).setAttribute("expressionRef","e"+p);
						}
					}
					if(finded==false)
					{
						Element add=new Element("gate");
						add.setAttribute("id","g"+p);
						add.setAttribute("expressionRef","e"+p);
						add.setAttribute("sn",""+sn);
						toadd.addContent(add);
					}
				}
			}
		}
		Element ad=new Element("expression");
		ad.setAttribute("id","e"+p);
		ad.setAttribute("content",ex);
		root.addContent(ad);
	}
	public void addGH(int num,int sn,String ex)
	{
		List all=root.getChildren("nodes");
		List all2=root.getChildren("expression");
		int p=all2.size()+1;
		for(int i=0;i<all.size();i++)
		{
			if(((Element)all.get(i)).getAttribute("type").getValue().equals("G"))
			{
				if(((Element)all.get(i)).getAttribute("id").getValue().equals("n"+num))
				{
					Element toadd=(Element)all.get(i);
					List g=toadd.getChildren("gate");
					boolean finded=false;
					for(int q=0;q<g.size();q++)
					{
						if(((Element)g.get(q)).getAttribute("sn").getValue().equals(""+sn))
						{
							finded=true;
							((Element)g.get(q)).setAttribute("HexpressionRef","e"+p);
						}
					}
					if(finded==false)
					{
						Element add=new Element("gate");
						add.setAttribute("id","g"+p);
						add.setAttribute("HexpressionRef","e"+p);
						add.setAttribute("sn",""+sn);
						toadd.addContent(add);
					}
				}
			}
		}
		Element ad=new Element("expression");
		ad.setAttribute("id","e"+p);
		ad.setAttribute("content",ex);
		root.addContent(ad);
	}
	public void addline(int num,int type,int father,int son,String con,float r)
	{
		Element com=new Element("flows");
		com.setAttribute("id","f"+num);
		com.setAttribute("type",""+type);
		com.setAttribute("sourceRef","n"+father);
		com.setAttribute("targetRef","n"+son);
		com.setAttribute("weight",""+r);
		if(type==2)
		{
			List all2=root.getChildren("expression");
			int p=all2.size()+1;
			com.setAttribute("expressionRef","e"+p);
			Element ad=new Element("expression");
			ad.setAttribute("id","e"+p);
			ad.setAttribute("content",con);
			root.addContent(ad);
		}
		root.addContent(com);
	}
	public void addma(int father,int son,String ma,int num)
	{
		Element ad=new Element("matrix");
		ad.setAttribute("id","m"+num);
		int p,q;
		String[] arr=ma.split(",");
		for(p=0;p<son;p++)
		{
			Element add=new Element("rows");
			String addd=arr[p*father];
			add.setAttribute("id","r"+rn);
			rn++;
			add.setAttribute("state",""+p);
			for(q=1;q<father;q++)
			{
				addd=addd+" "+arr[p*father+q];
			}
			add.setAttribute("probabilities",addd);
			ad.addContent(add);
		}
		root.addContent(ad);
	}
	public boolean wrXML(int typ)
	{
		try
		{
			Document myDoc=new Document(root);
			if(typ==0)
			{
				File fileCrt = new File(pathout+"KBGraph//graph.xml");
				XMLOutputter outputter = new XMLOutputter();
				FileOutputStream writer = new FileOutputStream(fileCrt);
				outputter.output(myDoc, writer);
				writer.flush();
				writer.close();
			}
			if(typ==1)
			{
				File fileCrt1 = new File(pathout+"KBGraph//sav.xml");
				XMLOutputter outputter1 = new XMLOutputter();
				FileOutputStream writer1 = new FileOutputStream(fileCrt1);
				outputter1.output(myDoc, writer1);
				writer1.flush();
				writer1.close();
			}
			return true;
		}
		catch(Exception e){
			return false;
		}
	}
	public String getsav() throws IOException
	{
		String returnstr=new String();
		String strtmp=new String();
		returnstr="";
		FileInputStream fileinstream = new FileInputStream(pathout+"KBGraph//sav.xml");
		InputStreamReader inputreader = new InputStreamReader(fileinstream);
		BufferedReader bufreader=new BufferedReader(inputreader);
		while((strtmp=bufreader.readLine())!=null)
		{
			returnstr=returnstr+strtmp;
		}
		inputreader.close();
		return returnstr;
	}
}
