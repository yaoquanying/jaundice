package com.flex.demo;

import java.io.File;
import java.io.FileOutputStream;
import java.util.List;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;

public class Xml {
	private static Element getElementForAttr(String name, String[] keyfield,
			String[] key) {
		Element item = new Element(name);
		if (keyfield != null && keyfield.length > 0) {
			for (int i = 0; i < keyfield.length; i++) {
				item.setAttribute(keyfield[i], key[i]);
			}
		}
		return item;
	}

	/*
	 * 输出到指定的xml文件
	 */
	public static long WriteXMLDoc(Document Doc, String filepath,String filename) {
		try {
			Format format = Format.getCompactFormat();
			format.setEncoding("UTF-8");
			format.setIndent("    "); // 缩进4个空格后换行
			XMLOutputter XMLOut = new XMLOutputter(format);
			// 输出 XML 文件；
			File file = new File(filepath);
			boolean s = false;
			if (file.exists() == false) {
				s = file.mkdirs();
			}
			FileOutputStream fops = new FileOutputStream(filepath + filename);
			XMLOut.output(Doc, fops);
			fops.close();
			return 0;
		} catch (Exception e) {
			e.printStackTrace();
			return -1;
		}
	}

}
