package com.flex.demo;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import org.jdom.Document;
import org.jdom.Element;

public class Graph {
	
	public ArrayList<FakeComponent> component = new ArrayList<FakeComponent>();
	public ArrayList<Relation> relation = new ArrayList<Relation>();
	public String name;
	public String user;
	//bxs，修改graph函数，增加输出sp属性 2016.18
	Graph(String name){
		this.name=name;
		Connection con= DucgSql.con;
		Statement statement = DucgSql.statement;
		if (con == null) {
			return;
		}
		try {
			con.setAutoCommit(true);
			String sqlCode = "SELECT us_id FROM graph WHERE graph_id='"+name+"'";
			ResultSet result = statement.executeQuery(sqlCode);
			if(result.next())
				this.user = result.getString(1);
			
			sqlCode = "select com_id,com_type,glocation_x,glocation_y,sp from comgraph where graph_id='"+name+"'";
			result = statement.executeQuery(sqlCode);
			while(result.next()){
				FakeComponent temp= new FakeComponent();
				temp.id=result.getString(1);
				temp.type=result.getString(2);
				temp.locationX=result.getString(3);
				temp.locationY=result.getString(4);
				temp.sp=result.getString(5);
			//	System.out.println(temp.sp);
				component.add(temp);
			}
			
			sqlCode = "select rel_name from relgraph where graph_id='"+name+"'";
			result=statement.executeQuery(sqlCode);
			ArrayList<String> rel = new ArrayList<String>();
			while(result.next()){
				rel.add(result.getString(1));
			}
			for(String temp:rel){
				sqlCode ="select rel_begin_id,rel_begin_type,rel_end_id,rel_end_type,rel_type,rel_r,rel_message,bsp,esp from relation where rel_name='"+temp+"'";
				//System.out.println(sqlCode);
				result = statement.executeQuery(sqlCode);
				if(result.next()){
					Relation tempRel = new Relation();
					tempRel.name=temp;
					tempRel.beginId=result.getString(1);
					tempRel.beginType=result.getString(2);
					tempRel.endId=result.getString(3);
					tempRel.endType=result.getString(4);
					tempRel.type=result.getString(5);
					tempRel.r=result.getString(6);
					tempRel.message=result.getString(7);
					tempRel.bsp=Integer.toString(result.getInt(8));
					tempRel.esp=Integer.toString(result.getInt(9));
					//System.out.println(tempRel.esp);
					sqlCode="select line,arrange,message from juzhen where rel_name='"+temp+"'";
					//System.out.println(sqlCode);
					result = statement.executeQuery(sqlCode);
					if(result.next()){
						tempRel.juZhen.line=result.getInt(1);
						tempRel.juZhen.arrange=result.getInt(2);
						tempRel.juZhen.message=result.getString(3);
					}
					relation.add(tempRel);
				}
			}
			this.writeGraph();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	
	
	private String path="F:/shu/MySql/Xml/";
	class FakeComponent{
		public String id="";
		public String type="";
		public String locationX;
		public String locationY;
		public String sp;
	}
	
	public Element writeGraph(){
		
		Element root = new Element("graph");
		root.setAttribute("name",name );
		root.setAttribute("user",user );
		root.setAttribute("comNum", String.valueOf(component.size()));
		root.setAttribute("relNum", String.valueOf(relation.size()));
		
		Element comS=new Element("components");
		
		for(FakeComponent temp:component){
			Element comTemp = new Element("component");
			Element type= new Element("comType");
			type.addContent(temp.type);
			comTemp.addContent(type);
			Element id = new Element("comId");
			id.addContent(temp.id);
			comTemp.addContent(id);
			Element locationX= new Element("comLocationX");
			locationX.addContent(temp.locationX);
			comTemp.addContent(locationX);
			Element locationY= new Element("comLocationY");
			locationY.addContent(temp.locationY);
			comTemp.addContent(locationY);
			//bxs
			Element sp= new Element("sp");
			sp.addContent(temp.sp);
			comTemp.addContent(sp);
			
			comS.addContent(comTemp);
		}
		
		root.addContent(comS);
		
		Element relS=new Element("relations");
		
		for(Relation temp:relation){
			Element rootTemp = new Element("relation");
			rootTemp.setAttribute("type", temp.type);
			Element basicInfo = new Element("basicInfo");
			
			Element rName = new Element("rName");
			rName.addContent(temp.name);
			basicInfo.addContent(rName);
			
			//bxs
			Element bsp = new Element("bsp");
			bsp.addContent(temp.bsp);
			basicInfo.addContent(bsp);
			
			Element esp = new Element("esp");
			esp.addContent(temp.esp);
			basicInfo.addContent(esp);
			
			//
			Element rBeginId = new Element("rBeginId");
			rBeginId.addContent(temp.beginId);
			basicInfo.addContent(rBeginId);
			Element rBeginType = new Element("rBeginType");
			rBeginType.addContent(temp.beginType);
			basicInfo.addContent(rBeginType);
			Element rEndId = new Element("rEndId");
			rEndId.addContent(temp.endId);
			basicInfo.addContent(rEndId);
			Element rEndType = new Element("rEndType");
			rEndType.addContent(temp.endType);
			basicInfo.addContent(rEndType);
			Element rR = new Element("rR");
			rR.addContent(temp.r);
			basicInfo.addContent(rR);
			Element rMessage = new Element("rMessage");
			rMessage.addContent(temp.message);
			basicInfo.addContent(rMessage);
			Element rJuZhen = new Element("rJuZhen");
			rJuZhen.setAttribute("line", String.valueOf(temp.juZhen.line));
			rJuZhen.setAttribute("arrange", String.valueOf(temp.juZhen.arrange));
			Element jMessage = new Element("jMessage");
			jMessage.addContent(temp.juZhen.message);
			rJuZhen.addContent(jMessage);
			basicInfo.addContent(rJuZhen);
			rootTemp.addContent(basicInfo);
			relS.addContent(rootTemp);
		}
		
		root.addContent(relS);
		return root;
	}
}
