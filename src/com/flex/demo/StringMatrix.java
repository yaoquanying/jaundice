package com.flex.demo;

import java.util.ArrayList;;

public class StringMatrix
{
	public ArrayList<ArrayList<String>> mData;
	public int linesCount;
	public int columsCount;
	
	public StringMatrix(int lines, int colums)
	{
		mData = new ArrayList<ArrayList<String>>(lines);
		for(int i = 0 ; i < lines; i++){
			mData.add(new ArrayList<String>(colums));
			for(int j = 0 ; j < colums; j ++)
				mData.get(i).add("NaN");
		}
		this.linesCount = lines;
		this.columsCount = colums;
	}
	
	public void setCellAt(int line_index,int column_index, String data){
		mData.get(line_index).set(column_index, data);
	}
	
	public String getCellAt(int line_index,int column_index){
		return mData.get(line_index).get(column_index);
	}
	
	public void addNewLine(int line_index){
		mData.add(new ArrayList<String>(columsCount));
		for(int i = mData.size() ; i > line_index ; i--){
			mData.set(i, mData.get(i-1));
		}
		
		mData.set(line_index,  new ArrayList<String>(columsCount));
		for(int i = 0 ; i < columsCount; i++)
			mData.get(line_index).set(i, "NaN");
		
		linesCount ++;
	}
	
	public void removeLineAt(int line_index){
		for(int i = line_index; i < linesCount-1; i++)
			mData.set(i, mData.get(i+1));
		
		mData.remove(linesCount -1);
		
		linesCount --;
	}
	
	public void addNewColumn(int column_index){
		for(int i = 0 ; i < linesCount; i++){
			ArrayList<String> line = mData.get(i);
			for(int j = columsCount; j > column_index; j--)				
				line.set(j, line.get(j-1));
			line.set(column_index, "NaN");
		}
		
		columsCount ++;
	}
	
	public void removeColumn(int column_index){
		for(int i = 0 ; i < linesCount; i++){
			ArrayList<String> line = mData.get(i);
			for(int j = column_index; j < columsCount -1; j++)
				line.set(j, line.get(j+1));
			line.remove(columsCount - 1);
		}
		
		columsCount --;
	}
	
	public String toString(){
		StringBuilder buf = new StringBuilder();
		for(int i = 0 ; i < linesCount ; i++)
			for(int j = 0 ; j < columsCount ; j++)
				buf.append(mData.get(i).get(j)).append(',');
		return buf.toString();
	}
}