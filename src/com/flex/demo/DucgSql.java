﻿package com.flex.demo;

import gct.GetPathStr;
import gct.userVal.UtilsSQL;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;

import org.apache.xalan.templates.VarNameCollector;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;

import db.config.DbConfig;


public class DucgSql {

	public static Connection con;
	public static Statement statement;
	private static gct.GetPathStr getpath = new gct.GetPathStr();
	private static String pathout = getpath.getPathWeb();

	/**
	 * дXML
	 * @param Doc
	 * @param filepath
	 * @param filename
	 * @return
	 */
	public static boolean WriteXMLDoc(Document Doc, String filepath,String filename) {
		try {
			Format format = Format.getCompactFormat();
			format.setEncoding("UTF-8");
			format.setIndent("    "); // ����4���ո����
			XMLOutputter XMLOut = new XMLOutputter(format);
			// ��� XML �ļ���
			File file = new File(filepath);
			boolean s = false;
			if (file.exists() == false) {
				s = file.mkdirs();
			}
			FileOutputStream fops = new FileOutputStream(filepath + filename);
			XMLOut.output(Doc, fops);
			fops.close();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	
	
	/**
	 * ������ݿ�
	 * 
	 * @return
	 */
	public boolean connection() {
		//String dbName = "jaundice";
		try {
			Class.forName("com.mysql.jdbc.Driver");
			// �������Դ
			String url = "jdbc:mysql://"+DbConfig.host+"/" + DbConfig.dbname
					+ "?useUnicode=true&characterEncoding=UTF-8";
			con = DriverManager.getConnection(url, DbConfig.usname, DbConfig.dbpwd);
			statement = con.createStatement();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	/*
	 * bxs 2016.9
	 * 读取双线G，存入DG.txt
	 */
	public String getDG(){
		
		String str="";//
		File file=new File(GetPathStr.getPathWeb() + "KBGraph\\DG.txt");
		
		if(file.exists()){
			try {
				FileReader fileR=new FileReader(file);
				BufferedReader br = new BufferedReader(fileR);//构造一个BufferedReader类来读取文件
	            String s = null;
	            while((s = br.readLine())!=null){//使用readLine方法，一次读一行
	               str+=s;
	            }
	            br.close();
	            fileR.close();
//	            System.out.println(str + "----------");
			//	while(){}
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return str;
	}
	public int saveG(){
		if (con == null) {
			return 0;
		}
		
		String temp="";
		try {
			
			con.setAutoCommit(true);
			String sql="select COM_ID from com where  COM_TYPE='G' and sp=1";
			ResultSet result = statement.executeQuery(sql);
			while(result.next()){
				temp=temp+result.getInt(1)+",";
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//
		File file = new File(GetPathStr.getPathWeb() + "KBGraph\\DG.txt");
		if(!file.exists()){
			try {
				file.createNewFile();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		try {
			FileWriter fw = new FileWriter(file);
			   BufferedWriter bw = new BufferedWriter(fw);
			   bw.write(temp);
			   bw.close();
			   fw.close();
			   
		
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return 1;
	}
	/*public static void main(String arg[]){
		DucgSql  ds=new DucgSql();
		ds.connection();
		//ds.saveG();
		ds.getDG();
	}*/
	/**
	 * �����ͨ�û���¼
	 * 
	 * @param id
	 * @param password
	 * @return
	 */
	public int checkCommonUser(String id, String password) {
		if (con == null) {
			return 1;
		}
		try {
			con.setAutoCommit(true);
			String selCode = "SELECT US_PASSWORD FROM US WHERE US_ID='" + id+ "'";
			ResultSet result = statement.executeQuery(selCode);// ���
			if (result.next()) {
				if (result.getString(1).equals(password)) {
					return 0;
				} else {
					return 2;
				}
			} else {
				return 1;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			return 1;
		}
	}

	/**
	 * �����ͨ�û�
	 * 
	 * @param id
	 * @param password
	 * @return
	 */
	public int addCommonUser(String id, String password) {
		if (con == null) {
			return 0;
		}
		try {
			con.setAutoCommit(true);
			String selCode = "INSERT INTO US(US_ID,US_PASSWORD,US_TYPE) VALUES ('"+ id + "','" + password + "','1')";
			int result = statement.executeUpdate(selCode);
			if (result == 0) {
				return 0;
			}
			return 1;

		} catch (SQLException e) {
			return 0;
		}
	}

	/**
	 * �õ������û����û���
	 * 
	 * @return
	 */
	public String[] getAllUser() {
		if (con == null) {
			return null;
		}
		try {
			con.setAutoCommit(true);
			String sqlCode = "select US_ID from US";
			String array = "";
			ResultSet result = statement.executeQuery(sqlCode);
			while (result.next()) {
				array += result.getString(1) + "\t";
			}
			return array.split("\t");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	public void deluserinsql(String user)
	{
		if(con==null){
			return;
		}
		try {
			con.setAutoCommit(true);
			String sqlCode = "delete from US where us_id='"+user+"'";
			System.out.println(sqlCode);
			statement.executeUpdate(sqlCode);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public int deluser(String user)
	{
		int result= 1;;
		if(con==null){
			return 1;
		}
		
		try {
			con.setAutoCommit(true);
			String sqlCode ="select * from graph where us_id=?";
			PreparedStatement state = con.prepareStatement(sqlCode);
			state.setString(1, user);
			ResultSet re= state.executeQuery();
			if(re.next()){
				return 0;
			}
			else{
				return 1;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}
	public int changePassWord(String id, String password) {
		if (con == null) {
			return 0;
		}
		try {
			con.setAutoCommit(true);
			String sqlCode = "UPDATE US SET US_PASSWORD='" + password+ "'WHERE US_ID='" + id + "'";
			int result = statement.executeUpdate(sqlCode);
			if (result == 0) {
				return 0;
			}
			return 1;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			return 0;
		}
	}
	
	public boolean haveGraph(String graphName){
		
		if (con == null) {
			return false;
		}
		try {
			con.setAutoCommit(true);
			String sqlCode = "select * from graph where graph_id='"+graphName+"'";
			ResultSet result = statement.executeQuery(sqlCode);
			if (result.next()) {
				return true;
			}
			return false;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			return false;
		}
		
	}
	
	public void addUserGraph(String id,String graph){
		if (con == null) {
			return;
		}
		try {
			con.setAutoCommit(true);
			String sqlCode = "insert into graph(graph_id,us_id) values('"+graph+"','"+id+"')";
			statement.executeUpdate(sqlCode);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public boolean checkUserGraphName(String id, String graph){
		if (con == null) {
			return true;
		}
		try {
			con.setAutoCommit(true);
			String sql = "SELECT * FROM graph WHERE graph_id='" + graph + "'";
			ResultSet rs = statement.executeQuery(sql);
			return rs.next();			
		} catch (SQLException e) {
			e.printStackTrace();
			return true;
		}
	}
	
	public boolean renameUserGraph(String id, String oldName, String newName){
		if (con == null) {
			return false;
		}
		try {
			con.setAutoCommit(true);
			PreparedStatement stm = con.prepareStatement("UPDATE graph SET graph_id=? WHERE us_id=? AND graph_id=?");
			stm.setString(1, newName);
			stm.setString(2, id);
			stm.setString(3, oldName);
			int count = stm.executeUpdate();
			
			if(count == 1){
				stm = con.prepareStatement("UPDATE comgraph SET graph_id=? WHERE graph_id=?");
				stm.setString(1, newName);
				stm.setString(2, oldName);
				stm.executeUpdate();
				stm = con.prepareStatement("UPDATE relgraph SET graph_id=? WHERE graph_id=?");
				stm.setString(1, newName);
				stm.setString(2, oldName);
				stm.executeUpdate();
			}			
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public void addD(int number){
		if (con == null) {
			return;
		}
		try {
			con.setAutoCommit(true);
		
				String sqlCode = "insert into com(com_id,com_type,com_name) values('"+number+"','D','D"+number+"')";
				statement.executeUpdate(sqlCode);
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	
	
	
	public void addG(int number,int sp){
		if (con == null) {
			return;
		}
		try {
			con.setAutoCommit(true);
			String sqlCode = "insert into com(com_id,com_type,sp) values('"+number+"','G','"+sp+"')";
			System.out.print(sqlCode);
			statement.executeUpdate(sqlCode);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public boolean saveG(int number,String name,String info,int length,String[] state){
		if (con == null) {
			return false;
		}
		try {
			con.setAutoCommit(false);
			String sqlCode = "delete from state where com_type='G' and com_id='"+number+"'";
			statement.executeUpdate(sqlCode);
			sqlCode = "update com set com_name='G"+number+"',com_des='"+info+"'"+"where com_type='G' and com_id='"+number+"'";
			statement.execute(sqlCode);
			for(int i=0;i<length;i++){
				sqlCode="insert into state(state_id,com_id,com_type,state_des) values('"+i+"','"+number+"','G','"+state[i]+"')";
				statement.execute(sqlCode);
			}
			con.commit();
			return true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			try {
				con.rollback();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			return false;
		}
	}
	public int canDeleteG(String id,String type){
		int result= 1;;
		if(con==null){
			return 1;
		}
		
		try {
			con.setAutoCommit(true);
			String sqlCode ="select * from comgraph where com_id=? and com_type=?";
			PreparedStatement state = con.prepareStatement(sqlCode);
			state.setString(1, id);
			state.setString(2, type);
			ResultSet re= state.executeQuery();
			if(re.next()){
				return 0;
			}
			else{
				return 1;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}
	
	public int canDeletegraph(String graph){
		int result= 1;
		if(con==null){
			return 1;
		}
		
		try {
			con.setAutoCommit(true);
			String sqlCode ="select * from comgraph where graph_id=?";
			PreparedStatement state = con.prepareStatement(sqlCode);
			state.setString(1, graph);
			ResultSet re= state.executeQuery();
			if(re.next()){
				return 0;
			}
			else{
				return 1;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}
	public void delgraph(String graph){
		if(con==null){
			return;
		}
		try {
			con.setAutoCommit(true);
			String sqlCode = "delete from graph where graph_id='"+graph+"'";
			System.out.println(sqlCode);
			statement.executeUpdate(sqlCode);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	/**
	 * ��һ��ͼ�����һ��������type��B��X��G��D֮һ,graph��ͼ�����֣�number�Ǳ������,x�Ǻ���꣬y�������
	 * @param graph
	 * @param type
	 * @param number
	 * @param x
	 * @param y
	 */
	public void addComOnGraph(String graph,String type,int number,int x,int y,int sp){
	//	System.out.print(number+" "+type);
		if(con==null){
			return;
		}
		try {
			//
			con.setAutoCommit(true);
			String sqlCode = "insert into comgraph(com_id,com_type,graph_id,glocation_x,glocation_y,sp) values('"+number+"','"+type+"','"+graph+"','"+x+"','"+y+"','"+sp+"')";
			
			statement.executeUpdate(sqlCode);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * 向数据库中增加一条新线
	 * @param graph
	 * @param typestart
	 * @param numstart
	 * @param typeend
	 * @param numend
	 * @param type
	 */
	
	//bxs,2016.6 add to properties esp bsp to discriminate x and sx
	public String addLineOnGraph(String graph,String typestart,int numstart,String typeend,int numend,int type,int bsp,int esp){
		if(con==null){
			return "-1";
		}
		int result = 0;
		try {
			con.setAutoCommit(false);
			String sqlCode = "insert into relation(rel_begin_type,rel_begin_id,rel_end_type,rel_end_id,rel_type,bsp,esp) " +
					"values('"+typestart+"','"+numstart+"','"+typeend+"','"+numend+"','"+type+"','"+bsp+"','"+esp+"')";
			//System.out.println(sqlCode);
			statement.executeUpdate(sqlCode);
			sqlCode = "select max(rel_name) from relation"; 
			ResultSet temp = statement.executeQuery(sqlCode);
			if(temp.next()){
				result=temp.getInt(1);
			}
			sqlCode="insert into relgraph(rel_name,graph_id) values ('"+result+"','"+graph+"')";
			statement.executeUpdate(sqlCode);
			con.commit();
			
		} catch (SQLException e) {
			e.printStackTrace();
			try {
				con.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			return "-1";
		}
		return ""+result+","+typestart+","+numstart+","+typeend+","+numend+","+graph+","+bsp+","+esp;
	}
	
	public String getRelationById(String id,int wri){
		if(con==null){
			return null;
		}
		Element root = new Element("rel");
		root.setAttribute("wri",""+wri);
		try {
			con.setAutoCommit(true);
			String sqlCode ="select rel_begin_id,rel_begin_type,rel_end_id,rel_end_type" +
					",rel_type,rel_r,rel_message from relation where rel_name='"+id+"'";
			Statement sta = con.createStatement();
			ResultSet result = sta.executeQuery(sqlCode);
			if(result.next()){
				Element begin_id = new Element("begin_id");
				begin_id.addContent(result.getString(1));
				root.addContent(begin_id);
				
				Element begin_type= new Element("begin_type");
				begin_type.addContent(result.getString(2));
				root.addContent(begin_type);
				
				Element end_id = new Element("end_id");
				end_id.addContent(result.getString(3));
				root.addContent(end_id);
				
				Element end_type = new Element("end_type");
				end_type.addContent(result.getString(4));
				root.addContent(end_type);
				
				Element type = new Element("type");
				type.addContent(result.getString(5));
				root.addContent(type);
				
				Element r = new Element("r");
				r.addContent(result.getString(6));
				root.addContent(r);
				
				Element message = new Element("message");
				message.addContent(result.getString(7));
				root.addContent(message);
				
			}
			
			sqlCode = "select line,arrange,message from juzhen where rel_name='"+id+"'";
			result = sta.executeQuery(sqlCode);
			if(result.next()){
				Element line = new Element("line");
				line.addContent(result.getString(1));
				root.addContent(line);
				
				Element arrange = new Element("arrange");
				arrange.addContent(result.getString(2));
				root.addContent(arrange);
				
				Element juzhenMsg = new Element("juzhenMsg");
				juzhenMsg.addContent(result.getString(3));
				root.addContent(juzhenMsg);
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		XMLOutputter outputter = new XMLOutputter();
		return outputter.outputString(new Document(root));
	}

	
	public String addexistlineingraph(int num,String graph)
	{
		String typestart = "";
		String numstart = "";
		String typeend = "";
		String numend = "";
		if(con==null){
			return "-1";
		}
		try {
			con.setAutoCommit(true);
			String sqlCode ="insert into relgraph(rel_name,graph_id) values ('"+num+"','"+graph+"')";
			//System.out.println(sqlCode);
			statement.executeUpdate(sqlCode);
			sqlCode = "select rel_begin_type,rel_begin_id,rel_end_type,rel_end_id from relation where rel_name = '" + num + "'";
			ResultSet rs = statement.executeQuery(sqlCode);
			if(rs.next())
			{
				typestart = rs.getString(1);
				numstart = rs.getString(2);
				typeend = rs.getString(3);
				numend = rs.getString(4);
			}
			rs.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "-1";
		}
		return ""+num+","+typestart+","+numstart+","+typeend+","+numend+","+graph;
	}
	public String hasRelationBefore2(String beginId,String beginType,String endId,String endType,int writenum){
		String result= "1";
		if(con==null){
			return "1";
		}
		
		try {
			con.setAutoCommit(true);
			String sqlCode ="select * from relation where rel_begin_id=? and rel_begin_type=?" +
					" and rel_end_id=? and rel_end_type=?";
			PreparedStatement state = con.prepareStatement(sqlCode);
			state.setString(1, beginId);
			state.setString(2, beginType);
			state.setString(3, endId);
			state.setString(4, endType);
			ResultSet re= state.executeQuery();
			if(re.next()){
				return "1,"+writenum+","+re.getInt(1);
			}
			else{
				return "-1,"+writenum;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}
	
	public int hasRelationBefore(String beginId,String beginType,String endId,String endType){
		int result= 1;;
		if(con==null){
			return 1;
		}
		
		try {
			con.setAutoCommit(true);
			String sqlCode ="select * from relation where rel_begin_id=? and rel_begin_type=?" +
					" and rel_end_id=? and rel_end_type=?";
			PreparedStatement state = con.prepareStatement(sqlCode);
			state.setString(1, beginId);
			state.setString(2, beginType);
			state.setString(3, endId);
			state.setString(4, endType);
			ResultSet re= state.executeQuery();
			if(re.next()){
				return re.getInt(1);
			}
			else{
				return -1;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}
	/**
	 * �ı����������x����y���
	 * @param graph
	 * @param type
	 * @param number
	 * @param x
	 * @param y
	 */
	public void saveComOnGraph(String graph,String type,int number,int x,int y){
		if(con==null){
			return;
		}
		try {
			con.setAutoCommit(true);
			String sqlCode = "update comgraph set glocation_x='"+x+"', glocation_y='"+y+"'where com_id='"+number+"' and com_type='"+type+"' and graph_id='"+graph+"'";
			statement.executeUpdate(sqlCode);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * ��һ��ͼ��ı�һ���ߵ����
	 * @param number
	 * @param r
	 * @param condition
	 * @param arr
	 */
	public void saveLine(int number,double r,String condition,String arr){
		if(con==null){
			return;
		}
		try {
			con.setAutoCommit(false);
			String sqlCode = "delete from juzhen where rel_name='"+number+"'";
			statement.executeUpdate(sqlCode);
			
			sqlCode = "insert into juzhen(rel_name,line,arrange,message)values('"+number+"','5','5','"+arr+"')";
			statement.executeUpdate(sqlCode);
			
			sqlCode = "update relation set rel_r='"+r+"', rel_message='"+condition+"' where rel_name='"+number+"'";
			statement.executeUpdate(sqlCode);
			con.commit();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			try {
				con.rollback();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	}

	public boolean updateLine(int id, double range, int line_count, int column_count, String condition, String data){
		if(con==null){
			return false;
		}
		try {
			con.setAutoCommit(false);
			
			String sqlCode = "delete from juzhen where rel_name='" + id + "'";
			statement.executeUpdate(sqlCode);
			
			sqlCode = "insert into juzhen(rel_name,line,arrange,message)values('"+id+"','"+line_count+"','"+column_count+"','"+data+"')";
			statement.executeUpdate(sqlCode);
			
			sqlCode = "update relation set rel_r='"+range+"', rel_message='"+condition+"' where rel_name='"+id+"'";
			statement.executeUpdate(sqlCode);
			con.commit();
			//con.setAutoCommit(true);
			//System.out.println("Line1 weight="+range);
			//System.out.println("Line1 updated: ID="+id+" Data="+data+" DIM="+line_count+"*"+column_count);
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
			try {
				con.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			return false;
		}
	}
	
	public void updatePartialLine(int id, int line_count, int column_count, String data){
		if(con==null){
			return;
		}
		try {
			con.setAutoCommit(false);
			String sqlCode = "delete from juzhen where rel_name='"+id+"'";
			statement.executeUpdate(sqlCode);
			sqlCode = "insert into juzhen(rel_name,line,arrange,message)values('"+id+"','"+line_count+"','"+column_count+"','"+data+"')";
			statement.executeUpdate(sqlCode);
			//sqlCode = "update relation set rel_r='"+range+"', rel_message='"+condition+"' where rel_name='"+id+"'";
			//statement.executeUpdate(sqlCode);
			con.commit();
			//System.out.println("Line2 updated: ID="+id+" Data="+data+" DIM="+line_count+"*"+column_count);
		} catch (SQLException e) {
			e.printStackTrace();
			try {
				con.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}
	}
	
	
	public boolean lineDataVersionHugeUpdate(){		
		if(con != null){
			try {
				con.setAutoCommit(true);
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			System.out.println("!!!  Huge Data Version Updating Begining...");
			try{
				PreparedStatement stmt = con.prepareStatement("SHOW TABLES LIKE 'line_matrix_updated'");
				ResultSet result = stmt.executeQuery();
				
				if(result.next()){
					System.out.println("Data storage had been updaetd before.");
				}
				else{
					stmt = con.prepareStatement("CREATE TABLE line_matrix_updated (nonsense int)");
					stmt.execute();
					
					stmt = con.prepareStatement("select * from relation");
					result = stmt.executeQuery();
					int count = 0;
					while(result.next()){
						System.out.print("Updating Line:"+(++count));
						
						int beginId = result.getInt("REL_BEGIN_ID");
						String beginType = result.getString("REL_BEGIN_TYPE");
						int endId = result.getInt("REL_END_ID");
						String endType = result.getString("REL_END_TYPE");
						Component begin = new Component(Integer.toString(beginId), beginType);
						Component end  = new Component(Integer.toString(endId), endType);
						int lineCount = end.getFullStatesCount(), columnCount = begin.getFullStatesCount();
						
						boolean parentG = beginType.equals("G"), childG = endType.equals("G");
						
						if(parentG)
							columnCount += 1;
						if(childG)
							lineCount += 1;
						
						if(beginType.equals("D"))
							columnCount = 1;
						if(endType.equals("D"))
							lineCount = 1;
						
						PreparedStatement lineSt = con.prepareStatement("SELECT * From juzhen WHERE REL_NAME=?");
						lineSt.setInt(1, result.getInt("REL_NAME"));
						ResultSet lineRS = lineSt.executeQuery();
						
						if(lineRS.next()){
							String raw = lineRS.getString("MESSAGE");
							System.out.println("RAW MSG: "+ raw);
							String[] data = new String[25];
							int index = 0;
							do{
								int comma = raw.indexOf(',');
								
								if(comma >= 0)
									data[index++] = raw.substring(0, comma);
								else if(raw.length() > 0){
									data[index++] = raw;
									raw = "";
								}
								
								if(comma < raw.length()-1)
									raw = raw.substring(comma+1);
								else
									raw = "";
							}while(!raw.equals(""));
							
							System.out.println(" DataArrayLen=" + index);
							System.out.print("\t DataArray:\t");
							for(int i = 0 ; i < index ; i++)
								System.out.print(data[i]+"\t");
							System.out.println();
							System.out.println(" lineCount="+lineCount+" ; columnCount="+columnCount);
							
							StringMatrix matrix = new StringMatrix(lineCount, columnCount);
							for(int i = 0 ; i < lineCount; i++)
								for(int j = 0 ; j < columnCount; j++){
									System.out.print("i="+i+",j="+j+"  ");
									if(5*i+j >= index)
										matrix.setCellAt(i, j, "NaN");
									else
										matrix.setCellAt(i,j, data[5*i + j]);
								}
							PreparedStatement upSt = con.prepareStatement("UPDATE juzhen SET LINE=?, ARRANGE=?, MESSAGE=? WHERE REL_NAME=?");
							upSt.setInt(1, lineCount);
							upSt.setInt(2, columnCount);
							upSt.setString(3, matrix.toString());
							upSt.setInt(4, result.getInt("REL_NAME"));
							upSt.execute();
							System.out.println(upSt.toString());
						}
					}
					System.out.println("!!!  Huge Data Version Updating Completed!");
					return true;
				}
			}
			catch(Exception e){
				e.printStackTrace();
				return false;
			}
		}
		return false;
	}
	
	
	public String getRelatedLines(int id, String type){
		try{
			con.setAutoCommit(true);
			PreparedStatement stmt = con.prepareStatement("select * from relation where REL_END_ID=? and REL_END_TYPE=?");
			stmt.setInt(1, id);
			stmt.setString(2, type);
			ResultSet result = stmt.executeQuery();
			
			Element in_lines = new Element("lineins");
			Element out_lines = new Element("lineouts");
			while(result.next()){
				int line_id = result.getInt("REL_NAME");
				
				PreparedStatement stmtline = con.prepareStatement("select * from juzhen where REL_NAME=?");
				stmtline.setInt(1, line_id);
				ResultSet lineResult = stmtline.executeQuery();
				if(lineResult.next()){
					Element line = new Element("line");
					line.setAttribute("id", Integer.toString(line_id));
					line.setAttribute("type", result.getString("REL_TYPE"));
					Element lineData = new Element("LineData");
					lineData.addContent(lineResult.getString("MESSAGE"));
					lineData.setAttribute("lines", lineResult.getString("LINE"));
					lineData.setAttribute("columns", lineResult.getString("ARRANGE"));
					
					
					Element rEndId = new Element("rEndId");
					rEndId.addContent(result.getString("REL_END_ID"));
					Element rBeginId = new Element("rBeginId");
					rBeginId.addContent(result.getString("REL_BEGIN_ID"));
					Element rEndType = new Element("rEndType");
					Element rBeginType = new Element("rBeginType");
					rEndType.addContent(result.getString("REL_END_TYPE"));
					rBeginType.addContent(result.getString("REL_BEGIN_TYPE"));
					Element bsp = new Element("bsp");
					bsp.addContent(Integer.toString(result.getInt("bsp")));
					Element esp = new Element("esp");
					esp.addContent(Integer.toString(result.getInt("esp")));
					line.addContent(bsp);
					line.addContent(esp);
					line.addContent(lineData);
					line.addContent(rBeginId);
					line.addContent(rBeginType);
					line.addContent(rEndId);
					line.addContent(rEndType);
					
					in_lines.addContent(line);
				}
			}
			
			stmt = con.prepareStatement("select * from relation where REL_BEGIN_ID=? and REL_BEGIN_TYPE=?");
			stmt.setInt(1, id);
			stmt.setString(2, type);
			result = stmt.executeQuery();
			while(result.next()){
				int line_id = result.getInt("REL_NAME");
				
				PreparedStatement stmtline = con.prepareStatement("select * from juzhen where REL_NAME=?");
				stmtline.setInt(1, line_id);
				ResultSet lineResult = stmtline.executeQuery();
				if(lineResult.next()){
					Element line = new Element("line");
					line.setAttribute("id", Integer.toString(line_id));
					
					Element lineData = new Element("LineData");
					lineData.addContent(lineResult.getString("MESSAGE"));
					lineData.setAttribute("lines", lineResult.getString("LINE"));
					lineData.setAttribute("columns", lineResult.getString("ARRANGE"));
					
					Element rEndId = new Element("rEndId");
					rEndId.addContent(result.getString("REL_END_ID"));
					Element rBeginId = new Element("rBeginId");
					rBeginId.addContent(result.getString("REL_BEGIN_ID"));
					Element rEndType = new Element("rEndType");
					Element rBeginType = new Element("rBeginType");
					rEndType.addContent(result.getString("REL_END_TYPE"));
					rBeginType.addContent(result.getString("REL_BEGIN_TYPE"));
					//
					/*Element espp = new Element("esp");
					espp.setAttribute("esp", Integer.toString(esp));
					Element bspp = new Element("bsp");
					bspp.setAttribute("bsp", Integer.toString(bsp));*/
					//
					Element bsp = new Element("bsp");
					bsp.addContent(Integer.toString(result.getInt("bsp")));
					Element esp = new Element("esp");
					esp.addContent(Integer.toString(result.getInt("esp")));
					line.addContent(bsp);
					line.addContent(esp);
					line.addContent(lineData);
					line.addContent(rBeginId);
					line.addContent(rBeginType);
					line.addContent(rEndId);
					line.addContent(rEndType);
					
					out_lines.addContent(line);
				}
			}
			
			Element root = new Element("RelatedLines");
			root.addContent(in_lines);
			root.addContent(out_lines);
			Document doc = new Document(root);
			XMLOutputter outputter = new XMLOutputter();
			return outputter.outputString(doc);
		}
		catch(SQLException e){
			e.printStackTrace();
			Element in_lines = new Element("lineins");
			Element out_lines = new Element("lineouts");
			Element root = new Element("RelatedLines");
			root.addContent(in_lines);
			root.addContent(out_lines);
			Document doc = new Document(root);
			XMLOutputter outputter = new XMLOutputter();
			return outputter.outputString(doc);
		}
	}
	
	public RelatedLines getRelatedLinesObject(int id, String type){
		try{
			con.setAutoCommit(false);
			PreparedStatement stmt = con.prepareStatement("select * from relation where REL_END_ID=? and REL_END_TYPE=?");
			stmt.setInt(1, id);
			stmt.setString(2, type);
			ResultSet result = stmt.executeQuery();
						
			RelatedLines related = new RelatedLines();
			
			while(result.next()){
				int line_id = result.getInt("REL_NAME");
				PreparedStatement stmtline = con.prepareStatement("select * from juzhen where REL_NAME=?");
				stmtline.setInt(1, line_id);
				ResultSet lineResult = stmtline.executeQuery();
				if(lineResult.next()){
					Line line = new Line(line_id, 
							Integer.parseInt(result.getString("REL_BEGIN_ID")),result.getString("REL_BEGIN_TYPE"), 
							Integer.parseInt(result.getString("REL_END_ID")), result.getString("REL_END_TYPE"));
					
					line.initMatrix(Integer.parseInt(lineResult.getString("LINE")), Integer.parseInt(lineResult.getString("ARRANGE")), 
							lineResult.getString("MESSAGE"));
					
					related.addInLine(line);
				}
			}
			
			stmt = con.prepareStatement("select * from relation where REL_BEGIN_ID=? and REL_BEGIN_TYPE=?");
			stmt.setInt(1, id);
			stmt.setString(2, type);
			result = stmt.executeQuery();
			while(result.next()){
				int line_id = result.getInt("REL_NAME");
				PreparedStatement stmtline = con.prepareStatement("select * from juzhen where REL_NAME=?");
				stmtline.setInt(1, line_id);
				ResultSet lineResult = stmtline.executeQuery();
				if(lineResult.next()){
					Line line = new Line(line_id, 
							Integer.parseInt(result.getString("REL_BEGIN_ID")),result.getString("REL_BEGIN_TYPE"), 
							Integer.parseInt(result.getString("REL_END_ID")), result.getString("REL_END_TYPE"));
					
					line.initMatrix(Integer.parseInt(lineResult.getString("LINE")), Integer.parseInt(lineResult.getString("ARRANGE")), 
							lineResult.getString("MESSAGE"));
					
					related.addOutLine(line);
				}
			}
			
			return related;
		}
		catch(SQLException e){
			e.printStackTrace();
			return new RelatedLines();
		}
	}

	
	public String delLinePd(int number)
	{
		String res="";
		if(con==null){
			return "";
		}
		try {
			con.setAutoCommit(true);
			String sqlCode ="select count(*) from relgraph where rel_name='"+number+"'";
			ResultSet re = statement.executeQuery(sqlCode);
			if(re.next()){
				res=""+number+","+re.getInt(1);
				return res;
			}
			else{
				return "";
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return res;
	}
	public void delLineongraph(int number,String graph)
	{
		if(con==null){
			return;
		}
		try {
			con.setAutoCommit(true);
			String sqlCode = "delete from relgraph where rel_name='"+number+"'and graph_id='"+graph+"'";
			System.out.println(sqlCode);
			statement.executeUpdate(sqlCode);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * ɾ��һ��ͼ���һ����
	 * @param number
	 */
	public void delLine(int number){
		if(con==null){
			return;
		}
		try {
			con.setAutoCommit(true);
			String sqlCode = "delete from relgraph where rel_name='"+number+"'";
			System.out.println(sqlCode);
			statement.executeUpdate(sqlCode);
			sqlCode = "delete from relation where rel_name='"+number+"'";
			System.out.println(sqlCode);
			statement.executeUpdate(sqlCode);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	public void delD(int number){
		if(con==null){
			return;
		}
		try {
			con.setAutoCommit(true);
			String sqlCode = "delete from com where com_id='"+number+"'and com_type='D'";
			System.out.println(sqlCode);
			statement.executeUpdate(sqlCode);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void delcom(int number,String type){
		if(con==null){
			return;
		}
		try {
			con.setAutoCommit(true);
			String sqlCode = "delete from com where com_id='"+number+"'and com_type='"+type+"'";
			System.out.println(sqlCode);
			statement.executeUpdate(sqlCode);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void delComOnGraph(String graph,String type,int number){
		if(con==null){
			return;
		}
		try {
			con.setAutoCommit(true);
			String sqlCode = "delete from comgraph where graph_id='"+graph+"'and com_id='"+number+"'and com_type='"+type+"'";
			statement.executeUpdate(sqlCode);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	/**
	 * 
	 * @param graph
	 */
	public String getGraph(String graph)throws IOException
	{
		Graph temp = new Graph(graph);
		Document doc = new Document(temp.writeGraph());
		XMLOutputter outputter = new XMLOutputter();
		return outputter.outputString(doc);
	}
	public void mustdelgraph(String name)
	{
		String sqlCode="";
		try {
			con.setAutoCommit(true);
			sqlCode = "select rel_name from relgraph where graph_id='"+name+"'";
			ResultSet result = statement.executeQuery(sqlCode);
			ArrayList<String> rel = new ArrayList<String>();
			while(result.next()){
				rel.add(result.getString(1));
			}
			sqlCode = "delete from relgraph where graph_id='"+name+"'";
			statement.executeUpdate(sqlCode);
			for(String temp:rel){
				int number=Integer.parseInt(temp);
				String res=delLinePd(number);
				String[] n=res.split(",");
				if(n[1].equals("0"))
				{
					sqlCode = "delete from relation where rel_name='"+number+"'";
					statement.executeUpdate(sqlCode);
					System.out.println(temp+"����ɾ��");
				}
				else{System.out.println(temp+"����ɾ��");}
			}
			sqlCode = "select com_id from comgraph where graph_id='"+name+"'and com_type='D'";
			result = statement.executeQuery(sqlCode);
			ArrayList<String> Dcom = new ArrayList<String>();
			while(result.next()){
				rel.add(result.getString(1));
			}
			sqlCode = "delete from comgraph where graph_id='"+name+"'";
			statement.executeUpdate(sqlCode);
			for(String temp:rel)
			{
				sqlCode = "delete from com where com_id='"+temp+"' and com_type='D'";
				statement.executeUpdate(sqlCode);
			}
		}catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public String[] getUserGraph(String userName){
		Connection conTemp = null;
		try
		{
		    Class.forName("com.mysql.jdbc.Driver");
		    String url = "jdbc:mysql://"+DbConfig.host+"/"+DbConfig.dbname
				    + "?useUnicode=true&characterEncoding=UTF-8";
		    conTemp = DriverManager.getConnection(url, DbConfig.usname, DbConfig.dbpwd);
		}
		catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		String[] rs = null;
		try {
//			con.setAutoCommit(true);
			Statement stateTemp = conTemp.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
			String sqlCode="";
			if(!userName.equals("admin"))
			{
				sqlCode = "select graph_id from graph where us_id='"+userName+"'";
			}
			else
			{
				sqlCode = "select graph_id from graph";
			}
			ResultSet result = stateTemp.executeQuery(sqlCode);
			result.last(); 
			int number = result.getRow();  
			result.beforeFirst();
			rs=new String[number];
			number=0;
			while(result.next()){
				rs[number]=result.getString(1);
				number++;
			}
			result.close();
			stateTemp.close();
			conTemp.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		return rs;
	}
	// bxs 
	public String getComponent(String id, String type,int sp,float dval,int iscount,int cost) {
		Connection conTemp = null;
		try
		{
		    Class.forName("com.mysql.jdbc.Driver");
		    String url = "jdbc:mysql://"+DbConfig.host+"/"+DbConfig.dbname
				    + "?useUnicode=true&characterEncoding=UTF-8";
		    conTemp = DriverManager.getConnection(url, DbConfig.usname,DbConfig.dbpwd);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			return null;
		}
		
		try 
		{
			Statement temp = conTemp.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
			
			Element comp = new Component(id, type,sp,dval,iscount,cost, temp).writeCom();
			Document doc = new Document(comp);
			XMLOutputter outputter = new XMLOutputter();
			temp.close();
			conTemp.close();
			
			return outputter.outputString(doc);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
	//获取所有的组件
	public String getAllComponent(){
//		if (con == null) {
//			return null;
//		}
		Connection conTemp = null;
		try
		{
		    Class.forName("com.mysql.jdbc.Driver");
		    String url = "jdbc:mysql://"+DbConfig.host+"/"+DbConfig.dbname
				    + "?useUnicode=true&characterEncoding=UTF-8";
		    conTemp = DriverManager.getConnection(url, DbConfig.usname, DbConfig.dbpwd);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			return null;
		}
		try 
		{
			Statement temp = conTemp.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
			Statement temp1 = conTemp.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
			
			Element components = new Element("components");
			String sqlCode = "select com_id,com_type,sp,dval,iscount,cost from com";
			ResultSet result = temp.executeQuery(sqlCode);
			
			while(result.next())
			{   
				String tempSp=result.getString(3);
				if(tempSp==null)
				{
					tempSp="0";
				}
				components.addContent(new Component(result.getString(1),result.getString(2),  Integer.parseInt(tempSp),result.getFloat(4),result.getInt(5),result.getInt(6),temp1).writeCom());
			}
			Document slctDoc = new Document(components);
			XMLOutputter outputter = new XMLOutputter();
			temp.close();
			temp1.close();
			conTemp.close();
			return outputter.outputString(slctDoc);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
	public String getParentByChild(String id,String type,String cnum)
	{
		if(con==null)
		{
			return null;
		}
		
		Element root = new Element("vals");
		root.setAttribute("cnum", cnum);
		try 
		{
			con.setAutoCommit(true);
			String sqlCode = "select rel_end_id,rel_end_type from relation where " +
			"rel_begin_id=? and rel_begin_type=?";
			PreparedStatement state = con.prepareStatement(sqlCode);
			state.setString(1, id);
			state.setString(2, type);
			ResultSet result = state.executeQuery();
			while(result.next()){
				Element val = new Element("val");
				
				Element vType = new Element("type");
				vType.addContent(result.getString(2));
				val.addContent(vType);
				
				Element vId = new Element("vId");
				vId.addContent(result.getString(1));
				val.addContent(vId);
				
				root.addContent(val);
			}
			XMLOutputter outputter = new XMLOutputter();
			return outputter.outputString(root);
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}
	
	public String getVarStateDes(String[] state){
		if (con == null) 
		{
			return null;
		}

		Element root = new Element("vBValList");
		try
		{
			con.setAutoCommit(true);
			Statement statement = con.createStatement();
			for(int i = 0; i < state.length; i++)
			{
				String varTypeName = state[i].split(",")[0];
				String varType = varTypeName.substring(0, 1);
				String varID = varTypeName.substring(1);
				String varState = state[i].split(",")[1];
				String sqlCode = "select state_des from state "
				             + "where com_type = '" + varType + "' and com_id = '" + varID
				             + "' and state_id = '" + varState + "'";
				ResultSet result = statement.executeQuery(sqlCode);
				while (result.next()) 
				{
					Element BVal = new Element("BVal");
					
					Element BValTypeID = new Element("varIDState");;
					BValTypeID.addContent(state[i]);
					BVal.addContent(BValTypeID);
	
					Element com_des = new Element("state_des");
					com_des.addContent(result.getString(1));
					BVal.addContent(com_des);
					
					root.addContent(BVal);
				}
			}			

			Document slctDoc = new Document(root);
			XMLOutputter outputter = new XMLOutputter();
			return outputter.outputString(slctDoc);

		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * �ر�����
	 */
	public void close() {
		if (con == null) {
			return;
		}
		try {
			con.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
