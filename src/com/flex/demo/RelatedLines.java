package com.flex.demo;

import java.util.ArrayList;

public class RelatedLines {
	public ArrayList<Line> inLines;
	public ArrayList<Line> outLines;
	
	public RelatedLines(){
		this.inLines = new ArrayList<Line>();
		this.outLines = new ArrayList<Line>();
	}
	
	public void addInLine(Line inLine){
		this.inLines.add(inLine);
	}
	
	public void addOutLine(Line outLine){
		this.outLines.add(outLine);
	}
}
