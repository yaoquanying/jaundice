package com.flex.demo;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
public class readSimple {
	private String circlepath="D:\\show\\";
	private String simplepath="D:\\simple\\";
	public Number getcirclenum()
	{
		File directory = new File(circlepath);
		File[] files = directory.listFiles();
		return files.length;
	}
	public Number getsimplenum()
	{
		File directory = new File(simplepath);
		File[] files = directory.listFiles();
		return files.length;
	}
	public String getcircle(int num) throws IOException
	{
		String returnstr=new String();
		File directory = new File(circlepath);
		File[] files = directory.listFiles();
		String strtmp=new String();
		returnstr=""+num+" ";
		FileInputStream fileinstream = new FileInputStream(circlepath+files[num].getName());
		InputStreamReader inputreader = new InputStreamReader(fileinstream);
		BufferedReader bufreader=new BufferedReader(inputreader);
		while((strtmp=bufreader.readLine())!=null){
			returnstr=returnstr+strtmp;
			returnstr=returnstr+" ";
		}
		fileinstream.close();
		return returnstr;
	}
	public String getsimple(int num) throws IOException
	{
		String returnstr=new String();
		File directory = new File(simplepath);
		File[] files = directory.listFiles();
		String strtmp=new String();
		returnstr=""+num+" ";
		FileInputStream fileinstream = new FileInputStream(simplepath+files[num].getName());
		InputStreamReader inputreader = new InputStreamReader(fileinstream);
		BufferedReader bufreader=new BufferedReader(inputreader);
		while((strtmp=bufreader.readLine())!=null)
		{
			returnstr=returnstr+strtmp;
			returnstr=returnstr+" ";
		}
		fileinstream.close();
		return returnstr;
	}
}
