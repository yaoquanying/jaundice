package com.engine;

import java.util.*;


public class Expression
{
	private ArrayList<String> ori = new ArrayList<String>();// 存储中序表达式
	private ArrayList<String> right = new ArrayList<String>();// 存储右序表达式
	private String result;// 结果
	// 依据输入信息创建对象，将数值与操作符放入ArrayList中
	private DucgGraph ducgGraph;
	
	public Expression(String input)
	{
		StringTokenizer st = new StringTokenizer(input, "+-*/()", true);
		while (st.hasMoreElements())
		{
			ori.add(st.nextToken());
		}
	}
	public Expression()
	{
		
	}
	/**
	 * DUCGGraph类的使用主要为了得到变量的状态数，主要在SpreadTest类中的getAllNotNo和getAllState函数中使用
	 */
	/*public Expression(DucgGraph ducgGraph)
	{
		this.ducgGraph = ducgGraph;
	}*/
	
	// 将中序表达式转换为右序表达式
	public void toRight()
	{
		Stack<String> s = new Stack<String>();// 运算符栈
		int position = 0;
		while (true)
		{
			String currentOriEle = ori.get(position);
			if (Calculate.isOperator(currentOriEle))
			{// 如果是运算符需要处理
				if (s.empty() || currentOriEle.equals("("))
				{// 如果是空栈，或者是"("直接入栈
					s.push(currentOriEle);
				}
				else if (currentOriEle.equals(")"))
				{// 若为')'，出栈并顺序输出运算符直到遇到第一个'('，遇到的第一个'('出栈但不输出；
					while (!s.empty())
					{
						if (s.peek().equals("("))
						{
							s.pop();
							break;
						}
						else
						{
							right.add(s.pop());
						}
//						currentOriEle = (String) s.pop();
						if (!s.isEmpty())
						{
							currentOriEle = (String) s.pop();
							if (currentOriEle.equals("!"))
							{
								right.add(currentOriEle);
							}
							else
							{
								s.push(currentOriEle);
							}
						}
					}
				}
				else
				{// 若为其它，比较stackOperator栈顶元素与当前元素的优先级：如果栈顶元素是'('，当前元素入栈；如果栈顶元素
					// >= 当前元素，出栈并顺序输出运算符直到 栈顶元素 < 当前元素，然后当前元素入栈； 如果 栈顶元素 <
					// 当前元素，直接入栈。
					if (s.peek().equals("("))
					{
						s.push(currentOriEle);
					}
					else
					{
						while (!s.empty()
								&& Calculate.priority(currentOriEle) <= Calculate
										.priority(s.peek()))
						{
							right.add(s.pop());
						}
						s.push(currentOriEle);
					}
				}
			}
			else
			{// 数字直接进list
				right.add(currentOriEle);
			}
			position++;
			if (position >= ori.size()) break;
		}
		while (!s.empty())
		{// 运算符栈不为空入list
			right.add(s.pop());
		}
	}
	
	// 对右序表达式进行求值
	public void getResult()
	{
		this.toRight();
		Stack<String> s = new Stack<String>();
		String n1 = "";
		String n2 = "";
		String e = null;
		Iterator<String> it = right.iterator();
		
		Calculate cl = new Calculate(this.ducgGraph);
		while (it.hasNext())
		{
			e = it.next();
			if (Calculate.isOperator(e))
			{
				if (e.equals("!"))
				{
					n1 = s.pop();
					s.push(cl.twoResult(e, n1, ""));
				}
				else
				{
					n1 = s.pop();
					n2 = s.pop();
					s.push(cl.twoResult(e, n1, n2));
				}
			}
			else
			    s.push(e);
		}
		result = String.valueOf(s.pop());
//		it = right.iterator();
//		while (it.hasNext())
//		{
//			System.out.print(it.next());
//		}
//		System.out.println("=" + result);
	}
	
	
	/**
	 * 得到异或表达式
	 */
	public String getXORExpression(int num,int typ,String strExpression)
	{
		SpreadTest spreadTest = new SpreadTest();
		StringTokenizer st = new StringTokenizer(strExpression, "+-*/()", true);
		while (st.hasMoreElements())
		{
			ori.add(st.nextToken());
		}
		
		getResult();
		return ""+typ+" "+num+" "+spreadTest.getXORofExpression(result);
	}
	
	
	/**
	 * 得到项最简表达式
	 */
	public String getTermSimple(int num,int typ,String strExpression)
	{
//		if(strExpression.contains("(")) return strExpression;
		SpreadTest spreadTest = new SpreadTest();
		StringTokenizer st = new StringTokenizer(strExpression, "+-*/()", true);
		while (st.hasMoreElements())
		{
			ori.add(st.nextToken());
		}
		
		getResult();
		return ""+typ+" "+num+" "+spreadTest.getSimple(result);
	}
	public String getMDucg(String strExpression)
	{
		ori.clear();
		right.clear();
//		if(strExpression.contains("(")) return strExpression;
		SpreadTest spreadTest = new SpreadTest();
		StringTokenizer st = new StringTokenizer(strExpression, "+-*/()", true);
		while (st.hasMoreElements())
		{
			ori.add(st.nextToken());
		}		
		getResult();
		return spreadTest.getMDucg(result);
	}

}
