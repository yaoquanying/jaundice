package com.engine;

import java.util.LinkedList;
import java.util.List;

public class Matrix {

	private String id;

	private List<Row> rList;

	public Matrix() {
		id = "";
		rList = new LinkedList<Row>();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public List<Row> getRList() {
		return rList;
	}

	public void setRList(List<Row> list) {
		rList = list;
	}

	public void addRList(Row r) {
		rList.add(r);
	}

	public void removeRList(Row r) {
		rList.remove(r);
	}

	public void clearRList() {
		rList.clear();
	}

}
