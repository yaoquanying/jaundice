package com.engine;

import java.util.LinkedList;
import java.util.List;

public class Flow {

	private String id;

	private int type;

	private String sourceRef;

	private String targetRef;

	private List<Expression> eList;

	public Flow() {
		id = "";
		type = 0;
		sourceRef = "";
		targetRef = "";
		eList = new LinkedList<Expression>();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public String getSourceRef() {
		return sourceRef;
	}

	public void setSourceRef(String sourceRef) {
		this.sourceRef = sourceRef;
	}

	public String getTargetRef() {
		return targetRef;
	}

	public void setTargetRef(String targetRef) {
		this.targetRef = targetRef;
	}

	public List<Expression> getEList() {
		return eList;
	}

	public void setEList(List<Expression> list) {
		eList = list;
	}
	
	public void addEList(Expression e){
		eList.add(e);
	}
	
	public void removeEList(Expression e){
		eList.remove(e);
	}
	
	public void clearEList(){
		eList.clear();
	}


}
