package com.engine;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Stack;
import java.util.StringTokenizer;
import java.util.regex.Pattern;

public class SimplifyExpression
{
	private ArrayList<String> ori = new ArrayList<String>();// 存储中序表达式
	private ArrayList<String> right = new ArrayList<String>();// 存储右序表达式
	public String result;// 结果
	
	// 依据输入信息创建对象，将数值与操作符放入ArrayList中
	// private DucgGraph ducgGraph;
	
	public SimplifyExpression(String input)
	{
		StringTokenizer st = new StringTokenizer(input, "+-*/()", true);
		while (st.hasMoreElements())
		{
			ori.add(st.nextToken());
		}
	}
	
	/**
	 * DUCGGraph类的使用主要为了得到变量的状态数，主要在SpreadTest类中的getAllNotNo和getAllState函数中使用
	 */
	public SimplifyExpression()
	{
		// this.ducgGraph = ducgGraph;
	}
	
	// 将中序表达式转换为右序表达式
	public void toRight()
	{
		Stack<String> s = new Stack<String>();// 运算符栈
		int position = 0;
		while (true)
		{
			String currentOriEle = ori.get(position);
			if (RemoveBrackets.isOperator(currentOriEle))
			{// 如果是运算符需要处理
				if (s.empty() || currentOriEle.equals("("))
				{// 如果是空栈，或者是"("直接入栈
					s.push(currentOriEle);
				}
				else if (currentOriEle.equals(")"))
				{// 若为')'，出栈并顺序输出运算符直到遇到第一个'('，遇到的第一个'('出栈但不输出；
					while (!s.empty())
					{
						if (s.peek().equals("("))
						{
							s.pop();
							break;
						}
						else
						{
							right.add(s.pop());
						}
						// currentOriEle = (String) s.pop();
						if (!s.isEmpty())
						{
							currentOriEle = (String) s.pop();
							if (currentOriEle.equals("!"))
							{
								right.add(currentOriEle);
							}
							else
							{
								s.push(currentOriEle);
							}
						}
					}
				}
				else
				{// 若为其它，比较stackOperator栈顶元素与当前元素的优先级：如果栈顶元素是'('，当前元素入栈；如果栈顶元素
					// >= 当前元素，出栈并顺序输出运算符直到 栈顶元素 < 当前元素，然后当前元素入栈； 如果 栈顶元素 <
					// 当前元素，直接入栈。
					if (s.peek().equals("("))
					{
						s.push(currentOriEle);
					}
					else
					{
						while (!s.empty() && RemoveBrackets.priority(currentOriEle) <= RemoveBrackets.priority(s.peek()))
						{
							right.add(s.pop());
						}
						s.push(currentOriEle);
					}
				}
			}
			else
			{// 数字直接进list
				right.add(currentOriEle);
			}
			position++;
			if (position >= ori.size()) break;
		}
		while (!s.empty())
		{// 运算符栈不为空入list
			right.add(s.pop());
		}
	}
	
	// 对右序表达式进行求值
	public void getResult()
	{
		this.toRight();
		Stack<String> s = new Stack<String>();
		String n1 = "";
		String n2 = "";
		String e = null;
		Iterator<String> it = right.iterator();
		
		RemoveBrackets cl = new RemoveBrackets();
		while (it.hasNext())
		{
			e = it.next();
			if (RemoveBrackets.isOperator(e))
			{
				if (e.equals("!"))
				{
					if (!s.empty())
					{
						n1 = s.pop();
					}
					
					if (!isLegal(n1))
					{
						result = "";
						return;
					}
					s.push(cl.twoResult(e, n1, ""));
				}
				else
				{
					if (!s.empty())
					{
						n1 = s.pop();
					}
					if (!s.empty())
					{
						n2 = s.pop();
					}
					
					if (!isLegal(n1) || !isLegal(n2))
					{
						result = "";
						return;
					}
					s.push(cl.twoResult(e, n1, n2));
				}
			}
			else
				s.push(e);
		}
		result = String.valueOf(s.pop());
	}
	
	public boolean isNumeric(String str)
	{
		Pattern pattern = Pattern.compile("[0-9]*");
		return pattern.matcher(str).matches();
	}
	
	public boolean isLegal(String str)
	{
		String[] strTerm = str.split("\\+");
		for (int i = 0; i < strTerm.length; i++)
		{
			String[] strFactor = strTerm[i].split("\\*");
			for (int j = 0; j < strFactor.length; j++)
			{
				if (!isTermLegal(strFactor[j]))
				{
					return false;
				}
			}
		}
		return true;
	}
	
	public boolean isTermLegal(String str)
	{
		String[] strTerm = str.split(",");
		if (strTerm.length != 2)
		{
			return false;
		}
		String strVariable = strTerm[0].substring(0, 1);
		String strName = strTerm[0].substring(1);
		if (!(strVariable.equals("X") || strVariable.equals("B") || strVariable.equals("D")))
		{
			return false;
		}
		
		if (!isNumeric(strName))
		{
			return false;
		}
		
		if (!isNumeric(strTerm[1]))
		{
			return false;
		}
		
		return true;
	}
	
	private int charCount(String expression, char ch)
	{ // 表达式中某个字符的个数
		int count = 0;
		for (int i = 0; i < expression.length(); i++)
		{
			if (expression.charAt(i) == ch)
			{
				count++;
			}
		}
		return count;
	}
	
	 /**
	 * 得到异或表达式
	 */
	 public String getXORExpression(int num,int typ,String strExpression)
	 {
		 if(strExpression.contains("F"))
		 {
			 return ""+typ+" "+num+" "+strExpression;
		 }
		 ori.clear();
		 right.clear();
		 SpreadTest spreadTest = new SpreadTest();
		 StringTokenizer st = new StringTokenizer(strExpression, "+-*/()", true);
		 while (st.hasMoreElements())
		 {
			 ori.add(st.nextToken());
		 }
			
		 getResult();
		 return ""+typ+" "+num+" "+spreadTest.getXORofExpression(result);
	 }
	
	private boolean isOperation(String strExpession)
	{
		for (int j = 1; j < strExpession.length() - 1; j++)
		{
			///应该判断一下非
			if(strExpession.charAt(j) == '!')
			{
				if(!RemoveBrackets.isOperator1(String.valueOf(strExpession.charAt(j-1))))
				{
					return false;
				}
				if("(".indexOf((strExpession.charAt(j + 1))) == -1)
				{
					return false;
				}
			}
			if (RemoveBrackets.isOperator1(String.valueOf(strExpession.charAt(j))))
			{
			
				if (("0123456789)(".indexOf((strExpession.charAt(j - 1))) != -1 || "".indexOf((strExpession.charAt(j - 1))) != -1) && "XBD(".indexOf(strExpession.charAt(j + 1)) != -1)
				{
					continue;
				}
				else
				{
					if ("0123456789)".indexOf((strExpession.charAt(j - 1))) == -1 || "XBD(!".indexOf(strExpession.charAt(j + 1)) == -1)
					{
						return false;
					}
				}
			}
		}
		return true;
	}
	
	/**
	 * 得到项最简表达式
	 */
	public String getTermSimple(int num,int typ,String strExpression)
	{
		if(strExpression.contains("F"))
		{
			 return ""+typ+" "+num+" "+strExpression;
		}
		ori.clear();
		right.clear();
		if (charCount(strExpression, '(') != charCount(strExpression, ')'))
		{
			return "";
		}
		
		if(!isOperation(strExpression))
		{
			return "";
		}
		// if(strExpression.contains("(")) return strExpression;
		SpreadTest spreadTest = new SpreadTest();
		StringTokenizer st = new StringTokenizer(strExpression, "+-*/()", true);
		while (st.hasMoreElements())
		{
			ori.add(st.nextToken());
		}
		getResult();
		return ""+typ+" "+num+" "+spreadTest.getSimple(result);
	}
	
}
