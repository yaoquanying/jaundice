package com.engine;

import java.util.*;

public class Stacks<T>
{
	private LinkedList<T> ll = new LinkedList<T>();  
	  
    public void push(T e) {  
        ll.addFirst(e);  
    }  
  
    public T pop() {  
        return ll.removeFirst();  
    }  
  
    public T peek() {  
        return ll.getFirst();  
    }  
  
    public boolean empty() {  
        return ll.isEmpty();  
    }  
}