package com.engine;

import java.util.HashMap;
import java.util.Map;

public class DucgGraph implements Cloneable {

	private String name;

	private Map<String, Node> nMap;

	private Map<String, Flow> fMap;

	private Map<String, Double[][]> mxMap;
	
	private Map<String, Node> namenMap;

	public DucgGraph() {
		name = "";
		nMap = new HashMap<String, Node>();
		fMap = new HashMap<String, Flow>();
		namenMap = new HashMap<String , Node>();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Map<String, Node> getNMap() {
		return nMap;
	}

	public void setNMap(Map<String, Node> map) {
		nMap = map;
	}

	public void putNMap(String s, Node n) {
		nMap.put(s, n);
	}

	public void removeNMap(String s) {
		nMap.remove(s);
	}

	public void clearNMap() {
		nMap.clear();
	}

	public Map<String, Flow> getFMap() {
		return fMap;
	}

	public void setFMap(Map<String, Flow> map) {
		fMap = map;
	}

	public void putFMap(String s, Flow f) {
		fMap.put(s, f);
	}

	public void removeFMap(String s) {
		fMap.remove(s);
	}

	public void clearFMap() {
		fMap.clear();
	}

	public void setMxMap(Map<String, Double[][]> map) {
		mxMap = map;
	}
	
	public Map<String,Double[][]> getMxMap(){
		return mxMap;
	}

	public void putnamenMap(String s,Node node){
		namenMap.put(s, node);
	}
	
	public Map<String , Node> getnamenMap(){
		return namenMap;
	}
	
	public Object clone() {
		Object Obj = null;
		try {
			Obj = super.clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
		return Obj;
	}

}
