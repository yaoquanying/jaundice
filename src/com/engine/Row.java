package com.engine;

public class Row {

	private String id;

	private int state;

	private String probabilities;

	public Row() {
		id = "";
		state = 0;
		probabilities = "";
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}

	public String getProbabilities() {
		return probabilities;
	}

	public void setProbabilities(String probabilities) {
		this.probabilities = probabilities;
	}

}
