package com.engine;

import java.util.LinkedList;
import java.util.List;

public class Node {

	private String id;

	private String name;

	private String type;

	private int states;

	private String probabilities;

	private List<Parent> pList;

	private List<Gate> gList;

	private int statenow;
	
	private int color;
	public Node() {
		id = "";
		name = "";
		type = "";
		states = 0;
		probabilities = "";
		pList = new LinkedList<Parent>();
		gList = new LinkedList<Gate>();
		color = 0;
		statenow = -1;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getStates() {
		return states;
	}

	public void setStates(int states) {
		this.states = states;
	}

	public String getProbabilities() {
		return probabilities;
	}

	public void setProbabilities(String probabilities) {
		this.probabilities = probabilities;
	}

	public List<Parent> getPList() {
		return pList;
	}

	public void setPList(List<Parent> list) {
		pList = list;
	}

	public void addPList(Parent p) {
		pList.add(p);
	}

	public void removePList(Parent p) {
		pList.remove(p);
	}

	public void clearPList() {
		pList.clear();
	}

	public List<Gate> getGList() {
		return gList;
	}

	public void setGList(List<Gate> list) {
		gList = list;
	}
	
	public void addGList(Gate g) {
		gList.add(g);
	}

	public void removeGList(Gate g) {
		gList.remove(g);
	}

	public void clearGList() {
		gList.clear();
	}
	public int getColor(){
		return color;
	}
	public void  setColor(int newcolor){
		color = newcolor;
	}
	
	public void setStatenow(int newstate){
		statenow = newstate;
	}
	
	public int getStatenow(){
		return statenow;
	}
}
