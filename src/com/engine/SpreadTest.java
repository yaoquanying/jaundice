package com.engine;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import db.config.DbConfig;


public class SpreadTest
{
//	private DucgGraph ducgGraph;
	
    private static Connection con = connection();
	
	/*
	public SpreadTest(DucgGraph ducg)
	{
		this.ducgGraph = ducg;
		
	}*/
	
	/**
	 * 
	 * @return
	 */
	public static Connection connection() {
		//String dbName = "jaundice";
		try {
			Class.forName("com.mysql.jdbc.Driver");
			// 配置数据源
			String url = "jdbc:mysql://"+DbConfig.host+"/" + DbConfig.dbname
					+ "?useUnicode=true&characterEncoding=UTF-8";
			Connection con = DriverManager.getConnection(url, DbConfig.usname, DbConfig.dbpwd);
			return con;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	// ////////////////gengsc以下为20110329
	
	
	
	/**
	 * 得到变量的状态数量
	 */
	public int getStatesNum(String id,String type){
		
		if(con==null){
			con=connection();
		}
		try {
			con.setAutoCommit(true);
			String sqlCode ="select count(*) from state where com_id='"+id+"' and com_type='"+type+"'";
			Statement statement = con.createStatement();
			
			ResultSet result = statement.executeQuery(sqlCode);
			if(result.next()){
				return result.getInt(1);
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return 1;
		}
		return 1;
	}
	
	/**
	 * 得到一个状态的其余状态
	 * 
	 */
	public String getAllNotNo(String strVariableName, int iState)
	{
		String strAllNotNo = "";
		//Node node = ducgGraph.getnamenMap().get(strVariableName);
		// node.getGList().
		// /得到变量的状态数量，可以改成从数据库中取得
		//int iStateNum = node.getStates();
		String id = strVariableName.substring(1);
		String type=strVariableName.substring(0,1);
		int iStateNum=getStatesNum(id,type);
		for (int i = 0; i < iStateNum; i++)
		{
			if (i != iState)
			{
				if (strAllNotNo.equals(""))
				{
					strAllNotNo = strVariableName + "," + String.valueOf(i);
				}
				else
				{
					strAllNotNo = strAllNotNo + "+" + strVariableName + ","
							+ String.valueOf(i);
				}
			}
		}
		return strAllNotNo;
	}
	
	/**
	 * 得到变量的所有状态组合
	 * 
	 * @param strVariableName
	 * @param bIsZero
	 * @return
	 */
	public String getAllState(String strVariableName)
	{
		String strResult = "";
		//Node node = ducgGraph.getnamenMap().get(strVariableName);
		// node.getGList().
		//int iStateNum = node.getStates();
		
		String id = strVariableName.substring(1);
		String type=strVariableName.substring(0,1);
		int iStateNum=getStatesNum(id,type);
		
		boolean bFlag = false;
		for (int i = 0; i < iStateNum; i++)
		{
			if (strResult.equals(""))
			{
				strResult = strVariableName + "," + String.valueOf(i);
			}
			else
			{
				strResult = strResult + "+" + strVariableName + "," + String.valueOf(i);
				bFlag = true;
			}
		}
		if(bFlag)
		{
			strResult = "(" + strResult + ")";
		}
		System.out.println("shu"+strResult);
		return strResult;
	}
	/**
	 * 展开成异或表达式,
	 * 
	 * @param strExpression
	 * @return
	 */
	public String getXORofExpression(String strExpression)
	{
		// Expression expression = new Expression(strExpression);
		// expression.getResult();
		
		String strXOR = "";
		String[] strTerm = strExpression.split("\\+");
		String[] strNot = new String[strTerm.length];
		String[] strXORTerm = new String[strTerm.length];
		this.sortTerm(strTerm);
		
		// /得到每一项的非
		for (int i = 0; i < strTerm.length; i++)
		{
			if (strTerm[i].contains("*"))
			{
				strNot[i] = getCutset(strTerm[i]);
			}
			else
			{
				String[] strVariableInfo = strTerm[i].split(",");
				String strVariableName = strVariableInfo[0];
				String strVariableState = strVariableInfo[1];
				strNot[i] = this.getAllNotNo(strVariableName, Integer.parseInt(strVariableState));
			}
		}
		
		// //按照C1+C2+C3等于C1 + C1的非*C2+ C1的非*C2的非*C3得到每展开后的每一项
		for (int i = 0; i < strNot.length; i++)
		{
			String strTemp = strTerm[i];
			for (int j = i - 1; j >= 0; j--)
			{
				// if (i == 0)
				// {
				// strXORTerm[i] = strTerm[i];
				// }
				// else
				// {
				strTemp = additionMultiply(strNot[j], strTemp);
				// }
			}
			strXORTerm[i] = strTemp;
		}
		
		// /将每一项组合
		for (int i = 0; i < strXORTerm.length; i++)
		{
			if (strXOR.equals(""))
			{
				strXOR = strXORTerm[i];
			}
			else
			{
				if (!strXORTerm[i].equals("")) strXOR = strXOR + "+" + strXORTerm[i];
			}
		}
		return strXOR;
	}
	
	/**
	 * 处理形式为X1,2*X2,1的非
	 * 
	 * @param strExpression
	 * @return
	 */
	public String getANDMorgan(String strExpression)
	{
		String strAND = "";
		String[] strTerm = strExpression.split("\\*");
		for (int i = 0; i < strTerm.length; i++)
		{
			String[] strVariableInfo = strTerm[i].split(",");
			String strVariableName = strVariableInfo[0];
			String strVariableState = strVariableInfo[1];
			String strAllNotNo = this.getAllNotNo(strVariableName, Integer.parseInt(strVariableState));
			if (!strAllNotNo.equals(""))
			{
				if (strAND.equals(""))
				{
					strAND = strAllNotNo;
				}
				else
				{
					strAND = strAND + "+" + strAllNotNo;
				}
			}
		}
		return strAND;
	}
	
	/**
	 * 得到割集
	 * 
	 * @param strExpression
	 * @return
	 */
	public String getCutset(String strExpression)
	{
		String strResult = "";
		String[] strTerm = strExpression.split("\\*");
		String[] strTermNon = new String[strTerm.length];
		for (int i = 0; i < strTerm.length; i++)
		{
			String[] strVariableInfo = strTerm[i].split(",");
			String strVariableName = strVariableInfo[0];
			String strVariableState = strVariableInfo[1];
			String strAllNotNo = this.getAllNotNo(strVariableName, Integer.parseInt(strVariableState));
			strTermNon[i] = strAllNotNo;
		}
		for (int i = 0; i < strTermNon.length; i++)
		{
			String strTemp = "";
			for (int j = 0; j < i; j++)
			{
				if (strTemp.equals(""))
				{
					strTemp = strTerm[j];
				}
				else
				{
					strTemp = mulDoubleMultiply(strTemp, strTerm[j]);
				}
			}
			if (strResult.equals(""))
			{
				if (strTemp.equals(""))
				{
					strResult = strTermNon[i];
				}
				else
				{
					strResult = strTemp + "*" + strTermNon[i];
				}
			}
			else
			{
				if (strTemp.equals(""))
				{
					strResult = strResult + "*" + strTermNon[i];
				}
				else
				{
					strResult = strResult + "+" + additionMultiply(strTemp, strTermNon[i]);
				}
			}
			
		}
		return strResult;
	}
	
	// /**
	// * 得到异或的非如得到X1,1+X2,1+X3,1的非 是否可以去掉
	// *
	// * @param strExpression
	// * @return
	// */
	// public String getORMorgan(String strExpression)
	// {
	// String strOR = "";
	// String[] strTerm = strExpression.split("\\+");
	// String[] strExpandTerm = new String[strTerm.length];
	// for (int i = 0; i < strTerm.length; i++)
	// {
	// String[] strVariableInfo = strTerm[i].split(",");
	// String strVariableName = strVariableInfo[0];
	// String strVariableState = strVariableInfo[1];
	// strExpandTerm[i] = this.getAllNotNo(strVariableName, Integer
	// .parseInt(strVariableState));
	// }
	//		
	// String strFirst = "";
	// String strSecond = "";
	// int j = 0;
	// while (j < strExpandTerm.length)
	// {
	// if (strFirst.equals(""))
	// {
	// strFirst = strExpandTerm[j];
	// }
	// if (strSecond.equals(""))
	// {
	// j++;
	// strSecond = strExpandTerm[j];
	// }
	// else
	// {
	// strSecond = strExpandTerm[j];
	// }
	// strFirst = this.additionMultiply(strFirst, strSecond);
	// j++;
	// }
	// strOR = strFirst;
	// return strOR;
	// }
	
	/**
	 * 两个异或的式子相与。 需调用一个与表达式和一个变量进行与运算
	 * 
	 * 样式X1 ,1+X1,0*X2,1+X1,2*X2,1 样式X1 ,1+X1,0*X3,1+X1,2*X3,1
	 * 
	 * @return
	 */
	public String additionMultiply(String strFirst, String strSecond)
	{
		String strResult = "";
		String[] strFirstFactor = strFirst.split("\\+");
		String[] strSecondFactor = strSecond.split("\\+");
		for (int i = 0; i < strFirstFactor.length; i++)
		{
			for (int j = 0; j < strSecondFactor.length; j++)
			{
				String strFactor = mulDoubleMultiply(strFirstFactor[i], strSecondFactor[j]);
				if (!strFactor.equals(""))
				{
					if (strResult.equals(""))
					{
						strResult = strFactor;
					}
					else
					{
						strResult = strResult + "+" + strFactor;
					}
				}
			}
		}
		return strResult;
	}
	
	/**
	 * 将两个与表达式相与
	 * 
	 * 样式X1 ,1*X2,1 样式X1 ,1*X3,1*X4,1
	 * 
	 * @return
	 */
	public String mulDoubleMultiply(String strFirst, String strSecond)
	{
		String strResult = strFirst;
		String[] strMulVariable = strSecond.split("\\*");
		for (int i = 0; i < strMulVariable.length; i++)
		{
			strResult = varMulMultiply(strResult, strMulVariable[i]);
			if (strResult.equals(""))
			{
				break;
			}
		}
		return strResult;
	}
	
	/**
	 * 一个与表达式和一个变量进行与运算。将与表达式分割成变量每个变量和指定变量进行与运算。 如果有一个与运算为空，则整个表达式为空。与表达式包含变量得状态吸收掉 需要调用两个变量与运算例子X1,0*X2,1与X3,1
	 * 
	 * @param strMulExpression
	 *            X1,0*X2,1
	 * @param strVariable
	 *            X3,1
	 * @return
	 */
	public String varMulMultiply(String strMulExpression, String strVariable)
	{
		String strResult = "";
		String[] strMulVariable = strMulExpression.split("\\*");
		boolean boolFlag = false;
		for (int i = 0; i < strMulVariable.length; i++)
		{
			String strTemp = variableMultiply(strMulVariable[i], strVariable);
			if (strTemp.equals(""))
			{
				strResult = "";
				break;
			}
			else
			{
				strResult = strTemp;
			}
			if (strTemp.equals(strVariable))
			{
				boolFlag = true;
			}
		}
		if (!strResult.equals(""))
		{
			if (boolFlag)
			{
				
				strResult = strMulExpression;
				
			}
			else
			{
				strResult = strMulExpression + "*" + strVariable;
			}
		}
		return strResult;
	}
	
	/**
	 * 两个变量与运算，同一个变量状态相同则合并为一个变量，同一个变量不同状态则为空，不同变量则用*连接
	 * 
	 * 第一个变量X1,2 第二个变量X2,2
	 * 
	 * @return
	 */
	public String variableMultiply(String strFirstVariable, String strSecondVariable)
	{
		String strResult = "";
		String[] strVariable1 = strFirstVariable.split(",");
		String[] strVariable2 = strSecondVariable.split(",");
		if (strVariable1[0].equals(strVariable2[0]))
		{
			if (strVariable1[1].equals(strVariable2[1]))
			{
				strResult = strFirstVariable;
			}
			else
			{
				strResult = "";
			}
		}
		else
		{
			strResult = strFirstVariable + "*" + strSecondVariable;
		}
		return strResult;
	}
	
	/**
	 * 得到表达式的非
	 * 
	 * @param strExpression
	 * @return
	 */
	public String getNon0fExpression(String strExpression)
	{
		String strResult = "";
		String[] strTerm = strExpression.split("\\+");
		String[] strTermNon = new String[strTerm.length];
		for (int i = 0; i < strTerm.length; i++)
		{
			if (strTerm[i].contains("*"))
			{
				strTermNon[i] = getANDMorgan(strTerm[i]);
			}
			else
			{
				String[] strVariableInfo = strTerm[i].split(",");
				String strVariableName = strVariableInfo[0];
				String strVariableState = strVariableInfo[1];
				strTermNon[i] = this.getAllNotNo(strVariableName, Integer.parseInt(strVariableState));
			}
		}
		for (int i = 0; i < strTermNon.length; i++)
		{
			if (strResult.equals(""))
			{
				strResult = strTermNon[i];
			}
			else
			{
				strResult = additionMultiply(strResult, strTermNon[i]);
			}
		}
		return strResult;
	}
	
	/**
	 * 将项按照*分割，并按照分割后的数量排序
	 * 
	 * @param strTerm
	 */
	public void sortTerm(String[] strTerm)
	{
		// String[] strResult = new String[strTerm.length];
		
		for (int i = strTerm.length - 1; i >= 0; i--)
		{
			boolean bIsChanged = false;
			for (int j = 0; j < i; j++)
			{
				int iFirst = strTerm[j].split("\\*").length;
				int iSecond = strTerm[j + 1].split("\\*").length;
				if (iFirst > iSecond)
				{
					String strTemp = strTerm[j];
					strTerm[j] = strTerm[j + 1];
					strTerm[j + 1] = strTemp;
					bIsChanged = true;
				}
			}
			if (!bIsChanged) break;
		}
		for(int i=0; i<strTerm.length; i++)
		{
			String[] strTemp = strTerm[i].split("\\*");
			Arrays.sort(strTemp);
			String strResult = "";
			for(int j=0; j<strTemp.length; j++)
			{
				if(strResult.equals(""))
				{
					strResult = strTemp[j];
				}
				else
				{
					strResult = strResult + "*" + strTemp[j];
				}
			}
			strTerm[i] = strResult;
		}
		
		// return strResult;
	}
	
	
	/////////////////////////////////gengsc20110409///////////////////////////////////
	
//	/**
//	 * 得到项最简，提取公因式，然后求出每项的剩余部分，
//	 * 
//	 * @param strExpression
//	 * @return
//	 */
//	public String getProductSimple(String strExpression)
//	{
//		String strResult = "";
//		String[] strTerm = strExpression.split("\\+");
//		String strTemp = strTerm[0];
//		for (int i = 1; i < strTerm.length; i++)
//		{
//			strTemp = getCommonPart(strTemp, strTerm[i]);
//		}
//		
//		// String[] strSurplus = new String[strTerm.length];
//		// for(int i=0; i<strTerm.length; i++)
//		// {
//		// int iStart = strTerm[i].indexOf(strTemp);
//		// if(iStart != 0)
//		// {
//		// strSurplus[i] = strTerm[i].substring(0, iStart-1);
//		// if(iStart+strTemp.length()<=strTerm[i].length())
//		// {
//		// strSurplus[i] = strSurplus[i] + strTerm[i].substring(iStart+strTemp.length());
//		// }
//		// }
//		// else
//		// {
//		//				
//		// }
//		// }
//		//		
//		// String str
//		
//		String[] strSurplus = new String[strTerm.length];
//		for (int i = 0; i < strTerm.length; i++)
//		{
//			strSurplus[i] = this.getSurplus(strTemp, strTerm[i]);
//		}
//		boolean bNum = true;
//		String strValue = "";
//		String strAllState = "";
//		for (int i = 0; i < strSurplus.length; i++)
//		{
//			if (!strSurplus[i].equals(""))
//			{
//				int iNum = strSurplus[i].split("\\*").length;
//				if (iNum > 1)
//				{
//					bNum = false;
//					break;
//				}
//				else
//				{
//					strValue = strSurplus[i].split(",")[0];
//					if (strAllState.equals(""))
//					{
//						strAllState = strSurplus[i];
//					}
//					else
//					{
//						strAllState = strAllState + "+" + strSurplus[i];
//					}
//				}
//			}
//		}
//		if (!bNum)
//		{
//			strResult = strExpression;
//		}
//		else
//		{
//			if (strAllState.equals(this.getAllState(strValue)))
//			{
//				strResult = strTemp;
//			}
//			else
//			{
//				strResult = strExpression;
//			}
//		}
//		return strResult;
//	}
	
//	/**
//	 * 得到两项的公共部分
//	 * 
//	 * @param strFirstTerm第一项
//	 * @param strSecondTerm第二项
//	 * @return公共部分
//	 */
//	public String getCommonPart(String strFirstTerm, String strSecondTerm)
//	{
//		String strResult = "";
//		String[] strFirst = strFirstTerm.split("\\*");
//		String[] strSecond = strSecondTerm.split("\\*");
//		for (int i = 0; i < strFirst.length; i++)
//		{
//			for (int j = 0; j < strSecond.length; j++)
//			{
//				if (strFirst[i].equals(strSecond[j]))
//				{
//					if (strResult.equals(""))
//					{
//						strResult = strFirst[i];
//					}
//					else
//					{
//						strResult = strResult + "*" + strSecond[j];
//					}
//				}
//			}
//		}
//		return strResult;
//	}
//	
	
	/**
	 * 去掉项的公共部分，得到剩余部分
	 * 
	 * @param strCommon公共部分
	 * @param strTerm项
	 * @return剩余部分
	 */
	public String getSurplus(String strCommon, String strTerm)
	{
		String strResult = "";
		String[] strCommonTerm = strCommon.split("\\*");
		String strTemp = strTerm;
		for (int i = 0; i < strCommonTerm.length; i++)
		{
			if (strTerm.contains(strCommonTerm[i]))
			{
				int iStart = strTerm.indexOf(strCommonTerm[i]);
				if (iStart == 0)
				{
					if (strCommonTerm[i].length() < strTerm.length())
					{
						strTemp = strTerm.substring(strCommonTerm[i].length() + 1);
					}
					else
					{
						strTemp = strTerm;
					}
				}
				else
				{
					strTemp = strTerm.substring(0, iStart - 1);
					if (iStart + strCommonTerm[i].length() <= strTerm.length())
					{
						strTemp = strTemp + strTerm.substring(iStart + strCommonTerm[i].length());
					}
				}
				strTerm = strTemp;
			}
		}
		strResult = strTemp;
		return strResult;
	}
	
	/**
	 * 处理吸收问题
	 * 
	 * @param strExpression
	 * @return
	 */
	public String absorbExpression(String strExpression)
	{
		String strResult = "";
		String[] strTerm = strExpression.split("\\+");
		this.sortTerm(strTerm);
		// List<String> listOneTerm = new ArrayList<String>();
		boolean[] bIsabsorb = new boolean[strTerm.length];
		for (int i = 0; i < strTerm.length; i++)
		{
			bIsabsorb[i] = true;
		}
		for (int i = 0; i < strTerm.length; i++)
		{
			
			for (int j = i + 1; j < strTerm.length; j++)
			{
				if (strTerm[j].contains(strTerm[i]))
				{
					bIsabsorb[j] = false;
				}
			}
		}
		for (int i = 0; i < strTerm.length; i++)
		{
			if (bIsabsorb[i])
			{
				if (strResult.equals(""))
				{
					strResult = strTerm[i];
				}
				else
				{
					strResult = strResult + "+" + strTerm[i];
				}
			}
		}
		return strResult;
	}
	
	
	/**
	 * 双取补运算，不将补展开成
	 * @param strExpression
	 * @return
	 */
	public String doubleComplement(String strExpression)
	{
		String strResult = "";
//		strResult = getANDComplement(strExpression);
//		strResult = absorbExpression(strResult);
		String[] strTerm = strExpression.split("\\+");
		String[] strTermComplement = new String[strTerm.length];
		for (int i = 0; i < strTerm.length; i++)
		{
			strTermComplement[i] = getANDComplement(strTerm[i]);
		}
		String strComplement = strTermComplement[0];
		for (int i = 1; i < strTermComplement.length; i++)
		{
			strComplement = this.multiplyComplement(strComplement, strTermComplement[i]);
		}
		
		strComplement = this.absorbExpression(strComplement);
		strTerm = strComplement.split("\\+");
		for (int i = 0; i < strTerm.length; i++)
		{
			if (strResult.equals(""))
			{
				strResult = this.getNonComplement(strTerm[i]);
			}
			else
			{
				strResult = strResult + "*" + this.getNonComplement(strTerm[i]);
			}
		}
		return strResult;
	}
	
	/**
	 * 多项式相乘
	 * @param strFirst
	 * @param strSecond
	 * @return
	 */
	public String multiplyComplement(String strFirst, String strSecond)
	{
		String strResult = "";
		String[] strFirstTerm = strFirst.split("\\+");
		String[] strSecondTerm = strSecond.split("\\+");
		for (int i = 0; i < strFirstTerm.length; i++)
		{
			for (int j = 0; j < strSecondTerm.length; j++)
			{
				if (strFirstTerm[i].contains(strSecondTerm[j]))
				{
					if (strResult.equals(""))
					{
						strResult = strFirstTerm[i];
					}
					else
					{
						strResult = strResult + "+" + strFirstTerm[i];
					}
				}
				else
				{
					if (strResult.equals(""))
					{
						strResult = strFirstTerm[i] + "*" + strSecondTerm[j];
					}
					else
					{
						strResult = strResult + "+" + strFirstTerm[i] + "*" + strSecondTerm[j];
					}
				}
			}
		}
		return strResult;
	}
	
	/**
	 * 处理X1.1*X2,1*X3,1的非，处理结果为!(X1,1)+!(X2,1)+!(X3,1)
	 * 
	 * @param strExpression
	 * @return
	 */
	public String getANDComplement(String strExpression)
	{
		String strResult = "";
		String[] strTerm = strExpression.split("\\*");
		for (int i = 0; i < strTerm.length; i++)
		{
			if (strResult.equals(""))
			{
				strResult = "!(" + strTerm[i] + ")";
			}
			else
			{
				strResult = strResult + "+!(" + strTerm[i] + ")";
			}
		}
		return strResult;
	}
	
	
	/**
	 * 处理非的非
	 * @param strExpression
	 * @return
	 */
	public String getNonComplement(String strExpression)
	{
		String strResult = "";
		String[] strTerm = strExpression.split("\\*");
		boolean bIsManyTerm = false;
		for (int i = 0; i < strTerm.length; i++)
		{
			if (strResult.equals(""))
			{
				strResult = strTerm[i].substring(strTerm[i].indexOf("(") + 1, strTerm[i].indexOf(")"));
			}
			else
			{
				strResult = strResult + "+" + strTerm[i].substring(strTerm[i].indexOf("(") + 1, strTerm[i].indexOf(")"));
				bIsManyTerm = true;
			}
		}
		if (bIsManyTerm)
		{
			strResult = "(" + strResult + ")";
		}
		return strResult;
	}
	
	//////////////////////////////////gengsc20110409/////////////////////////////////
	
	/**
	 * 进行行最简项最简运算。
	 * 
	 */
	public String getStateSimple(String strExpression, String[] strVariable)
	{
		String strResult = "";
		String[] strTerm = strExpression.split("\\+");
		String[] strDepartment = new String[strVariable.length + 1];
		sortTerm(strTerm);
		Arrays.sort(strTerm);
		for(int i=0; i<strVariable.length; i++)
		{
			for(int j=0; j<strTerm.length; j++)
			{
				if(strTerm[j].contains(strVariable[i]))
				{
					if(strDepartment[i]==null)
					{
						strDepartment[i] = strTerm[j];
					}
					else
					{
						strDepartment[i] = strDepartment[i] + "+" + strTerm[j];
					}
				}
			}
		}
		
		
		///得到剩余的项
		for(int i=0; i<strTerm.length; i++)
		{
			boolean bFlag = false;
			for(int j=0; j<strVariable.length; j++)
			{
				if(strDepartment[j].contains(strTerm[i]))
				{
					bFlag = true;
				}
			}
			if(!bFlag)
			{
				if(strDepartment[strVariable.length] == null)
				{
				    strDepartment[strVariable.length] = strTerm[i];
				}
				else
				{
					strDepartment[strVariable.length] = strDepartment[strVariable.length] + "+" + strTerm[i];
				}
			}
		}
		
		for(int i=0; i<strVariable.length; i++)
		{
			strDepartment[i] = this.doubleComplement(strDepartment[i]);
		}
		
		for(int i=0; i<strVariable.length; i++)
		{
			String strAllState = this.getAllState(strVariable[i]);
			SimplifyExpression e = new SimplifyExpression(this.getSurplus(strAllState, strDepartment[i]));
			e.getResult();
			if(strResult.equals(""))
			{
				strResult = e.result;
			}
			else
			{
				strResult = strResult + "+" + e.result;
			}
		}
		strResult = this.absorbExpression(strResult);
		
		if(strDepartment[strVariable.length] != null)
		{
			if (strResult.equals(""))
			{
				strResult = strDepartment[strVariable.length];
			}
			else
			{
				strResult = strResult + "+" + strDepartment[strVariable.length];
			}
		}
		
		
		return strResult;
	}
	
	
	public String getSimple(String strExpression)
	{
		String strResult = "";
		String[] strTerm = strExpression.split("\\+");
		List<String> listCommon = new ArrayList<String>();
		String[] strTemp = strTerm[0].split("\\*");
		for(int i=0; i<strTemp.length; i++)
		{
			listCommon.add(strTemp[i]);
		}
		
		for(int i=1; i<strTerm.length; i++)
		{
			strTemp = strTerm[i].split("\\*");
		    for(int j=0; j<strTemp.length; j++)
		    {
				if(!listCommon.contains(strTemp[j]))
				{
					listCommon.add(strTemp[j]);
				}
			}
		}
		List<String> listResult = new ArrayList<String>();
		for(int i=0; i<listCommon.size(); i++)
		{
			String strVariable = listCommon.get(i).split(",")[0];
			for(int j=0; j<listCommon.size(); j++)
			{
				if(i!=j)
				{
					String strVariableTemp = listCommon.get(j).split(",")[0];
					if(strVariable.equals(strVariableTemp) && !listResult.contains(strVariableTemp))
					{
						listResult.add(strVariable);
					}
				}
			}
		}
		
		String[] strOut = new String[listResult.size()];
		for(int i=0; i<listResult.size(); i++)
		{
			strOut[i] = listResult.get(i);
		}

	    strResult = this.getStateSimple(strExpression, strOut);

		return strResult;
	}
	
	public boolean getIsXor(String strExpression)
	{
		boolean bIsXor = true;
		String[] strTerm = strExpression.split("\\+");
		for(int i=0; i<strTerm.length; i++)
		{
			for(int j=0; j<strTerm.length; j++)
			{
				if(i!=j)
				{
					String strTemp = this.mulDoubleMultiply(strTerm[i], strTerm[j]);
					if(!strTemp.equals(""))
						return false;
				}
			}
		}
		return bIsXor;
	}
	
	public String getMDucg(String strExpression)
	{
		String strResult = "";
		if(!this.getIsXor(strExpression))
		{
			String[] strTerm = strExpression.split("\\+");
			String strFactor = "1/" + String.valueOf(strTerm.length);
			for(int i=0; i<strTerm.length; i++)
			{
				if(strResult.equals(""))
				{
					strResult = strFactor + "*" + strTerm[i];
				}
				else
				{
					strResult = strResult + "+" + strFactor + "*" + strTerm[i];
				}
			}
		}
		else
		{
			strResult = strExpression;
		}
		return strResult;
	}
}
