package com.engine;

public class Gate {

	private String id;

	private Expression expressionRef;

	public Gate() {
		id = "";
		expressionRef = null;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Expression getExpressionRef() {
		return expressionRef;
	}

	public void setExpressionRef(Expression expressionRef) {
		this.expressionRef = expressionRef;
	}

}
