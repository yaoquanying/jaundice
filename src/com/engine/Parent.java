package com.engine;

public class Parent {

	private String id;

	private String sourceRef;

	private double weight;

	private Matrix matrixRef;

	public Parent() {
		id = "";
		sourceRef = "";
		weight = 0.0;
		matrixRef = null;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getSourceRef() {
		return sourceRef;
	}

	public void setSourceRef(String sourceRef) {
		this.sourceRef = sourceRef;
	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	public Matrix getMatrixRef() {
		return matrixRef;
	}

	public void setMatrixRef(Matrix matrixRef) {
		this.matrixRef = matrixRef;
	}

}
