import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import com.flex.demo.*;
import java.io.BufferedInputStream;   
import java.io.File;  
import java.util.Enumeration;
import java.util.zip.CRC32;   
import java.util.zip.CheckedOutputStream;   
import java.util.zip.ZipEntry;   
import java.util.zip.ZipOutputStream; 
import java.util.zip.ZipFile;

public class SubmitKB
{
	public void integrateKB()
	{
		System.out.println("开始备份数据库！！");
		backup();
		System.out.println("备份数据库完成！！");
		System.out.println("开始拷贝工程图！！");
		copyImgUp();
		System.out.println("工程图拷贝完成！！");
		System.out.println("拷贝出知识库的XML文件！！");
		copyKBGraph();
		System.out.println("拷贝完知识库的XML文件！！");
		System.out.println("开始压缩提交的文件！！！");
		compress();
		System.out.println("压缩完需要提交的文件！！！");
	}
	
	public void restoreKB()
	{  
		System.out.println("解压提交的压缩文件！！");
		release();
		System.out.println("解压完成！！！");
		System.out.println("还原数据库开始！！！");
		Restore();
		System.out.println("还原数据库完成！！！");
		System.out.println("还原imgUP文件夹！！！");
		restoreImgUp();
		System.out.println("完成还原imgUP文件夹！！！");
		System.out.println("知识库文件还原！！！");
		restoreKBGraph();
		System.out.println("知识库文件还原完成！！！");
		
	}
	
	/*
	 * DUCG的目录
	 */
	private static String pathout=gct.GetPathStr.getPathWeb();
	
	/*
	 * 备份数据库
	 */
	public void backup() 
	{
		System.out.println(pathout);
		String strCommand =  pathout + "//mysql/bin//mysqldump -h localhost -uroot -p1234 ducg";
		try
		{
			File f = new File(pathout + "//submitKB");
			if (f.exists()) 
			{
				delFile(f);
			}
			else
			{
			    f.mkdir();
			}
			Runtime rt = Runtime.getRuntime();
			Process child = rt.exec(strCommand);
			InputStream in = child.getInputStream();
			InputStreamReader xx = new InputStreamReader(in, "utf-8");
			String inStr;
			StringBuffer sb = new StringBuffer("");
			String outStr;
			BufferedReader br = new BufferedReader(xx);
			while((inStr = br.readLine()) != null)
			{
			    sb.append(inStr + "\r\n");
			}
			outStr = sb.toString();
			FileOutputStream fout  = new FileOutputStream(pathout + "//submitKB//ducg.sql");
			OutputStreamWriter writer = new OutputStreamWriter(fout, "utf-8");
			writer.write(outStr);
			writer.flush();
			in.close();
			xx.close();
			br.close();
			writer.close();
			fout.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/*
	 * 恢复数据库，需要在Mysql中有空数据库，只要有DUCG数据库即可，里面不需要有表
	 */
	public static void Restore()
	{
		try 
		{      
            String fPath = pathout + "//submitKB//ducg.sql";    
            Runtime rt = Runtime.getRuntime();      
     
            // 调用 mysql 的 cmd:      
            Process child = rt.exec(pathout + "//mysql//bin////mysql.exe -uroot -p1234 ducg ");      
            OutputStream out = child.getOutputStream();   
            String inStr;      
            StringBuffer sb = new StringBuffer("");      
            String outStr;      
            BufferedReader br = new BufferedReader(new InputStreamReader(      
                    new FileInputStream(fPath), "utf8"));      
            while ((inStr = br.readLine()) != null)
            {      
                sb.append(inStr + "\r\n");     
            }      
            outStr = sb.toString();      
     
            OutputStreamWriter writer = new OutputStreamWriter(out, "utf8");      
            writer.write(outStr); 
            writer.flush();     
            out.close();      
            br.close();      
            writer.close();      
     
            System.out.println("");      
     
        } 
		catch (Exception e)
		{      
            e.printStackTrace();      
        }      
	}
	
	///压缩submitKB文件夹，为submitKB.zip，用户需要选择此文件上传
	public void compress()
	{
		compress(pathout + "//submitKB", pathout + "//submitKB.zip");
	}
	
	
	///上传成功后，解压submitKB.zip文件
	public void release()
	{
		releaseZipToFile(pathout + "//submitKB.zip", pathout);
	}
	
	
	/*
	 * 压缩的最大缓存
	 */
	static final int BUFFER = 65536;
	private void compress(String srcPathName, String pathName) 
	{   
		File zipFile = new File(pathName);  
		File file = new File(srcPathName);   
		if (!file.exists())   
		throw new RuntimeException(srcPathName + "不存在！");   
		try 
		{   
		    FileOutputStream fileOutputStream = new FileOutputStream(zipFile);   
		    CheckedOutputStream cos = new CheckedOutputStream(fileOutputStream, new CRC32());   
		    ZipOutputStream out = new ZipOutputStream(cos);   
		    String basedir = "";   
		    compress(file, out, basedir);   
		    out.close();   
		} 
		catch (Exception e) 
		{   
		    throw new RuntimeException(e);   
		}   
    } 
	
	private void compress(File file, ZipOutputStream out, String basedir) 
	{   
		/* 判断是目录还是文件 */   
		if (file.isDirectory()) 
		{   
		    System.out.println("压缩：" + basedir + file.getName());   
		    this.compressDirectory(file, out, basedir);   
		} 
		else 
		{   
		    System.out.println("压缩：" + basedir + file.getName());   
		    this.compressFile(file, out, basedir);   
		}   
	}   
	
	/** 压缩一个目录 */   
	private void compressDirectory(File dir, ZipOutputStream out, String basedir) 
	{   
	    if (!dir.exists())   
	        return;   
	  
	    File[] files = dir.listFiles();   
	    for (int i = 0; i < files.length; i++) 
	    {   
	        compress(files[i], out, basedir + dir.getName() + "/");   
	    }   
	}   
	  
	/** 压缩一个文件 */   
	private void compressFile(File file, ZipOutputStream out, String basedir) 
	{   
	    if (!file.exists()) 
	    {   
	        return;   
	    }   
	    try 
	    {   
	        BufferedInputStream bis = new BufferedInputStream(new FileInputStream(file));   
	        ZipEntry entry = new ZipEntry(basedir + file.getName());   
	        out.putNextEntry(entry);   
	        int count;   
	        byte data[] = new byte[BUFFER];   
	        while ((count = bis.read(data, 0, BUFFER)) != -1) 
	        {   
	            out.write(data, 0, count);   
	        }   
	        bis.close();
	    } 
	    catch (Exception e) 
	    {   
	        throw new RuntimeException(e);   
	    }   
	}
	
	public void releaseZipToFile(String sourceZip, String outFileName)
	{
	    try
		{
            ZipFile zfile=new ZipFile(sourceZip);
            System.out.println(zfile.getName());
            Enumeration zList=zfile.entries();
            ZipEntry ze=null;
            byte[] buf=new byte[10240];
            while(zList.hasMoreElements())
            {
            //从ZipFile中得到一个ZipEntry
                ze=(ZipEntry)zList.nextElement();
                if(ze.isDirectory())
                {
                    continue;
                }
                //以ZipEntry为参数得到一个InputStream，并写到OutputStream中
                OutputStream os=new BufferedOutputStream(new FileOutputStream(getRealFileName(outFileName, ze.getName())));
                InputStream is=new BufferedInputStream(zfile.getInputStream(ze));
                int readLen=0;
                while ((readLen=is.read(buf, 0, 10240))!=-1) 
                {
                    os.write(buf, 0, readLen);
                }
                is.close();
                os.close();
                System.out.println("Extracted: "+ze.getName());
            }
            zfile.close();
		}
		catch (Exception e) 
		{   
	        throw new RuntimeException(e);   
		}
    }
	
	private File getRealFileName(String baseDir, String absFileName) 
	{
        String[] dirs = absFileName.split("/");
        //System.out.println(dirs.length);
        File ret = new File(baseDir);
        //System.out.println(ret);
        if (dirs.length > 1) 
        {
            for (int i = 0; i < dirs.length - 1; i++) 
            {                         
            	ret = new File(ret, dirs[i]);
            }
        }
        if (!ret.exists()) 
        {
        	ret.mkdirs();
        }
        ret = new File(ret, dirs[dirs.length - 1]);
        return ret;
     }
	
	public void copyImgUp()
	{
	    copyFolder(new File(pathout + "//imgUp"), new File(pathout + "//submitKB//imgUp"));
	}
	 
	
	public void restoreImgUp()
	{
	    copyFolder(new File(pathout + "//submitKB//imgUp"),new File(pathout + "//imgUp"));
	}
	
	public void copyKBGraph()
	{
	    copyFolder(new File(pathout + "//KBGraph//graph.xml"), new File(pathout + "//submitKb//graph.xml"));
	    copyFolder(new File(pathout + "//KBGraph//sav.xml"), new File(pathout + "//submitKb//sav.xml"));
	    copyFolder(new File(pathout + "//KBGraph//v_result.xml"), new File(pathout + "//submitKb//v_result.xml"));
	}
	
	public void restoreKBGraph()
	{
		copyFolder(new File(pathout + "//submitKb//graph.xml"), new File(pathout + "//KBGraph//graph.xml"));
		copyFolder(new File(pathout + "//submitKb//sav.xml"), new File(pathout + "//KBGraph//sav.xml"));
		copyFolder(new File(pathout + "//submitKb//v_result.xml"), new File(pathout + "//KBGraph//v_result.xml"));
	}
	
	/** 
	 * 复制一个目录及其子目录、文件到另外一个目录 
	 * @param src 
	 * @param dest 
	 * @throws IOException 
	 */  
	private void copyFolder(File src, File dest){  
	    if (src.isDirectory()) 
	    {  
	        if (!dest.exists()) 
	        {  
	            dest.mkdir();  
	        }
	        else
	        {
	        	delFile(dest);
	            dest.mkdir();
	        }
	        String files[] = src.list();  
	        for (String file : files) 
	        {  
	            File srcFile = new File(src, file);  
	            File destFile = new File(dest, file);  
	            // 递归复制  
	            copyFolder(srcFile, destFile);  
	        }  
	    } 
	    else 
	    { 
	    	try
	    	{
	            InputStream in = new FileInputStream(src);  
	            OutputStream out = new FileOutputStream(dest);  
	  
	            byte[] buffer = new byte[1024];  
	  
	            int length;  
	          
	            while ((length = in.read(buffer)) > 0) 
	            {  
	                out.write(buffer, 0, length);  
	            }  
	            in.close();  
	            out.close(); 
	    	}
	    	catch (Exception e) 
			{   
		        throw new RuntimeException(e);   
			}
	    }  
	} 
	
	private void delFile(File f)
	{
		try
		{
//			File f = new File(filepath);
			if(f.exists() && f.isDirectory())
			{
				if(f.listFiles().length==0)
				{//若目录下没有文件则直接删除  
					f.delete();		    
				}
				else
				{//若有则把文件放进数组，并判断是否有下级目录 
					File delFile[]=f.listFiles();  
		            int i =f.listFiles().length;  
		            for(int j=0;j<i;j++)
		            {
		            	if(delFile[j].isDirectory())
		            	{
		            		delFile(new File(delFile[j].getAbsolutePath()));//递归调用del方法并取得子目录路径  
		                }
		                delFile[j].delete();
		            }
		        }
			}
		}
		catch (Exception e) 
		{   
	        throw new RuntimeException(e);   
		}
	}  
	 
}

